<?php if( ! defined("BASEPATH")) exit('No direct script access allowed');

class ApiSMSModel extends CI_Model {

  public function _construct(){
		parent::_construct();
	}

  /*
     Method to getMobileNumbers
     Created By: Manzz Baria
  */
  public function getMobileNumbers($userId){
      $this->db->select("countryCode,mobileNumber");
      $this->db->from("usermaster");
      $this->db->where("Id",$userId);
      $query = $this->db->get();
      return $this->displayMobileNumbers($query->result());
  }

 public function getNumberbyCountry($type,$countrycode,$language,$userIds){
    $this->db->select("usermaster.countryCode,usermaster.mobileNumber");
    $this->db->from("usermaster");
	 //$this->db->join('devicemaster','usermaster.Id = devicemaster.userId','inner');
	//$this->db->where("devicemaster.language",$language);
$this->db->where_in('usermaster.Id', explode(',', $userIds));
	if($countrycode!=null && $countrycode!="")
	{	
	 	$this->db->where("usermaster.countryCode",$countrycode);
	}
	$this->db->where("usermaster.userType",$type);
    $this->db->where_not_in("usermaster.status",USER_STATUS_BLOCK);
  
    $query = $this->db->get();
    
    return $this->displayMobileNumbers($query->result());
}
    /*
       Method to displayMobileNumbers
       Created By: Manzz Baria
    */
    public function displayMobileNumbers($result){
      $mobileObject = null;
      $Object = null;
      if($result != null){
        foreach ($result as $row) {
              $countryCode = (int)$row->countryCode;
              if ($countryCode > 0) 
                {
                  $mobileObject[]= $countryCode."".$row->mobileNumber;

                 $mobileObject[]= $countryCode."".$row->mobileNumber;
              }else{
                  $mobileObject[]="".$row->mobileNumber;
              }
        }
      }
      
      return $mobileObject;
    }

    /* Method to Send OTP SMS
       Created By: Manzz Baria
    */
    public function sendOTPSMS($userId){
      $this->load->model("Utility","utility");
      $this->load->model("ApiUserModel","userModel");
      $OTPNumber = $this->utility->generateRandomNumber();
      $this->userModel->saveAccessToken($OTPNumber,$userId);
      $message = "Your CarSpa verification code is ".$OTPNumber;
      $this->sendSMS($message,$userId);
      return true;
    }

    /*
       Method to sendOTPSMSByMobileNumber
       Created By: Manzz Baria
    */
    public function sendOTPSMSByMobileNumber($mobileNumber){
      $this->load->model("Utility","utility");
      $this->load->model("ApiUserModel","userModel");
      $userId = $this->userModel->getUserIdFromMobileNumber($mobileNumber);
      $OTPNumber = $this->utility->generateRandomNumber();
      $this->userModel->saveAccessToken($OTPNumber,$userId);
      $message = "Your CarSpa activation code is ".$OTPNumber;
      $this->sendSMS($message,$userId);
      return true;
    }

  /*
      Method to sendSMS
      Created By: Manzz Baria
  */
 public function sendSMS($message,$userId){

   $mobileNumbers = $this->getMobileNumbers($userId);

   $url = "https://api.mblox.com/xms/v1/carspa13/batches";
   $senderID = "CarSpa";
   $Bearer = "Bearer bacb1e715cde4f9dba86baa1c4e5d1d6";

   $fields = array(
      'from'  => $senderID,
      'to'  => $mobileNumbers,
      'body'  => $message
   );

   $headers = array('Authorization: ' . $Bearer,'Content-Type: application/json');
   $ch = curl_init();
   curl_setopt($ch, CURLOPT_URL, $url);
   curl_setopt( $ch, CURLOPT_POST, true );
   curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
   curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
   curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
   curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $fields ) );
   $result = curl_exec($ch);
   curl_close($ch);
   return true;
 }

/*
      Method to sendSMSforAdmin
      Created By: Manzz Baria
  */
 public function sendSMSforAdmin($message,$mobileNumbers){

   $url = "https://api.mblox.com/xms/v1/carspa13/batches";
   $senderID = "CarSpa";
   $Bearer = "Bearer bacb1e715cde4f9dba86baa1c4e5d1d6";

   $fields = array(
      'from'  => $senderID,
      'to'  => $mobileNumbers,
      'body'  => $message
   );

   $headers = array('Authorization: ' . $Bearer,'Content-Type: application/json');
   $ch = curl_init();
   curl_setopt($ch, CURLOPT_URL, $url);
   curl_setopt( $ch, CURLOPT_POST, true );
   curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
   curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
   curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
   curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $fields ) );
   $result = curl_exec($ch);
   curl_close($ch);
   return true;
 }

 /*
     Method to sendSMSByMobileNumber
     Created By: Manzz Baria
 */
public function sendSMSByMobileNumber($message,$mobiles){

  $mobiles = str_replace(' ', '', $mobiles);
  $mobileNumbers = explode(',', $mobiles);

  $url = "https://api.mblox.com/xms/v1/carspa13/batches";
  $senderID = "CarSpa";
  $Bearer = "Bearer bacb1e715cde4f9dba86baa1c4e5d1d6";

  $fields = array(
     'from'  => $senderID,
     'to'  => $mobileNumbers,
     'body'  => $message
  );

  $headers = array('Authorization: ' . $Bearer,'Content-Type: application/json');
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt( $ch, CURLOPT_POST, true );
  curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
  curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
  curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
  curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $fields ) );
  $result = curl_exec($ch);
  curl_close($ch);
  return true;
}





}
?>
