<?php if( ! defined("BASEPATH")) exit('No direct script access allowed');

class ApiRateModel extends CI_Model {

  public function _construct(){
		parent::_construct();
	}

  /* Method to addRateQuestion
     Created By: Manzz Baria
  */
  public function addRateQuestion($question){
    $data = array('question'=>$question);
    $this->db->insert('ratemaster', $data);
    $insert_Id = $this->db->insert_id();
    return $this->getRateQuestionById($insert_Id);
  }

  /* Method to getRateQuestionById
   Created By: Manzz Baria
  */
  public function getRateQuestionById($rateId){
      $this->db->select("Id,question,datetime");
      $this->db->from("ratemaster");
      $this->db->where('Id', $rateId);
      $query = $this->db->get();
      return $this->displayRateQuestionById($query->result());
  }

  /* Method to displayRateQuestionById
     Created By: Manzz Baria
  */
  public function displayRateQuestionById($result){
    $Object = null;
    if($result != null){
      $this->load->model("Utility","utility");
      foreach ($result as $row) {
        $displayDate = $this->utility->timeAgoFormat($row->datetime);
        $Object = array(
          'Id'=>(int)$row->Id,
          'question'=>$row->question,
          'datetime'=>$displayDate
        );
         return $Object;
      }
    }
  }

  /* Method to getRateQuestions
   Created By: Manzz Baria
  */
   public function getRateQuestions($language=ENGLISH_LANGUAGE){
      $this->db->select("Id,question,questionAr,datetime");
      $this->db->from("ratemaster");
      $query = $this->db->get();
      return $this->displayRateQuestions($query->result(),$language);
  }

  /* Method to displayRateQuestions
     Created By: Manzz Baria
  */
    public function displayRateQuestions($result,$language=LANGUAGE_ENGLISH){
    $companyObject = null;
    $Object = null;
    if($result != null){
      $this->load->model("Utility","utility");
      foreach ($result as $row) {
        $displayDate = $this->utility->timeAgoFormat($row->datetime,$language);
		
		 $question = $row->question;
        if ($language == LANGUAGE_ARABIC) {
          $question = $row->questionAr;
        }
		
        $Object = array(
          'Id'=>(int)$row->Id,
          'question'=>$question,
		  'questionAR'=>$row->questionAr,
          'datetime'=>$displayDate
        );
         $companyObject[]=$Object;
      }
    }
    return $companyObject;
  }


  /* Method to addRateCommentOfRequest
     Created By: Manzz Baria
  */
  public function addRateCommentOfRequest($userId,$appointmentId,$comment){
    $this->load->model("Utility","utility");
    $datetime = $this->utility->getCurrentDate('Y/m/d h:i:s');
     $data = array(
                     'userId'=>$userId,
                     'appointmentId'=>$appointmentId,
                     'comment'=>$comment,
                     'datetime'=>$datetime);
     $this->db->insert('rateotherfeedback', $data);
     $insert_Id = $this->db->insert_id();
     return true;
  }


  /*   Method to addRateOnRequest
       Created By: Manzz Baria
    */
    public function addRateOnRequest($userId,$appointmentId,$rateDetail,$comment){
      $this->load->model("ApiAppoinmnetModel","appoinmnetModel");
      $this->load->model("ApiVanOperatorModel","vanOperatorModel");
      $rateDetail = str_replace(' ', '', $rateDetail);
      $isAdded = $this->isRateAlreadyAddedOnRequestByUser($userId,$appointmentId);
        if (!$isAdded) {
            $myArray = explode(',', $rateDetail);

            $this->addRateCommentOfRequest($userId,$appointmentId,$comment);
            for ($i=0; $i< sizeof($myArray); $i++) {
                $myDetails = explode(':', $myArray[$i]);
                $this->addRateEntry($myDetails[0],$userId,$appointmentId,$myDetails[1]);
            }

          return $this->getRateOfRequestId($appointmentId);
        }else{
          return null;
        }
    }

    /* Method to check isRateAlreadyAddedOnRequestByUser
       Created By: Manzz Baria
    */
    public function isRateAlreadyAddedOnRequestByUser($userId,$appointmentId){
      $this->db->select('Id');
      $this->db->from('ratedetail');
      $this->db->where("userId",$userId);
      $this->db->where('appointmentId',$appointmentId);
      $query = $this->db->get();
      if ($query->result() != null) {

        return true;
      }
      return false;
    }


    /* Method to addRateEntry
      Created By: Manzz Baria
   */
   public function addRateEntry($rateId,$userId,$appointmentId,$rate){
          $this->load->model("Utility","utility");
          $datetime = $this->utility->getCurrentDate('Y/m/d h:i:s');
           $data = array(
                           'rateId'=>$rateId,
                           'userId'=>$userId,
                           'appointmentId'=>$appointmentId,
                           'rate'=>$rate,
                           'datetime'=>$datetime);
           $this->db->insert('ratedetail', $data);
           $insert_Id = $this->db->insert_id();
           return true;

   }

   /* Method to getRateOfRequestId
     Created By: Manzz Baria
  */

   public function getRateOfRequestId($appointmentId){

     $this->db->select("ratedetail.Id,ratedetail.userId,ratedetail.rateId,ratemaster.question,ratedetail.rate,ratedetail.datetime");
     $this->db->from("ratedetail");
     $this->db->join('ratemaster','ratedetail.rateId = ratemaster.Id','inner');
     $this->db->where('ratedetail.appointmentId',$appointmentId);
     $query = $this->db->get();

     return $this->displayRateOfRequestId($query->result(),$appointmentId);

   }

   /*
      Method to displayRateOfRequestId
      Created By: Manzz Baria
   */
   public function displayRateOfRequestId($result,$appointmentId){

         $companyObject = null;
         $Object = null;
         if($result != null){
           $this->load->model("Utility","utility");
           foreach ($result as $row) {
             $displayDate = $this->utility->timeAgoFormat($row->datetime);
             $Object = array(
               'Id'=>(int)$row->Id,
               'userId'=>(int)$row->userId,
               'rateId'=>(int)$row->rateId,
               'question'=>$row->question,
               'rate'=>(int)$row->rate,
               'datetime'=>$displayDate
             );
              $companyObject[]=$Object;
           }
           $commnetObject = $this->getCommentOfRequestId($appointmentId);
           $companyObject[]=$commnetObject;
         }
         return $companyObject;
   }

   /* Method to getCommentOfRequestId
     Created By: Manzz Baria
  */
   public function getCommentOfRequestId($appointmentId){

     $this->db->select("Id,userId,comment,datetime");
     $this->db->from("rateotherfeedback");
     $this->db->where('appointmentId',$appointmentId);
     $query = $this->db->get();
     return $this->displayCommentOfRequestId($query->result());

   }

   /*
      Method to displayCommentOfRequestId
      Created By: Manzz Baria
   */
   public function displayCommentOfRequestId($result){

         $companyObject = null;
         $Object = null;
         if($result != null){
           $this->load->model("Utility","utility");
           foreach ($result as $row) {
             $displayDate = $this->utility->timeAgoFormat($row->datetime);
             $Object = array(
               'Id'=>(int)$row->Id,
               'userId'=>(int)$row->userId,
               'comment'=>$row->comment,
               'datetime'=>$displayDate
             );
            return $Object;
           }
         }
        // return $companyObject;
   }




}
?>
