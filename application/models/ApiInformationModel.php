<?php if( ! defined("BASEPATH")) exit('No direct script access allowed');

class ApiInformationModel extends CI_Model {

  public function _construct(){
		parent::_construct();
	}



  /*
     Method to update bid
     Created By: Manzz Baria
  */
  public function updateAboutus($aboutusId,$aboutusdetail,$aboutustitle,$flag,$language = LANGUAGE_ENGLISH)
  {
  
  if($flag==0)
  {
      $data = array(
        'aboutusEnTitle' => $aboutustitle,
		 'aboutusEN' => $aboutusdetail
        );
      $this->db->where('Id', $aboutusId);
      $this->db->update('informationmaster', $data);
  }
else
{
      $data = array(
        'aboutusArTitle' => $aboutustitle,
		 'aboutusAR' => $aboutusdetail
        );
      $this->db->where('Id', $aboutusId);
      $this->db->update('informationmaster', $data);
}
    return $this->getAboutusById($aboutusId,$language);
  }
  
 public function updatepolicy($policyId,$policydetail,$policytitle,$flag,$language = LANGUAGE_ENGLISH)
  {
  
  if($flag==0)
  {
      $data = array(
        'policyTitle' => $policytitle,
		 'policyEN' => $policydetail
        );
      $this->db->where('Id', $policyId);
      $this->db->update('informationmaster', $data);
  }
else
{
      $data = array(
        'policyTitleAR' => $policytitle,
		 'policyAR' => $policydetail
        );
      $this->db->where('Id', $policyId);
      $this->db->update('informationmaster', $data);
}
    return $this->getPolicyById($policyId,$language);
  }

public function getAboutusById($aboutusId,$language=LANGUAGE_ENGLISH){
 $this->db->select("Id,aboutusEN,aboutusAR,aboutusEnTitle,aboutusArTitle");
  $this->db->from("informationmaster");
  $this->db->where("Id",$aboutusId);
  $query = $this->db->get();
  return $this->displayabout($query->result(),$language);
}

public function getPolicyById($policyId,$language=LANGUAGE_ENGLISH)
{
  $this->db->select("Id,policyTitle,policyEN,policyTitleAR,policyAR");
  $this->db->from("informationmaster");
  $this->db->where("Id",$policyId);
  $query = $this->db->get();
  return $this->displaypolicy($query->result(),$language);
}


  /*
    Method to getCarTypeById
    Created By: Manzz Baria
  */
public function getAboutUs($language = LANGUAGE_ENGLISH){
  $this->db->select("Id,aboutusEN,aboutusAR,aboutusEnTitle,aboutusArTitle");
  $this->db->from("informationmaster");
 // $this->db->where('isBlocked', UNBLOCKED);
  $query = $this->db->get();
  return $this->displayabout($query->result(),$language);
}

public function getPolicy($language = LANGUAGE_ENGLISH){
  $this->db->select("Id,policyTitle,policyEN,policyTitleAR,policyAR");
  $this->db->from("informationmaster");
 // $this->db->where('isBlocked', UNBLOCKED);
  $query = $this->db->get();
  return $this->displaypolicy($query->result(),$language);
}

/*
    Method to displayCarTypeById
     Created By: Manzz Baria
  */
  public function displayabout($result,$language = LANGUAGE_ENGLISH){
    $infoObject = null;
    if($result != null){
    // $this->load->model("Utility","utility");
      foreach ($result as $row) {
       // $displayDate = $this->utility->timeAgoFormat($row->datetime,$language);
          $aboutusTitle = $row->aboutusEnTitle;
		  $aboutus = $row->aboutusEN;
        	if ($language == LANGUAGE_ARABIC) {
            $aboutus = $row->aboutusAR;
			 $aboutusTitle = $row->aboutusArTitle;
          }
        $Object = array(
          'Id'=>(int)$row->Id,
		   'aboutusTitle' => $aboutusTitle,
          'aboutus'=>$aboutus,
		   'aboutusArTitle' => $row->aboutusArTitle,
          'aboutusAR'=>$row->aboutusAR,
        );
        $carObject[]=$Object;
      }
    }
    return $Object;
  }

 public function displaypolicy($result,$language = LANGUAGE_ENGLISH){
    $infoObject = null;
    if($result != null){
    // $this->load->model("Utility","utility");
      foreach ($result as $row) {
       // $displayDate = $this->utility->timeAgoFormat($row->datetime,$language);
          $policyTitle = $row->policyTitle;
		  $policy = $row->policyEN;
        	if ($language == LANGUAGE_ARABIC) {
            $policy = $row->policyAR;
			 $policyTitle = $row->policyTitleAR;
          }
        $Object = array(
          'Id'=>(int)$row->Id,
		   'policyTitle' => $policyTitle,
          'policy'=>$policy,
		   'policyTitleAR' => $row->policyTitleAR,
          'policyAR'=>$row->policyAR,
        );
        $carObject[]=$Object;
      }
    }
    return $Object;
  }

}
?>
