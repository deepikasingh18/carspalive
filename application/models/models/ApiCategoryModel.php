<?php if( ! defined("BASEPATH")) exit('No direct script access allowed');

class ApiCategoryModel extends CI_Model {

  public function _construct(){
    parent::_construct();
  }


  /*
     Method to check isCategoryAlreadyAdded
     Created By: Manzz Baria
  */
  public function isCategoryAlreadyAdded($name){
    $this->db->select('Id');
    $this->db->from('categorymaster');
    $this->db->where("name",$name);
    $this->db->where("isBlocked",UNBLOCKED);
    $query = $this->db->get();
    if ($query->result() != null) {
      return true;
    }
    return false;
  }

   public function isCategoryBlocked($id){
    $this->db->select('Id');
    $this->db->from('categorymaster');
    $this->db->where("Id",$id);
    $this->db->where("isBlocked",BLOCKED);
    $query = $this->db->get();
    if ($query->result() != null) {
      return true;
    }
    return false;
  }
    /* Method to addCategory
       Created By: Manzz Baria
    */
    public function addCategory($name,$nameAR,$fileUrl,$imageUrlName,$language = LANGUAGE_ENGLISH){
      $isAdded = $this->isCategoryAlreadyAdded($name);
        if (!$isAdded) {
          $this->load->model("Utility","utility");
          $datetime = $this->utility->getCurrentDate('Y/m/d h:i:s');
          $data = array(
                          'name'=>$name,
                          'nameAR'=>$nameAR,
                          'icon'=>$fileUrl,
                          'imageUrl'=>$imageUrlName,
                          'datetime'=>$datetime,
                          'isBlocked'=>UNBLOCKED
                       );
          $this->db->insert('categorymaster', $data);
          $insert_Id = $this->db->insert_id();
          return $this->getCategoryById($insert_Id,$language);
        }else{
          return null;
        }
    }

    /* Method to getCategoryById
     Created By: Manzz Baria
  */
  public function getCategoryById($categoryId,$language = LANGUAGE_ENGLISH){
    $this->db->select("Id,name,nameAR,icon,imageUrl,datetime");
    $this->db->from("categorymaster");
    $this->db->where("Id",$categoryId);
    $query = $this->db->get();
    return $this->displayCategoryById($query->result(),$language);
  }

  /* Method to displayCategoryById
       Created By: Manzz Baria
    */
    public function displayCategoryById($result,$language = LANGUAGE_ENGLISH){
      $Object = null;
      if($result != null){
        $this->load->model("Utility","utility");
        foreach ($result as $row) {
          $displayDate = $this->utility->timeAgoFormat($row->datetime,$language);
          $name = $row->name;
          if ($language == LANGUAGE_ARABIC) {

            $name = $row->nameAR;
          }

          $Object = array(
            'Id'=>(int)$row->Id,
            'name'=>$name,
            'nameAR'=>$row->nameAR,
            'icon'=>IMAGE_URL.$row->icon,
            'iconThump'=>THUMB_URL.$row->icon,
            'imageUrl'=>IMAGE_URL.$row->imageUrl,
            'imageUrlThump'=>THUMB_URL.$row->imageUrl,
            'datetime'=>$displayDate
          );
          return $Object;
        }
      }
      return $Object;
    }

    /*
       Method to updateCategory
       Created By: Manzz Baria
    */
    public function updateCategory($name,$nameAR,$categoryId,$fileUrl,$imageUrlName,$language = LANGUAGE_ENGLISH){
      $this->load->model("Utility","utility");
      $datetime = $this->utility->getCurrentDate('Y/m/d h:i:s');
      $data = array(
        'name' => $name,
        'datetime'=>$datetime
        );
      $this->db->where('Id', $categoryId);
      $this->db->update('categorymaster', $data);
      if ($nameAR != '') {
        $data = array(
          'nameAR' => $nameAR
          );
        $this->db->where('Id', $categoryId);
        $this->db->update('categorymaster', $data);
      }
      if ($fileUrl != "") {
        $data = array(
          'icon' => $fileUrl
          );
        $this->db->where('Id', $categoryId);
        $this->db->update('categorymaster', $data);
      }
      if ($imageUrlName != "") {
        $data = array(
          'imageUrl' => $imageUrlName
          );
        $this->db->where('Id', $categoryId);
        $this->db->update('categorymaster', $data);
      }
      return $this->getCategoryById($categoryId,$language);
    }

    /*
      Method to deleteCategoryId
       Created By: Manzz Baria
    */
    public function deleteCategoryId($categoryId){
      $data = array(
                  'isBlocked'=>BLOCKED);
      $this->db->where('Id', $categoryId);
      $this->db->update('categorymaster', $data);
      return true;
    }

    /*
        Method get getCategoryNameOfAppointment
        Created By: Manzz Baria
     */
     public function getCategoryNameOfAppointment($appointmentId){
       $this->db->select('categorymaster.name');
       $this->db->from('categorymaster');
       $this->db->join("appoinmentmaster","categorymaster.Id = appoinmentmaster.categoryId","inner");
       $this->db->where('appoinmentmaster.Id',$appointmentId);
       $query = $this->db->get();
       $result = $query->result();
       $name = null;
       if($result != null){
         foreach ($result as $row) {
           $name = $row->name;
         }
       }
       return $name;
     }
     
      public function getCategoryNameArOfAppointment($appointmentId){
       $this->db->select('categorymaster.nameAR');
       $this->db->from('categorymaster');
       $this->db->join("appoinmentmaster","categorymaster.Id = appoinmentmaster.categoryId","inner");
       $this->db->where('appoinmentmaster.Id',$appointmentId);
       $query = $this->db->get();
       $result = $query->result();
       $name = null;
       if($result != null){
         foreach ($result as $row) {
           $namear = $row->nameAR;
         }
       }
       return $namear;
     }

     /*
         Method get getCategoryNameOfAppointment
         Created By: Manzz Baria
      */
      public function getCategoryIdOfAppointment($appointmentId){
        $this->db->select('categorymaster.Id');
        $this->db->from('categorymaster');
        $this->db->join("appoinmentmaster","categorymaster.Id = appoinmentmaster.categoryId","inner");
        $this->db->where('appoinmentmaster.Id',$appointmentId);
        $query = $this->db->get();
        $result = $query->result();
        $categoryId = null;
        if($result != null){
          foreach ($result as $row) {
            $categoryId = $row->Id;
          }
        }
        return $categoryId;
      }


      /*
        Method to getCategoriesTest
        Created By: Manzz Baria
      */
    public function getCategoriesTest(){
      $this->db->select("Id,name,icon,imageUrl,datetime");
      $this->db->from("categorymaster");
      $this->db->where('isBlocked', UNBLOCKED);
      $query = $this->db->get();

      return $this->displayCategoriesTest($query->result(),$language);
    }
    
    public function searchGetCategoryServices($name){
      $this->db->select("Id,name,nameAR,icon,imageUrl,datetime");
      $this->db->from("categorymaster");
      $this->db->where('isBlocked', UNBLOCKED);
      $this->db->like('name', $name);
      $query = $this->db->get();
      return $this->displayCategories($query->result());
    }
    
    /*
        Method to displayCategoriesTest
         Created By: Manzz Baria
      */
      public function displayCategoriesTest($result){
        $carObject = null;
        if($result != null){
          $this->load->model("Utility","utility");
          $ObjectF = array(
            'Id'=>'ID',
            'name'=>'CategoryName',
            'icon'=>'ImageURL',
            'iconThump'=>'ThumbURL',
            'datetime'=>'DateTime'
          );
          $carObject[] = $ObjectF;
          foreach ($result as $row) {
            $displayDate = $this->utility->timeAgoFormat($row->datetime);
            $Object = array(
              'Id'=>(int)$row->Id,
              'name'=>$row->name,
              'icon'=>IMAGE_URL.$row->icon,
              'iconThump'=>THUMB_URL.$row->icon,
              'imageUrl'=>IMAGE_URL.$row->imageUrl,
              'imageUrlThump'=>THUMB_URL.$row->imageUrl,
              'datetime'=>$displayDate
            );
            $carObject[]=$Object;
          }
        }
        return $carObject;
      }


      /*
        Method to getCategories
        Created By: Manzz Baria
      */
    public function getCategories($language = LANGUAGE_ENGLISH){
      $this->db->select("Id,name,nameAR,icon,imageUrl,datetime");
      $this->db->from("categorymaster");
      $this->db->where('isBlocked', UNBLOCKED);
      $query = $this->db->get();
      return $this->displayCategories($query->result(),$language);
    }

  /*
      Method to displayCategories
       Created By: Manzz Baria
    */
    public function displayCategories($result,$language = LANGUAGE_ENGLISH){
      $carObject = null;
      if($result != null){
        $this->load->model("Utility","utility");
        foreach ($result as $row) {
          $displayDate = $this->utility->timeAgoFormat($row->datetime,$language);
          $name = $row->name;
          if ($language == LANGUAGE_ARABIC) {
            $name = $row->nameAR;
          }
          $Object = array(
            'Id'=>(int)$row->Id,
            'name'=>$name,
            'nameAR'=>$row->nameAR,
            'icon'=>IMAGE_URL.$row->icon,
            'iconThump'=>THUMB_URL.$row->icon,
            'imageUrl'=>IMAGE_URL.$row->imageUrl,
            'imageUrlThump'=>THUMB_URL.$row->imageUrl,
            'datetime'=>$displayDate
          );
          $carObject[]=$Object;
        }
      }
      return $carObject;
    }

    /*
      Method to getCategoryServices
      Created By: Manzz Baria
    */
  public function getCategoryServices($language = LANGUAGE_ENGLISH){
    $this->db->select("Id,name,nameAR,icon,imageUrl,datetime");
    $this->db->from("categorymaster");
    $this->db->where('isBlocked', UNBLOCKED);
    $query = $this->db->get();
    return $this->displayCategoryServices($query->result(),$language);
  }

  /*
      Method to displayCategoryServices
       Created By: Manzz Baria
    */
    public function displayCategoryServices($result,$language = LANGUAGE_ENGLISH){
      $carObject = null;
      if($result != null){
        $this->load->model("Utility","utility");
        $this->load->model("ApiServiceModel","srvModel");
        foreach ($result as $row) {
          $displayDate = $this->utility->timeAgoFormat($row->datetime,$language);
          $serviceObj = $this->srvModel->getServicesByCategory($row->Id,$language);
          $name = $row->name;
          if ($language == LANGUAGE_ARABIC) {
            $name = $row->nameAR;
          }
          $Object = array(
            'Id'=>(int)$row->Id,
            'name'=>$name,
            'nameAR'=>$row->nameAR,
            'icon'=>IMAGE_URL.$row->icon,
            'iconThump'=>THUMB_URL.$row->icon,
            'imageUrl'=>IMAGE_URL.$row->imageUrl,
            'imageUrlThump'=>THUMB_URL.$row->imageUrl,
            'datetime'=>$displayDate,
            'service'=>$serviceObj
          );
          $carObject[]=$Object;
        }
      }
      return $carObject;
    }


    public function searchService($name,$language = LANGUAGE_ENGLISH){
    $this->db->select("Id,name,nameAR,icon,imageUrl,datetime");
    $this->db->from("categorymaster");
    $this->db->where('isBlocked', UNBLOCKED);
    $query = $this->db->get();
    return $this->displaySearchCategoryServices($query->result(),$name,$language);
  }

 public function displaySearchCategoryServices($result,$name,$language = LANGUAGE_ENGLISH){
      $carObject = null;
      if($result != null){
        $this->load->model("Utility","utility");
        $this->load->model("ApiServiceModel","srvModel");
        foreach ($result as $row) {
          $displayDate = $this->utility->timeAgoFormat($row->datetime,$language);
          $serviceObj = $this->srvModel->searchgetServicesByCategory($row->Id,$name,$language);
         
          $Object = array(
            'Id'=>(int)$row->Id,
            'name'=>$row->name,
            'nameAR'=>$row->nameAR,
            'icon'=>IMAGE_URL.$row->icon,
            'iconThump'=>THUMB_URL.$row->icon,
            'imageUrl'=>IMAGE_URL.$row->imageUrl,
            'imageUrlThump'=>THUMB_URL.$row->imageUrl,
            'datetime'=>$displayDate,
            'service'=>$serviceObj
          );
          $carObject[]=$Object;
        }
      }
      return $carObject;
    }

    /*
      Method to getCategoryServicesForReport
      Created By: Manzz Baria
    */
  public function getCategoryServicesForReport($categoryId,$serviceId,$vanId,$language = LANGUAGE_ENGLISH){
    $this->db->select("Id,name,nameAR,icon,imageUrl,datetime");
    $this->db->from("categorymaster");
    $this->db->where('isBlocked', UNBLOCKED);
    if ($categoryId > 0) {
      $this->db->where('Id', $categoryId);
    }
    $query = $this->db->get();
    return $this->displayCategoryServicesForReport($query->result(),$serviceId,$vanId,$language);
  }

  /*
      Method to displayCategoryServicesForReport
       Created By: Manzz Baria
    */
    public function displayCategoryServicesForReport($result,$serviceId,$vanId,$language = LANGUAGE_ENGLISH){
      $carObject = null;
      if($result != null){
        $this->load->model("Utility","utility");
        $this->load->model("ApiServiceModel","srvModel");
        foreach ($result as $row) {
          $displayDate = $this->utility->timeAgoFormat($row->datetime,$language);
          $serviceObj = $this->srvModel->getServicesByCategoryForReport($row->Id,$serviceId,$vanId);
          $name = $row->name;
          if ($language == LANGUAGE_ARABIC) {
            $name = $row->nameAR;
          }
          $Object = array(
            'Id'=>(int)$row->Id,
            'name'=>$name,
            'nameAR'=>$row->nameAR,
            'icon'=>IMAGE_URL.$row->icon,
            'iconThump'=>THUMB_URL.$row->icon,
            'imageUrl'=>IMAGE_URL.$row->imageUrl,
            'imageUrlThump'=>THUMB_URL.$row->imageUrl,
            'datetime'=>$displayDate,
            'service'=>$serviceObj
          );
          if ($serviceObj != null) {
              $carObject[]=$Object;
          }

        }
      }
      return $carObject;
    }




}
?>
