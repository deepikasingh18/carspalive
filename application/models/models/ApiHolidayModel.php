<?php if( ! defined("BASEPATH")) exit('No direct script access allowed');

class ApiHolidayModel extends CI_Model {

  public function _construct(){
		parent::_construct();
	}



/* Method to deleteHolidays
   Created By: Manzz Baria
*/
public function deleteHolidays($shiftId,$type){
  if ($type == 3) {
    $this->db->where('shiftId', $shiftId);
    $this->db->where('type', 1);
    $this->db->delete('holidaymaster');
    $this->db->where('shiftId', $shiftId);
    $this->db->where('type', 2);
    $this->db->delete('holidaymaster');
  }else{
    $this->db->where('shiftId', $shiftId);
    $this->db->where('type', $type);
    $this->db->delete('holidaymaster');
  }

  return true;
}

/*
  Method to addShift Holidays
   Created By: Manzz Baria
*/

  public function addShiftHolidays($shiftId,$days,$dates){
    if ($days != "") {
      $type = 1;
      $this->deleteHolidays($shiftId,$type);
        if ($days != "Empty") {
          $days = explode(',', $days);
          for ($i=0; $i< sizeof($days); $i++) {
              $data = array(
                              'shiftId'=>$shiftId,
                              'type'=>$type,
                              'day'=>$days[$i]);
              $this->db->insert('holidaymaster', $data);
          }
        }

    }
    if ($dates != "") {
      $type = 2;
      $this->deleteHolidays($shiftId,$type);
      if ($dates != "Empty") {
          $dates = explode(',', $dates);
          for ($i=0; $i< sizeof($dates); $i++) {
            $data = array(
                            'shiftId'=>$shiftId,
                            'type'=>$type,
                            'date'=>$dates[$i]);
            $this->db->insert('holidaymaster', $data);
          }
      }

    }
    return true;

  }

  /*
    Method to getHolidays
    Created By: Manzz Baria
  */

  public function getHolidays($shiftId){
    $days = $this->getHolidaysByType($shiftId,DAYS);
    $dates = $this->getHolidaysByType($shiftId,DATES);
    $Object = array(
      'days'=>$days,
      'dates'=>$dates
    );

    return $Object;
  }

  /*
    Method to getHolidays
    Created By: Manzz Baria
  */

  public function getHolidaysByType($shiftId,$type){
      $this->db->select("Id,shiftId,type,day,date");
      $this->db->from("holidaymaster");
      $this->db->where('shiftId', $shiftId);
      $this->db->where('type', $type);
      $query = $this->db->get();
      return $this->displayHolidaysByType($query->result());
  }

  /* Method to displayHolidaysByType
     Created By: Manzz Baria
  */
  public function displayHolidaysByType($result){
    $holidayObject = null;
    if($result != null){
      foreach ($result as $row) {
        $Object = array(
          'Id'=>(int)$row->Id,
          'shiftId'=>$row->shiftId,
          'type'=>(int)$row->type,
          'day'=>$row->day,
          'date'=>$row->date
        );
        $holidayObject [] = $Object;
      }
    }
    return $holidayObject;
  }



}
?>
