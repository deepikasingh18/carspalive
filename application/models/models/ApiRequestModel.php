<?php if( ! defined("BASEPATH")) exit('No direct script access allowed');

class ApiRequestModel extends CI_Model {

  public function _construct(){
    parent::_construct();
  }

  /***************************    NEW APIS    ***************************/

  /* Method to addRequestJobs
     Created By: Manzz Baria
  */
  public function addRequestJobs($userId,$addressId,$carTypeId,$carId){

        $this->load->model("Utility","utility");
        $datetime = $this->utility->getCurrentDate('Y/m/d h:i:s');
        $data = array(
                    'userId'=>$userId,
                    'addressId'=>$addressId,
                    'carTypeId'=>$carTypeId,
                    'carId'=>$carId,
                    'datetime'=>$datetime);
        $this->db->insert('requestmaster', $data);
        $insert_Id = $this->db->insert_id();
        return $insert_Id;

  }


/**************************    Unused    ***************************/



  /* Method to getRequestById
     Created By: Manzz Baria
  */
  public function getRequestById($requestId){
    $this->db->select("Id,userId,addressId,carTypeId,carId,datetime");
    $this->db->from("requestmaster");
    $this->db->where('Id', $requestId);
    $query = $this->db->get();
    return $this->displayRequestById($query->result());
  }
  /* Method to displayRequests
       Created By: Manzz Baria
    */
    public function displayRequestById($result){
      $requestObject = null;
      if($result != null){
        $this->load->model("Utility","utility");
        $this->load->model("ApiAddressModel","addressModel");
        $this->load->model("ApiCarModel","carModel");
        $this->load->model("ApiAppoinmnetModel","appoinmnetModel");
        $this->load->model("ApiUserModel","userModel");
        foreach ($result as $row) {

            $displayDate = $this->utility->timeAgoFormat($row->datetime);
            $carTypeObject = $this->carModel->getCarTypeById($row->carTypeId);
            $carObject = $this->carModel->getCarDetailsById($row->carId);
            $addressObject = $this->addressModel->getAddressById($row->addressId);
            $userObject = $this->userModel->getUserDetail($row->userId);
            $appointmentObject = $this->appoinmnetModel->getAppointmentByRequestId($row->Id);
            $requestObject = array(
                                    'Id'=>(int)$row->Id,
                                    'datetime'=>$displayDate,
                                    'carType'=>$carTypeObject,
                                    'car'=>$carObject,
                                    'address'=>$addressObject,
                                    'user'=>$userObject,
                                    'appointment'=>$appointmentObject
                                 );
        }
      }
      return $requestObject;
    }



























  /***************************    OLD APIS    ***************************/



   /*
      Method get getAddressOfRequest
      Created By: Manzz Baria
    */
    public function getAddressOfRequest($requestId){
      $this->db->select('address');
      $this->db->from('addressmaster');
      $this->db->join('requestmaster','addressmaster.Id = requestmaster.addressId','inner');
      $this->db->where('requestmaster.Id',$requestId);
      $query = $this->db->get();
      $result = $query->result();
      $address = null;
      if($result != null){
        foreach ($result as $row) {
          $address = $row->address;
        }
      }
      return $address;
    }

    /*
       Method get getServiceIdByRequetId
       Created By: Manzz Baria
     */
     public function getServiceIdByRequetId($requestId){
       $this->db->select('serviceId');
       $this->db->from('requestdetails');
       $this->db->join('requestmaster','requestdetails.requestId = requestmaster.Id','inner');
       $this->db->where('requestmaster.Id',$requestId);
       $query = $this->db->get();
       $result = $query->result();
       $serviceId = null;
       if($result != null){
         foreach ($result as $row) {
           $serviceId = $row->serviceId;
         }
       }
       return $serviceId;
     }

    /*
       Method get getCarTypeIdOfRequest
       Created By: Manzz Baria
     */
     public function getCarTypeIdOfRequest($requestId){
       $this->db->select('carTypeId');
       $this->db->from('requestmaster');
       $this->db->where('Id',$requestId);
       $query = $this->db->get();
       $result = $query->result();
       $carTypeId = null;
       if($result != null){
         foreach ($result as $row) {
           $carTypeId = $row->carTypeId;
         }
       }
       return $carTypeId;
     }

    /*
       Method get getCarTypeOfRequest
       Created By: Manzz Baria
     */
     public function getCarTypeOfRequest($requestId){
       $this->db->select('name');
       $this->db->from('cartypemaster');
       $this->db->join('requestmaster','cartypemaster.Id = requestmaster.carTypeId','inner');
       $this->db->where('requestmaster.Id',$requestId);
       $query = $this->db->get();
       $result = $query->result();
       $name = null;
       if($result != null){
         foreach ($result as $row) {
           $name = $row->name;
         }
       }
       return $name;
     }

     /*
        Method get getCategoryOfRequest
        Created By: Manzz Baria
      */
      public function getCategoryOfRequest($requestId){
        $this->db->select('name');
        $this->db->from('categorymaster');
        $this->db->join('requestmaster','categorymaster.Id = requestmaster.categoryId','inner');
        $this->db->where('requestmaster.Id',$requestId);
        $query = $this->db->get();
        $result = $query->result();
        $name = null;
        if($result != null){
          foreach ($result as $row) {
            $name = $row->name;
          }
        }
        return $name;
      }

      /*
         Method get getCategoryOfRequest
         Created By: Manzz Baria
       */
       public function getCategoryObjectOfRequest($requestId){
         $this->load->model("ApiCategoryModel","categoryModel");
         $this->db->select('categorymaster.Id');
         $this->db->from('categorymaster');
         $this->db->join('requestmaster','categorymaster.Id = requestmaster.categoryId','inner');
         $this->db->where('requestmaster.Id',$requestId);
         $query = $this->db->get();
         $result = $query->result();
         $categoryId = null;
         if($result != null){
           foreach ($result as $row) {
             $categoryId = $row->Id;
           }
         }
         return $this->categoryModel->getCategoryById($categoryId);;
       }

  /* Method to check isAdditionalSercvieAlreadyAddedToRequest
     Created By: Manzz Baria
  */
  public function isAdditionalSercvieAlreadyAddedToRequest($additionalServiceId,$requestId){
    $this->db->select('Id');
    $this->db->from('requestadditionalservice');
    $this->db->where("requestId",$requestId);
    $this->db->where("additionalServiceDetailId",$additionalServiceId);
    $query = $this->db->get();
    if ($query->result() != null) {
      return true;
    }
    return false;
  }

  /* Method to saveAdditionalServiceOfRequest
     Created By: Manzz Baria
  */
  public function saveAdditionalServiceOfRequest($requestId,$additionalServiceIds){
        $myArray = explode(',', $additionalServiceIds);
        for ($i=0; $i< sizeof($myArray); $i++) {
          $this->addAdditionalServiceToRequest($requestId,$myArray[$i]);
        }
        return true;

  }

  /* Method to addAdditionalServiceToRequest
     Created By: Manzz Baria
  */
  public function addAdditionalServiceToRequest($requestId,$additionalServiceId){

        $isAdded = $this->isAdditionalSercvieAlreadyAddedToRequest($additionalServiceId,$requestId);
        if (!$isAdded) {
          $data = array(
                          'additionalServiceDetailId'=>$additionalServiceId,
                          'requestId'=>$requestId);
          $this->db->insert('requestadditionalservice', $data);
          $insert_Id = $this->db->insert_id();
          return true;
      }else{
            return false;
      }
  }


    /* Method to addRequest
       Created By: Manzz Baria
    */
    public function addRequest($userId,$carTypeId,$serviceIds,$addressId,$amount,$currencyId,$appoinmentDateTime,$carId,$note,$vanId,$additionalServiceIds){
      $this->load->model("ApiAppoinmnetModel","appoinmnetModel");
      $this->load->model("ApiBillModel","billModel");
    //  $this->load->model("ApiEmailModel","emailModel");
      $isAdded = $this->appoinmnetModel->isAppoinmentAlreadyFixed($appoinmentDateTime,$vanId);
      if (!$isAdded) {
          $this->load->model("Utility","utility");
          $datetime = $this->utility->getCurrentDate('Y/m/d h:i:s');
          $data = array(
                      'userId'=>$userId,
                      'addressId'=>$addressId,
                      'carTypeId'=>$carTypeId,
                      'carId'=>$carId,
                      'note'=>$note,
                      'datetime'=>$datetime);
          $this->db->insert('requestmaster', $data);
          $insert_Id = $this->db->insert_id();
          $this->saveRequestDetails($insert_Id,$serviceIds);
          $this->appoinmnetModel->addAppoinment($insert_Id,$vanId,$appoinmentDateTime);
          $this->billModel->saveBill($insert_Id,$amount,$currencyId);
          $this->requestAction($insert_Id,$userId,REQUEST_STATUS_PENDING,"");
          $this->saveAdditionalServiceOfRequest($insert_Id,$additionalServiceIds);
          $this->load->library('mylibrary');
          $url = base_url()."Api/Email/fixAppointmentConfirmation";
          $param = array('appointmentId' => "".$insert_Id );
          $this->mylibrary->do_in_background($url, $param);
          return $this->getRequestById($insert_Id);
       }else{
          return null;
        }

    }


    /* Method to saveRequestDetails
       Created By: Manzz Baria
    */
 public function saveRequestDetails($requestId,$serviceIds){
    $serviceIds = str_replace(' ', '', $serviceIds);
      $serviceId = explode(',', $serviceIds);
      $status = REQUEST_STATUS_PENDING;
      $dataMain =null;
      for ($i=0; $i< sizeof($serviceId); $i++) {
            $isAdded = $this->isRequestDetailAlreadyAdded($requestId,$serviceId[$i]);
            if (!$isAdded) {
                $data = array(
                            'requestId'=>$requestId,
                            'serviceId'=>$serviceId[$i],
                            'status'=>$status);
                $dataMain[] = $data;
          }
     }

     $this->db->insert_batch('requestdetails', $dataMain);
     return true;
  }
  /* Method to check isRequestDetailAlreadyAdded
     Created By: Manzz Baria
  */
  public function isRequestDetailAlreadyAdded($requestId,$serviceId){
    $this->db->select('Id');
    $this->db->from('requestdetails');
    $this->db->where("requestId",$requestId);
    $this->db->where("serviceId",$serviceId);
    $query = $this->db->get();
    if ($query->result() != null) {
      return true;
    }
    return false;
  }



  /* Method to getTotalPagesOfRequests
     Created By: Manzz Baria
  */
  public function getTotalPagesOfRequests($userId){
    $this->db->select("requestmaster.Id,requestmaster.userId,requestmaster.addressId,requestmaster.carTypeId,requestmaster.carId,requestmaster.categoryId,requestmaster.note,requestmaster.datetime");
    $this->db->from("requestmaster");
    $this->db->join('appoinmentmaster','requestmaster.Id = appoinmentmaster.requestId','inner');
    $this->db->where('userId', $userId);
    $this->db->where_not_in('appoinmentmaster.status',REQUEST_STATUS_FINISH);
    $this->db->where_not_in('appoinmentmaster.status',REQUEST_STATUS_REJECTED);
    $this->db->order_by("appoinmentmaster.appointmentDatetime","ASC");
    $query = $this->db->count_all_results();
    $result  = $query / 20;
    return ceil($result);
  }

/* Method to getRequests
   Created By: Manzz Baria
*/
public function getRequests($userId,$pageIndex){
  $this->db->select("requestmaster.Id,requestmaster.userId,requestmaster.addressId,requestmaster.carTypeId,requestmaster.carId,requestmaster.categoryId,requestmaster.note,requestmaster.datetime");
  $this->db->from("requestmaster");
  $this->db->join('appoinmentmaster','requestmaster.Id = appoinmentmaster.requestId','inner');
  $this->db->where('userId', $userId);
  $this->db->where_not_in('appoinmentmaster.status',REQUEST_STATUS_FINISH);
  $this->db->where_not_in('appoinmentmaster.status',REQUEST_STATUS_REJECTED);
  $this->db->order_by("appoinmentmaster.appointmentDatetime","ASC");
  $pageNo = "00";
  if ($pageIndex > 0) {
    $pageIndex = $pageIndex * 2;
    $pageNo = $pageIndex.'0';
  }
  $this->db->limit(20,$pageNo);
  $query = $this->db->get();
  return $this->displayRequests($query->result());

}

public function getrequestdate($userId){
  $this->db->select("requestmaster.Id,requestmaster.userId,max(appoinmentmaster.appointmentDatetime) as LastestDate");
  $this->db->from("requestmaster");
  $this->db->join('usermaster','usermaster.Id = requestmaster.userId','inner');
  $this->db->join('appoinmentmaster','requestmaster.Id = appoinmentmaster.requestId','inner');
  $this->db->where('requestmaster.userId', $userId);
  $this->db->order_by("appoinmentmaster.appointmentDatetime","ASC");
 
  $query = $this->db->get();
  return $this->displayRequestsdate($query->result());

}


public function displayRequestsdate($result){
$requestObject = null;
$Object=null;

if($result != null ){
 $this->load->model("Utility","utility");

  foreach ($result as $row) {
     $CustomerDate = $this->utility->changeDateFromat($row->LastestDate,'d-m-Y h:i');
        $CustomerDate= date("Y-m-d h:i:s", strtotime('+3 hours', strtotime($CustomerDate)));
    //   

            if($row->userId!=null)
            {
                 $Object = array(
                                      'Id'=>(int)$row->Id,
                                      'userId'=>$row->userId,
                                      'datetime'=>$CustomerDate
                                  );
                      $requestObject[]=$Object;
              }
        }
    }
      return $Object;
}

public function getsumofrequestjob($fromDate,$toDate,$userId){
 
  $this->db->select("count(appoinmentmaster.Id) as NoOfJob,sum(billmaster.localAmount) as SumOfJobAmount");
  $this->db->from("appoinmentmaster");
 $this->db->join('requestmaster','requestmaster.Id = appoinmentmaster.requestId','inner');
  $this->db->join('billmaster','billmaster.appointmentId = appoinmentmaster.Id','inner');
  $this->db->where('requestmaster.userId', $userId);
 $this->db->where('appoinmentmaster.appointmentDatetime >=', $fromDate);
  $this->db->where('appoinmentmaster.appointmentDatetime <=', $toDate);

 
  $query = $this->db->get();
  //print_r($query->result());
  return $this->displaysumofrequestjob($query->result());

}
public function displaysumofrequestjob($result){
$requestObject = null;
$Object=null;

if($result != null ){
 $this->load->model("ApiBillModel","BillModel");

  foreach ($result as $row) {
    
            if($row->NoOfJob>0)
            {
                 $Object = array(
                                      'NoOfJob'=>$row->NoOfJob,
                                      'SumOfJobAmount'=>$row->SumOfJobAmount
                                  );
                      $requestObject[]=$Object;
              }
        }
    }
      return $Object;
}


/*
public function getRequestAppointIds($vanId,$ownerId){
 
  $this->db->select("Id,requestId");
  $this->db->from("appoinmentmaster");
  $this->db->where('vanId',$vanId);
   $this->db->where('status',3);
 
  $query = $this->db->get();
  //print_r($query->result());
  return $this->displayRequestAppointIds($query->result(),$ownerId);

}

public function displayRequestAppointIds($result,$ownerId){
$requestObject = null;
$Object=null;
$carId=0;
$serviceobj=null;

if($result != null ){
  
   
  foreach ($result as $row) {
      $carId = $this->getcartypeByRequestId($row->requestId);
       $serviceobj = $this->getserviceIdByappointId($row->Id, $carId,$ownerId);

                 $Object = array(
                                      'Id'=>(int)$row->Id,
                                      'requestId'=>$row->requestId,
                                      'carTypeId'=>$carId,
                                      'serviceobj'=>$serviceobj
                                  );
                      $requestObject[]=$Object;
              }
    }
      return $requestObject;
}


public function getcartypeByRequestId($requestId){
  $cartypeId=0;
    $this->db->select("requestmaster.Id,requestmaster.carTypeId");
    $this->db->from("requestmaster");
    $this->db->join('cartypemaster','requestmaster.carTypeId = cartypemaster.Id','inner');
    $this->db->where('requestmaster.Id',$requestId);
    $query = $this->db->get();

      if($query->result() != null ){
  
   
      foreach ($query->result() as $row) 
      {
        $cartypeId=$row->carTypeId;
      }
  
      return $cartypeId;

  }
}*/


public function getOutStandingAmountForAllOperator()
{
    $this->db->select('usermaster.fullName,usermaster.email,usermaster.Id as userId');
    $this->db->from('usermaster');
    $this->db->where('usermaster.userType',2);
     $query=$this->db->get();
     $result=$query->result();
     $Object =null;
      $ObjectAll =null;
     foreach ($result as $row) {

      $amount= $this->getOutStandingAmountById($row->userId);
       $Object = array(
                          'operatorId'=>$row->userId,
                          'name'=>$row->fullName,
                          'email'=>$row->email,
                          'OutStandingCost'=> $amount
                        );

       $ObjectAll[]= $Object;
     }
     return $ObjectAll;
}

public function getOutStandingAmountById($userId)
{
   
   $remainingAmount=0;
   $outstand=0;
  $total=0;
   $this->db->select('requestmaster.carTypeId,appoinmentdetail.serviceId,vanownermaster.Id as supplierId,usermaster.fullName,usermaster.email,usermaster.Id as userId');
   $this->db->from('usermaster');
   $this->db->join('vanmaster','vanmaster.Id=usermaster.vanId','inner');
   $this->db->join('vanownermaster','vanownermaster.Id=vanmaster.ownerId','inner');
    $this->db->join('appoinmentmaster','appoinmentmaster.vanId=vanmaster.Id','inner');
    $this->db->join('requestmaster','requestmaster.Id=appoinmentmaster.requestId','inner');
    $this->db->join('appoinmentdetail','appoinmentdetail.appointmentId=appoinmentmaster.Id','inner');
   // $this->db->distinct('appoinmentdetail.serviceId');
   $this->db->where('usermaster.Id',$userId);
    $this->db->where('usermaster.userType',2);
   $this->db->where('appoinmentmaster.status',3);

   $query=$this->db->get();
   $result=$query->result();
     foreach ($result as $row) {
       $amount= $this->getcostbyservice($row->serviceId,$row->carTypeId,$row->supplierId);
       $remainingAmount=$this->getremainingAmountOfOperator($row->userId);
       
       $total=$total+$amount;
      
     }
     return  $total-$remainingAmount ;

}


public function getgetOutStandingCostByUserId($userId)
{
   $Object =null;
  
   $remainingAmount=0;
   $outstand=0;
  $total=0;
   $this->db->select('requestmaster.carTypeId,appoinmentdetail.serviceId,vanownermaster.Id as supplierId,usermaster.fullName,usermaster.email,usermaster.Id as userId');
   $this->db->from('usermaster');
   $this->db->join('vanmaster','vanmaster.Id=usermaster.vanId','inner');
   $this->db->join('vanownermaster','vanownermaster.Id=vanmaster.ownerId','inner');
    $this->db->join('appoinmentmaster','appoinmentmaster.vanId=vanmaster.Id','inner');
    $this->db->join('requestmaster','requestmaster.Id=appoinmentmaster.requestId','inner');
    $this->db->join('appoinmentdetail','appoinmentdetail.appointmentId=appoinmentmaster.Id','inner');
   // $this->db->distinct('appoinmentdetail.serviceId');
   $this->db->where('usermaster.Id',$userId);
    $this->db->where('usermaster.userType',2);
   $this->db->where('appoinmentmaster.status',3);

   $query=$this->db->get();
   $result=$query->result();
     foreach ($result as $row) {
       $amount= $this->getcostbyservice($row->serviceId,$row->carTypeId,$row->supplierId);
       $remainingAmount=$this->getremainingAmountOfOperator($row->userId);
       
       $total=$total+$amount;
      

        $Object = array(
                          'operatorId'=>$row->userId,
                          'name'=>$row->fullName,
                          'email'=>$row->email,
                          'OutStandingCost'=> $total-$remainingAmount
                        );
       
     }
     return  $Object ;

}


public function getOutStandingCostBySupplierId($supplierId)
{
   $Object =null;
  $total=0;
   $this->db->select('requestmaster.carTypeId,appoinmentdetail.serviceId,vanownermaster.Id as supplierId,vanownermaster.name,vanownermaster.email');
   $this->db->from('vanownermaster');
   $this->db->join('vanmaster','vanownermaster.Id=vanmaster.ownerId','inner');
    $this->db->join('appoinmentmaster','appoinmentmaster.vanId=vanmaster.Id','inner');
    $this->db->join('requestmaster','requestmaster.Id=appoinmentmaster.requestId','inner');
    $this->db->join('appoinmentdetail','appoinmentdetail.appointmentId=appoinmentmaster.Id','inner');
    
   $this->db->where('vanownermaster.Id',$supplierId);
   $this->db->where('appoinmentmaster.status',3);
   $query=$this->db->get();
   $result=$query->result();
   $this->load->model("ApiVanModel","vanModel");
   $remainingAmount=$this->getremainingAmount($supplierId);

     foreach ($result as $row) {
       $amount= $this->getcostbyservice($row->serviceId,$row->carTypeId,$row->supplierId);
       
       $total=$total+$amount;
    
        $Object = array(
                          'supplierId'=>$row->supplierId,
                          'name'=>$row->name,
                          'email'=>$row->email,
                          'OutStandingCost'=> $total-$remainingAmount
                        );
     }
     return  $Object ;
 
}


public function getcostbyservice($serviceId,$carTypeId,$supplierId)
{
    //echo $supplierId;
  $this->db->select('servicedetail.Id,servicedetail.amount,servicecost.cost,servicecost.type');
  $this->db->from('servicedetail');
  $this->db->join('servicecost','servicecost.serviceDetailId=servicedetail.Id','inner');
    $this->db->where('servicedetail.serviceId',$serviceId);
      $this->db->where('servicedetail.carTypeId',$carTypeId);
       $this->db->where('servicecost.supplierIds',$supplierId);
      $query=$this->db->get();
  // print_r( $query->result());
   $result=$query->result();
   $cost=0;
   foreach ($result as $row) {
     // $remainingAmount=$this->getremainingAmount($supplierId);
            if($row->type==1)
            {
                $cost=$row->cost;
                $cost= ($row->amount * $cost) / 100;
                $cost= ($row->amount - $cost);
            }
            else
            {
                $cost=$row->cost;
                $cost=$row->amount - $cost;
            }

     }
     return $cost;
}



public function getremainingAmount($supplierId)
{
  $this->db->select('receivedmargin.Id,receivedmargin.supplierId,sum(receivedmargin.receivedAmount) as amount');
  $this->db->from('receivedmargin');
  $this->db->join('vanownermaster','vanownermaster.Id=receivedmargin.supplierId','inner');
  $this->db->where('receivedmargin.supplierId',$supplierId);
  $this->db->distinct();
  $query=$this->db->get();
//  print_r( $query->result());
   $result=$query->result();
   $cost=0;
   foreach ($result as $row) {
  //    echo $row->amount;
     }
    return $row->amount;
}


public function getremainingAmountOfOperator($userId)
{
  //echo $userId;
  $this->db->select('receivedmargin.Id,receivedmargin.operatorId,sum(receivedmargin.receivedAmount) as amount');
  $this->db->from('receivedmargin');
 // $this->db->join('vanownermaster','vanownermaster.Id=receivedmargin.supplierId','inner');
  $this->db->where('receivedmargin.operatorId',$userId);
  $this->db->distinct();
  $query=$this->db->get();
 // print_r( $query->result());
   $result=$query->result();
   $cost=0;
   foreach ($result as $row) {
      //echo $row->amount;
     }
    // echo $row->amount;
    return $row->amount;
}



/* Method to displayRequests
     Created By: Manzz Baria
  */
  public function displayRequests($result){
    $requestObject = null;
    if($result != null){
      $this->load->model("Utility","utility");
      $this->load->model("ApiAddressModel","addressModel");
      $this->load->model("ApiCarModel","carModel");
      $this->load->model("ApiCategoryModel","categoryModel");
      $this->load->model("ApiServiceModel","serviceModel");
      $this->load->model("ApiAppoinmnetModel","appoinmnetModel");
      $this->load->model("ApiBillModel","billModel");
      $this->load->model("ApiEvaluationModel","evaluationModel");
      $this->load->model("ApiRateModel","rateModel");
      foreach ($result as $row) {
          $displayDate = $this->utility->timeAgoFormat($row->datetime);
          $addressObject = $this->addressModel->getAddressById($row->addressId);
          $carTypeObject = $this->carModel->getCarTypeById($row->carTypeId);
          $carObject = $this->carModel->getCarDetailsById($row->carId);
          $categoryObject = $this->categoryModel->getCategoryById($row->categoryId);
          $appointmentObject = $this->appoinmnetModel->getAppointmentByRequestId($row->Id);
          $billObject = $this->billModel->getBillDetailsByRequestId($row->Id);
          $serviceObject = $this->serviceModel->getServicesByRequestId($row->Id,$row->carTypeId);
          $evaluationObject = $this->evaluationModel->getEvaluationByRequestId($row->Id);
          $status = $this->getRequestActions($row->Id);
          $rate = $this->rateModel->getRateOfRequestId($row->Id);
          $Object = array(
                          'Id'=>(int)$row->Id,
                          'note'=>$row->note,
                          'datetime'=>$displayDate,
                          'address'=>$addressObject,
                          'carType'=>$carTypeObject,
                          'car'=>$carObject,
                          'category'=>$categoryObject,
                          'appointment'=>$appointmentObject,
                          'bill'=>$billObject,
                          'service'=>$serviceObject,
                          'status' =>$status,
                          'evaluation'=>$evaluationObject,
                          'rate'=>$rate
                      );
          $requestObject[]=$Object;
      }
    }
    return $requestObject;
  }

/*************************** Start Request list by status ****************************/

  /* Method to getTotalPagesOfRequests
     Created By: Manzz Baria
  */
  public function getTotalPagesOfRequestsByStatus($status){
    $this->db->select("requestmaster.Id,requestmaster.userId,requestmaster.addressId,requestmaster.carTypeId,requestmaster.carId,requestmaster.categoryId,requestmaster.note,requestmaster.datetime");
    $this->db->from("requestmaster");
    $this->db->join('appoinmentmaster','requestmaster.Id = appoinmentmaster.requestId','inner');

    if ($status == REQUEST_STATUS_IN_PROGRESS) {
      $strWhere = "appoinmentmaster.status ='".$status."' or appoinmentmaster.status ='".REQUEST_STATUS_ON_THE_WAY."'";
      $this->db->where($strWhere,null,false);
    }else{
      $this->db->where('appoinmentmaster.status',$status);
    }

    if ($status == REQUEST_STATUS_REFUND) {
      $this->db->join('billmaster','requestmaster.Id = billmaster.requestId','inner');
      $this->db->where('billmaster.status',BILL_STATUS_REFUND);
    }
    $this->db->order_by("appoinmentmaster.appointmentDatetime","ASC");
      $query = $this->db->count_all_results();
      $result  = $query / 20;
      return ceil($result);
  }

  /* Method to getRequestsByStatus
   Created By: Manzz Baria
  */
  public function getRequestsByStatus($status,$pageIndex){
      $this->db->select("requestmaster.Id,requestmaster.userId,requestmaster.addressId,requestmaster.carTypeId,requestmaster.carId,requestmaster.categoryId,requestmaster.note,requestmaster.datetime");
      $this->db->from("requestmaster");
      $this->db->join('appoinmentmaster','requestmaster.Id = appoinmentmaster.requestId','inner');

      if ($status == REQUEST_STATUS_IN_PROGRESS) {
        $strWhere = "appoinmentmaster.status ='".$status."' or appoinmentmaster.status ='".REQUEST_STATUS_ON_THE_WAY."'";
        $this->db->where($strWhere,null,false);
      }else{
        $this->db->where('appoinmentmaster.status',$status);
      }

      if ($status == REQUEST_STATUS_REFUND) {
        $this->db->join('billmaster','requestmaster.Id = billmaster.requestId','inner');
        $this->db->where('billmaster.status',BILL_STATUS_REFUND);
      }
      $this->db->order_by("appoinmentmaster.appointmentDatetime","ASC");
      $pageNo = "00";
      if ($pageIndex > 0) {
        $pageIndex = $pageIndex * 2;
        $pageNo = $pageIndex.'0';
      }
      $this->db->limit(20,$pageNo);
      $query = $this->db->get();
      return $this->displayRequestsByStatusAndOperator($query->result());
  }

  /* Method to displayRequestsByStatusAndOperator
     Created By: Manzz Baria
  */
  public function displayRequestsByStatusAndOperator($result){
    $requestObject = null;
    if($result != null){
      $this->load->model("Utility","utility");
      $this->load->model("ApiAddressModel","addressModel");
      $this->load->model("ApiCarModel","carModel");
      $this->load->model("ApiCategoryModel","categoryModel");
      $this->load->model("ApiServiceModel","serviceModel");
      $this->load->model("ApiAppoinmnetModel","appoinmnetModel");
      $this->load->model("ApiBillModel","billModel");
      $this->load->model("ApiUserModel","userModel");
      $this->load->model("ApiEvaluationModel","evaluationModel");
      $this->load->model("ApiRateModel","rateModel");
      foreach ($result as $row) {
          $displayDate = $this->utility->timeAgoFormat($row->datetime);
          $addressObject = $this->addressModel->getAddressById($row->addressId);
          $carTypeObject = $this->carModel->getCarTypeById($row->carTypeId);
          $carObject = $this->carModel->getCarDetailsById($row->carId);
          $categoryObject = $this->categoryModel->getCategoryById($row->categoryId);
          $appointmentObject = $this->appoinmnetModel->getAppointmentByRequestId($row->Id);
          $billObject = $this->billModel->getBillDetailsByRequestId($row->Id);
          $serviceObject = $this->serviceModel->getServicesByRequestId($row->Id,$row->carTypeId);
          $userObject = $this->userModel->getUserDetail($row->userId);
          $status = $this->getRequestActions($row->Id);
          $evaluationObject = $this->evaluationModel->getEvaluationByRequestId($row->Id);
          $rate = $this->rateModel->getRateOfRequestId($row->Id);
          $Object = array(
                          'Id'=>(int)$row->Id,
                          'note'=>$row->note,
                          'datetime'=>$displayDate,
                          'user'=>$userObject,
                          'address'=>$addressObject,
                          'carType'=>$carTypeObject,
                          'car'=>$carObject,
                          'category'=>$categoryObject,
                          'appointment'=>$appointmentObject,
                          'bill'=>$billObject,
                          'service'=>$serviceObject,
                          'status' =>$status,
                          'evaluation'=>$evaluationObject,
                          'rate'=>$rate
                      );
          $requestObject[]=$Object;
      }
    }
    return $requestObject;
  }

/*************************** End Request list by status ****************************/

/*************************** Start Request list for operater ****************************/

  /* Method to getTotalPagesOfRequestsByOperator
     Created By: Manzz Baria
  */
  public function getTotalPagesOfRequestsByOperator($userId){
    $this->db->select("requestmaster.Id,requestmaster.userId,requestmaster.addressId,requestmaster.carTypeId,requestmaster.carId,requestmaster.categoryId,requestmaster.note,requestmaster.datetime");
    $this->db->from("requestmaster");
    $this->db->join('appoinmentmaster','requestmaster.Id = appoinmentmaster.requestId','inner');
    $this->db->join('usermaster','appoinmentmaster.vanId = usermaster.vanId','inner');
    $this->db->where('usermaster.Id',$userId);
    $this->db->where_not_in('appoinmentmaster.status',REQUEST_STATUS_FINISH);
    $this->db->where_not_in('appoinmentmaster.status',REQUEST_STATUS_REJECTED);
    $this->db->order_by("appoinmentmaster.appointmentDatetime","ASC");
      $query = $this->db->count_all_results();
      $result  = $query / 20;
      return ceil($result);
  }

  /* Method to getRequestsByOperator
   Created By: Manzz Baria
  */
  public function getRequestsByOperator($userId,$pageIndex){
      $this->db->select("requestmaster.Id,requestmaster.userId,requestmaster.addressId,requestmaster.carTypeId,requestmaster.carId,requestmaster.categoryId,requestmaster.note,requestmaster.datetime");
      $this->db->from("requestmaster");
      $this->db->join('appoinmentmaster','requestmaster.Id = appoinmentmaster.requestId','inner');
      $this->db->join('usermaster','appoinmentmaster.vanId = usermaster.vanId','inner');
      $this->db->where('usermaster.Id',$userId);
      $this->db->where_not_in('appoinmentmaster.status',REQUEST_STATUS_FINISH);
      $this->db->where_not_in('appoinmentmaster.status',REQUEST_STATUS_REJECTED);
      $this->db->order_by("appoinmentmaster.appointmentDatetime","ASC");
      $pageNo = "00";
      if ($pageIndex > 0) {
        $pageIndex = $pageIndex * 2;
        $pageNo = $pageIndex.'0';
      }
      $this->db->limit(20,$pageNo);
      $query = $this->db->get();
      return $this->displayRequestsByStatusAndOperator($query->result());
  }


/*************************** End Request list for operater ****************************/
/*************************** Start Request list between two dates ****************************/

  /* Method to getTotalPagesOfOperatorRequestsByTwoDates
     Created By: Manzz Baria
  */
  public function getTotalPagesOfOperatorRequestsByTwoDates($userId,$fromDate,$toDate){
    $fromDate = $fromDate." 00:00:00";
    $toDate = $toDate." 23:59:59";
    $this->db->select("requestmaster.Id,requestmaster.userId,requestmaster.addressId,requestmaster.carTypeId,requestmaster.carId,requestmaster.categoryId,requestmaster.note,requestmaster.datetime");
    $this->db->from("requestmaster");
    $this->db->join('appoinmentmaster','requestmaster.Id = appoinmentmaster.requestId','inner');
    $this->db->join('usermaster','appoinmentmaster.vanId = usermaster.vanId','inner');
    $this->db->where('usermaster.Id',$userId);
    $this->db->where('appoinmentmaster.appointmentDatetime >=', $fromDate);
    $this->db->where('appoinmentmaster.appointmentDatetime <=', $toDate);
      $query = $this->db->count_all_results();
      $result  = $query / 20;
      return ceil($result);
  }

  /* Method to getOperatorRequestsByTwoDates
   Created By: Manzz Baria
  */
  public function getOperatorRequestsByTwoDates($userId,$fromDate,$toDate,$pageIndex){
      $fromDate = $fromDate." 00:00:00";
      $toDate = $toDate." 23:59:59";
      $this->db->select("requestmaster.Id,requestmaster.userId,requestmaster.addressId,requestmaster.carTypeId,requestmaster.carId,requestmaster.categoryId,requestmaster.note,requestmaster.datetime");
      $this->db->from("requestmaster");
      $this->db->join('appoinmentmaster','requestmaster.Id = appoinmentmaster.requestId','inner');
      $this->db->join('usermaster','appoinmentmaster.vanId = usermaster.vanId','inner');
      $this->db->where('usermaster.Id',$userId);
      $this->db->where('appoinmentmaster.appointmentDatetime >=', $fromDate);
      $this->db->where('appoinmentmaster.appointmentDatetime <=', $toDate);
      $this->db->order_by("appoinmentmaster.appointmentDatetime","ASC");
      $pageNo = "00";
      if ($pageIndex > 0) {
        $pageIndex = $pageIndex * 2;
        $pageNo = $pageIndex.'0';
      }
      $this->db->limit(20,$pageNo);
      $query = $this->db->get();
      return $this->displayRequestsByStatusAndOperator($query->result());
  }

  /* Method to getTotalPagesOfUserRequestsByTwoDates
     Created By: Manzz Baria
  */
  public function getTotalPagesOfUserRequestsByTwoDates($userId,$fromDate,$toDate){
    $fromDate = $fromDate." 00:00:00";
    $toDate = $toDate." 23:59:59";
    $this->db->select("requestmaster.Id,requestmaster.userId,requestmaster.addressId,requestmaster.carTypeId,requestmaster.carId,requestmaster.categoryId,requestmaster.note,requestmaster.datetime");
    $this->db->from("requestmaster");
    $this->db->join('appoinmentmaster','requestmaster.Id = appoinmentmaster.requestId','inner');
    $this->db->where('requestmaster.userId',$userId);
    $this->db->where('appoinmentmaster.appointmentDatetime >=', $fromDate);
    $this->db->where('appoinmentmaster.appointmentDatetime <=', $toDate);
      $query = $this->db->count_all_results();
      $result  = $query / 20;
      return ceil($result);
  }
  /* Method to getUserRequestsByTwoDates
   Created By: Manzz Baria
  */
  public function getUserRequestsByTwoDates($userId,$fromDate,$toDate,$pageIndex){
      $fromDate = $fromDate." 00:00:00";
      $toDate = $toDate." 23:59:59";
      $this->db->select("requestmaster.Id,requestmaster.userId,requestmaster.addressId,requestmaster.carTypeId,requestmaster.carId,requestmaster.categoryId,requestmaster.note,requestmaster.datetime");
      $this->db->from("requestmaster");
      $this->db->join('appoinmentmaster','requestmaster.Id = appoinmentmaster.requestId','inner');
      $this->db->where('requestmaster.userId',$userId);
      $this->db->where('appoinmentmaster.appointmentDatetime >=', $fromDate);
      $this->db->where('appoinmentmaster.appointmentDatetime <=', $toDate);
      $this->db->order_by("appoinmentmaster.appointmentDatetime","ASC");
      $pageNo = "00";
      if ($pageIndex > 0) {
        $pageIndex = $pageIndex * 2;
        $pageNo = $pageIndex.'0';
      }
      $this->db->limit(20,$pageNo);
      $query = $this->db->get();
      return $this->displayRequestsByStatusAndOperator($query->result());
  }

  /* Method to getTotalPagesOfAdminRequestsByTwoDates
     Created By: Manzz Baria
  */
  public function getTotalPagesOfAdminRequestsByTwoDates($fromDate,$toDate){
    $fromDate = $fromDate." 00:00:00";
    $toDate = $toDate." 23:59:59";
    $this->db->select("requestmaster.Id,requestmaster.userId,requestmaster.addressId,requestmaster.carTypeId,requestmaster.carId,requestmaster.categoryId,requestmaster.note,requestmaster.datetime");
    $this->db->from("requestmaster");
    $this->db->join('appoinmentmaster','requestmaster.Id = appoinmentmaster.requestId','inner');
    $this->db->where('appoinmentmaster.appointmentDatetime >=', $fromDate);
    $this->db->where('appoinmentmaster.appointmentDatetime <=', $toDate);
    $this->db->order_by("appoinmentmaster.appointmentDatetime","ASC");
      $query = $this->db->count_all_results();
      $result  = $query / 20;
      return ceil($result);
  }
  /* Method to getAdminRequestsByTwoDates
   Created By: Manzz Baria
  */
  public function getAdminRequestsByTwoDates($fromDate,$toDate,$pageIndex){
      $fromDate = $fromDate." 00:00:00";
      $toDate = $toDate." 23:59:59";
      $this->db->select("requestmaster.Id,requestmaster.userId,requestmaster.addressId,requestmaster.carTypeId,requestmaster.carId,requestmaster.categoryId,requestmaster.note,requestmaster.datetime");
      $this->db->from("requestmaster");
      $this->db->join('appoinmentmaster','requestmaster.Id = appoinmentmaster.requestId','inner');
      $this->db->where('appoinmentmaster.appointmentDatetime >=', $fromDate);
      $this->db->where('appoinmentmaster.appointmentDatetime <=', $toDate);
      $this->db->order_by("appoinmentmaster.appointmentDatetime","ASC");
      $pageNo = "00";
      if ($pageIndex > 0) {
        $pageIndex = $pageIndex * 2;
        $pageNo = $pageIndex.'0';
      }
      $this->db->limit(20,$pageNo);
      $query = $this->db->get();
      return $this->displayRequestsByStatusAndOperator($query->result());
  }

/*************************** End Request list between two dates ****************************/



    /* Method to getRequestActions
       Created By: Manzz Baria
    */
    public function getRequestActions($requestId){
      $this->db->select("Id,requestId,userId,status,reason,datetime");
      $this->db->from("requestactionmaster");
      $this->db->where('requestId', $requestId);
      $this->db->order_by("Id","DESC");
      $query = $this->db->get();
      return $this->displayRequestActions($query->result());
    }

    /* Method to displayRequestActions
         Created By: Manzz Baria
      */
      public function displayRequestActions($result){
        $requestObject = null;
        if($result != null){
          $this->load->model("Utility","utility");
          $this->load->model("ApiUserModel","uModel");
          foreach ($result as $row) {
              $displayDate = $this->utility->timeAgoFormat($row->datetime);
              $userObject = $this->uModel->getUserDetail($row->userId);
              $Object = array(
                              'Id'=>(int)$row->Id,
                              'status'=>(int)$row->status,
                              'reason'=>$row->reason,
                              'datetime'=>$displayDate,
                              'ActionBy'=>$userObject
                          );
              $requestObject[]=$Object;
          }
        }
        return $requestObject;
      }



}
?>
