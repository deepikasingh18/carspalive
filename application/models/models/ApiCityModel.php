<?php if( ! defined("BASEPATH")) exit('No direct script access allowed');

class ApiCityModel extends CI_Model {

  public function _construct(){
		parent::_construct();
	}

/* Method to check isCityAlreadyAdded
   Created By: Manzz Baria
*/
public function isCityAlreadyAdded($name){
  $this->db->select('Id');
  $this->db->from('citymaster');
  $this->db->where("name",$name);
  $this->db->where("isBlocked",UNBLOCKED);
  $query = $this->db->get();
  if ($query->result() != null) {
    return true;
  }
  return false;
}

  /* Method to addCity
     Created By: Manzz Baria
  */
  public function addCity($name){
    $isAdded = $this->isCityAlreadyAdded($name);
      if (!$isAdded) {
        $this->load->model("Utility","utility");
        $datetime = $this->utility->getCurrentDate('Y/m/d h:i:s');
        $data = array('name'=>$name,'datetime'=>$datetime,'isBlocked'=>UNBLOCKED);
        $this->db->insert('citymaster', $data);
        $insert_Id = $this->db->insert_id();
        return $this->getCityById($insert_Id);
      }else{
        return null;
      }
  }
  /* Method to getCityById
   Created By: Manzz Baria
*/
public function getCityById($cityId){
  $this->db->select("Id,name,datetime");
  $this->db->from("citymaster");
  $this->db->where("Id",$cityId);
  $query = $this->db->get();
  return $this->displayCityById($query->result());
}

/* Method to displayCityById
     Created By: Manzz Baria
  */
  public function displayCityById($result){
    $Object = null;
    if($result != null){
      $this->load->model("Utility","utility");
      foreach ($result as $row) {
        $displayDate = $this->utility->timeAgoFormat($row->datetime);
        $Object = array(
          'Id'=>(int)$row->Id,
          'name'=>$row->name,
          'datetime'=>$displayDate
        );
        return $Object;
      }
    }
    return $Object;
  }

  /* Method to updateCity
     Created By: Manzz Baria
  */
  public function updateCity($name,$cityId){
    $this->load->model("Utility","utility");
    $datetime = $this->utility->getCurrentDate('Y/m/d h:i:s');
    $data = array(
      'name' => $name,
      'datetime'=>$datetime
      );
    $this->db->where('Id', $cityId);
    $this->db->update('citymaster', $data);
    return $this->getCityById($cityId);
  }


  /* Method to deleteCityId
     Created By: Manzz Baria
  */
  public function deleteCityId($cityId){
    $data = array(
                'isBlocked'=>BLOCKED);
    $this->db->where('Id', $cityId);
    $this->db->update('citymaster', $data);
    return true;
  }

  /* Method to getCities
   Created By: Manzz Baria
*/
public function getCities(){
  $this->db->select("Id,name,datetime");
  $this->db->from("citymaster");
  $this->db->where('isBlocked', UNBLOCKED);
  $query = $this->db->get();
  return $this->displayCities($query->result());
}

/* Method to displayCities
     Created By: Manzz Baria
  */
  public function displayCities($result){
    $carObject = null;
    if($result != null){
      $this->load->model("Utility","utility");
      foreach ($result as $row) {
        $displayDate = $this->utility->timeAgoFormat($row->datetime);
        $Object = array(
          'Id'=>(int)$row->Id,
          'name'=>$row->name,
          'datetime'=>$displayDate
        );
        $carObject[]=$Object;
      }
    }
    return $carObject;
  }


}
?>
