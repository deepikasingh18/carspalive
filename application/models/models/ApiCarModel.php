<?php if( ! defined("BASEPATH")) exit('No direct script access allowed');

class ApiCarModel extends CI_Model {

  public function _construct(){
    parent::_construct();
  }

/* Method to check isCarTypeAlreadyAdded
   Created By: Manzz Baria
*/
public function isCarTypeAlreadyAdded($name){
  $this->db->select('Id');
  $this->db->from('cartypemaster');
  $this->db->where("name",$name);
  $this->db->where("isBlocked",UNBLOCKED);
  $query = $this->db->get();
  if ($query->result() != null) {
    return true;
  }
  return false;
}

  /* Method to addCarType
     Created By: Manzz Baria
  */
  public function addCarType($name,$nameAR,$fileUrl,$language =LANGUAGE_ENGLISH){
    $isAdded = $this->isCarTypeAlreadyAdded($name);
      if (!$isAdded) {
        $this->load->model("Utility","utility");
        $datetime = $this->utility->getCurrentDate('Y/m/d h:i:s');
        $data = array('name'=>$name,'nameAR'=>$nameAR,'icon'=>$fileUrl,'datetime'=>$datetime,'isBlocked'=>UNBLOCKED);
        $this->db->insert('cartypemaster', $data);
        $insert_Id = $this->db->insert_id();
        return $this->getCarTypeById($insert_Id,$language);
      }else{
        return null;
      }
  }

  /* Method to getCarTypeByAppointmentId
     Created By: Manzz Baria
  */
  public function getCarTypeByAppointmentId($appointmentId,$language =LANGUAGE_ENGLISH){
    $this->db->select("cartypemaster.Id,cartypemaster.name,cartypemaster.nameAR,cartypemaster.icon,cartypemaster.datetime");
    $this->db->from("requestmaster");
      $this->db->join('cartypemaster','requestmaster.carTypeId = cartypemaster.Id','inner');
    $this->db->join('appoinmentmaster','requestmaster.Id = appoinmentmaster.requestId','inner');
    $this->db->where('appoinmentmaster.Id',$appointmentId);
    $query = $this->db->get();
    return $this->displayCarTypeById($query->result(),$language);
  }

  /* Method to getCarTypeById
   Created By: Manzz Baria
*/
public function getCarTypeById($carTypeId,$language =LANGUAGE_ENGLISH){
  $this->db->select("Id,name,nameAR,icon,datetime");
  $this->db->from("cartypemaster");
  $this->db->where("Id",$carTypeId);
  $query = $this->db->get();
  return $this->displayCarTypeById($query->result(),$language);
}

 /* Method to displayCarTypeById
     Created By: Manzz Baria
  */
  public function displayCarTypeById($result,$language = LANGUAGE_ENGLISH){
    $Object = null;
    if($result != null){
      $this->load->model("Utility","utility");
      foreach ($result as $row) {
        $displayDate = $this->utility->timeAgoFormat($row->datetime,$language);
        $name = $row->name;
        if ($language == LANGUAGE_ARABIC) {
          $name = $row->nameAR;
        }
        $Object = array(
          'Id'=>(int)$row->Id,
          'name'=>$name,
          'nameAR'=>$row->nameAR,
          'icon'=>IMAGE_URL.$row->icon,
          'iconThump'=>THUMB_URL.$row->icon,
          'datetime'=>$displayDate
        );
        return $Object;
      }
    }
    return $Object;
  }

  /*
     Method to update bid
     Created By: Manzz Baria
  */
  public function updateCarType($name,$nameAR,$carTypeId,$fileUrl,$language = LANGUAGE_ENGLISH){
    $this->load->model("Utility","utility");
    $datetime = $this->utility->getCurrentDate('Y/m/d h:i:s');
    $data = array(
      'name' => $name,
      'datetime'=>$datetime
      );
    $this->db->where('Id', $carTypeId);
    $this->db->update('cartypemaster', $data);
  if ($nameAR != "") {
      $data = array(
        'nameAR' => $nameAR
        );
      $this->db->where('Id', $carTypeId);
      $this->db->update('cartypemaster', $data);
    }
    if ($fileUrl != "") {
      $data = array(
        'icon' => $fileUrl
        );
      $this->db->where('Id', $carTypeId);
      $this->db->update('cartypemaster', $data);
    }
    return $this->getCarTypeById($carTypeId,$language);
  }


  /*
    Method to delete bids by bidId
     Created By: Manzz Baria
  */
  public function deleteCarTypeId($cartypeId){
    $data = array(
                'isBlocked'=>BLOCKED);
    $this->db->where('Id', $cartypeId);
    $this->db->update('cartypemaster', $data);
    return true;
  }

  /*
    Method to getCarTypeById
    Created By: Manzz Baria
  */
public function getCarTypes($language = LANGUAGE_ENGLISH){
  $this->db->select("Id,name,nameAR,icon,datetime");
  $this->db->from("cartypemaster");
  $this->db->where('isBlocked', UNBLOCKED);
  $query = $this->db->get();
  return $this->displayCarTypes($query->result(),$language);
}

/*
    Method to displayCarTypeById
     Created By: Manzz Baria
  */
  public function displayCarTypes($result,$language = LANGUAGE_ENGLISH){
    $carObject = null;
    if($result != null){
      $this->load->model("Utility","utility");
      foreach ($result as $row) {
        $displayDate = $this->utility->timeAgoFormat($row->datetime,$language);
          $name = $row->name;
          if ($language == LANGUAGE_ARABIC) {
            $name = $row->nameAR;
          }
        $Object = array(
          'Id'=>(int)$row->Id,
          'name'=>$name,
          'nameAR'=>$row->nameAR,
          'icon'=>IMAGE_URL.$row->icon,
          'iconThump'=>THUMB_URL.$row->icon,
          'datetime'=>$displayDate
        );
        $carObject[]=$Object;
      }
    }
    return $carObject;
  }


/****************      CarType End and Car Start         *****************/

/* Method to addCar
   Created By: Manzz Baria
*/
public function addCar($userId,$brand,$model,$plateNumber,$language = LANGUAGE_ENGLISH){
//  $isAdded = $this->isCarAlreadyAdded($userId,$plateNumber);
//    if (!$isAdded) {
      $this->load->model("Utility","utility");
      $datetime = $this->utility->getCurrentDate('Y/m/d h:i:s');
      $data = array(
        'userId'=>$userId,
        'brand'=>$brand,
        'model'=>$model,
        'plateNumber'=>$plateNumber,
        'datetime'=>$datetime);
      $this->db->insert('carmaster', $data);
      $insert_Id = $this->db->insert_id();
      return $this->getCarDetailsById($insert_Id,$language);
//    }else{
//      return null;
//    }
}

/* Method to check isCarAlreadyAdded
   Created By: Manzz Baria
*/
public function isCarAlreadyAdded($userId,$plateNumber){
  $this->db->select('Id');
  $this->db->from('carmaster');
  $this->db->where("userId",$userId);
  $this->db->where("plateNumber",$plateNumber);
  $query = $this->db->get();
  if ($query->result() != null) {
    return true;
  }
  return false;
}

/*
   Method to getCarDetailsByAppointmentId
   Created By: Manzz Baria
*/
public function getCarDetailsByAppointmentId($appointmentId){
  $this->db->select("carmaster.Id,carmaster.userId,carmaster.brand,carmaster.model,carmaster.plateNumber,carmaster.datetime");
  $this->db->from("carmaster");
  $this->db->join('requestmaster','carmaster.Id = requestmaster.carId','inner');
  $this->db->join('appoinmentmaster','requestmaster.Id = appoinmentmaster.requestId','inner');
  $this->db->where('appoinmentmaster.Id',$appointmentId);
  $query = $this->db->get();
  return $this->displayCarDetailsById($query->result());

}

/* Method to getCarTypeById
 Created By: Manzz Baria
*/
public function getCarDetailsById($carId){
    $this->db->select("Id,userId,brand,model,plateNumber,datetime");
    $this->db->from("carmaster");
    $this->db->where("Id",$carId);
    $query = $this->db->get();
    return $this->displayCarDetailsById($query->result());
}

/* Method to displayCarTypeById
     Created By: Manzz Baria
  */
  public function displayCarDetailsById($result){
    $Object = null;
    if($result != null){
      $this->load->model("Utility","utility");
      foreach ($result as $row) {
        $displayDate = $this->utility->timeAgoFormat($row->datetime);
      //  $carType = $this->getCarTypeById($row->carTypeId);
        $Object = array(
          'Id'=>(int)$row->Id,
          'brand'=>$row->brand,
          'model'=>$row->model,
          'plateNumber'=>$row->plateNumber,
          'datetime'=>$displayDate
        );
        return $Object;
      }
    }
    return $Object;
  }


  /* Method to update bid
     Created By: Manzz Baria
  */
  public function updateCar($carId,$userId,$brand,$model,$plateNumber){
    $isMy = $this->isMyCar($carId,$userId);
      if ($isMy) {
        $this->load->model("Utility","utility");
        $datetime = $this->utility->getCurrentDate('Y/m/d h:i:s');
          if ($brand != null && $brand != "") {
              $data = array(
                'brand' => $brand,
                'datetime' => $datetime
                );
              $this->db->where('Id', $carId);
              $this->db->update('carmaster', $data);
          }
          if ($model != null && $model != "") {
              $data = array(
                'model' => $model,
                'datetime' => $datetime
                );
              $this->db->where('Id', $carId);
              $this->db->update('carmaster', $data);
          }
          if ($plateNumber != null && $plateNumber != "") {
              $data = array(
                'plateNumber' => $plateNumber,
                'datetime' => $datetime
                );
              $this->db->where('Id', $carId);
              $this->db->update('carmaster', $data);
          }
          return $this->getCarDetailsById($carId);
      }else{
        return  null;
      }
  }

  /* Method to check isUserAlreadyRegister
  Created By: Manzz Baria
  */
  public function isMyCar($carId,$userId){
    $this->db->select('Id');
    $this->db->from('carmaster');
    $this->db->where('userId', $userId);
    $this->db->where('Id', $carId);
    $query = $this->db->get();
    if ($query->result() != null) {
      return true;
    }
    return false;
  }


    /* Method to deleteCarId
       Created By: Manzz Baria
    */
    public function deleteCarId($userId,$carId){
      $isMy = $this->isMyCar($carId,$userId);
      if ($isMy) {
        $this->db->where('Id', $carId);
        $this->db->delete('carmaster');
        if($this->db->affected_rows() > 0){
          return true;
        }else{
          return false;
        }
      }else{
        return false;
      }
    }


    /* Method to getCars
     Created By: Manzz Baria
  */
  public function getCars($userId){
    $this->db->select("Id,brand,model,plateNumber,datetime");
    $this->db->from("carmaster");
    $this->db->where('userId', $userId);
    $query = $this->db->get();
    return $this->displayCars($query->result());
  }

  /* Method to displayCarTypeById
       Created By: Manzz Baria
    */
    public function displayCars($result){
      $carObject = null;
      if($result != null){
        $this->load->model("Utility","utility");
        foreach ($result as $row) {
          $displayDate = $this->utility->timeAgoFormat($row->datetime);
          //$carType = $this->getCarTypeById($row->carTypeId);
          $Object = array(
            'Id'=>(int)$row->Id,
            'brand'=>$row->brand,
            'model'=>$row->model,
            'plateNumber'=>$row->plateNumber,
            'datetime'=>$displayDate
          );
          $carObject[]=$Object;
        }
      }
      return $carObject;
    }


}
?>
