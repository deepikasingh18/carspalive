<?php if( ! defined("BASEPATH")) exit('No direct script access allowed');

class ApiCurrencyModel extends CI_Model {

  public function _construct(){
    parent::_construct();
  }



      /*
       Method get getCurrencyFromCountryCode
       Created By: Manzz Baria
     */
     
     public function getCurrencyFromCountryCode($countryCode){
   
       $this->db->select('currencymaster.currency');
       $this->db->from('countrymaster');
       $this->db->join('currencymaster','countrymaster.Id = currencymaster.countryId','inner');
       $this->db->where('countrymaster.countryCode',$countryCode);
       $query = $this->db->get();
       $result = $query->result();
       $currency = null;
       if($result != null){
         foreach ($result as $row) {
           $currency = $row->currency;
         }
       }
       return $currency;
     }
     
     
         /*
       Method get getCurrencyARFromCountryCode
       Created By: Manzz Baria
     */
     
     public function getCurrencyARFromCountryCode($countryCode){
   
       $this->db->select('currencymaster.currencyAR');
       $this->db->from('countrymaster');
       $this->db->join('currencymaster','countrymaster.Id = currencymaster.countryId','inner');
       $this->db->where('countrymaster.countryCode',$countryCode);
       $query = $this->db->get();
       $result = $query->result();
       $currency = null;
       if($result != null){
         foreach ($result as $row) {
           $currency = $row->currencyAR;
         }
       }
       return $currency;
     }
     
     
         /*
       Method get getExchangeRateFromCountryCode
       Created By: Manzz Baria
     */
     
     public function getExchangeRateFromCountryCode($countryCode){
       $this->db->select('currencymaster.excahngeRate');
       $this->db->from('countrymaster');
       $this->db->join('currencymaster','countrymaster.Id = currencymaster.countryId','inner');
       $this->db->where('countrymaster.countryCode',$countryCode);
       $query = $this->db->get();
       $result = $query->result();
       $excahngeRate = null;
       if($result != null){
         foreach ($result as $row) {
           $excahngeRate = $row->excahngeRate;
         }
       }
       return $excahngeRate;
     }
     


      /*
       Method get getCurrencyIdOfAppointment
       Created By: Manzz Baria
     */
     public function getCurrencyIdOfAppointment($appointmentId){
       $this->db->select('currencyId');
       $this->db->from('billmaster');
       $this->db->where('appointmentId',$appointmentId);
       $query = $this->db->get();
       $result = $query->result();
       $currencyId = null;
       if($result != null){
         foreach ($result as $row) {
           $currencyId = (float)$row->currencyId;
         }
       }
       return $currencyId;
     }
     
      /*
       Method get getCurrencyIdOfAppointment
       Created By: Manzz Baria
     */
     public function getCurrencyIdFromCountryCode($countryCode){
        $this->db->select('currencymaster.Id');
       $this->db->from('countrymaster');
       $this->db->join('currencymaster','countrymaster.Id = currencymaster.countryId','inner');
       $this->db->where('countrymaster.countryCode',$countryCode);
       $query = $this->db->get();
       $result = $query->result();
       $currencyId = null;
       if($result != null){
         foreach ($result as $row) {
           $currencyId = $row->Id;
         }
       }
       return $currencyId;
     }
     
     
     
           /*
       Method get getCurrencyAndExchangeRateFromCountryCode
       Created By: Manzz Baria
     */
     public function getCurrencyAndExchangeRateFromCountryCode($countryCode){
     
       $currencyId = $this->getCurrencyIdFromCountryCode($countryCode);
       $currency = $this->getCurrencyFromCountryCode($countryCode);
       $currencyAR = $this->getCurrencyARFromCountryCode($countryCode);
       $exchangerate = $this->getExchangeRateFromCountryCode($countryCode);
       $data = array(
              'currencyId'=>$currencyId,
                          'currency'=>$currency,
                           'currencyAR'=>$currencyAR,
                          'excahngeRate'=>$exchangerate
                       );
       return $data;
     }
     
     
     /*
       Method get getExchangeRateOfCurrency
       Created By: Manzz Baria
     */
     public function getExchangeRateOfCurrency($currencyId){
       $this->db->select('excahngeRate');
       $this->db->from('currencymaster');
       $this->db->where('Id',$currencyId);
       $query = $this->db->get();
       $result = $query->result();
       $excahngeRate = null;
       if($result != null){
         foreach ($result as $row) {
           $excahngeRate = $row->excahngeRate;
         }
       }
       return $excahngeRate;
     }
     
     
              /*
       Method get getAmountCurrencyOfAppointment
       Created By: Manzz Baria
     */
     public function getAmountCurrencyOfAppointment($appointmentId,$language = LANGUAGE_ENGLISH){
       $this->db->select('currencymaster.currency,currencymaster.currencyAR');
       $this->db->from('billmaster');
       $this->db->join('currencymaster','billmaster.currencyId = currencymaster.Id','inner');
       $this->db->where('billmaster.appointmentId',$appointmentId);
       $query = $this->db->get();
       $result = $query->result();
       $currency = null;
       if($result != null){
         foreach ($result as $row) {
           $currency = $row->currency;
           
           if($language==LANGUAGE_ARABIC)
           {
            $currency = $row->currencyAR;
           }
         }
       }
       return $currency;
     }
     
     public function getAmountCurrencyAROfAppointment($appointmentId){
       $this->db->select('currencymaster.currencyAR');
       $this->db->from('billmaster');
       $this->db->join('currencymaster','billmaster.currencyId = currencymaster.Id','inner');
       $this->db->where('billmaster.appointmentId',$appointmentId);
       $query = $this->db->get();
       $result = $query->result();
       $currencyAr = null;
       if($result != null){
         foreach ($result as $row) {
           $currencyAr = $row-> currencyAR;
           
         }
       }
       return $currencyAr;
     }
          
     /*
    Method get getAmountByAppointmentCurrency
    Created By: Manzz Baria
     */
     public function getAmountByAppointmentCurrency($appointmentId,$amount){
       $currencyAmount = 0;
       $currencyId = $this->getCurrencyIdOfAppointment($appointmentId);
       $excahngeRate = $this->getExchangeRateOfCurrency($currencyId);
       $currencyAmount = $excahngeRate * $amount;
       //$currencyAmount= number_format((float)$currencyAmount, 2, '.', ''); 
      $currencyAmount = round($currencyAmount);
       return $currencyAmount;
     }
     
     
     


  /*
     Method to check isCountryAlreadyAdded
     Created By: deepika Merai
  */
  public function isCountryAlreadyAdded($Id){
    $this->db->select('Id');
    $this->db->from('currencymaster');
    $this->db->where("countryId",$Id);
    $this->db->where("isBlocked",UNBLOCKED);
    $query = $this->db->get();
    if ($query->result() != null) {
      return true;
    }
    return false;
  }

    /* Method to addContact
      Created By: deepika Merai
    */
    public function addContact($country,$currency,$currencyAR,$exchangerate,$language = LANGUAGE_ENGLISH){
      $isAdded = $this->isCountryAlreadyAdded($country);
        if (!$isAdded) 
    {
          $data = array(
                          'countryId'=>$country,
                          'currency'=>$currency,
              'currencyAR'=>$currencyAR,
                          'excahngeRate'=>$exchangerate,
                          'isBlocked'=>UNBLOCKED
                       );
          $this->db->insert('currencymaster', $data);
          $insert_Id = $this->db->insert_id();
          return $this->getCurrency($insert_Id,$language);
        }
    else{
          return null;
        }
    }


  /* Method to updateCurrency
      Created By: deepika Merai
    */

  public function updateCurrency($currrencyId,$countryId,$curerncy,$currencyAR,$exchangerate,$language = LANGUAGE_ENGLISH)
  {
  
      $data = array(
          'countryId' => $countryId,
          'currency'=>$curerncy,
      'currencyAR'=>$currencyAR,
      'excahngeRate'=>$exchangerate
        );
      $this->db->where('Id',$currrencyId);
      $this->db->update('currencymaster', $data);
      return $this->getCurrency($currrencyId,$language);
    }
  
  
    /* Method to getCurrency by Id
    Created By: deepika Merai
  */
  public function getCurrency($currencyId,$language = LANGUAGE_ENGLISH){
    $this->db->select("Id,countryId,currency,currencyAR,excahngeRate");
    $this->db->from("currencymaster");
  $this->db->where("Id",$currencyId);
  $query = $this->db->get();
  return $this->displayCurrencyById($query->result(),$language);
  }

  /* Method to displayCurrencyById
     Created By: deepika Merai
    */
    public function displayCurrencyById($result,$language= LANGUAGE_ENGLISH){
      $Object = null;
    $countryObject=null;
      if($result != null)
    {
        foreach ($result as $row) 
    {
      $countryObject=$this->getCountry($row->countryId);
      $currency = $row->currency;
      if ($language == LANGUAGE_ARABIC) {
      $currency = $row->currencyAR;
          }
    //echo $language;
          $Object = array(
            'Id'=>(int)$row->Id,
            'country'=>$countryObject,
            'currency'=>$currency,
       'currenyAR'=>$row->currencyAR,
            'exchangeRate'=>$row->excahngeRate
          );
           return $Object;
        }
      }
      return $Object;      

    }

   
 /* Method to deleteCountryId
     Created By: deepika Merai
    */   
    public function deleteCountryId($currencyId){
      $data = array('isBlocked'=>BLOCKED);
      $this->db->where('Id', $currencyId);
      $this->db->update('currencymaster', $data);
      return true;
    }

   

      /*
        Method to getCountry 
       Created By: deepika Merai
      */
    public function getCountry($countryId)
  {
      $this->db->select("Id,name,countryCode");
      $this->db->from("countrymaster");
    if($countryId==0)
    {
        $query = $this->db->get();
        return $this->displayCountry($query->result(),$countryId);
    }
    else
    {
      $this->db->where('Id',$countryId);
    $query = $this->db->get();
        return $this->displayCountry($query->result(),$countryId);
    }
    }

  /*
      Method to displayCountry
      Created By: deepika Merai
    */
    public function displayCountry($result,$countryId){
      $countryObject = null;
    $Object=null;
      if($result != null)
    {
        foreach ($result as $row) 
    {
          $Object = array(
            'Id'=>(int)$row->Id,
            'name'=>$row->name,
            'countryCode'=>$row->countryCode
          );
          $countryObject[]=$Object;
        }
      }
    if($countryId==0)
    {
        return $countryObject;
    }
    else
    {
      return $Object;
    } 
    }

/* Method to getAllCurrency
     Created By: deepika Merai
  */
  
public function getAllCurrency($language = LANGUAGE_ENGLISH)
{

  $this->db->select("currencymaster.Id as Cid,countryId,currency,currencyAR,excahngeRate,isBlocked, countrymaster.Id as CID");
  $this->db->from("currencymaster");
  $this->db->join('countrymaster','countrymaster.Id = currencymaster.countryId','inner');
  $this->db->where('isBlocked',UNBLOCKED);
  $query = $this->db->get();
  return $this->displayAllCurrency($query->result(),$language);
}


/* Method to displayAllCurrency
     Created By: deepika Merai
  */
  
public function displayAllCurrency($result,$language = LANGUAGE_ENGLISH)
{
    $ChannelObject = null;
    if($result != null)
  {
      foreach ($result as $row) 
      {
         $currency = $row->currency;
          if ($language == LANGUAGE_ARABIC) {
            $currency = $row->currencyAR;
          }
  
      $countryObject=$this->getCountry($row->countryId);
      $Object = array(
        'Id'=>(int)$row->Cid,
            'country'=>$countryObject,
            'currency'=>$currency,
      'currencyAR'=>$row->currencyAR,
            'exchangeRate'=>$row->excahngeRate
      );
      $ChannelObject[]=$Object;
      }
  }
    return $ChannelObject;
}
 
}
?>
