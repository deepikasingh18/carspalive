<?php if( ! defined("BASEPATH")) exit('No direct script access allowed');

class ApiVanModel extends CI_Model {

  public function _construct(){
    parent::_construct();
  }

  /* Method to getTotalVans
  Created By: Manzz Baria
  */
  public function getTotalVans(){
    $this->db->select("Id");
    $this->db->from("vanmaster");
    $this->db->where_not_in("status",VAN_STATUS_BLOCKED);
    $query = $this->db->count_all_results();
    return ceil($query);
  }

  /* Method to get All Van
     Created By: Manzz Baria
  */
  public function getAllVanByAvailableServices($serviceIds,$ownerId,$requestDate,$addLatitude,$addLongitude){
    $serviceIdArray = explode(',', $serviceIds);
    $size = count($serviceIdArray);
    $this->db->select("count(*) as totalServices,vanmaster.Id,vanmaster.name,vanmaster.plateno,vanmaster.cartype,vanmaster.type,vanmaster.ownerId,vanmaster.latitude,vanmaster.longitude,vanmaster.status,vanmaster.datetime");
    $this->db->from("vanmaster");
    $this->db->join("vanservicemaster","vanmaster.Id = vanservicemaster.vanId","inner");
    $this->db->where_not_in("vanmaster.status",VAN_STATUS_BLOCKED);
    $this->db->where_in('vanservicemaster.serviceId', $serviceIdArray);
    $this->db->where('vanmaster.ownerId', $ownerId);
    $this->db->group_by("vanservicemaster.vanId");
    $this->db->having('totalServices',  $size);
    $query = $this->db->get();
    $result = $query->result();
    $shiftList = [];
    if($result != null){
      $this->load->model("Utility","utility");
      $this->load->model("ApiVanOperatorModel","vOperatorModel");
      $this->load->model("ApiCarModel","CarModel");
      $formet = "Y-m-d";
      $currentDate = $this->utility->getCurrentDate($formet);
        foreach ($result as $row) {
          $displayDate = $this->utility->timeAgoFormat($row->datetime);
          if ($row->latitude != 0 && $addLatitude != 0 && $row->longitude != 0 && $addLongitude != 0) {
            $travelingTime = $this->utility->GetDrivingDistance($row->latitude,$row->longitude,$addLatitude,$addLongitude);
            $distance = $travelingTime['distance'];
            $travelTime = $travelingTime['time'];
            //echo $row->longitude;
          }else{
            $distance = ADD_MINT;
            $travelTime = ADD_MINT;
          }

           $isVanOperator = $this->vOperatorModel->isVanOperatorAvailable($row->Id);

          if ($isVanOperator) {
            # code...

             $cartype=$this->CarModel->getCarTypeById($row->cartype); 
            if ($currentDate == $requestDate) {
              $isAvailableVan = $this->vOperatorModel->isVanOperatorOnlineForVan($row->Id);
           //   $cartype=$this->CarModel->getCarTypeById($row->cartype);
              if ($isAvailableVan) {
                    $array = array('Id'=>(int)$row->Id,
                        'name'=>$row->name,
                        'plateno'=>$row->plateno,
                        'cartype'=>$cartype,
                        'type'=>(int)$row->type,
                        'status'=>(int)$row->status,
                        'latitude'=>(double)$row->latitude,
                        'longitude'=>(double)$row->longitude,
                        'distance'=>$distance,
                        'travelTime'=>$travelTime,
                        'datetime'=>$displayDate);
                    $shiftList[] = $array;
              }
          }else{
              $array = array('Id'=>(int)$row->Id,
                  'name'=>$row->name,
                  'plateno'=>$row->plateno,
                  'cartype'=>$cartype,
                  'type'=>(int)$row->type,
                  'status'=>(int)$row->status,
                  'latitude'=>(double)$row->latitude,
                  'longitude'=>(double)$row->longitude,
                  'distance'=>$distance,
                  'travelTime'=>$travelTime,
                  'datetime'=>$displayDate);
              $shiftList[] = $array;
          }
          }
          

        }
    }

  $sizeArray  = sizeof($shiftList);
    if ($sizeArray > 0) {
        usort($shiftList, array($this, "distance_compare"));
    }

    return $shiftList;
  }
  function distance_compare($a, $b)
  {
      //echo $a['dateTime'];
      $t1 = $a['travelTime'];
      $t2 = $b['travelTime'];
      return $t1 - $t2;
  }

  /* Method to getTotalPagesForVanList
     Created By: Manzz Baria
  */
  public function getTotalPagesForVanList(){
    $this->db->select("Id,name,type,ownerId,latitude,longitude,status,datetime");
    $this->db->from("vanmaster");
    $this->db->where_not_in("status",VAN_STATUS_BLOCKED);
    $query = $this->db->count_all_results();
    $result  = $query / 20;
    return ceil($result);
  }
  
    /* Method to getVanListBySupplierId
   Created By: Manzz Baria
  */
  public function getVanListBySupplierIdForServices($supplierId,$serviceIds){
  $serviceIdArray = explode(',', $serviceIds);
      $this->db->select("vanmaster.Id,vanmaster.name,vanmaster.plateno,vanmaster.cartype,vanmaster.type,vanmaster.ownerId,vanmaster.latitude,vanmaster.longitude,vanmaster.status,vanmaster.datetime");
      $this->db->from("vanmaster");
       $this->db->join("vanservicemaster","vanmaster.Id = vanservicemaster.vanId","inner");
      $this->db->where_not_in("vanmaster.status",VAN_STATUS_BLOCKED);
      $this->db->where_in('vanservicemaster.serviceId', $serviceIdArray);
      $this->db->where("vanmaster.ownerId",$supplierId);
      $this->db->order_by("vanmaster.Id","DESC");
      $this->db->distinct();
      $query = $this->db->get();
      return $this->displayVanList($query->result());
  }


  public function getSupplierByServiceId($serviceId,$servicedetailId){
   
     $this->db->select("vanownermaster.Id,vanownermaster.name");
      $this->db->from("vanownermaster");
        $this->db->join("vanmaster","vanmaster.OwnerId = vanownermaster.Id","inner");
       $this->db->join("vanservicemaster","vanmaster.Id = vanservicemaster.vanId","inner");
      $this->db->where_not_in("vanmaster.status",VAN_STATUS_BLOCKED);
      $this->db->where_in('vanservicemaster.serviceId', $serviceId);
      $this->db->distinct();
      $query = $this->db->get();
      //print_r($query->result());
     return $this->displaySupplierList($query->result(),$servicedetailId);
  }


public function displaySupplierList($result,$servicedetailId){
   
    $Object = null;
    $VanObject=null;
    $servicecostdetail=null;
    if($result != null){
      
    
      
      foreach ($result as $row) {
          $servicecostdetail=$this->getServiceCostId($servicedetailId,$row->Id);
        $Object = array(
          'Id'=>(int)$row->Id,
          'name'=>$row->name,
          'servicecostdetail'=>$servicecostdetail
        );
         $VanObject[]=$Object;
      }
    }
    return $VanObject;
  }


  public function searchVan($name){
      $this->db->select("Id,name,plateno,cartype,type,ownerId,latitude,longitude,status,datetime");
      $this->db->from("vanmaster");
      $this->db->where_not_in("status",VAN_STATUS_BLOCKED);
      $this->db->like('name', $name);
      $query = $this->db->get();
      return $this->displayVanList($query->result());
  }

  /* Method to getVanListByServices
   Created By: Manzz Baria
  */
  public function getVanListByServices($serviceIds){
  $serviceIdArray = explode(',', $serviceIds);
      $this->db->select("vanmaster.Id,vanmaster.name,vanmaster.plateno,vanmaster.cartype,vanmaster.type,vanmaster.ownerId,vanmaster.latitude,vanmaster.longitude,vanmaster.status,vanmaster.datetime");
      $this->db->from("vanmaster");
       $this->db->join("vanservicemaster","vanmaster.Id = vanservicemaster.vanId","inner");
      $this->db->where_not_in("vanmaster.status",VAN_STATUS_BLOCKED);
      $this->db->where_in('vanservicemaster.serviceId', $serviceIdArray);
      $this->db->order_by("vanmaster.Id","DESC");
      $this->db->distinct();
      $query = $this->db->get();
      return $this->displayVanList($query->result());
  }
  

  /* Method to getVanList
   Created By: Manzz Baria
  */
  public function getVanList($pageIndex){
      $this->db->select("Id,name,plateno,cartype,type,ownerId,latitude,longitude,status,datetime");
      $this->db->from("vanmaster");
      $this->db->where_not_in("status",VAN_STATUS_BLOCKED);
      $this->db->order_by("Id","DESC");
      $pageNo = "00";
      if ($pageIndex > 0) {
        $pageIndex = $pageIndex * 2;
        $pageNo = $pageIndex.'0';
      }
      $this->db->limit(20,$pageNo);
      $query = $this->db->get();
      return $this->displayVanList($query->result());
  }

  /* Method to displayVanList
     Created By: Manzz Baria
  */
  public function displayVanList($result){
    $VanObject = null;
    $Object = null;
    if($result != null){
      $this->load->model("Utility","utility");
      $this->load->model("ApiVanOperatorModel","vanOperatorModel");
       $this->load->model("ApiCarModel","CarModel");
      foreach ($result as $row) {
        $displayDate = $this->utility->timeAgoFormat($row->datetime);
        $ownerObject = $this->getVanOwnerByVanId($row->ownerId);
        $servicesObject = $this->getVanServicesByVanId($row->Id);
        $operatorObject = $this->vanOperatorModel->getVanOperatorsByVanId($row->Id);
        $cartype=$this->CarModel->getCarTypeById($row->cartype);
        $Object = array(
          'Id'=>(int)$row->Id,
          'name'=>$row->name,
          'plateno'=>$row->plateno,
          'cartype'=>$cartype,
          'type'=>(int)$row->type,
          'status'=>(int)$row->status,
          'latitude'=>(double)$row->latitude,
          'longitude'=>(double)$row->longitude,
          'datetime'=>$displayDate,
          'owner'=>$ownerObject,
          'services'=>$servicesObject,
          'operator'=>$operatorObject
        );
         $VanObject[]=$Object;
      }
    }
    return $VanObject;
  }

  /* Method to getProviderContactsByProviderContactId
   Created By: Manzz Baria
  */
  public function getProviderContactsByProviderContactId($contactId){
      $this->db->select("Id,vanOwnerId,fullName,email,title,countryCode,mobileNumber,countryCodeTwo,mobileNumberTwo,isPrimary");
      $this->db->from("vanprovidercontact");
      $this->db->where("Id",$contactId);
      $query = $this->db->get();
      return $this->displayProviderContactsByProviderId($query->result(),false);
  }

  /* Method to getProviderContactsByProviderId
   Created By: Manzz Baria
  */
  public function getProviderContactsByProviderId($vanOwnerId){
      $this->db->select("Id,vanOwnerId,fullName,email,title,countryCode,mobileNumber,countryCodeTwo,mobileNumberTwo,isPrimary");
      $this->db->from("vanprovidercontact");
      $this->db->where("isBlocked",UNBLOCKED);
      $this->db->where("vanOwnerId",$vanOwnerId);
      $query = $this->db->get();
      return $this->displayProviderContactsByProviderId($query->result(),true);
  }
  /* Method to displayAddresses
     Created By: Manzz Baria
  */
  public function displayProviderContactsByProviderId($result,$isArray){
    $ownerObject = null;
    $Object = null;
    if($result != null){
      foreach ($result as $row) {
        $Object = array(
          'Id'=>$row->Id,
          'vanOwnerId'=>$row->vanOwnerId,
          'fullName'=>$row->fullName,
          'citemaily'=>$row->email,
          'title'=>$row->title,
          'countryCode'=>$row->countryCode,
          'mobileNumber'=>$row->mobileNumber,
          'countryCodeTwo'=>$row->countryCodeTwo,
          'mobileNumberTwo'=>$row->mobileNumberTwo,
          'isPrimary'=>$row->isPrimary
        );
        $ownerObject[]=$Object;
      }
    }
    if ($isArray) {
      return $ownerObject;
    }else{
      return $Object;
    }
  }

  /* Method to getOwnerListByShift
   Created By: Manzz Baria
  */
  public function getOwnerListByShift($shiftId){
  $this->db->select("Id,name,email,city,country,countryCode,mobileNumber,countryCodeTwo,mobileNumberTwo,salesPerson,maxdistance,address,icon");
  $this->db->from("vanownermaster");
  $this->db->where("shiftId",$shiftId);
  $this->db->where("isBlocked",UNBLOCKED);
  $query = $this->db->get();
  return $this->displayOwnerList($query->result());
  }

  /* Method to getOwnerList
   Created By: Manzz Baria
  */
  public function getOwnerList($suppplierId){
  $this->db->select("Id,name,email,city,country,countryCode,mobileNumber,countryCodeTwo,mobileNumberTwo,salesPerson,maxdistance,address,icon");
  $this->db->from("vanownermaster");
  $this->db->where("isBlocked",UNBLOCKED);
  if($suppplierId > 0)
  {
      $this->db->where("Id",$suppplierId);
  }
  $query = $this->db->get();
  return $this->displayOwnerList($query->result());
  }

  public function getOperatorBySupplier($suppplierId){
  $this->db->select("usermaster.Id,usermaster.fullName,usermaster.vanId");
  $this->db->from("usermaster");
  $this->db->join('vanmaster','usermaster.vanId=vanmaster.Id');
  $this->db->join('vanownermaster','vanownermaster.Id=vanmaster.ownerId');
  $this->db->where("vanownermaster.Id",$suppplierId);
  $this->db->where("isBlocked",UNBLOCKED);
  $this->db->where("userType",USER_TYPE_OPERATOR);
  $query = $this->db->get();
  //print_r($query->result());
  return $this->displayOperatorBySupplier($query->result());
  }
  
  public function displayOperatorBySupplier($result)
  {
        $operatorObject = null;
        $Object=null;
        if($result != null)
        {
            foreach ($result as $row) 
            {
              $Object = array(
                'Id'=>(int)$row->Id,
                'name'=>$row->fullName,
                'vanId'=>$row->vanId
              );
              $operatorObject[]=$Object;
            }
        }
        return $operatorObject;
  }

  
  public function searchSuppliers($name)
  {
      $this->db->select("Id,name,email,city,country,countryCode,mobileNumber,countryCodeTwo,mobileNumberTwo,salesPerson,address,maxdistance,icon");
      $this->db->from("vanownermaster");
      $this->db->where("isBlocked",UNBLOCKED);
      $this->db->like("name",$name);
      $query = $this->db->get();
      return $this->displayOwnerList($query->result());
  } 
  
  public function insertOperatorReceivedCost($supplierId,$Operatordataarrays)
  {
      $this->load->model("Utility","utility");  
      $datetime = $this->utility->getCurrentDate('Y/m/d h:i:s');
      
        foreach($Operatordataarrays as $Operatordataarray)
         {
              $operatorId=$Operatordataarray->operatorId;
              $operatorCost=$Operatordataarray->operatorCost;
              $operatorDescription=$Operatordataarray->operatorDescription;
              $operatorPaymentRecivedDate=$Operatordataarray->operatorPaymentRecivedDate;
          
                $datetime = $this->utility->getCurrentDate('Y/m/d h:i:s');
                 $data = array('supplierId'=>$supplierId,'operatorId'=>$operatorId,'description'=>$operatorDescription,'paymentDate'=>$operatorPaymentRecivedDate, 'receivedAmount'=>$operatorCost ,'dateTime'=>$datetime);
                 $this->db->insert('receivedmargin', $data);
          }
        return true;
  }
  
  /* Method to displayAddresses
     Created By: Manzz Baria
  */
  public function displayOwnerList($result){
    $ownerObject = null;
    if($result != null){
      foreach ($result as $row) {
        $providerContacts = $this->getProviderContactsByProviderId($row->Id);
        $Object = array(
          'Id'=>(int)$row->Id,
          'name'=>$row->name,
          'email'=>$row->email,
          'city'=>$row->city,
          'country'=>$row->country,
          'countryCode'=>$row->countryCode,
          'mobileNumber'=>$row->mobileNumber,
          'countryCodeTwo'=>$row->countryCodeTwo,
          'mobileNumberTwo'=>$row->mobileNumberTwo,
          'salesPerson'=>$row->salesPerson,
          'maxdistance'=>$row->maxdistance,
          'address'=>$row->address,
      'icon'=>IMAGE_URL.$row->icon,
           'iconThump'=>THUMB_URL.$row->icon,
          'contactPersons'=>$providerContacts
        );
        $ownerObject[]=$Object;
      }
    }
    return $ownerObject;
  }

  /* Method to getVanLocationById
   Created By: Manzz Baria
  */
  public function getVanLocationById($vanId){
      $this->db->select("Id,name,plateno,cartype,type,ownerId,latitude,longitude,status,datetime");
      $this->db->from("vanmaster");
      $this->db->where("Id",$vanId);
      $query = $this->db->get();
      return $this->displayVanLocationById($query->result());
  }

  /* Method to displayVanLocationById
     Created By: Manzz Baria
  */
  public function displayVanLocationById($result){
    $Object = null;
    if($result != null){
      $this->load->model("Utility","utility");
      $this->load->model("ApiVanOperatorModel","vanOperatorModel");
      $this->load->model("ApiCarModel","CarModel");
      foreach ($result as $row) {
        $displayDate = $this->utility->timeAgoFormat($row->datetime);
        $ownerObject = $this->getVanOwnerByVanId($row->ownerId);
        $operatorObject = $this->vanOperatorModel->getVanOperatorsByVanId($row->Id);
         $cartype=$this->CarModel->getCarTypeById($row->cartype);
        $Object = array(
          'Id'=>(int)$row->Id,
          'name'=>$row->name,
          'plateno'=>$row->plateno,
          'cartype'=>$cartype,
          'type'=>(int)$row->type,
          'status'=>(int)$row->status,
          'latitude'=>(double)$row->latitude,
          'longitude'=>(double)$row->longitude,
          'datetime'=>$displayDate,
          'operator'=>$operatorObject,
          'owner'=>$ownerObject
        );
        return $Object;
      }
    }
    return $Object;
  }

  /* Method to getVanDetailsById
   Created By: Manzz Baria
  */
  public function getVanDetailsById($vanId){
      $this->db->select("Id,name,plateno,cartype,type,ownerId,latitude,longitude,status,datetime");
      $this->db->from("vanmaster");
      $this->db->where("Id",$vanId);
      $query = $this->db->get();
      return $this->displayVanDetailsById($query->result());
  }
  


public function getVanServicesByVanandServiceId($vanId,$serviceId){
      $this->db->select("vanservicemaster.Id,vanservicemaster.vanId,vanservicemaster.serviceId");
      $this->db->from("vanservicemaster");
      $this->db->join('servicemaster','vanservicemaster.serviceId = servicemaster.Id','inner');
      $this->db->where("vanservicemaster.vanId",$vanId);
      if($serviceId>0)
      {
          $this->db->where("vanservicemaster.serviceId",$serviceId);
       }
       $this->db->where('servicemaster.isBlocked',UNBLOCKED);
      $query = $this->db->get();
      return $this->displayVanServicesByVanId($query->result());
  }

 public function getVanByvanId($vanId,$serviceId){
      $this->db->select("Id,ownerId");
      $this->db->from("vanmaster");
      $this->db->where("Id",$vanId);
      $query = $this->db->get();
      return $this->displayVanByVanId($query->result(),$serviceId);
  }


  public function getmaxdistanceById($vanId){
  //  echo $vanId;
    $maxDistance=null;
      $this->db->select("maxdistance");
      $this->db->from("vanownermaster");
       $this->db->join("vanmaster",'vanmaster.ownerId=vanownermaster.Id');
      $this->db->where("vanmaster.Id",$vanId);
      $query = $this->db->get();
  //    print_r($query->result() );
      foreach ($query->result() as $row) {
         $maxDistance=$row->maxdistance;
        }
        return $maxDistance;
   //   return $this->displayVanByVanId($query->result(),$serviceId);
  }

  public function getmaxdistanceByAdminId($SupplierId){
  //  echo $vanId;
    $maxDistance=null;
      $this->db->select("maxdistance");
      $this->db->from("vanownermaster");
       //$this->db->join("vanmaster",'vanmaster.ownerId=vanownermaster.Id');
      $this->db->where("vanownermaster.Id",$SupplierId);
      $query = $this->db->get();
      //print_r($query->result() );
      foreach ($query->result() as $row) {
         $maxDistance=$row->maxdistance;
        }
        return $maxDistance;
   //   return $this->displayVanByVanId($query->result(),$serviceId);
  }


  

  public function displayVanByVanId($result,$serviceId){
    $Object = null;
    if($result != null){
    //  $this->load->model("Utility","utility");
    //  $this->load->model("ApiVanOperatorModel","vanOperatorModel");
    //  $this->load->model("ApiCarModel","CarModel");
      foreach ($result as $row) {
        //$displayDate = $this->utility->timeAgoFormat($row->datetime);
        $ownerObject = $this->getVanOwnerByVanId($row->ownerId);
        $servicesObject = $this->getVanServicesByVanandServiceId($row->Id,$serviceId);
      //  $operatorObject = $this->vanOperatorModel->getVanOperatorsByVanId($row->Id);
       // $cartype=$this->CarModel->getCarTypeById($row->cartype);
        $Object = array(
          
          'owner'=>$ownerObject,
          'servicesObject'=>$servicesObject
        //  'services'=>$servicesObject,
         // 'operator'=>$operatorObject
        );
        return $Object;
      }
    }
    return $Object;
  }


   public function insertcostmargin($servicedetailId,$marginDetail){
     $insert_Id=null;
          $myArray = explode(',', $marginDetail);
          for ($i=0; $i< sizeof($myArray); $i++) {
            $myAmount = explode(':', $myArray[$i]);
          //  (CarTypeId:CategoryId:Amount:Duration)
                 $insert_Id= $this->saveservicecost($servicedetailId,$myAmount[0],$myAmount[1],$myAmount[2],$myAmount[3]);
          }
          return  $insert_Id;
    }


  public function saveservicecost($serviceDetailId,$supplierIds,$type,$cost,$servicecostId){
      $insert_Id=0;
          $isAdded = $this->isServicecostAlreadySaved($serviceDetailId,$supplierIds,$type,$cost);
          if (!$isAdded) {
         //   $this->load->model("Utility","utility");
         //   $datetime = $this->utility->getCurrentDate('Y/m/d h:i:s');
            $data = array(
                            'servicedetailId'=>$serviceDetailId,
                            'supplierIds'=>$supplierIds,
                            'type'=>$type,
                            'cost'=>$cost,
                          );

                  if($servicecostId==0)
                  {
                    $this->db->insert('servicecost', $data);
                    $insert_Id = $this->db->insert_id();
                    // return $insert_Id;
                  }
                  else
                  {
                //    echo $servicecostId;
                        if ($type != "") 
                        {
                            $data = array(
                              'type' => $type
                            );
                            $this->db->where('Id',$servicecostId);
                           $this->db->update('servicecost', $data);
                            $insert_Id=$servicecostId;
                          //  return $insert_Id;
                       }

                       if ($cost != "") 
                        {
                            $data = array(
                              'cost' => $cost
                            );
                            $this->db->where('Id',$servicecostId);
                           $this->db->update('servicecost', $data);
                           $insert_Id=$servicecostId;
                          

                       }
                     
                  }
               return $insert_Id;

          }else{
              return $servicecostId;
          }
    }

     public function insertReceivedAmount($supplierId,$amount)
     {
              $data = array(
                            'supplierId'=>$supplierId,
                            'receivedAmount'=>$amount
                          );
              $this->db->insert('receivedmargin', $data);
                    $insert_Id = $this->db->insert_id();

               if($insert_Id>0)
                {
                   return $insert_Id;
                }
                else
                {
                  return null;
                }

    }


    public function getServiceCostId($serviceDetailId,$supplierId){
     // echo $serviceDetailId;
      $this->db->select("Id,serviceDetailId,supplierIds,cost,type");
      $this->db->from("servicecost");
      $this->db->where("serviceDetailId",$serviceDetailId);
      $this->db->where("supplierIds",$supplierId);
    //   $this->db->where("serviceDetailId",$serviceDetailId);
      $query = $this->db->get();
    //  print_r($query->result());
      return $this->displayServiceCostId($query->result());
    }

    public function displayServiceCostId($result){
        $Object = null;
         $ServicecostObject = null;
        if($result != null){
          foreach ($result as $row) {
            $Object = array(
              'Id'=>(int)$row->Id,
              'serviceDetailId'=>$row->serviceDetailId,
              'supplierIds'=>$row->supplierIds,
              'type'=>$row->type,
              'cost'=>$row->cost,
            );
           //$ServicecostObject[]=$Object;
          }
           
        }
        return $Object;
      }

     public function isServicecostAlreadySaved($serviceDetailId,$supplierIds,$type,$cost){
      $this->db->select('Id');
      $this->db->from('servicecost');
      $this->db->where("serviceDetailId",$serviceDetailId);
      $this->db->where("supplierIds",$supplierIds);
      $this->db->where("type",$type);
      $this->db->where("cost",$cost);
      $query = $this->db->get();
      if ($query->result() != null) {
        return true;
      }
      return false;
    }

  public function getVanDetailsByServiceId($serviceId){
  //echo ($serviceId);
      $this->db->select("vanmaster.Id,vanmaster.name,vanmaster.plateno,vanmaster.cartype,vanmaster.type,vanmaster.ownerId,vanmaster.latitude,vanmaster.longitude,vanmaster.status,vanmaster.datetime");
      $this->db->from("vanmaster");
      $this->db->join('vanservicemaster','vanservicemaster.vanId=vanmaster.Id');
      $this->db->where("vanservicemaster.serviceId",$serviceId);
      $query = $this->db->get();
      return $this->displayVanDetailsByserviceId($query->result());
  }

  /* Method to displayVanDetailsById
     Created By: Manzz Baria
  */
  public function displayVanDetailsById($result){
    $Object = null;
    if($result != null){
      $this->load->model("Utility","utility");
      $this->load->model("ApiVanOperatorModel","vanOperatorModel");
      $this->load->model("ApiCarModel","CarModel");
      foreach ($result as $row) {
        $displayDate = $this->utility->timeAgoFormat($row->datetime);
        $ownerObject = $this->getVanOwnerByVanId($row->ownerId);
        $servicesObject = $this->getVanServicesByVanId($row->Id);
        $operatorObject = $this->vanOperatorModel->getVanOperatorsByVanId($row->Id);
        $cartype=$this->CarModel->getCarTypeById($row->cartype);
        $Object = array(
          'Id'=>(int)$row->Id,
          'name'=>$row->name,
          'plateno'=>$row->plateno,
          'cartype'=>$cartype,
          'type'=>(int)$row->type,
          'status'=>(int)$row->status,
          'latitude'=>(double)$row->latitude,
          'longitude'=>(double)$row->longitude,
          'datetime'=>$displayDate,
          'owner'=>$ownerObject,
          'services'=>$servicesObject,
          'operator'=>$operatorObject
        );
        return $Object;
      }
    }
    return $Object;
  }
  
    public function displayVanDetailsByserviceId($result){
    $Object = null;
    $disObject = null;
    if($result != null){
     
      foreach ($result as $row) {
        
        $Object = array(
          'Id'=>(int)$row->Id,
          'latitude'=>(double)$row->latitude,
          'longitude'=>(double)$row->longitude
        );
        $disObject[]=$Object;
      }
    }
    return $disObject;
  }

  /* Method to getVanOwnerByVanId
   Created By: Manzz Baria
  */
  public function getVanOwnerByVanId($ownerId){
      $this->db->select("Id,name,email,city,country,countryCode,mobileNumber,countryCodeTwo,mobileNumberTwo,salesPerson,maxdistance,address,icon");
      $this->db->from("vanownermaster");
      $this->db->where("Id",$ownerId);
      $query = $this->db->get();
      return $this->displayVanOwnerByVanId($query->result());
  }

  /* Method to displayVanOwnerByVanId
     Created By: Manzz Baria
  */
  public function displayVanOwnerByVanId($result){
    $Object = null;
    if($result != null){
      foreach ($result as $row) {
        $providerContacts = $this->getProviderContactsByProviderId($row->Id);
        $Object = array(
          'Id'=>(int)$row->Id,
          'name'=>$row->name,
          'email'=>$row->email,
          'city'=>$row->city,
          'country'=>$row->country,
          'countryCode'=>$row->countryCode,
          'mobileNumber'=>$row->mobileNumber,
          'countryCodeTwo'=>$row->countryCodeTwo,
          'mobileNumberTwo'=>$row->mobileNumberTwo,
          'salesPerson'=>$row->salesPerson,
          'maxdistance'=>$row->maxdistance,
          'address'=>$row->address,
        'icon'=>IMAGE_URL.$row->icon,
           'iconThump'=>THUMB_URL.$row->icon,
          'contactPersons'=>$providerContacts
        );
      }
    }
    return $Object;
  }

  /* Method to getVanServicesByVanId
   Created By: Manzz Baria
  */
  public function getVanServicesByVanId($vanId){
      $this->db->select("vanservicemaster.Id,vanservicemaster.vanId,vanservicemaster.serviceId");
      $this->db->from("vanservicemaster");
      $this->db->join('servicemaster','vanservicemaster.serviceId = servicemaster.Id','inner');
      $this->db->where("vanservicemaster.vanId",$vanId);
       $this->db->where('servicemaster.isBlocked',UNBLOCKED);
      $query = $this->db->get();
      return $this->displayVanServicesByVanId($query->result());
  }
  /* Method to displayVanServicesByVanId
     Created By: Manzz Baria
  */
  public function displayVanServicesByVanId($result){
    $VanServicesObject = null;
    if($result != null){
      $this->load->model("ApiServiceModel","serviceModel");
      foreach ($result as $row) {
        $Object =  $this->serviceModel->getServiceSmallDetailById($row->serviceId);
        $VanServicesObject[]=$Object;
      }
    }
    return $VanServicesObject;
  }


  /* Method to check isVanAlreadyAdded
     Created By: Manzz Baria
  */
  public function isVanAlreadyAdded($vanName){
    $this->db->select('Id');
    $this->db->from('vanmaster');
    $this->db->where_not_in("status",VAN_STATUS_BLOCKED);
    $this->db->where("name",$vanName);
    $query = $this->db->get();
    if ($query->result() != null) {
      return true;
    }
    return false;
  }

  /* Method to check isMyVan
     Created By: Manzz Baria
  */
  public function isMyVan($vanId,$name){
    $this->db->select('Id');
    $this->db->from('vanmaster');
    $this->db->where_not_in("status",VAN_STATUS_BLOCKED);
    $this->db->where("name",$name);
    $this->db->where("Id",$vanId);
    $query = $this->db->get();
    if ($query->result() != null) {
      return true;
    }
    return false;
  }

  /* Method to check isVanAlreadyAddedORItsOwnVan
     Created By: Manzz Baria
  */
  public function isVanAlreadyAddedORItsOwnVan($vanId,$name){
    $isMy = $this->isMyVan($vanId,$name);
    if (!$isMy) {
      return $this->isVanAlreadyAdded($name);
    }else{
      return false;
    }
  }


  /* Method to updatedVan
     Created By: Manzz Baria
  */
  public function updatedVan($vanId,$vanName,$plate,$carType,$type,$ownerId){
    // update owner
  /*  if ($type != 0) {
      if ($type == VAN_TYPE_THIRDPARTY) {
          if ($ownerId == 0) {
              $ownerId = $this->saveOwner($name,$email,$countryCode,$mobileNumber,$address);
            }
      }
      $data = array(
        'ownerId' => $ownerId,
        'type' => $type
      );
      $this->db->where('Id', $vanId);
      $this->db->update('vanmaster', $data);
    }
  */
    if ($ownerId != "" && $ownerId >0 ) {
        $data = array(
          'ownerId' => $ownerId,
          'type' => $type
        );
        $this->db->where('Id', $vanId);
        $this->db->update('vanmaster', $data);
    }
    //update Van
    if ($vanName != "") {
      $data = array(
        'name' => $vanName
      );
      $this->db->where('Id', $vanId);
      $this->db->update('vanmaster', $data);
    }

    if ($plate != "") {
      $data = array(
        'plateno' => $plate
      );
      $this->db->where('Id', $vanId);
      $this->db->update('vanmaster', $data);
    }

       if ($carType != "") {
      $data = array(
        'cartype' => $carType
      );
      $this->db->where('Id', $vanId);
      $this->db->update('vanmaster', $data);
    }

    return $this->getVanDetailsById($vanId);
  }

  /* Method to updatedProvider
     Created By: Manzz Baria
  */
 public function updatedProvider($ownerId,$name,$email,$password,$city,$country,$countryCode,$mobileNumber,$address,$salesPerson,$maxDistance,$countryCodeTwo,$mobileNumberTwo,$fileUrl){

    if ($name != "") {
      $data = array(
        'name' => $name
      );
      $this->db->where('Id', $ownerId);
      $this->db->update('vanownermaster', $data);
    }
    if ($email != "") {
      $data = array(
        'email' => $email
      );
      $this->db->where('Id', $ownerId);
      $this->db->update('vanownermaster', $data);
    }
    if ($countryCode != "") {
      $data = array(
        'countryCode' => $countryCode
      );
      $this->db->where('Id', $ownerId);
      $this->db->update('vanownermaster', $data);
    }
    if ($mobileNumber != "") {
      $data = array(
        'mobileNumber' => $mobileNumber
      );
      $this->db->where('Id', $ownerId);
      $this->db->update('vanownermaster', $data);
    }
    if ($address != "") {
      $data = array(
        'address' => $address
      );
      $this->db->where('Id', $ownerId);
      $this->db->update('vanownermaster', $data);
    }
    if ($city != "") {
      $data = array(
        'city' => $city
      );
      $this->db->where('Id', $ownerId);
      $this->db->update('vanownermaster', $data);
    }
    if ($country != "") {
      $data = array(
        'country' => $country
      );
      $this->db->where('Id', $ownerId);
      $this->db->update('vanownermaster', $data);
    }
    if ($maxDistance != "") {
      $data = array(
        'maxDistance' => $maxDistance
      );
      $this->db->where('Id', $ownerId);
      $this->db->update('vanownermaster', $data);
    }
    if ($salesPerson != "") {
      $data = array(
        'salesPerson' => $salesPerson
      );
      $this->db->where('Id', $ownerId);
      $this->db->update('vanownermaster', $data);
    }
    if ($countryCodeTwo != "") {
      $data = array(
        'countryCodeTwo' => $countryCodeTwo
      );
      $this->db->where('Id', $ownerId);
      $this->db->update('vanownermaster', $data);
    }
    if ($mobileNumberTwo != "") {
      $data = array(
        'mobileNumberTwo' => $mobileNumberTwo
      );
      $this->db->where('Id', $ownerId);
      $this->db->update('vanownermaster', $data);
    }
   if ($fileUrl != "") {
        $data = array(
          'icon' => $fileUrl
          );
    $this->db->where('Id', $ownerId);
    $this->db->update('vanownermaster', $data);
  }
  
   if ($password != "") {
      $this->load->model("Utility","utility");
     $securePassword = $this->utility->encrypt($password);
      $data = array(
        'password' => $securePassword
      );
      $this->db->where('Id', $ownerId);
      $this->db->update('vanownermaster', $data);
    } 
    return $this->getVanOwnerByVanId($ownerId);
  }


  /* Method to save van owner
     Created By: Manzz Baria
  */

  //Unused
  public function saveOwner($name,$email,$countryCode,$mobileNumber,$address){
    $data = array(
                'name'=>$name,
                'email'=>$email,
                'countryCode'=>$countryCode,
                'mobileNumber'=>$mobileNumber,
                'address'=>$address);
    $this->db->insert('vanownermaster', $data);
    $ownerId = $this->db->insert_id();
    return $ownerId;
  }

  /* Method to save van owner
     Created By: Manzz Baria
  */

  public function addOwner($name,$email,$password,$city,$country,$countryCode,$mobileNumber,$countryCodeTwo,$mobileNumberTwo,$salesPerson,$maxDistance,$address,$fileUrl){
   $this->load->model("Utility","utility");
   $securePassword = $this->utility->encrypt($password);
    $data = array(
                'name'=>$name,
                'email'=>$email,
                'password'=>$securePassword,
                'city'=>$city,
                'country'=>$country,
                'countryCode'=>$countryCode,
                'mobileNumber'=>$mobileNumber,
                'countryCodeTwo'=>$countryCodeTwo,
                'mobileNumberTwo'=>$mobileNumberTwo,
                'salesPerson'=>$salesPerson,
                'maxdistance'=>$maxDistance,
                'address'=>$address,
        'icon'=>$fileUrl);
    $this->db->insert('vanownermaster', $data);
    $ownerId = $this->db->insert_id();
    return $this->getVanOwnerByVanId($ownerId);
  }

  /* Method to addProviderContact
     Created By: Manzz Baria
  */
  public function addProviderContact($vanOwnerId,$fullName,$email,$title,$countryCode,$mobileNumber,$countryCodeTwo,$mobileNumberTwo,$isPrimary){
    $data = array(
                'vanOwnerId'=>$vanOwnerId,
                'fullName'=>$fullName,
                'email'=>$email,
                'title'=>$title,
                'countryCode'=>$countryCode,
                'mobileNumber'=>$mobileNumber,
                'countryCodeTwo'=>$countryCodeTwo,
                'mobileNumberTwo'=>$mobileNumberTwo,
                'isPrimary'=>$isPrimary);
    $this->db->insert('vanprovidercontact', $data);
    $ownerId = $this->db->insert_id();
    return "Remain";
  }


  /* Method to addVan
     Created By: Manzz Baria
  */
  public function addVan($vanName,$plateNo,$carType,$type,$serviceIds,$operatorIds,$ownerId){
    $isAdded = $this->isVanAlreadyAdded($vanName);
      if (!$isAdded) {
        $this->load->model("Utility","utility");
        $this->load->model("ApiVanOperatorModel","vanOperatorModel");
        $datetime = $this->utility->getCurrentDate('Y/m/d h:i:s');
        $status = VAN_STATUS_OFFLINE;
      /*  if ($type == VAN_TYPE_THIRDPARTY) {
            if ($ownerId == 0) {
                $ownerId = $this->saveOwner($name,$email,$countryCode,$mobileNumber,$address);
            }
        }
      */
        $operatorIds = str_replace(' ', '', $operatorIds);
        $operatorId = explode(',', $operatorIds);
        $data = array(
                    'name'=>$vanName,
                    'plateno'=>$plateNo,
                    'cartype'=>$carType,  
                    'type'=>$type,
                    'ownerId'=>$ownerId,
                    'status'=>$status,
                    'datetime'=>$datetime);

        $this->db->insert('vanmaster', $data);
        $insert_Id = $this->db->insert_id();
        $this->addServicesToVan($insert_Id,$serviceIds);
        $this->addOperatorsToVan($insert_Id,$operatorIds);
        $vanLocation = $this->getVanLocationById($insert_Id);
        $vanLat = VAN_DEFAULT_LATITUDE;
        $vanLong = VAN_DEFAULT_LONGITUDE;
        if ($vanLocation != null) {
            $vanLat = $vanLocation['latitude'];
            $vanLong = $vanLocation['longitude'];
            if ($vanLat <= 0 || $vanLong <= 0 ) {
               $this->updateVanLocation($insert_Id,VAN_DEFAULT_LATITUDE,VAN_DEFAULT_LONGITUDE);
            }else{
               $this->updateVanLocation($insert_Id,$vanLat,$vanLong);
            }
        }
        $this->load->model("Utility","util");
      //  $this->util->dumpTester('addOperatorsToVan',$vanLat,$vanLong);
        return $this->getVanDetailsById($insert_Id);

      }else{
        return null;
      }
  }

  /* Method to check isServiceAlreadyAddedToVan
     Created By: Manzz Baria
  */
  public function isServiceAlreadyAddedToVan($vanId,$serviceId){
    $this->db->select('Id');
    $this->db->from('vanservicemaster');
    $this->db->where("vanId",$vanId);
    $this->db->where("serviceId",$serviceId);
    $query = $this->db->get();
    if ($query->result() != null) {
      return true;
    }
    return false;
  }
  /* Method to addServicesToVan
     Created By: Manzz Baria
  */
  public function addServicesToVan($vanId,$serviceIds){
        $serviceIds = str_replace(' ', '', $serviceIds);
        $serviceId = explode(',', $serviceIds);
        for ($i=0; $i< sizeof($serviceId); $i++) {
          $this->addServiceToVan($vanId,$serviceId[$i]);
        }
       return true;
  }

  /* Method to addServiceToVan
     Created By: Manzz Baria
  */
  public function addServiceToVan($vanId,$serviceId){
        $this->load->model("Utility","utility");
        $datetime = $this->utility->getCurrentDate('Y/m/d h:i:s');
        $isAdded = $this->isServiceAlreadyAddedToVan($vanId,$serviceId);
          if (!$isAdded) {
              $data = array(
                            'vanId'=>$vanId,
                            'serviceId'=>$serviceId,
                            'datetime'=>$datetime);
              $this->db->insert('vanservicemaster', $data);
          }
       return true;
  }

  /* Method to addOperatorsToVan
     Created By: Manzz Baria
  */
  public function addOperatorsToVan($vanId,$operatorIds){
        $this->load->model("ApiUserModel","userModel");
        $this->load->model("ApiVanOperatorModel","vanOperatorModel");
        $operatorIds = str_replace(' ', '', $operatorIds);
        $operatorId = explode(',', $operatorIds);
        $latitude = VAN_DEFAULT_LATITUDE;
        $longitude = VAN_DEFAULT_LONGITUDE;
        for ($i=0; $i< sizeof($operatorId); $i++) {
          $this->userModel->addOperatorToVan($vanId,$operatorId[$i]);
          $operators = null;
          $latitude = VAN_DEFAULT_LATITUDE;
          $longitude = VAN_DEFAULT_LONGITUDE;
          $operators = $this->vanOperatorModel->getVanOperatorById($operatorId[$i]);
          if ($operators != null) {
              $latitude = $operators['latitude'];
              $longitude = $operators['longitude'];

             if($latitude <= 0 || $longitude <= 0){
               $this->updateVanLocation($vanId,VAN_DEFAULT_LATITUDE,VAN_DEFAULT_LONGITUDE);

             }else{
                $this->updateVanLocation($vanId,$latitude,$longitude);
             }
          }else{
            $this->updateVanLocation($vanId,VAN_DEFAULT_LATITUDE,VAN_DEFAULT_LONGITUDE);
          }
        }
        $this->load->model("Utility","util");
       // $this->util->dumpTester('addOperatorsToVan',$latitude,$longitude);
       return true;
  }
  /* Method to blockVan
     Created By: Manzz Baria
  */
  public function blockVan($vanId){
    $status = VAN_STATUS_BLOCKED;
    $data = array(
      'status' => $status
      );
    $this->db->where('Id', $vanId);
    $this->db->update('vanmaster', $data);
    return true;
  }

  /* Method to blockVanProvider
     Created By: Manzz Baria
  */
  public function blockVanProvider($ownerId){
    $data = array(
      'isBlocked' => BLOCKED
      );
    $this->db->where('Id', $ownerId);
    $this->db->update('vanownermaster', $data);
    return true;
  }

  /* Method to removeServiceFromVanById
     Created By: Manzz Baria
  */
  public function removeServiceFromVanById($serviceId,$vanId){
      $this->db->where('serviceId', $serviceId);
      $this->db->where('vanId', $vanId);
      $this->db->delete('vanservicemaster');
      if($this->db->affected_rows() > 0){
        return true;
      }else{
        return false;
      }
  }

  /* Method to removeOperatorFromVanById
     Created By: Manzz Baria
  */

  public function removeOperatorFromVanById($vanOperatorId){
     $this->load->model("ApiUserModel","userModel");
      return $this->userModel->removeOperatorFromVanById(0,$vanOperatorId);
  }

  /*
     Method to updateVanLocation
     Created By: Manzz Baria
  */
  public function updateVanLocation($vanId,$latitude,$longitude){

      if ($latitude <= 0 || $longitude <= 0) {
          $latitude = VAN_DEFAULT_LATITUDE;
          $longitude = VAN_DEFAULT_LONGITUDE;
      }
       $this->load->model("Utility","utility");
       $datetime = $this->utility->getCurrentDate('Y-m-d H:i:s');

      $data = array(
        'latitude' => $latitude,
        'longitude' => $longitude,
        'locationUpdateTime' => $datetime
        );
      $this->db->where('Id', $vanId);
      $this->db->update('vanmaster', $data);
      return true;
  }


  /* Method to getNearByVanList
   Created By: Manzz Baria
  */
  public function getNearByVanList($latitude = 0,$longitude = 0){
      $this->db->select("vanmaster.Id,vanmaster.name,vanmaster.plateno,vanmaster.cartype,vanmaster.type,vanmaster.ownerId,vanmaster.latitude,vanmaster.longitude,vanmaster.status,vanmaster.datetime,vanmaster.locationUpdateTime,usermaster.vanId");
      $this->db->from("vanmaster");
   $this->db->join('usermaster','usermaster.vanId=vanmaster.Id','inner');
      $this->db->where_not_in("vanmaster.status",VAN_STATUS_BLOCKED);
      $this->db->where_not_in("usermaster.status",USER_STATUS_OFFLINE);
       $this->db->where('usermaster.isActive', 1);
      $this->db->order_by("vanmaster.Id","DESC");
      $pageNo = "00";
      $query = $this->db->get();
      return $this->displayNearByVanList($query->result(),$latitude,$longitude);
  }

  function getMinusTimes($mainTime,$minusMint){
    //  $mainDateTime = new DateTime($mainTime);
      $datetime_from = (new DateTime($mainTime))->sub(DateInterval::createFromDateString($minusMint.' minutes'))->format('Y-m-d H:i:s');
      return $datetime_from;
    }

  /*
    Method to displayNearByVanList
     Created By: Manzz Baria
  */
  public function displayNearByVanList($result,$latitude = 0,$longitude = 0){
    $VanObject = null;
    $Object = null;
    if($result != null){
      $this->load->model("Utility","utility");
      $this->load->model("ApiVanOperatorModel","vanOperatorModel");
      $this->load->model("ApiAdminModel","adminModel");
      $this->load->model("ApiVanOperatorModel","userModel");
      $this->load->model("ApiCarModel","CarModel");

      //$maxDistance = $this->adminModel->getMaxDistance();
      foreach ($result as $row) {
        $maxDistance=$this->getmaxdistanceById($row->Id);
        $displayDate = $this->utility->timeAgoFormat($row->datetime);
        $currentTime = $this->utility->getCurrentDate('Y-m-d H:i:s');
        $cartype=$this->CarModel->getCarTypeById($row->cartype);
        $locationUpdateTime = $row->locationUpdateTime;
        $validTime = $this->getMinusTimes($currentTime,10);
        $one = strtotime($validTime);
        $two = strtotime($locationUpdateTime);
        
  //echo "validTime: ".$currentTime . " locationUpdateTime: ".$locationUpdateTime;
      
        if ($one <= $two) {
          # code...

              $toLatitude = (double)$row->latitude;
              $toLongitude = (double)$row->longitude;
              $distance = $this->utility->calculetDistance($latitude,$longitude,$toLatitude,$toLongitude,KM);
              $kmDistance = round($distance, 2);
              $radiusKm = $maxDistance;

             // $distance = 500;
              if($distance <= $radiusKm){
                  $Object = array(
                    'Id'=>(int)$row->Id,
                    'name'=>$row->name,
                    'plateno'=>$row->plateno,
                    'cartype'=>$cartype,
                    'type'=>(int)$row->type,
                    'status'=>(int)$row->status,
                    'latitude'=>(double)$toLatitude,
                    'longitude'=>(double)$toLongitude,
                    'datetime'=>$displayDate,
                  );
                  $VanObject[]=$Object;
              }

        }else{
            $this->vanOperatorModel->updateUserSatusByVanId($row->vanId,USER_STATUS_DISCONNECT);
        }


    

      }
    }
    return $VanObject;
  }


  /* Method to updateProviderContacts
     Created By: Manzz Baria
  */
  public function updateProviderContacts($providerContactId,$fullName,$email,$title,$countryCode,$mobileNumber,$countryCodeTwo,$mobileNumberTwo,$isPrimary){

    if ($fullName != "") {
      $data = array(
        'fullName' => $fullName
      );
      $this->db->where('Id', $providerContactId);
      $this->db->update('vanprovidercontact', $data);
    }

     if ($plateno != "") {
      $data = array(
        'plateno' => $plateno
      );
      $this->db->where('Id', $providerContactId);
      $this->db->update('vanprovidercontact', $data);
    }

     if ($cartype != "") {
      $data = array(
        'cartype' => $cartype
      );
      $this->db->where('Id', $providerContactId);
      $this->db->update('vanprovidercontact', $data);
    }


    if ($email != "") {
      $data = array(
        'email' => $email
      );
      $this->db->where('Id', $providerContactId);
      $this->db->update('vanprovidercontact', $data);
    }
    if ($title != "") {
      $data = array(
        'title' => $title
      );
      $this->db->where('Id', $providerContactId);
      $this->db->update('vanprovidercontact', $data);
    }
    if ($countryCode != "") {
      $data = array(
        'countryCode' => $countryCode
      );
      $this->db->where('Id', $providerContactId);
      $this->db->update('vanprovidercontact', $data);
    }
    if ($mobileNumber != "") {
      $data = array(
        'mobileNumber' => $mobileNumber
      );
      $this->db->where('Id', $providerContactId);
      $this->db->update('vanprovidercontact', $data);
    }

    if ($countryCodeTwo != "") {
      $data = array(
        'countryCodeTwo' => $countryCodeTwo
      );
      $this->db->where('Id', $providerContactId);
      $this->db->update('vanprovidercontact', $data);
    }
    if ($mobileNumberTwo != "") {
      $data = array(
        'mobileNumberTwo' => $mobileNumberTwo
      );
      $this->db->where('Id', $providerContactId);
      $this->db->update('vanprovidercontact', $data);
    }
    if ($isPrimary != "") {
      $data = array(
        'isPrimary' => $isPrimary
      );
      $this->db->where('Id', $providerContactId);
      $this->db->update('vanprovidercontact', $data);
    }
    return $this->getProviderContactsByProviderContactId($providerContactId);
  }

  /* Method to deleteProviderContact
     Created By: Manzz Baria
  */
  public function deleteProviderContact($providerContactId){
    $data = array(
      'isBlocked' => BLOCKED
      );
    $this->db->where('Id', $providerContactId);
    $this->db->update('vanprovidercontact', $data);
    return true;
  }

/*************   Reports        **************/

/* Method to getSupplierReport
 Created By: Manzz Baria
*/
public function getSupplierReport($supplierId,$categotyId,$serviceId,$operatorStatus){
$this->db->select("vanownermaster.Id,vanownermaster.name,vanownermaster.email,vanownermaster.city,vanownermaster.country,vanownermaster.countryCode,vanownermaster.mobileNumber,vanownermaster.countryCodeTwo,vanownermaster.mobileNumberTwo,vanownermaster.salesPerson,vanownermaster.maxdistance,vanownermaster.address");
$this->db->from("vanownermaster");
$this->db->where("vanownermaster.isBlocked",UNBLOCKED);
if ($supplierId >0) {
    $this->db->where("vanownermaster.Id",$supplierId);
  }
$query = $this->db->get();
return $this->displaySupplierReport($query->result(),$categotyId,$serviceId,$operatorStatus);

}
/* Method to displaySupplierReport
   Created By: Manzz Baria
*/
public function displaySupplierReport($result,$categoryId,$serviceId,$operatorStatus){
  $ownerObject = null;
  if($result != null){
    foreach ($result as $row) {
      $providerContacts = $this->getProviderContactsByProviderId($row->Id);
      $vanObject = $this->getVanListByOwnerIdForReport($row->Id,$categoryId,$serviceId,$operatorStatus);
      $Object = array(
        'Id'=>(int)$row->Id,
        'name'=>$row->name,
        'email'=>$row->email,
        'city'=>$row->city,
        'country'=>$row->country,
        'countryCode'=>$row->countryCode,
        'mobileNumber'=>$row->mobileNumber,
        'countryCodeTwo'=>$row->countryCodeTwo,
        'mobileNumberTwo'=>$row->mobileNumberTwo,
        'salesPerson'=>$row->salesPerson,
        'address'=>$row->address,
        'van'=>$vanObject,
        'contactPersons'=>$providerContacts
      );
      if ($vanObject != null) {
        $ownerObject[]=$Object;
      }
    }
  }
  return $ownerObject;
}


/* Method to getVanListByOwnerId
 Created By: Manzz Baria
*/
public function getVanListByOwnerIdForReport($ownerId,$categoryId,$serviceId,$operatorStatus){
    $this->db->select("Id,name,plateno,cartype,type,ownerId,latitude,longitude,status,datetime");
    $this->db->from("vanmaster");
    $this->db->where_not_in("status",VAN_STATUS_BLOCKED);
    $this->db->where("ownerId",$ownerId);
    $this->db->order_by("Id","DESC");
    $query = $this->db->get();
    return $this->displayVanListByOwnerId($query->result(),$categoryId,$serviceId,$operatorStatus);
}

/* Method to displayVanListByOwnerId
   Created By: Manzz Baria
*/
public function displayVanListByOwnerId($result,$categoryId,$serviceId,$operatorStatus){
  $VanObject = null;
  $Object = null;
  if($result != null){
    $this->load->model("Utility","utility");
    $this->load->model("ApiVanOperatorModel","vanOperatorModel");
    $this->load->model("ApiCategoryModel","categoryModel");
    $this->load->model("ApiCarModel","CarModel");
    foreach ($result as $row) {
      $displayDate = $this->utility->timeAgoFormat($row->datetime);
      $operatorObject = $this->vanOperatorModel->getVanOperatorsByVanIdForReport($row->Id,$operatorStatus);
      $categotyObject = $this->categoryModel->getCategoryServicesForReport($categoryId,$serviceId,$row->Id);
       $cartype=$this->CarModel->getCarTypeById($row->cartype);
      $Object = array(
        'Id'=>(int)$row->Id,
        'name'=>$row->name,
        'plateno'=>$row->plateno,
        'cartype'=>$cartype,
        'type'=>(int)$row->type,
        'status'=>(int)$row->status,
        'latitude'=>(double)$row->latitude,
        'longitude'=>(double)$row->longitude,
        'datetime'=>$displayDate,
        'category'=>$categotyObject,
        'operator'=>$operatorObject
      );
      if ($categotyObject != null) {
          $VanObject[]=$Object;
      }
    }
  }
  return $VanObject;
}


/* Method to adminLogin
     Created By: Manzz Baria
  */
  public function supplierLogin($email,$password){
    $this->load->model("Utility","utility");
    $encryptedPassword = $this->utility->encrypt($password);
    $this->db->select("Id,name,email,icon");
    $this->db->from("vanownermaster");
    $this->db->where("email",$email);
    $this->db->where("password",$encryptedPassword);
    $query = $this->db->get();
    return $this->displaySupplierLogin($query->result());
  }


  /* Method to displayAdminLogin
   Created By: Manzz Baria
*/
public function displaySupplierLogin($result){
  $Object = null;
  if($result != null){
      foreach ($result as $row) {
      $Object = array(
        'Id'=>(int)$row->Id,
        'fullName'=>$row->name,
        'email'=>$row->email,
        'icon'=>IMAGE_URL.$row->icon,
        'iconThump'=>THUMB_URL.$row->icon
      );
      return $Object;
    }
  }
  return $Object;
}


}
?>
