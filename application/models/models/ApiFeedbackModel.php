<?php if( ! defined("BASEPATH")) exit('No direct script access allowed');

class ApiFeedbackModel extends CI_Model {

  public function _construct(){
		parent::_construct();
	}

  /* Method to addFeedback
     Created By: Manzz Baria
  */
  public function addFeedback($feedback,$userId,$name,$email,$phoneNumber){
        $this->load->model("Utility","utility");
      //  $this->load->model("ApiEmailModel","emailModel");
        $datetime = $this->utility->getCurrentDate('Y/m/d h:i:s');
        $data = array(
          'feedback'=>$feedback,
          'userId'=>$userId,
          'name'=>$name,
          'email'=>$email,
          'phoneNumber'=>$phoneNumber,
          'datetime'=>$datetime);
        $this->db->insert('feedbackmaster', $data);
      //  $this->emailModel->sendFeedbackEmailToAdmin($userId);

      $this->load->library('mylibrary');
      $url = base_url()."Api/Email/sendFeedbackEmailToAdmin";
      $param = array('userId' => "".$userId );
      $this->mylibrary->do_in_background($url, $param);

        return true;
  }

  /* Method to getTotalFeedBack
     Created By: Manzz Baria
  */
  public function getTotalFeedBack(){
    $this->db->select("Id");
    $this->db->from("feedbackmaster");
    $query = $this->db->count_all_results();
    return ceil($query);
  }

  /* Method to getTotalFeedBack
     Created By: Manzz Baria
  */
  public function getTotalUnreadFeedBack(){
    $this->db->select("Id");
    $this->db->from("feedbackmaster");
    $this->db->where('isRead', 0);
    $query = $this->db->count_all_results();
    return ceil($query);
  }

  /* Method to getTotalPagesOfFeedbacks
     Created By: Manzz Baria
  */
  public function getTotalPagesOfFeedbacks(){
    $this->db->select("Id,feedback,userId,name,email,phoneNumber,isRead,datetime");
    $this->db->from("feedbackmaster");
    $query = $this->db->count_all_results();
    $result  = $query / 20;
    return ceil($result);
  }

  /* Method to getFeedbacks
     Created By: Manzz Baria
  */
  public function getUserFeedbacks($userId){
    $this->db->select("Id,feedback,userId,name,email,phoneNumber,isRead,datetime");
    $this->db->from("feedbackmaster");
    $this->db->where('userId', $userId);
    $query = $this->db->get();
    return $this->displayFeedbacks($query->result());
  }


  /* Method to getFeedbacks
     Created By: Manzz Baria
  */
  public function getFeedbacks($pageIndex){
    $this->db->select("Id,feedback,userId,name,email,phoneNumber,isRead,datetime");
    $this->db->from("feedbackmaster");
    $this->db->order_by("Id","DESC");
    $pageNo = "00";
    if ($pageIndex > 0) {
      $pageIndex = $pageIndex * 2;
      $pageNo = $pageIndex.'0';
    }
    $this->db->limit(20,$pageNo);
    $query = $this->db->get();
    return $this->displayFeedbacks($query->result());
  }

  /*
      Method to displayFeedbacks
       Created By: Manzz Baria
    */
    public function displayFeedbacks($result){
      $feedbackObject = null;
      if($result != null){
        $this->load->model("Utility","utility");
        $this->load->model("ApiUserModel","userModel");
        foreach ($result as $row) {
            $displayDate = $this->utility->timeAgoFormat($row->datetime);
            $userObject = $this->userModel->getUserDetail($row->userId);
            $Object = array(
                            'Id'=>(int)$row->Id,
                            'name'=>$row->name,
                            'email'=>$row->email,
                            'phoneNumber'=>$row->phoneNumber,
                            'isRead'=>(bool)$row->isRead,
                            'feedback'=>$row->feedback,
                            'datetime'=>$displayDate,
                            'user'=>$userObject,
                        );
            $feedbackObject[]=$Object;
        }
      }
      return $feedbackObject;
    }

    /* Method to getFeedbackDetail
       Created By: Manzz Baria
    */
    public function getFeedbackDetail($feedbackId){
      $this->db->select("Id,feedback,userId,name,email,phoneNumber,isRead,datetime");
      $this->db->from("feedbackmaster");
      $this->db->where('Id', $feedbackId);
      $query = $this->db->get();
      return $this->displayFeedbackDetail($query->result());
    }

    /*
        Method to displayFeedbackDetail
         Created By: Manzz Baria
      */
      public function displayFeedbackDetail($result){
        $feedbackObject = null;
        if($result != null){
          $this->load->model("Utility","utility");
          $this->load->model("ApiUserModel","userModel");
          foreach ($result as $row) {
              $displayDate = $this->utility->timeAgoFormat($row->datetime);
              $userObject = $this->userModel->getUserDetail($row->userId);
              $this->setReadFeedback($row->Id);
              $feedbackObject = array(
                              'Id'=>(int)$row->Id,
                              'name'=>$row->name,
                              'email'=>$row->email,
                              'phoneNumber'=>$row->phoneNumber,
                              'isRead'=>(bool)$row->isRead,
                              'feedback'=>$row->feedback,
                              'datetime'=>$displayDate,
                              'user'=>$userObject,
                          );
               return $feedbackObject;

          }
        }

      }

      /* Method to setReadFeedback
         Created By: Manzz Baria
      */
      public function setReadFeedback($feedbackId){
        $data = array(
                    'isRead'=>1);
        $this->db->where('Id', $feedbackId);
        $this->db->update('feedbackmaster', $data);
        return true;
      }



}
?>
