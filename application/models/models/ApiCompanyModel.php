<?php if( ! defined("BASEPATH")) exit('No direct script access allowed');

class ApiCompanyModel extends CI_Model {

  public function _construct(){
		parent::_construct();
	}


  /* Method to check isUserAlreadyRegister
  Created By: Manzz Baria
  */
  public function isUserAlreadyRegister($email,$mobileNumber){
    $this->db->select('Id');
    $this->db->from('companymaster');
    $strWhere = "email='".$email."' or mobileNumber='".$mobileNumber."'";
    $this->db->where($strWhere,null,false);
    $query = $this->db->get();
    if ($query->result() != null) {
      return true;
    }
    return false;
  }
  /* Method to addDeviceToken
     Created By: Manzz Baria
  */
  public function addCompany($fullName,$email,$countryCode,$mobileNumber,$password,$fileUrl){
    $isAdded = $this->isUserAlreadyRegister($email,$mobileNumber);
      if (!$isAdded) {
        $this->load->model("Utility","utility");
        $datetime = $this->utility->getCurrentDate('Y/m/d h:i:s');
        $securePassword = $this->utility->encrypt($password);
        $isActive = 0;
        $data = array(
          'fullName'=>$fullName,
          'email'=>$email,
          'countryCode'=>$countryCode,
          'mobileNumber'=>$mobileNumber,
          'password'=>$securePassword,
          'fileUrl'=>$fileUrl,
          'datetime'=>$datetime);
        $this->db->insert('companymaster', $data);
        $insert_Id = $this->db->insert_id();
        $this->utility->sendEmailForApproved($insert_Id);
        return true;
      }else{
        return false;
      }
  }


    /* Method to getTotalPagesOfCompanies
       Created By: Manzz Baria
    */
    public function getTotalPagesOfCompanies(){
      $this->db->select("Id,fullName,email,countryCode,mobileNumber,fileUrl,datetime");
      $this->db->from("companymaster");
      $query = $this->db->count_all_results();
      $result  = $query / 20;
      return ceil($result);
    }

    /* Method to getCompanies
     Created By: Manzz Baria
    */
    public function getCompanies($pageIndex){
        $this->db->select("Id,fullName,email,countryCode,mobileNumber,fileUrl,datetime");
        $this->db->from("companymaster");
        $this->db->order_by("Id","DESC");
        $pageNo = "00";
        if ($pageIndex > 0) {
          $pageIndex = $pageIndex * 2;
          $pageNo = $pageIndex.'0';
        }
        $this->db->limit(20,$pageNo);
        $query = $this->db->get();
        return $this->displayCompanies($query->result());
    }

    /* Method to displayCompanies
       Created By: Manzz Baria
    */
    public function displayCompanies($result){
      $companyObject = null;
      $Object = null;
      if($result != null){
        $this->load->model("Utility","utility");
        foreach ($result as $row) {
          $displayDate = $this->utility->timeAgoFormat($row->datetime);
          $Object = array(
            'Id'=>(int)$row->Id,
            'fullName'=>$row->fullName,
            'email'=>$row->email,
            'countryCode'=>(int)$row->countryCode,
            'mobileNumber'=>$row->mobileNumber,
            'fileUrl'=>IMAGE_URL.$row->fileUrl,
            'thumbUrl'=>THUMB_URL.$row->fileUrl,
            'datetime'=>$displayDate
          );
           $companyObject[]=$Object;
        }
      }
      return $companyObject;
    }





}
?>
