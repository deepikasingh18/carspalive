<?php if( ! defined("BASEPATH")) exit('No direct script access allowed');

class ApiEmailModel extends CI_Model {

  public function _construct(){
		parent::_construct();
	}

  function sendResetPasswordLink($email){

/*
    $data=json_decode(file_get_contents('php://input'));
    $this->verifyRequiredParams(array('email'),POST_TYPE);
    $email = $_POST['email'];
*/
      $this->load->model("ApiUserModel","userModel");
      $this->load->model("ApiAdminModel","adminModel");
      $this->load->model("Utility","utility");

      $userId = $this->userModel->getUserIdFromEmail($email);
      $userName = $this->userModel->getFullName($userId);
      $validationToken = $userId;

      $accessToken = $this->utility->generateRandomString(16);
      $this->userModel->saveAccessToken($accessToken,$userId);

      $contacts = $this->adminModel->getCustomerServiceSupport();

      $title = $contacts['name'];
      $phoneNumber = "+".$contacts['countryCode'].$contacts['phoneNumber'];
      $contactemail = $contacts['email'];
      $address = $contacts['address'];
      $logoUrl = LOGO_URL;
      $body='<table width="100%" bgcolor="#68DFF0"><tr><td><div style="width: 100%;"><table border="0"
      width="100%"><tr><td><img src="'.$logoUrl.'"
      width="60px" height="60px" align="middle" style="vertical-align:middle"/></td><td align="right">
      <h2>Car Spa</h2></td></tr></table></div></td></tr></table><table align="center" width="100%"
      border="0"><tr><td bgcolor="#ebebeb"><div align="center"><table border="0" align="center" style="padding:15px">
      <tr><td style="background-color: #FFFFFF;padding:15px"><h3>Dear '.$userName.',</h3><p style="font-size:17px;">
      We got a request to reset your Car Spa password</p><table width="100%" border="0" cellpadding="0" cellspacing="0" >
      <tr align="center"><td><p style="Margin-bottom: 35px;">
      <a href="'.base_url().'resetpassword.html?validationToken='.$validationToken.' &accessToken = '.$accessToken.'"
      style="text-decoration:none;border:solid 1px;color:#68DFF0;padding:10px;font-weight:600">Reset Password</a>
      </p></td></tr></table></p><p>If you ignore this message, your password wont be changed</p>
      <table style="background-color: #ebebeb;" width="100%"><tr><td><table align="left" style="float:left;width:100%;">
      <tr><td><h5>For More Queries Contact on Below:</h5><p><span style="font-size:18px;font-weight:600">
      '.$title.'</span><br>Phone: <strong>'.$phoneNumber.'</strong>
      <br/>Email: <strong><a href="emailto:hseldon@trantor.com">'.$contactemail.'</a></strong><br>'.$address.'</p></td></tr>
      </table> <span></span></td></tr></table> </td></tr></table></div> </td></tr>';

        //  ->from('support@carspaco.com','CarSpa')
          $this->load->library('email');
          $result = $this->email
                  ->from('noreply@carspaco.com',''.$title)
                  ->to(''.$email)
                  ->subject("Reset password link")
                  ->message($body)
                  ->send();

          return $result;

    }


}
?>
