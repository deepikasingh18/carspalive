<?php if( ! defined("BASEPATH")) exit('No direct script access allowed');

class ApiPartnerModel extends CI_Model {

  public function _construct(){
		parent::_construct();
	}


  /*
      Method to check isUserAlreadyRegister
      Created By: Manzz Baria
  */
  public function isPartnerAlreadyAdded($email,$mobileNumber){
    $this->db->select('Id');
    $this->db->from('ourpartners');
    $strWhere = "email='".$email."' or mobileNo='".$mobileNumber."'";
    $this->db->where($strWhere,null,false);
    $this->db->where('isBlocked',UNBLOCKED);
   // $this->db->where_not_in("status",USER_STATUS_BLOCK);
    $query = $this->db->get();
    if ($query->result() != null) {
      return true;
    }
    return false;
  }

 
/* Method to registration
   Created By: Manzz Baria
*/
public function addOurPartners($userId,$fullName,$email,$age,$nationality,$nationalId,$countryCode,$mobileNo,$year,$joinDate,$coveringCountry,$coveringCity,$providedcategoryId,$suggestion,$document,$document1,$document2,$language=LANGUAGE_ENGLISH)
{

		$joiningdate = date ("Y-m-d H:i:s", strtotime($joinDate));

      $data = array(
	    'userId'=>$userId,
        'fullName'=>$fullName,
        'email'=>$email,
        'age'=>$age,
        'nationality'=>$nationality,
        'nationalId'=>$nationalId,
	'countryCode'=>$countryCode,
        'mobileNo'=>$mobileNo,
        'year'=>$year,
        'dateTime'=>$joiningdate,	
        'coveringCountry'=>$coveringCountry,
        'coveringCity'=>$coveringCity,
		'suggestion'=>$suggestion,
		 'uploaddocument'=>$document,
		 'uploaddocument1'=>$document1,
		 'uploaddocument2'=>$document2,
		);
      $this->db->insert('ourpartners', $data);
      $insert_Id = $this->db->insert_id();
  	  $this->addPartnerCategory($insert_Id,$providedcategoryId);
          $this->load->library('mylibrary');
            $url = base_url()."Api/Email/newRequestForPartner";
  	$param = array('partnerId' => "".$insert_Id );
  	$this->mylibrary->do_in_background($url, $param);
        $mobileno=$countryCode.$mobileNo;
					$messageEn="Thank you for your Interest to join our Team; Your request is under process and we will come back to you shortly.";
					$messageAr="شكرا لرغبتك للانضمام لفريق عملنا: سوف ندرس طلبك و نتواصل معك قريبا .";
					
					
					$this->load->model("ApiSMSModel","sModel");
					if($language==LANGUAGE_ARABIC)
					{
						//$this->sModel->sendSMSByMobileNumber($messageAr,$mobileno);
					}
					else
					{
						//$this->sModel->sendSMSByMobileNumber($messageEn,$mobileno);
					}
      return true;
   /* }else{
      return false;
    }*/
}

public function addPartnerCategory($ourpartnerId,$providedcategoryId)
{
	$category = explode(',' , $providedcategoryId);
          for ($i=0; $i< sizeof($category); $i++)
		     {
              	
				$data = array(
								'categoryId'=>$category[$i],
								'ourPartnerId'=>$ourpartnerId);
				$this->db->insert('partnercategory', $data);
            }

      return true;
}



public function getOurPartners($language=LANGUAGE_ENGLISH,$fromdate,$todate,$categoryId,$country)
{
	 $fromdate = $fromdate." 00:00:00";
     $todate = $todate." 23:59:59";
  
	$this->db->select('ourpartners.Id,userId,fullName,email,age,nationality,nationalId,mobileNo,year,coveringCountry,coveringCity,countryCode,suggestion,uploaddocument,uploaddocument1,uploaddocument2,ourpartners.dateTime');
	$this->db->from('ourpartners');
	if ($categoryId > 0 ) 
	{
    	$this->db->join('partnercategory','partnercategory.ourPartnerId = ourpartners.Id','inner');
		$this->db->join('categorymaster','partnercategory.categoryId = categorymaster.Id','inner');
		$this->db->where('partnercategory.categoryId',$categoryId);
  	}
	
	if ($country > 0) 
	{
		//echo $country;
    	  $this->db->where('countryCode', $country);
  	}
  	$this->db->where('ourpartners.isBlocked',UNBLOCKED);
	$this->db->where('ourpartners.dateTime >=', $fromdate);
    $this->db->where('ourpartners.dateTime <=', $todate);
    $this->db->order_by("ourpartners.dateTime","ASC");
	$query=$this->db->get();
	 return $this->displaypartner($query->result(),$language);
}

public function searchOurPartners($language=LANGUAGE_ENGLISH,$fromdate,$todate,$categoryId,$country,$name)
{
	 $fromdate = $fromdate." 00:00:00";
     $todate = $todate." 23:59:59";
  
	$this->db->select('ourpartners.Id,userId,fullName,email,age,nationality,nationalId,mobileNo,year,coveringCountry,coveringCity,countryCode,suggestion,uploaddocument,uploaddocument1,uploaddocument2,ourpartners.dateTime');
	$this->db->from('ourpartners');
	if ($categoryId > 0 ) 
	{
    	$this->db->join('partnercategory','partnercategory.ourPartnerId = ourpartners.Id','inner');
		$this->db->join('categorymaster','partnercategory.categoryId = categorymaster.Id','inner');
		$this->db->where('partnercategory.categoryId',$categoryId);
  	}
	
	if ($country > 0) 
	{
		//echo $country;
    	  $this->db->where('countryCode', $country);
  	}
	$this->db->where('ourpartners.dateTime >=', $fromdate);
    $this->db->where('ourpartners.dateTime <=', $todate);
    $this->db->like("fullName",$name);
	$query=$this->db->get();
	 return $this->displaypartner($query->result(),$language);
}


public function deleteOurPartnerById($partnerId)
{
	$data = array(
                    'isBlocked'=>BLOCKED);
        $this->db->where('Id', $partnerId);
        $this->db->update(' ourpartners', $data);
        return true;
//	return $query->result();
}
public function getOurPartnerById($partnerId)
{
	$this->db->select('*');
	$this->db->from('ourpartners');
	$this->db->where('Id',$partnerId);
	$this->db->where('isBlocked',UNBLOCKED);
	$query=$this->db->get();
	 return $this->displaypartner($query->result());
//	return $query->result();
}

public function displaypartner($result,$language=LANGUAGE_ENGLISH)
{
	$Object = null;
  $provideObject=null;
 $uploaddocumentone="";
 $uploaddocumenttwo="";
 $uploaddocumentthree="";
  $country=null;
      if($result != null)
	  {
           //$categoryObject[] = $this->CategoryModel->getCategoryById($category[$i],$language);
	  		$this->load->model("ApiContactModel","ContactModel");
		   foreach($result as $row)
		   {

		   		 $country=$this->ContactModel->getcountry($row->countryCode);
			   	if($row->uploaddocument!=null)
		  		{
		  			$uploaddocumentone=DOCUMENTONE_URL.$row->uploaddocument;
		  		}

		  		if($row->uploaddocument1!=null)
		  		{
		  			$uploaddocumenttwo=DOCUMENTONE_URL.$row->uploaddocument1;
		  		}

		  		if($row->uploaddocument2!=null)
		  		{
		  			$uploaddocumentthree=DOCUMENTONE_URL.$row->uploaddocument2;
		  		}
	  		
		 	 $partnerCategory=$this->getpartnerCategory($row->Id);
          $Object = array(
				  "Id"=>(int)$row->Id,
				  "userId"=>$row->userId,
				  "fullName"=>$row->fullName,
				  "email"=>$row->email,
    			  "age"=> $row->age,
    			  "nationality"=>$row->nationality,
    			  "nationalId"=> $row->nationalId,
				  "countryCode"=> $row->countryCode,
    			  "mobileNo"=> $row->mobileNo,
    			  "year"=> $row->year,
    			  "coveringCountry"=> $country,
    			  "coveringCity"=> $row->coveringCity,
				  "suggestion"=>$row->suggestion,
				  "uploaddocumentone"=>$uploaddocumentone,
				  "uploaddocumenttwo"=>$uploaddocumenttwo,
				  "uploaddocumentthree"=>$uploaddocumentthree,
    			  "providedcategory"=> $partnerCategory,
    			  "requestDate"=>$row->dateTime
          );
		   $provideObject[]= $Object;
		  }
         
        }
      return $provideObject;
}

public function getpartnerCategory($partnerId,$language=LANGUAGE_ENGLISH)
{
	$this->db->select('partnercategory.Id ,partnercategory.categoryId,partnercategory.ourPartnerId,categorymaster.name');
	$this->db->from('partnercategory');
	$this->db->join('categorymaster','partnercategory.categoryId = categorymaster.Id','inner');
	$this->db->where('partnercategory.ourPartnerId',$partnerId);
	//$this->db->where('isBlocked',UNBLOCKED);
	$query=$this->db->get();
	return $this->displaypartnercategory($query->result(),$language);
}

public function displaypartnercategory($result,$language=LANGUAGE_ENGLISH)
{
    $provideObject=null;
 
      if($result != null){
	   foreach($result as $row)
		   {
				  $Object = array(
					"Id"=>(int)$row->Id,
					 "categoryId"=>$row->categoryId,
					"ourPartnerId"=>$row->ourPartnerId,
					"name"	=>$row->name
				  );
		  		$provideObject[]= $Object;
		  }
          
        }
      return $provideObject;
}
}
?>
