<?php if( ! defined("BASEPATH")) exit('No direct script access allowed');

class ApiNotificationModel extends CI_Model {

  public function _construct(){
    parent::_construct();
  }


  /*
      Method to sendNotificationUserToOperatorForChangeRequestVan
      Created By: Manzz Baria
   */

   function sendNotificationUserToOperatorForChangeRequestVan($appointmentId,$oldVanId,$newVanId){

     $this->load->model("ApiDeviceModel","deviceModel");
     $this->load->model("ApiAppoinmnetModel","appoinmnetModel");
     $this->load->model("Utility","utility");

     $notificationType = NOTIFICATION_TYPE_REQUEST_TRANSFER;
     $androidGCMArrayEn = null;
   $androidGCMArrayAr = null;
     $iosAPNArray = null;
     $serviceId = 0;

     $userId = $this->appoinmnetModel->getUserIdOfAppointment($appointmentId);
     $requestId = $this->appoinmnetModel->getRequestIdOfAppointment($appointmentId);
     $appointmentTime = $this->appoinmnetModel->getTimeOfAppointment($appointmentId);
     $operatorDatetime = $this->appoinmnetModel->getTimeOfAppointmentOperator($appointmentId);
     $suffixId = $this->appoinmnetModel->getSuffixIdfAppointment($appointmentId);
     $suffixId = $this->appoinmnetModel->getSuffixIdfAppointment($appointmentId);
     $suffix = $this->utility->getSuffix($requestId,$suffixId);

     // send notification to old van operator
        $titleen="Request ".$suffix." removed";

        $titlear=$suffix. " لقد تم إلغاء طلب";

        $messageen= $suffix ." was removed from your job list";

        $messagear="لقد تم حذف الطلب من جدولك ". $suffix ;
     $androidGCMArrayEn = $this->deviceModel->getDeviceTokensFromVanId($oldVanId,LANGUAGE_EN);
   $androidGCMArrayAr = $this->deviceModel->getDeviceTokensFromVanId($oldVanId,LANGUAGE_AR);
     //$iosAPNArray = $this->deviceModel->getDeviceTokensFromVanId($oldVanId);
     if ($androidGCMArrayEn != null) {
         $this->sendNotificationToAndroidDevice($androidGCMArrayEn,$titleen,$messageen,$userId,$appointmentId,$serviceId,$notificationType);
     }
   if ($androidGCMArrayAr != null) {
         $this->sendNotificationToAndroidDevice($androidGCMArrayAr,$titlear,$messagear,$userId,$appointmentId,$serviceId,$notificationType);
     }
     

     // send notification to new van operator
     $notificationType =  NOTIFICATION_TYPE_BOOKED;
      $titleen="Request ".$suffix." : is transferred";

        $titlear=$suffix." لقد تم تحويل طلب رقم ";

        $messageen="New job was transferred to you on ".$operatorDatetime;

        $messagear=$operatorDatetime." لقد تم تحويل طلب جديد لجدولك بتاريخ";
        
     $androidGCMArrayEn = $this->deviceModel->getDeviceTokensFromVanId($newVanId,LANGUAGE_EN);
     $androidGCMArrayAr = $this->deviceModel->getDeviceTokensFromVanId($newVanId,LANGUAGE_AR);
     //$iosAPNArray = $this->deviceModel->getDeviceTokensFromVanId($newVanId);
     if ($androidGCMArrayEn != null) {
         $this->sendNotificationToAndroidDevice($androidGCMArrayEn,$titleen,$messageen,$userId,$appointmentId,$serviceId,$notificationType);
     }
   
   if ($androidGCMArrayAr != null) {
         $this->sendNotificationToAndroidDevice($androidGCMArrayAr,$titlear,$messagear,$userId,$appointmentId,$serviceId,$notificationType);
     }

      // send notification to user
     $notificationType =  NOTIFICATION_TYPE_REQUEST_UPDATED;
      $titleen="Request ".$suffix." : is transferred";

        $titlear=$suffix." لقد تم تحويل طلب رقم ";

        $messageen="We have changed the van which is going to serve you";

        $messagear="لقد تم تغيير مزود الخدمة المكلف بخدمتك";
        
    $androidGCMArrayEn = $this->deviceModel->getDeviceTokens($userId,LANGUAGE_EN);
    $androidGCMArrayAr = $this->deviceModel->getDeviceTokens($userId,LANGUAGE_AR);
     //$iosAPNArray = $this->deviceModel->getDeviceTokensFromVanId($newVanId);
     if ($androidGCMArrayEn != null) {
         $this->sendNotificationToAndroidDevice($androidGCMArrayEn,$titleen,$messageen,$userId,$appointmentId,$serviceId,$notificationType);
     }
   
   if ($androidGCMArrayAr != null) {
         $this->sendNotificationToAndroidDevice($androidGCMArrayAr,$titlear,$messagear,$userId,$appointmentId,$serviceId,$notificationType);
     }
    
    

   }


  /*
      Method to sendNotificationUserToOperatorForUpdateRequest
      Created By: Manzz Baria
   */
   function sendNotificationUserToOperatorForUpdateRequest($appointmentId,$addressId,$appoinmentDateTime){

     $this->load->model("ApiDeviceModel","deviceModel");
     $this->load->model("ApiAppoinmnetModel","appoinmnetModel");
     $this->load->model("Utility","utility");

     $notificationType = NOTIFICATION_TYPE_REQUEST_UPDATED;
     $androidGCMArrayEn = null;
   $androidGCMArrayAr = null;
     $iosAPNArray = null;
     $serviceId = 0;

     $userId = $this->appoinmnetModel->getUserIdOfAppointment($appointmentId);
     $requestId = $this->appoinmnetModel->getRequestIdOfAppointment($appointmentId);
     $appointmentTime = $this->appoinmnetModel->getTimeOfAppointment($appointmentId);
     $operatorDatetime = $this->appoinmnetModel->getTimeOfAppointmentOperator($appointmentId);
     $suffixId = $this->appoinmnetModel->getSuffixIdfAppointment($appointmentId);
     $suffix = $this->utility->getSuffix($requestId,$suffixId);

       $titleen="Request ".$suffix."updated";

        $titlear=$suffix."لقد تم تحديث طلبك";

     if ($addressId != "" && $appoinmentDateTime != "") {
         $messageen="Updated address and appointment time";

        $messagear="لقد تم تحديث العنوان و الموعد";
     }else if($addressId != ""){
         $messageen="Updated address";

        $messagear="لقد تم تحديث العنوان";
     }else if($appoinmentDateTime != ""){
       $messageen="Updated appointment time : ".$operatorDatetime;

        $messagear=$operatorDatetime." لقد تم تحديث الموعد إلى";
     }

     $androidGCMArrayEn = $this->deviceModel->getDeviceTokensOfOperators($appointmentId,LANGUAGE_EN);
     $androidGCMArrayAr = $this->deviceModel->getDeviceTokensOfOperators($appointmentId,LANGUAGE_AR);
    // $iosAPNArray = $this->deviceModel->getDeviceTokensOfOperators($appointmentId);

     //Send notification to android users
     if ($androidGCMArrayEn != null) {
         $this->sendNotificationToAndroidDevice($androidGCMArrayEn,$titleen,$messageen,$userId,$appointmentId,$serviceId,$notificationType);
     }
     if ($androidGCMArrayAr != null) {
         $this->sendNotificationToAndroidDevice($androidGCMArrayAr,$titlear,$messagear,$userId,$appointmentId,$serviceId,$notificationType);
     }
   

   }

   /* Method to sendNotificationUserToOperator
       Created By: Manzz Baria
    */
    function sendNotificationUserToOperator($appointmentId,$status,$reason){

      $this->load->model("Utility","utility");
      $this->load->model("ApiDeviceModel","deviceModel");
      $this->load->model("ApiAppoinmnetModel","appoinmnetModel");

      $notificationType = $status;
      $androidGCMArrayEn = null;
    $androidGCMArrayAr = null;
    
      $iosAPNArray = null;
      $serviceId = 0;

      $userId = $this->appoinmnetModel->getUserIdOfAppointment($appointmentId);
      $requestId = $this->appoinmnetModel->getRequestIdOfAppointment($appointmentId);
      $appointmentTime = $this->appoinmnetModel->getTimeOfAppointment($appointmentId);
      $operatorDatetime = $this->appoinmnetModel->getTimeOfAppointmentOperator($appointmentId);
      $suffixId = $this->appoinmnetModel->getSuffixIdfAppointment($appointmentId);
      $suffix = $this->utility->getSuffix($requestId,$suffixId);

          if ($status == REQUEST_STATUS_PENDING) {
            $notificationType = NOTIFICATION_TYPE_BOOKED;
           $titleen="Request ".$suffix." : is booked";

        $titlear=$suffix." : تأكيد حجز موعد ";



        $messageen="New request is booked on ".$operatorDatetime;

        $messagear=$operatorDatetime." لقد تم حجز موعد عند:";
          }else if ($status == REQUEST_STATUS_REJECTED){
            $notificationType = NOTIFICATION_TYPE_REQUEST_REJECTED;
           $titleen="Request ".$suffix." : is canceled";

        $titlear=$suffix." : لقد تم إلغاء طلبك ";
            $messageen = $reason;
      $messagear = $reason;
          }else{
           $titleen="Request ".$suffix;

        $titlear=$suffix."رقم الطلب";

            $messageen = "".$reason;
       $messagear = "".$reason;
          }

      $androidGCMArrayEn = $this->deviceModel->getDeviceTokensOfOperators($appointmentId,LANGUAGE_EN);
      $androidGCMArrayAr = $this->deviceModel->getDeviceTokensOfOperators($appointmentId,LANGUAGE_AR);
      //$iosAPNArray = $this->deviceModel->getDeviceTokensOfOperators($appointmentId);

        //Send notification to android users
        if ($androidGCMArrayEn != null) {
            $this->sendNotificationToAndroidDevice($androidGCMArrayEn,$titleen,$messageen,$userId,$appointmentId,$serviceId,$notificationType);
        }
     if ($androidGCMArrayAr != null) {
            $this->sendNotificationToAndroidDevice($androidGCMArrayAr,$titlear,$messagear,$userId,$appointmentId,$serviceId,$notificationType);
        }
     

    }

   /* Method to sendNotificationOperaterToUserForRequestAction
       Created By: Manzz Baria
    */
  function sendNotificationOperaterToUserForRequestAction($appointmentId,$status,$reason,$serviceId){

      /*
          1. On the way
          2. In progress
          3. Request Rejected
          4. Finished
          5. Service rejected
      */

      $this->load->model("Utility","utility");
      $this->load->model("ApiDeviceModel","deviceModel");
      $this->load->model("ApiAppoinmnetModel","appoinmnetModel");
      $this->load->model("ApiServiceModel","serviceModel");

      $notificationType = $status;
      $androidGCMArrayEn = null;
      $androidGCMArrayAr = null;
    $iosAPNArray = null;

      $requestId = $this->appoinmnetModel->getRequestIdOfAppointment($appointmentId);
      $userId = $this->appoinmnetModel->getUserIdOfAppointment($appointmentId);
      $suffixId = $this->appoinmnetModel->getSuffixIdfAppointment($appointmentId);
      $suffix = $this->utility->getSuffix($requestId,$suffixId);

      if ($status == REQUEST_STATUS_ON_THE_WAY) {
        $notificationType = NOTIFICATION_TYPE_ON_THE_WAY;
        $titleen = "Operator is On the way for Request".$suffix;
    $titlear = $suffix."مقدم الخدمة على الطريق لخدمة طلب رقم";
        $messageen = "Expect a 15 minutes delay due to unexpected traffic";
        $messagear = "قد يكون هناك تأخير لمدة 15 دقيقة بسبب زحمة السير";
      }else if ($status == REQUEST_STATUS_IN_PROGRESS){
        $notificationType = NOTIFICATION_TYPE_IN_PROGRESS;
        $titleen="Request ".$suffix."  is in progress";

        $titlear=$suffix." جاري التنفيذ";



        $messageen="Your request is now in progress";

        $messagear="جاري تنفيذ طلبك";
    
      }else if ($status == REQUEST_STATUS_REJECTED){
        $notificationType = NOTIFICATION_TYPE_REQUEST_REJECTED;
            $titleen="Request ".$suffix."  is Rejected";

        $titlear=$suffix." لقد تم إلغاء طلبك";
      $messageen = $reason;
        $messagear = $reason;
      }else if ($status == REQUEST_STATUS_FINISH){
        $notificationType = NOTIFICATION_TYPE_FINISH;
       $titleen="Request ".$suffix."  is finish";

        $titlear=$suffix." لقد تم الإنتهاء من تنفيذ طلبك";

        $messageen="Your request is now finish";

        $messagear="لقد إنتهينا من تنفيذ طلبك";

      }else if ($status == NOTIFICATION_TYPE_SERVICE_REJECTED){
        $notificationType = NOTIFICATION_TYPE_SERVICE_REJECTED;
        $serviceName = $this->serviceModel->getServiceNameByServiceId($serviceId);
        $titleen="Request ".$suffix." ".$serviceName." service rejected";
        //$messageen = "Your ".$serviceName." service has been rejected";
     $titlear=$suffix." ".$serviceName." لقد تم إلغاء طلب خدمتك";
    //    $messagear = "لقد تم إلغاء طلب خدمتك";
        $message = $reason;
      }else{
        $titleen = "Request ".$suffix;
        $messageen = "".$reason;
      $titlear=$suffix." رقم الطلب";
      $messagear = "".$reason;
      }
      $androidGCMArrayEn = $this->deviceModel->getDeviceTokens($userId,LANGUAGE_EN);
    $androidGCMArrayAr = $this->deviceModel->getDeviceTokens($userId,LANGUAGE_AR);
      //$iosAPNArray = $this->deviceModel->getDeviceTokens($userId);
      //Send notification to android users
      if ($androidGCMArrayEn != null) {
             $this->sendNotificationToAndroidDevice($androidGCMArrayEn,$titleen,$messageen,$userId,$appointmentId,$serviceId,$notificationType);
      }
      if ($androidGCMArrayAr != null) {
             $this->sendNotificationToAndroidDevice($androidGCMArrayAr,$titlear,$messagear,$userId,$appointmentId,$serviceId,$notificationType);
      }
    

return true;

    }


  /*
       Method to send Notification to android device
       Created By: Manzz Baria
    */
  function sendNotificationToAndroidDevice($deviceId,$title,$message,$userId,$appointmentId,$serviceId,$notificationType){
//http://stackoverflow.com/questions/38122240/firebase-cloud-messaging-authorizion-error
 $ANDROID_API = "AIzaSyCxOdFe2wYMwlBLU1tqsgjvinT8IGMUM3A";
   // $ANDROID_API = "AIzaSyCO8NyKWWGh4eXixJTMtAqai3mNG7KjbWU";
    //  $ANDROID_API = "AIzaSyDii3qhyKZf2HWywai-Xh2Gtteug0axw3Y";
  //  $ANDROID_API = "AIzaSyC52ZCCmBxr5t-tdbTcNP90g7Eh8CTP7D4";
    $ANDROID_APN_URL = "https://fcm.googleapis.com/fcm/send";
    $api_key = $ANDROID_API;
    //$DeviceId=$deviceId;
    $registrationIDs= $deviceId;
    $url = $ANDROID_APN_URL;

    $priority = "high";
    $sound = "beepNotification.caf";
    $fields = array(
      'priority'  => $priority,
      'registration_ids'  => $registrationIDs,
      'notification'  => array(
            "title" => $title,
            "body" => $message,
            "sound" => $sound,
            "badge" => $notificationType
          ),
        'data'  => array(
        "userId" => $userId,
        "appointmentId" => $appointmentId,
        "serviceId" => $serviceId,
        "notificationType" => $notificationType
      ),
    );
    $headers = array('Authorization: key=' . $api_key,'Content-Type: application/json');
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt( $ch, CURLOPT_POST, true );
    curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
    curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
    curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $fields ) );
    $result = curl_exec($ch);
    curl_close($ch);
    //return $result;
  }



/*
       Method to send Notification from Admin
       Created By: Manzz Baria
    */
   function sendNotificationFromAdmin($deviceId,$title,$message){
//http://stackoverflow.com/questions/38122240/firebase-cloud-messaging-authorizion-error
$ANDROID_API = "AIzaSyCxOdFe2wYMwlBLU1tqsgjvinT8IGMUM3A";
   // $ANDROID_API = "AIzaSyCO8NyKWWGh4eXixJTMtAqai3mNG7KjbWU";
    //  $ANDROID_API = "AIzaSyDii3qhyKZf2HWywai-Xh2Gtteug0axw3Y";
  //  $ANDROID_API = "AIzaSyC52ZCCmBxr5t-tdbTcNP90g7Eh8CTP7D4";
    $ANDROID_APN_URL = "https://fcm.googleapis.com/fcm/send";
    $notificationType=NOTIFICATION_TYPE_ADMIN;
    $api_key = $ANDROID_API;
    //$DeviceId=$deviceId;
    $registrationIDs= $deviceId;
    $url = $ANDROID_APN_URL;

    $priority = "high";
    $sound = "beepNotification.caf";
    $fields = array(
      'priority'  => $priority,
      'registration_ids'  => $registrationIDs,
      'notification'  => array(
            "title" => $title,
            "body" => $message,
            "sound" => $sound,
            "badge" => $notificationType
          ),
        'data'  => array(
        "notificationType" => $notificationType
      ),
    );
    $headers = array('Authorization: key=' . $api_key,'Content-Type: application/json');
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt( $ch, CURLOPT_POST, true );
    curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
    curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
    curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $fields ) );
    $result = curl_exec($ch);
    curl_close($ch);
    //return $result;
  }

/**************************      IPhone Notification                        **************************/

public function sendTestIphoneNotification(){
  $this->load->model("ApiDeviceModel","deviceModel");
  $iosAPNArray = $this->deviceModel->getAllIphoneDeviceTokens(IOS_NOTIFICATION);
  if ($iosAPNArray != null) {
      $title = "Test Notification";
      $message = "Test Notification Description";
      $userId = "10";
      $appointmentId = "10";
      $serviceId = "10";
      $notificationType = "2";
      //$iosAPNArray = "13ac0c4458b1bf05be2caafd24e28a1888535108f143c1855997e0e851623f4e";
      $this->sendNotificationToAndroidDevice($iosAPNArray,$title,$message,$userId,$appointmentId,$serviceId,$notificationType);
      //$this->sendDisNotificationiPhone($iosAPNArray,$title,$message,$userId,$appointmentId,$serviceId,$notificationType);

  }
  return true;
}


/* Method to send Notification to iPhone
   Created By: Nishit Patel
*/
public function sendDevNotificationiPhone($deviceId,$title,$message,$userId,$appointmentId,$serviceId,$notificationType){
//$deviceTokes,$notificationTitle,$notificationMessage,$thumbUrl,$userId,$fileId,$type,$notificationType
  // Put your device token here (without spaces):

  $deviceToken = $deviceId;
  $passphrase = 'welcome';
  $NotiTitle = $title;
  $message = $message;
  ////////////////////////////////////////////////////////////////////////////////
  $ctx = stream_context_create();

  stream_context_set_option($ctx, 'ssl', 'local_cert', './assets/ck.pem');
  //echo $deviceToken[0];
  stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
  // Open a connection to the APNS server
  $fp = stream_socket_client('ssl://gateway.push.apple.com:2195', $err,$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

  if (!$fp)
    exit("Failed to connect: $err $errstr" . PHP_EOL);

  $aps = array('alert' => $NotiTitle." ".$message,'sound' => 'beepNotification.caf');
  $bodyJson = array("aps"=>$aps ,"userId" => $userId,
    "appointmentId" => $appointmentId,
    "serviceId" => $serviceId,
    "notificationType" => $notificationType);
  // Encode the payload as JSON
  $payload = json_encode($bodyJson);

  if ($deviceId != null) {
    foreach ($deviceId as $device) {
      // Build the binary notification
     // $msg = chr(0) . pack('n', 32) . pack('H*', $device) . pack('n', strlen($payload)) . $payload;
      $msg = chr(0) . pack('n', 32) . pack('H*', str_replace(' ', '', sprintf('%u', CRC32($device)))) . pack('n', strlen($payload)) . $payload;
      // Send it to the server
      $result = fwrite($fp, $msg, strlen($msg));
      if (!$result){
        echo 'Message not delivered' . PHP_EOL;
      }else{
        echo 'Message successfully delivered' . PHP_EOL;
      }
    }
    // Close the connection to the server
    fclose($fp);
  }
}


  /* Method to send Notification to iPhone
     Created By: Nishit Patel
  */
  public function sendDisNotificationiPhone($deviceId,$title,$message,$userId,$appointmentId,$serviceId,$notificationType){
//$deviceTokes,$notificationTitle,$notificationMessage,$thumbUrl,$userId,$fileId,$type,$notificationType
    // Put your device token here (without spaces):
    
    $deviceToken = $deviceId;
    $passphrase = 'welcome';
    $NotiTitle = $title;
    $message = $message;
    ////////////////////////////////////////////////////////////////////////////////
    $ctx = stream_context_create();

    stream_context_set_option($ctx, 'ssl', 'local_cert', './assets/dis/ckdis.pem');

    stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
    // Open a connection to the APNS server
    $fp = stream_socket_client(
      'ssl://gateway.push.apple.com:2195', $err,
      $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

    if (!$fp)
      exit("Failed to connect: $err $errstr" . PHP_EOL);

    $aps = array('alert' => $NotiTitle." ".$message,'sound' => 'beepNotification.caf');
    $bodyJson = array("aps"=>$aps ,"userId" => $userId,
      "appointmentId" => $appointmentId,
      "serviceId" => $serviceId,
      "notificationType" => $notificationType);
    // Encode the payload as JSON
    $payload = json_encode($bodyJson);

    if ($deviceId != null) {
      foreach ($deviceId as $device) {
        // Build the binary notification
        //$msg = chr(0) . pack('n', 32) . pack('H*', $device) . pack('n', strlen($payload)) . $payload;
        $msg = chr(0) . pack('n', 32) . pack('H*', str_replace(' ', '', sprintf('%u', CRC32($device)))) . pack('n', strlen($payload)) . $payload;
        // Send it to the server
        $result = fwrite($fp, $msg, strlen($msg));
        if (!$result){
          echo 'Message not delivered' . PHP_EOL;
        }else{
          echo 'Message successfully delivered' . PHP_EOL;
        }
      }
      // Close the connection to the server
      fclose($fp);
    }
  }









}
?>
