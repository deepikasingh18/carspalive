<?php if( ! defined("BASEPATH")) exit('No direct script access allowed');

class ApiDeviceModel extends CI_Model {

  public function _construct(){
		parent::_construct();
	}

  /* Method to check isUserDeviceTokenAlreadyAdded
     Created By: Manzz Baria
  */
  public function isUserDeviceTokenAlreadyAdded($userId,$deviceToken,$deviceType){
    $this->db->select('Id');
    $this->db->from('devicemaster');
    $this->db->where("userId",$userId);
    $this->db->where("deviceToken",$deviceToken);
    $this->db->where("deviceType",$deviceType);
    $query = $this->db->get();
    if ($query->result() != null) {
      return true;
    }
    return false;
  }
  
  
  /*    Method to deleteDeviceToken
	Created By: Manzz Baria
 */
public function deleteDeviceToken($userId){
    $this->db->where('userId', $userId);
    $this->db->delete('devicemaster');
    if($this->db->affected_rows() > 0){
      return true;
    }else{
      return false;
    }
}

  

  /* Method to check isDeviceTokenAlreadyAdded
     Created By: Manzz Baria
  */
  public function isDeviceTokenAlreadyAdded($deviceToken){
    $this->db->select('Id');
    $this->db->from('devicemaster');
    $this->db->where("deviceToken",$deviceToken);
    $query = $this->db->get();
    if ($query->result() != null) {
      $this->db->where('deviceToken', $deviceToken);
      $this->db->delete('devicemaster');
      if($this->db->affected_rows() > 0){

      }
      return true;
    }
    return false;
  }

  public function isUserAlreadyAdded($userId){
    $this->db->select('Id');
    $this->db->from('devicemaster');
    $this->db->where("userId",$userId);
    $query = $this->db->get();
    if ($query->result() != null) {
      return true;
    }
    return false;
  }

    public function isUserDeviceAlreadyAdded($deviceToken){
    $this->db->select('Id');
    $this->db->from('devicemaster');
    $this->db->where("deviceToken",$deviceToken);
    $query = $this->db->get();
    if ($query->result() != null) {
      return true;
    }
    return false;
  }


  /* Method to addDeviceToken
     Created By: Manzz Baria
  */
    public function addDeviceToken($userId,$deviceToken,$deviceType){
//    $isAdded = $this->isUserDeviceTokenAlreadyAdded($userId,$deviceToken,$deviceType);
//	 $isAdded = $this->isUserAlreadyAdded($userId);
 //   if (!$isAdded) {
      $this->isDeviceTokenAlreadyAdded($deviceToken);
      $data = array('userId'=>$userId,
                    'deviceToken' => $deviceToken,
                    'deviceType' => $deviceType);
      $this->db->insert('devicemaster', $data);
      $insert_Id = $this->db->insert_id();
/*    }
	else
	{
		 $data = array('deviceToken' => $deviceToken);
		 $this->db->where('userId', $userId);
		 $this->db->update('devicemaster', $data);
	}
  */
    return true;
  }

  /* Method to getDeviceTokens
   Created By: Manzz Baria
  */
  public function getDeviceTokens($userId,$language){
      $this->db->select("deviceToken");
      $this->db->from("devicemaster");
	   $strWhere = "userId='".$userId."' and language='".$language."'";
      $this->db->where($strWhere);
     
      $query = $this->db->get();
      return $this->displayDeviceTokens($query->result());
  }
  
  
    /* Method to getAllIphoneDeviceTokens
   Created By: Manzz Baria
  */
  public function getAllIphoneDeviceTokens($deviceType){
      $this->db->select("deviceToken");
      $this->db->from("devicemaster");
     
      $query = $this->db->get();
      return $this->displayDeviceTokens($query->result());
  }


  /* Method to getDeviceTokensFromVanId
   Created By: Manzz Baria
  */
  public function getDeviceTokensFromVanId($vanId,$language){
      $this->db->select("devicemaster.deviceToken");
      $this->db->from("devicemaster");
      $this->db->join('usermaster','devicemaster.userId = usermaster.Id','inner');
      $strWhere = "usermaster.vanId='".$vanId."' and devicemaster.language='".$language."'";
	  $this->db->where($strWhere);
     // $this->db->where('usermaster.vanId',$vanId);
	  // $this->db->where('devicemaster.language',$language);
      $query = $this->db->get();
      return $this->displayDeviceTokens($query->result());
  }

  /* Method to getDeviceTokensOfOperators
   Created By: Manzz Baria
  */
   public function getDeviceTokensOfOperators($appointmentId,$language){

    /*
    SELECT devicemaster.deviceToken from devicemaster INNER JOIN usermaster on devicemaster.userId = usermaster.Id INNER JOIN appoinmentmaster on appoinmentmaster.vanId = usermaster.vanId and appoinmentmaster.requestId = 84 where devicemaster.deviceType = 1
    */
    /*
    select devicemaster.deviceToken from usermaster inner join appoinmentmaster on appoinmentmaster.vanId = usermaster.vanId LEFT join devicemaster on usermaster.Id = devicemaster.userId where appoinmentmaster.requestId = 84 and devicemaster.deviceType = 1
    */
      $this->db->select("devicemaster.deviceToken");
      $this->db->from("devicemaster");
      $this->db->join('usermaster','devicemaster.userId = usermaster.Id','inner');
      $this->db->join('appoinmentmaster','appoinmentmaster.vanId = usermaster.vanId','inner');
     // $this->db->where('appoinmentmaster.Id',$appointmentId);
      $strWhere = "appoinmentmaster.Id='".$appointmentId."' and devicemaster.language='".$language."'";
	  $this->db->where($strWhere);
      $query = $this->db->get();
      return $this->displayDeviceTokens($query->result());
  }

  /* Method to displayDeviceTokens
     Created By: Manzz Baria
  */
  public function displayDeviceTokens($result){
    $deviceObject = null;
    $Object = null;
    if($result != null){
      foreach ($result as $row) {
            $deviceObject[]=$row->deviceToken;
      }
    }
    return $deviceObject;
  }
  
public function getDeviceTokenbyCountry($type,$countrycode,$language,$userIds){
    $this->db->select("devicemaster.deviceToken");
    $this->db->from("devicemaster");
	 $this->db->join('usermaster','devicemaster.userId = usermaster.Id','inner');
	$this->db->where("devicemaster.language",$language);
        $this->db->where_in('usermaster.Id', explode(',', $userIds));
	if($countrycode!=null && $countrycode!="")
	{	
	 	$this->db->where("usermaster.countryCode",$countrycode);
	}
	$this->db->where("usermaster.userType",$type);
    $this->db->where_not_in("usermaster.status",USER_STATUS_BLOCK);
  
    $query = $this->db->get();
    return $this->displayDeviceTokens($query->result());
}
  



}
?>
