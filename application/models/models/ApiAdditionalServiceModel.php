<?php if( ! defined("BASEPATH")) exit('No direct script access allowed');

class ApiAdditionalServiceModel extends CI_Model {

  public function _construct(){
		parent::_construct();
	}

  /* Method to addAdditionalService
     Created By: Manzz Baria
  */
  public function addAdditionalService($name,$additionalDetail){
    $$additionalDetail = str_replace(' ', '', $additionalDetail);
    $isAdded = $this->isAdditionalServiceAlreadyAdded($name);
      if (!$isAdded) {
        $this->load->model("Utility","utility");
        $datetime = $this->utility->getCurrentDate('Y/m/d h:i:s');
        $status = 1;
        $data = array('name'=>$name,
                      'isBlocked'=>UNBLOCKED,
                      'datetime'=>$datetime);
        $this->db->insert('additionalservicemaster', $data);
        $insert_Id = $this->db->insert_id();
        return $this->addAdditionalServiceDetails($insert_Id,$$additionalDetail);
      }else{
        return null;
      }
  }

  /* Method to check isAdditionalServiceAlreadyAdded
     Created By: Manzz Baria
  */
  public function isAdditionalServiceAlreadyAdded($name){
    $this->db->select('Id');
    $this->db->from('additionalservicemaster');
    $this->db->where("name",$name);
    $this->db->where('isBlocked',UNBLOCKED);
    $query = $this->db->get();
    if ($query->result() != null) {
      return true;
    }
    return false;
  }

  /* Method to addAdditionalServiceDetails
     Created By: Manzz Baria
  */
  public function addAdditionalServiceDetails($additionalServiceId,$additionalDetail){
        $myArray = explode(',', $additionalDetail);
        for ($i=0; $i< sizeof($myArray); $i++) {
          $myDetails = explode(':', $myArray[$i]);
          $this->addAdditionalServiceDetailsEntry($additionalServiceId,$myDetails[0],$myDetails[1],$myDetails[2],$myDetails[3]);
        }
        return $this->getAdditionalServicesDetailsByAdditionalServiceId($additionalServiceId);

  }


    /* Method to addAdditionalServiceDetailsEntry
       Created By: Manzz Baria
    */
    public function addAdditionalServiceDetailsEntry($additionalServiceId,$carTypeId,$serviceId,$amount,$duration){

          $isAdded = $this->isAdditionalSercvieAlreadyAddedToService($additionalServiceId,$carTypeId,$serviceId);
          if (!$isAdded) {
            $data = array(
                            'additionalServiceId'=>$additionalServiceId,
                            'serviceId'=>$serviceId,
                            'carTypeId'=>$carTypeId,
                            'amount'=>$amount,
                            'duration'=>$duration);
            $this->db->insert('additionalservicedetail', $data);
            $insert_Id = $this->db->insert_id();
            return $this->getAdditionalServicesDetailById($insert_Id);
          }else{
              return false;
          }
    }


    /* Method to check isAdditionalSercvieAlreadyAddedToService
       Created By: Manzz Baria
    */
    public function isAdditionalSercvieAlreadyAddedToService($additionalServiceId,$carTypeId,$serviceId){
      $this->db->select('Id');
      $this->db->from('additionalservicedetail');
      $this->db->where("additionalServiceId",$additionalServiceId);
      $this->db->where("serviceId",$serviceId);
      $this->db->where("carTypeId",$carTypeId);
      $query = $this->db->get();
      if ($query->result() != null) {
        return true;
      }
      return false;
    }


    /* Method to getAdditionalServicesDetailById
       Created By: Manzz Baria
    */
    public function getAdditionalServicesDetailById($additionalServiceDetailId){
      $this->db->select("Id,additionalServiceId,serviceId,carTypeId,amount,duration");
      $this->db->from("additionalservicedetail");
      $this->db->where('Id',$additionalServiceDetailId);
      $query = $this->db->get();
      return $this->displayAdditionalServicesDetailById($query->result());
    }

    /* Method to displayAdditionalServicesDetailById
         Created By: Manzz Baria
      */
      public function displayAdditionalServicesDetailById($result){
        $Object = null;
        if($result != null){
          $this->load->model("ApiCarModel","carModel");
          $this->load->model("ApiServiceModel","serviceModel");
          foreach ($result as $row) {
            $carType = $this->carModel->getCarTypeById($row->carTypeId);
            $service = $this->serviceModel->getServiceMasterById($row->serviceId);
            $Object = array(
              'Id'=>(int)$row->Id,
              'amount'=>(float)$row->amount,
              'duration'=>(float)$row->duration,
              'carType'=>$carType,
              'service'=>$service
            );
          }
        }
        return $Object;
      }

      /* Method to getAdditionalServiceById
           Created By: Manzz Baria
        */
      public function getAdditionalServiceById($additionalServiceId){
        $this->db->select("Id,name,datetime");
        $this->db->from("additionalservicemaster");
        $this->db->where("Id",$additionalServiceId);
        $query = $this->db->get();
        return $this->displayAdditionalServiceById($query->result());
      }

      /* Method to displayAdditionalServiceById
           Created By: Manzz Baria
        */
        public function displayAdditionalServiceById($result){
          $Object = null;
          if($result != null){
            $this->load->model("Utility","utility");
            foreach ($result as $row) {
              $datetime = $this->utility->timeAgoFormat($row->datetime);
              $additionalDetail = $this->getAdditionalServicesDetailsByAdditionalServiceId($row->Id);
              $Object = array(
                'Id'=>(int)$row->Id,
                'name'=>$row->name,
                'datetime'=>$datetime,
                'additionalDetail'=>$additionalDetail
              );
            }
          }
          return $Object;
        }

        /* Method to getAdditionalServicesDetailsByAdditionalServiceId
           Created By: Manzz Baria
        */
        public function getAdditionalServicesDetailsByAdditionalServiceId($additionalServiceId){
          $this->db->select("Id,additionalServiceId,serviceId,carTypeId,amount,duration");
          $this->db->from("additionalservicedetail");
          $this->db->where('additionalServiceId',$additionalServiceId);
          $query = $this->db->get();
          return $this->displayAdditionalServicesDetailsById($query->result());
        }

        /* Method to displayAdditionalServicesDetailsById
             Created By: Manzz Baria
          */
          public function displayAdditionalServicesDetailsById($result){
            $carObject = null;
            if($result != null){
              $this->load->model("ApiCarModel","carModel");
              $this->load->model("ApiServiceModel","serviceModel");
              foreach ($result as $row) {
                $carType = $this->carModel->getCarTypeById($row->carTypeId);
                $service = $this->serviceModel->getServiceMasterById($row->serviceId);
                $Object = array(
                  'Id'=>(int)$row->Id,
                  'amount'=>(float)$row->amount,
                  'duration'=>$row->duration,
                  'carType'=>$carType,
                  'service'=>$service
                );
                $carObject[]=$Object;
              }
            }
            return $carObject;
          }


    /* Method to updateAdditionalService
       Created By: Manzz Baria
    */
    public function updateAdditionalService($additionalServiceId,$name,$additionalDetail){
        if ($name != "" && $name != null) {
            $data = array(
              'name' => $name
              );
            $this->db->where('Id', $additionalServiceId);
            $this->db->update('additionalservicemaster', $data);
        }

        if ($additionalDetail != "" && $additionalDetail != null) {
           $this->deleteAdditionalServiceDetailsByAdditionalServiceId($additionalServiceId);
           $this->addAdditionalServiceDetails($additionalServiceId,$additionalDetail);
        }

      return $this->getAdditionalServicesDetailsByAdditionalServiceId($additionalServiceId);
    }

    /* Method to deleteAdditionalServiceDetailsByAdditionalServiceId
       Created By: Manzz Baria
    */
    public function deleteAdditionalServiceDetailsByAdditionalServiceId($additionalServiceId){
        $this->db->where('additionalServiceId', $additionalServiceId);
        $this->db->delete('additionalservicedetail');
        if($this->db->affected_rows() > 0){
          return true;
        }else{
          return false;
        }
    }

    /* Method to deleteAdditionalServiceById
       Created By: Manzz Baria
    */
    public function deleteAdditionalServiceById($additionalServiceId){
        $data = array(
                    'isBlocked'=>BLOCKED);
        $this->db->where('Id', $additionalServiceId);
        $this->db->update('additionalservicemaster', $data);
        return true;
    }

    /* Method to getAdditionalServices
       Created By: Manzz Baria
    */
    public function getAdditionalServices(){
      $this->db->select("Id,name,datetime");
      $this->db->from("additionalservicemaster");
      $this->db->where('isBlocked',UNBLOCKED);
      $query = $this->db->get();
      return $this->displayAdditionalServices($query->result());
    }

    /* Method to displayAdditionalServices
         Created By: Manzz Baria
      */
      public function displayAdditionalServices($result){
        $carObject = null;
        if($result != null){
          $this->load->model("Utility","utility");
          foreach ($result as $row) {
            $datetime = $this->utility->timeAgoFormat($row->datetime);
            $additionalDetail = $this->getAdditionalServicesDetailsByAdditionalServiceId($row->Id);
            $Object = array(
              'Id'=>(int)$row->Id,
              'name'=>$row->name,
              'datetime'=>$datetime,
              'additionalDetail'=>$additionalDetail
            );
            $carObject[]=$Object;
          }
        }
        return $carObject;
      }

      /* Method to updateAdditionaServiceDetail
         Created By: Manzz Baria
      */
      public function updateAdditionaServiceDetail($Id,$amount,$duration){
              if ($amount != "") {
                  $data = array(
                    'amount' => $amount
                    );
                  $this->db->where('Id', $Id);
                  $this->db->update('additionalservicedetail', $data);
              }
              if ($duration != "") {
                  $data = array(
                    'duration'=>$duration
                    );
                  $this->db->where('Id', $Id);
                  $this->db->update('additionalservicedetail', $data);
              }
              return true;

      }

      /* Method to removeAdditionalServiceDetail
         Created By: Manzz Baria
      */
      public function removeAdditionalServiceDetail($Id){
          $this->db->where('Id', $Id);
          $this->db->delete('additionalservicedetail');
          if($this->db->affected_rows() > 0){
            return true;
          }else{
            return false;
          }
      }

      /* Method to getServicesByCarType
         Created By: Manzz Baria
      */
      public function getAdditionalServicesByServiceIds($carTypeId,$serviceIds){
        $serviceIdArray = explode(',', $serviceIds);
        $this->db->select("additionalservicedetail.Id,additionalservicemaster.name,additionalservicemaster.datetime,additionalservicedetail.amount,additionalservicedetail.duration");
        $this->db->from("additionalservicemaster");
        $this->db->join('additionalservicedetail','additionalservicemaster.Id = additionalservicedetail.additionalServiceId','inner');
        $this->db->where_in('additionalservicedetail.serviceId',$serviceIdArray);
        $this->db->where('additionalservicedetail.carTypeId',$carTypeId);
        $this->db->where('additionalservicemaster.isBlocked',UNBLOCKED);
        $query = $this->db->get();
        return $this->displayServicesByCarType($query->result(),$carTypeId);
      }

      /* Method to displayServices
           Created By: Manzz Baria
        */
        public function displayServicesByCarType($result,$carTypeId){
          $carObject = null;
          if($result != null){
            $this->load->model("Utility","utility");
            foreach ($result as $row) {
              $datetime = $this->utility->timeAgoFormat($row->datetime);
              $Object = array(
                'Id'=>(int)$row->Id,
                'name'=>$row->name,
                'datetime'=>$datetime,
                'amount'=>(float)$row->amount,
                'duration'=>(int)$row->duration
              );
              $carObject[]=$Object;
            }
          }
          return $carObject;
        }


        /* Method to getAdditionalServicesOfRequest
           Created By: Manzz Baria
        */
        public function getAdditionalServicesOfRequest($requestId,$serviceId,$carTypeId){
          $this->db->select("additionalservicemaster.Id,additionalservicemaster.name,additionalservicedetail.amount,additionalservicedetail.duration");
          $this->db->from("additionalservicemaster");
          $this->db->join('additionalservicedetail','additionalservicemaster.Id = additionalservicedetail.additionalServiceId','inner');
          $this->db->join('requestadditionalservice','additionalservicedetail.Id = requestadditionalservice.additionalServiceDetailId','inner');
          $this->db->where('requestadditionalservice.requestId',$requestId);
          $this->db->where('additionalservicedetail.serviceId',$serviceId);
          $this->db->where('additionalservicedetail.carTypeId',$carTypeId);
          $query = $this->db->get();
          return $this->displayAdditionalServicesOfRequest($query->result());
        }

        /* Method to displayAdditionalServicesOfRequest
             Created By: Manzz Baria
          */
          public function displayAdditionalServicesOfRequest($result){
            $carObject = null;
            if($result != null){
              $this->load->model("ApiCarModel","carModel");
              $this->load->model("ApiServiceModel","serviceModel");
              foreach ($result as $row) {
                $Object = array(
                  'Id'=>(int)$row->Id,
                  'name'=>$row->name,
                  'amount'=>(float)$row->amount,
                  'duration'=>(int)$row->duration
                );
                $carObject[]=$Object;
              }
            }
            return $carObject;
          }



}
?>
