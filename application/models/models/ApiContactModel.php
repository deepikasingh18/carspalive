<?php if( ! defined("BASEPATH")) exit('No direct script access allowed');

class ApiContactModel extends CI_Model {

  public function _construct(){
    parent::_construct();
  }


    /***************************    NEW APIS    ***************************/

    /* Method to addContact
       Created By: Henal Bhandari
    */
    public function addContact($countryId,$address,$countryCode,$mobileNumber,$email){
      $isAdded = $this->isContactAlreadyAdded($countryId,$address,$countryCode,$mobileNumber,$email);
        if (!$isAdded) {

          $data = array('countryId'=>$countryId,
                        'address'=>$address,
                        'countryCode'=>$countryCode,
                        'mobileNumber'=>$mobileNumber,
                        'email'=>$email,
                        'isBlocked'=>UNBLOCKED,);
          $this->db->insert('contactmaster', $data);
          $insert_Id = $this->db->insert_id();
          return $this->getContactById($insert_Id);
        }else{
          return null;
        }
    }

    /* Method to check isContactAlreadyAdded
       Created By: Henal Bhandari
    */
    public function isContactAlreadyAdded($countryId,$address,$countryCode,$mobileNumber,$email){
      $this->db->select('Id');
      $this->db->from('contactmaster');
      $strWhere = "countryId=".$countryId." and address='".$address."' and countryCode=".$countryCode." and mobileNumber=".$mobileNumber." and email='".$email."' and  isBlocked=".UNBLOCKED;
      $this->db->where($strWhere);
      $query = $this->db->get();
      if ($query->result() != null) {
        return true;
      }
      return false;
    }


    /* Method to getContactById
         Created By: Henal Bhandari
      */
    public function getContactById($contactId){
      $this->db->select("Id,countryId,address,countryCode,mobileNumber,email");
      $this->db->from("contactmaster");
      $this->db->where("Id",$contactId);
      $query = $this->db->get();
      return $this->displayContactById($query->result());
    }



    /* Method to displayContactById
         Created By: Henal Bhandari
      */
      public function displayContactById($result){
        $Object = null;
        if($result != null){
          $this->load->model("ApiCurrencyModel","currencyModel");
          $this->load->model("ApiBillModel","billModel");  
          foreach ($result as $row) {
            $country = $this->currencyModel->getCountry($row->countryId);
            $Object = array(
              'Id'=>(int)$row->Id,
              'country'=>$country,
              'address'=>$row->address,
              'countryCode'=>$row->countryCode,
              'mobileNumber'=>$row->mobileNumber,
              'email'=>$row->email
            );
          }
        }
        return $Object;
      }


   
    /* Method to updatecontact
       Created By: Henal Bhandari
    */
    public function updatecontact($contactId,$countryId,$address,$countryCode,$mobileNumber,$email){
         $data = array('countryId'=>$countryId,
                        'address'=>$address,
                        'countryCode'=>$countryCode,
                        'mobileNumber'=>$mobileNumber,
                        'email'=>$email,
                        'isBlocked'=>UNBLOCKED,);
            $this->db->where('Id', $contactId);
            $this->db->update('contactmaster', $data);

       return $this->getContactById($contactId);

    }

  
    /* Method to deleteContactById
       Created By: Henal Bhandari
    */
    public function deleteContactById($contactId){

        $data = array(
                    'isBlocked'=>BLOCKED);
        $this->db->where('Id', $contactId);
        $this->db->update('contactmaster', $data);
        return true;
    }


    /* Method to getAllContactsbycountry
       Created By: Henal Bhandari
    */
    public function getAllContactsbycountry($language=LANGUAGE_ENGLISH)
    {
        $this->db->select("Id,name,nameAr");
        $this->db->from("countrymaster");
        $query = $this->db->get();
        return $this->displayAllcontactsbycountry($query->result(),$language);
    }


      /* Method to displayContacts
         Created By: Henal Bhandari
      */
      public function displayAllcontactsbycountry($result,$language=LANGUAGE_ENGLISH){
        $contactsbycountryObject = null;
        if($result != null){
          foreach ($result as $row) {
        $name = $row->name;
          if ($language == LANGUAGE_ARABIC) {
            $name = $row->nameAr;
          }
            $contacts = $this->getContacts($row->Id);
            $Object = array(
              'countryId'=>(int)$row->Id,
              'name'=>$name,
        'nameAR'=>$row->nameAr,
              'contact'=>$contacts
            );
            $contactsbycountryObject[]=$Object;
          }
        }
        return $contactsbycountryObject;
      }




    /* Method to getContacts
       Created By: Henal Bhandari
    */
    public function getContacts($countryId){
      $this->db->select("Id,countryId,address,countryCode,mobileNumber,email");
      $this->db->from("contactmaster");
      $this->db->where('isBlocked',UNBLOCKED);
      $this->db->where('countryId',$countryId);
      $query = $this->db->get();
      return $this->displayContacts($query->result());
    }

    /* Method to displayContacts
         Created By: Henal Bhandari
      */
      public function displayContacts($result){
        $contactObject = null;
        if($result != null){
          foreach ($result as $row) {
            $Object = array(
              'Id'=>(int)$row->Id,
              'address'=>$row->address,
              'countryCode'=>$row->countryCode,
              'mobileNumber'=>$row->mobileNumber,
              'email'=>$row->email
            );
            $contactObject[]=$Object;
          }
        }
        return $contactObject;
      }
      
      
      
         /*
        Method to getAllCountries
        Created By: Henal Bhandari
      */
    public function getAllCountry($language = LANGUAGE_ENGLISH){
      $this->db->select("Id,name,nameAR,countryCode");
      $this->db->from("countrymaster");
      $query = $this->db->get();
      return $this->displayCountries($query->result(),$language);
    }
    
    public function getcountry($countrycode){
      $this->db->select("Id,name,nameAR,countryCode");
      $this->db->from("countrymaster");
      $this->db->where('countryCode',$countrycode);
      $query = $this->db->get();
      return $this->displayCountrycode($query->result());
    }
    
     public function displayCountrycode($result){
      $Object = null;
      if($result != null){
        foreach ($result as $row) {

          $Object = array(
            'Id'=>(int)$row->Id,
            'name'=>$row->name,
            'nameAR'=>$row->nameAR,
            'countryCode'=>$row->countryCode,
          );
          return $Object;
        }
      }
      return $Object;
    }


  /*
      Method to displayCountries
       Created By: Henal Bhandari
    */
    public function displayCountries($result,$language = LANGUAGE_ENGLISH){
      $countryObject = null;
      if($result != null){
        foreach ($result as $row) {
          $name = $row->name;
          if ($language == LANGUAGE_ARABIC) {
            $name = $row->nameAR;
          }
          $Object = array(
            'Id'=>(int)$row->Id,
            'name'=>$name,
            'nameAR'=>$row->nameAR,
            'countryCode'=>$row->countryCode,
          );
          $countryObject[]=$Object;
        }
      }
      return $countryObject;
    }


    public function getContactcountrycode($countryCode){
      $Object=null;
      $this->db->select("Id,countryId,address,countryCode,mobileNumber,email");
      $this->db->from("contactmaster");
      $this->db->where("countryCode",$countryCode);
      $query = $this->db->get();
      $row = $query->row_array();
         
     if(!$row)
     {
      $this->db->select("Id,countryId,address,countryCode,mobileNumber,email");
        $this->db->from("contactmaster");
        $this->db->where("countryCode",966);
        $query = $this->db->get();
        $row = $query->row_array();
        $Object = array(
             'Id'=>(int)$row['Id'],
              //'country'=>$country,
              'address'=>$row['address'],
              'countryCode'=>$row['countryCode'],
              'mobileNumber'=>$row['mobileNumber'],
              'email'=>$row['email']
          );
          
        
     }
     else
     {
       $Object = array(
             'Id'=>(int)$row['Id'],
              //'country'=>$country,
              'address'=>$row['address'],
              'countryCode'=>$row['countryCode'],
              'mobileNumber'=>$row['mobileNumber'],
              'email'=>$row['email']
          );
   
     }
      

     return $Object;
    
    }
}
?>
