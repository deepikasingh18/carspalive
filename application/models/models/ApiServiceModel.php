<?php if( ! defined("BASEPATH")) exit('No direct script access allowed');

class ApiServiceModel extends CI_Model {

  public function _construct(){
    parent::_construct();
  }


    /***************************    NEW APIS    ***************************/

    /* Method to addService
       Created By: Manzz Baria
    */
    public function addService($name,$nameAR,$description,$descriptionAR,$categoryId,$icon,$details,$language = LANGUAGE_ENGLISH){
      $details = str_replace(' ', '', $details);
      $isAdded = $this->isServiceAlreadyAdded($name);
        if (!$isAdded) {
          $this->load->model("Utility","utility");
          $datetime = $this->utility->getCurrentDate('Y/m/d h:i:s');
          $data = array('name'=>$name,
          'nameAR'=>$nameAR,
                        'description'=>$description,
                          'descriptionAR'=>$descriptionAR,
                        'categoryId'=>$categoryId,
                        'icon'=>$icon,
                        'isBlocked'=>UNBLOCKED,
                        'datetime'=>$datetime);
          $this->db->insert('servicemaster', $data);
          $insert_Id = $this->db->insert_id();
          return $this->addservicedetail($insert_Id,$details,$language);
        }else{
          return null;
        }
    }

    /* Method to check isServiceAlreadyAdded
       Created By: Manzz Baria
    */
    public function isServiceAlreadyAdded($name){
      $this->db->select('Id');
      $this->db->from('servicemaster');
      $this->db->where("name",$name);
      $this->db->where('isBlocked',UNBLOCKED);
      $query = $this->db->get();
      if ($query->result() != null) {
        return true;
      }
      return false;
    }

    /* Method to addservicedetail
       Created By: Manzz Baria
    */
    public function addservicedetail($serviceId,$details,$language = LANGUAGE_ENGLISH){
          $myArray = explode(',', $details);
          for ($i=0; $i< sizeof($myArray); $i++) {
            $myAmount = explode(':', $myArray[$i]);
          //  (CarTypeId:CategoryId:Amount:Duration)
                  $this->saveservicedetail($serviceId,$myAmount[0],$myAmount[1],$myAmount[2],$myAmount[3]);
          }
          return $this->getServiceById($serviceId,$language);
    }

    /* Method to saveservicedetail
       Created By: Manzz Baria
    */
    public function saveservicedetail($serviceId,$carTypeId,$amount,$duration,$variableAmount,$language = LANGUAGE_ENGLISH){

          $isAdded = $this->isServiceDetailAlreadySaved($serviceId,$carTypeId);
          if (!$isAdded) {
            $this->load->model("Utility","utility");
            $datetime = $this->utility->getCurrentDate('Y/m/d h:i:s');
            $data = array(
                            'serviceId'=>$serviceId,
                            'carTypeId'=>$carTypeId,
                            'amount'=>$amount,
                            'duration'=>$duration,
                            'variableAmount'=>$variableAmount);
            $this->db->insert('servicedetail', $data);
            $insert_Id = $this->db->insert_id();

            return $this->getServiceById($serviceId,$language);

          }else{
              return null;
          }
    }

    /* Method to check isServiceDetailAlreadySaved
       Created By: Manzz Baria
    */
    public function isServiceDetailAlreadySaved($serviceId,$carTypeId){
      $this->db->select('Id');
      $this->db->from('servicedetail');
      $this->db->where("serviceId",$serviceId);
      $this->db->where("carTypeId",$carTypeId);
      $query = $this->db->get();
      if ($query->result() != null) {
        return true;
      }
      return false;
    }

    /* Method to getServiceSmallDetailsById
         Created By: Manzz Baria
      */
    public function getServiceById($serviceId,$language = LANGUAGE_ENGLISH){
      $this->db->select("Id,name,nameAR,description,descriptionAR,icon,categoryId,isBlocked,datetime");
      $this->db->from("servicemaster");
      $this->db->where("Id",$serviceId);
      $query = $this->db->get();
      return $this->displayServiceById($query->result(),$language);
    }

    /* Method to displayServiceById
         Created By: Manzz Baria
      */
      public function displayServiceById($result,$language = LANGUAGE_ENGLISH){
        $Object = null;
        if($result != null){
          $this->load->model("Utility","utility");
          $this->load->model("ApiCategoryModel","categoryModel");
          foreach ($result as $row) {
            $datetime = $this->utility->timeAgoFormat($row->datetime,$language);
            $details = $this->getServicesDetailsByServiceId($row->Id,$language);
            $category = $this->categoryModel->getCategoryById($row->categoryId,$language);
            $name = $row->name;
            if ($language == LANGUAGE_ARABIC) {
              $name = $row->nameAR;
            }
            $description = $row->description;
            if ($language == LANGUAGE_ARABIC) {
              $description = $row->descriptionAR;
            }
            $Object = array(
              'Id'=>(int)$row->Id,
              'name'=>$name,
              'nameAR'=>$row->nameAR,
              'description'=>$description,
              'descriptionAR'=>$row->descriptionAR,
              'icon'=>IMAGE_URL.$row->icon,
              'iconThump'=>THUMB_URL.$row->icon,
              'isBlocked'=>(int)$row->isBlocked,
              'datetime'=>$datetime,
              'category'=>$category,
              'details'=>$details
            );
          }
        }
        return $Object;
      }

      /*
           Method to getServiceSmallDetailById
           Created By: Manzz Baria
      */
      public function getServiceSmallDetailById($serviceId,$language = LANGUAGE_ENGLISH){
        $this->db->select("Id,name,nameAR,description,descriptionAR,icon,isBlocked,datetime");
        $this->db->from("servicemaster");
        $this->db->where("Id",$serviceId);
        $query = $this->db->get();
        return $this->displayServiceSmallDetailById($query->result(),$language);
      }

      /* Method to displayServiceSmallDetailById
           Created By: Manzz Baria
        */
        public function displayServiceSmallDetailById($result,$language = LANGUAGE_ENGLISH){
          $Object = null;
          if($result != null){
            $this->load->model("Utility","utility");
            foreach ($result as $row) {
              $datetime = $this->utility->timeAgoFormat($row->datetime,$language);
              $name = $row->name;
              if ($language == LANGUAGE_ARABIC) {
                $name = $row->nameAR;
              }
              $description = $row->description;
              if ($language == LANGUAGE_ARABIC) {
                $description = $row->descriptionAR;
              }
              $Object = array(
                'Id'=>(int)$row->Id,
                'name'=>$name,
                'nameAR'=>$row->nameAR,
                'description'=>$description,
                'descriptionAR'=>$row->descriptionAR,
                'icon'=>IMAGE_URL.$row->icon,
                'iconThump'=>THUMB_URL.$row->icon,
                'isBlocked'=>(int)$row->isBlocked,
                'datetime'=>$datetime
              );
            }
          }
          return $Object;
        }

    /* Method to getServicesDetailsByServiceId
       Created By: Manzz Baria
    */
    public function getServicesDetailsByServiceId($serviceId){
      $this->db->select("Id,carTypeId,amount,duration,variableAmount");
      $this->db->from("servicedetail");
      $this->db->where('serviceId',$serviceId);
      $query = $this->db->get();
      return $this->displayServicesDetailsByServiceId($query->result());
    }

  /* Method to displayServicesDetailsByServiceId
       Created By: Manzz Baria
    */
    public function displayServicesDetailsByServiceId($result){
      $carObject = null;
      if($result != null){
        $this->load->model("ApiCarModel","carModel");
        foreach ($result as $row) {
          $carType = $this->carModel->getCarTypeById($row->carTypeId);
          $Object = array(
            'Id'=>(int)$row->Id,
            'amount'=>(float)$row->amount,
            'duration'=>(int)$row->duration,
            'variableAmount'=>(float)$row->variableAmount,
            'carType'=>$carType
          );
          $carObject[]=$Object;
        }
      }
      return $carObject;
    }

    /* Method to updateServiceMaster
       Created By: Manzz Baria
    */
    public function updateServiceMaster($serviceId,$name,$nameAR,$description,$descriptionAR,$categoryId,$fileUrl,$language = LANGUAGE_ENGLISH){
        if ($name != "" && $name != null) {
            $data = array(
              'name' => $name
              );
            $this->db->where('Id', $serviceId);
            $this->db->update('servicemaster', $data);
        }
        if ($description != "" && $description != null) {
            $data = array(
              'description' => $description
              );
            $this->db->where('Id', $serviceId);
            $this->db->update('servicemaster', $data);
        }
        if ($nameAR != "" && $nameAR != null) {
            $data = array(
              'nameAR' => $nameAR
              );
            $this->db->where('Id', $serviceId);
            $this->db->update('servicemaster', $data);
        }
        if ($descriptionAR != "" && $descriptionAR != null) {
            $data = array(
              'descriptionAR' => $descriptionAR
              );
            $this->db->where('Id', $serviceId);
            $this->db->update('servicemaster', $data);
        }
        if ($fileUrl != "" && $fileUrl != null) {
            $data = array(
              'icon' => $fileUrl
              );
            $this->db->where('Id', $serviceId);
            $this->db->update('servicemaster', $data);
        }
        if ($categoryId != "" && $categoryId != null) {
            $data = array(
              'categoryId' => $categoryId
              );
            $this->db->where('Id', $serviceId);
            $this->db->update('servicemaster', $data);
        }
      return $this->getServiceById($serviceId,$language);

    }

    /* Method to updateServiceDetail
       Created By: Manzz Baria
    */
    public function updateServiceDetail($serviceDetailId,$carTypeId,$amount,$duration,$variableAmount){
        $data = array(
                        'carTypeId' => $carTypeId,
                        'amount' => $amount,
                        'duration' => $duration,
                        'variableAmount' => $variableAmount
                      );
            $this->db->where('Id', $serviceDetailId);
            $this->db->update('servicedetail', $data);

       return $this->getServicesDetailById($serviceDetailId);

    }

    /* Method to getServicesDetailById
       Created By: Manzz Baria
    */
    public function getServicesDetailById($serviceDetailId){
      $this->db->select("Id,carTypeId,amount,duration,variableAmount");
      $this->db->from("servicedetail");
      $this->db->where('Id',$serviceDetailId);
      $query = $this->db->get();
      return $this->displayServicesDetailById($query->result());
    }

  /* Method to displayServicesDetailById
       Created By: Manzz Baria
    */
    public function displayServicesDetailById($result){
      $carObject = null;
      if($result != null){
        $this->load->model("ApiCarModel","carModel");
        foreach ($result as $row) {
          $carType = $this->carModel->getCarTypeById($row->carTypeId);
          $carObject = array(
            'Id'=>(int)$row->Id,
            'amount'=>(float)$row->amount,
            'duration'=>(int)$row->duration,
            'variableAmount'=>(float)$row->variableAmount,
            'carType'=>$carType
          );
        }
      }
      return $carObject;
    }

    /* Method to deleteServiceById
       Created By: Manzz Baria
    */
    public function deleteServiceById($serviceId){

        $data = array(
                    'isBlocked'=>BLOCKED);
        $this->db->where('Id', $serviceId);
        $this->db->update('servicemaster', $data);
        return true;
    }


    /* Method to getAllServices
       Created By: Manzz Baria
    */
    public function getAllServices($language = LANGUAGE_ENGLISH){
      $this->db->select("Id,name,nameAR,description,descriptionAR,icon,categoryId,isBlocked,datetime");
      $this->db->from("servicemaster");
      $this->db->where('isBlocked',UNBLOCKED);
      $query = $this->db->get();
      return $this->displayAllServices($query->result(),$language);
    }

    /* Method to displayAllServices
         Created By: Manzz Baria
      */
      public function displayAllServices($result,$language = LANGUAGE_ENGLISH){
        $carObject = null;
        if($result != null){
          $this->load->model("Utility","utility");
          $this->load->model("ApiCategoryModel","categoryModel");
          foreach ($result as $row) {
            $datetime = $this->utility->timeAgoFormat($row->datetime,$language);
            $category = $this->categoryModel->getCategoryById($row->categoryId,$language);
            $details = $this->getServicesDetailsByServiceId($row->Id);
            $name = $row->name;
            if ($language == LANGUAGE_ARABIC) {
              $name = $row->nameAR;
            }
            $description = $row->description;
            if ($language == LANGUAGE_ARABIC) {
              $description = $row->descriptionAR;
            }
            $Object = array(
              'Id'=>(int)$row->Id,
              'name'=>$name,
              'nameAR'=>$row->nameAR,
              'description'=>$description,
              'descriptionAR'=>$row->descriptionAR,
              'icon'=>IMAGE_URL.$row->icon,
              'iconThump'=>THUMB_URL.$row->icon,
              'isBlocked'=>(int)$row->isBlocked,
              'datetime'=>$datetime,
              'category'=>$category,
              'details'=>$details
            );
            $carObject[]=$Object;
          }
        }
        return $carObject;
      }

      /* Method to getServicesByCategoryForReport
         Created By: Manzz Baria
      */
      public function getServicesByCategoryForReport($categoryId,$serviceId,$vanId,$language = LANGUAGE_ENGLISH){
        $this->db->select("servicemaster.Id,servicemaster.name,servicemaster.nameAR,servicemaster.description,servicemaster.descriptionAR,servicemaster.icon,servicemaster.datetime,,servicemaster.isBlocked");
        $this->db->from("servicemaster");
        $this->db->where('categoryId',$categoryId);
        if ($serviceId > 0 ) {
          $this->db->join('vanservicemaster','servicemaster.Id = vanservicemaster.serviceId','inner');
          $this->db->where('vanservicemaster.serviceId', $serviceId);
          $this->db->where('vanservicemaster.vanId', $vanId);
        }
        $this->db->where('isBlocked',UNBLOCKED);
        $query = $this->db->get();
        return $this->displayServicesByCategory($query->result(),$language);
      }


      /* Method to getServicesByCategory
         Created By: Manzz Baria
      */
      public function getServicesByCategory($categoryId,$language = LANGUAGE_ENGLISH){
        $this->db->select("Id,name,nameAR,description,descriptionAR,icon,isBlocked,datetime");
        $this->db->from("servicemaster");
        $this->db->where('categoryId',$categoryId);
        $this->db->where('isBlocked',UNBLOCKED);
        $query = $this->db->get();
        return $this->displayServicesByCategory($query->result(),$language);
      }


 public function searchgetServicesByCategory($categoryId,$name,$language = LANGUAGE_ENGLISH){
        $this->db->select("Id,name,nameAR,description,descriptionAR,icon,isBlocked,datetime");
        $this->db->from("servicemaster");
        $this->db->where('categoryId',$categoryId);
        $this->db->like('name', $name);
        $this->db->where('isBlocked',UNBLOCKED);
        $query = $this->db->get();
        return $this->displayServicesByCategory($query->result(),$language);
      }
      
      /*   Method to displayServicesByCategory
           Created By: Manzz Baria
        */
           public function displayServicesByCategory($result,$language = LANGUAGE_ENGLISH){
          $carObject = null;
          if($result != null){
            $this->load->model("Utility","utility");
            foreach ($result as $row) {

              $datetime = $this->utility->timeAgoFormat($row->datetime,$language);
              $name = $row->name;
              if ($language == LANGUAGE_ARABIC) {
                $name = $row->nameAR;
              }
              $description = $row->description;
              if ($language == LANGUAGE_ARABIC) {
                $description = $row->descriptionAR;
              }
              $details = $this->getServicesDetailsByServiceId($row->Id);
              $Object = array(
                    'Id'=>(int)$row->Id,
                    'name'=>$name,
                    'nameAR'=>$row->nameAR,
                    'description'=>$description,
                    'descriptionAR'=>$row->descriptionAR,
                    'icon'=>IMAGE_URL.$row->icon,
                    'iconThump'=>THUMB_URL.$row->icon,
                    'isBlocked'=>(int)$row->isBlocked,
                    'datetime'=>$datetime,
                    'details'=>$details
              );
              $carObject[]=$Object;
            }
          }
          return $carObject;
        }

    /* Method to getServicesByCarTypeAndCategory
       Created By: Manzz Baria
    */
    public function getServicesByCarTypeAndCategory($categoryId,$carTypeId,$language = LANGUAGE_ENGLISH){
      $this->db->select("servicemaster.Id,servicemaster.name,servicemaster.nameAR,servicemaster.description,servicemaster.descriptionAR,servicemaster.icon,servicedetail.amount,servicedetail.duration,servicedetail.variableAmount");
      $this->db->from("servicemaster");
      $this->db->join('servicedetail','servicemaster.Id = servicedetail.serviceId','inner');
      $this->db->where('servicemaster.categoryId',$categoryId);
      $this->db->where('servicemaster.isBlocked',UNBLOCKED);
      $this->db->where('servicedetail.carTypeId',$carTypeId);
      $this->db->order_by('servicedetail.amount');
      $query = $this->db->get();
      return $this->displayServicesByCarTypeAndCategory($query->result(),$language);
    }


public function getServicesByCarTypeAndCategories($categoryId,$carTypeId,$language = LANGUAGE_ENGLISH,$latitude,$longitude){
      $this->db->select("servicemaster.Id,servicemaster.name,servicemaster.nameAR,servicemaster.description,servicemaster.descriptionAR,servicemaster.icon,servicedetail.amount,servicedetail.duration,servicedetail.variableAmount");
      $this->db->from("servicemaster");
      $this->db->join('servicedetail','servicemaster.Id = servicedetail.serviceId','inner');
      $this->db->where('servicemaster.categoryId',$categoryId);
      $this->db->where('servicemaster.isBlocked',UNBLOCKED);
      $this->db->where('servicedetail.carTypeId',$carTypeId);
      $this->db->order_by('servicedetail.amount');
      $query = $this->db->get();
      return $this->displayServicesByCarTypeAndCategories($query->result(),$language,$latitude,$longitude);
    }

    /*   Method to displayServicesByCarTypeAndCategory
         Created By: Manzz Baria
      */
      public function displayServicesByCarTypeAndCategory($result,$language = LANGUAGE_ENGLISH){
        $carObject = null;
        if($result != null){
          foreach ($result as $row) {
            $name = $row->name;
            if ($language == LANGUAGE_ARABIC) {
              $name = $row->nameAR;
            }
            $description = $row->description;
            if ($language == LANGUAGE_ARABIC) {
              $description = $row->descriptionAR;
            }
            $Object = array(
                  'Id'=>(int)$row->Id,
                  'name'=>$name,
                  'nameAR'=>$row->nameAR,
                  'description'=>$description,
                  'descriptionAR'=>$row->descriptionAR,
                  'icon'=>IMAGE_URL.$row->icon,
                  'iconThump'=>THUMB_URL.$row->icon,
                  'amount'=>(float)$row->amount,
                  'duration'=>(int)$row->duration,
                  'variableAmount'=>(float)$row->variableAmount,
            );
            $carObject[]=$Object;
          }
        }
        return $carObject;
      }
      
      public function displayServicesByCarTypeAndCategories($result,$language = LANGUAGE_ENGLISH,$latitude,$longitude)
      {
        $carObject = null;
      //  $nearvan=false;
        if($result != null){
         $this->load->model("ApiVanModel","vanModel");
         $this->load->model("Utility","utilityModel");
         $this->load->model("ApiAdminModel","AdminModel");
          foreach ($result as $row) {
          
          $vanObj=$this->vanModel->getVanDetailsByServiceId($row->Id);
       
          //$maxdistance=$this->AdminModel->getMaxDistance();
          foreach($vanObj as $dist)
          {
            $distanceobj=$this->utilityModel->calculetDistance($dist['latitude'],$dist['longitude'],$latitude,$longitude,KM);
            $maxdistance=$this->vanModel->getmaxdistanceById($dist['Id']);

          //  echo $maxdistance ;
            if($distanceobj<=$maxdistance)
            {
              //$nearvan=true;
               $name = $row->name;
            if ($language == LANGUAGE_ARABIC) {
              $name = $row->nameAR;
            }
            $description = $row->description;
            if ($language == LANGUAGE_ARABIC) {
              $description = $row->descriptionAR;
            }
            $Object = array(
                  'Id'=>(int)$row->Id,
                  'name'=>$name,
                  'nameAR'=>$row->nameAR,
                  'description'=>$description,
                  'descriptionAR'=>$row->descriptionAR,
                  'icon'=>IMAGE_URL.$row->icon,
                  'iconThump'=>THUMB_URL.$row->icon,
                  'amount'=>(float)$row->amount,
                  'duration'=>(int)$row->duration,
                  'variableAmount'=>(float)$row->variableAmount,
            );
            $carObject[]=$Object;
              break;
            }
           }
           
          
          }
        }
        return $carObject;
      }

      /* Method getAmountForServiceByCarType
           Created By: Manzz Baria
         */
    public function getAmountForServiceByCarType($serviceId,$carTypeId){
            $this->db->select('amount');
            $this->db->from('servicedetail');
            $this->db->where('carTypeId',$carTypeId);
            $this->db->where('serviceId',$serviceId);
            $query = $this->db->get();
            $result = $query->result();
            $amount = null;
            if($result != null){
              foreach ($result as $row) {
                      $amount = $row->amount;
              }
            }
            return $amount;
    }


    /* Method to get service duration from serviceid
       Created By: Nishit Patel
    */
    public function getServieDuration($serviceId,$carTypeId){
      $this->db->select("Id,serviceId,carTypeId,amount,duration");
      $this->db->from("servicedetail");
      $whereStr = "serviceId = ".$serviceId." AND carTypeId = ".$carTypeId;
      $this->db->where($whereStr);
      $query = $this->db->get();
      $result = $query->result();
      $duration = 0;
      if($result != null){
        foreach ($result as $row) {
          $duration = (int)$row->duration;
        }
      }
      return $duration;
    }

    /* Method to getRequestDuration
       Created By: Nishit Patel
    */
    public function getRequestDuration($requestId){
        $this->db->select("Id");
        $this->db->from("appoinmentmaster");
        $this->db->where('requestId',$requestId);
        $query = $this->db->get();
        $result = $query->result();
        $duration = 0;
        $appointmentDuration = 0;
        if($result != null){
          foreach ($result as $row) {
            $appointmentDuration = $this->getAppointmentDuration($row->Id);
            $duration = $duration + (int)$appointmentDuration;
          }
        }
        return $duration;
    }


    /* Method to getAppointmentDuration
       Created By: Manzz Baria
    */
    public function getAppointmentDuration($appointmentId){
      $totalDuration = 0;

      $this->db->select("duration");
      $this->db->from("appoinmentmaster");
      $whereStr = "Id = ".$appointmentId;
      $this->db->where($whereStr);
      $query = $this->db->get();
      $result = $query->result();
      if($result != null){
        foreach ($result as $row) {
          $totalDuration = (int)$row->duration;
        }
      }

    /*    $this->load->model("ApiCategoryModel","categoryModel");
        $this->load->model("ApiAddressModel","adModel");
        $this->load->model("ApiAppoinmnetModel","aptModel");
        $this->load->model("Utility","utilityModel");
        $categoryId = $this->categoryModel->getCategoryIdOfAppointment($appointmentId);

        $carTypeId = $this->getCarTypeIdFromAppointment($appointmentId);
        $this->db->select("servicedetail.duration");
        $this->db->from("appoinmentdetail");
        $this->db->join("servicedetail","appoinmentdetail.serviceId = servicedetail.serviceId","inner");
        $this->db->where('servicedetail.carTypeId',$carTypeId);
        $this->db->where('appoinmentdetail.appointmentId',$appointmentId);
        $query = $this->db->get();

        $result = $query->result();
        $duration = 0;
        $totalDuration = 0;
        if($result != null){
          foreach ($result as $row) {
            $duration = $duration + (int)$row->duration;
          }
        }
        $totalDuration = $duration;

        if ($categoryId == GATEGORY_TOW) {
            $categoryTravelDuration = 0;
            $addressAppointment = $this->adModel->getAddressByAppointmentId($appointmentId);
            $destinationAddress = $this->aptModel->getAppointmentDestinationAddress($appointmentId);
            $travelingTime = $this->utilityModel->GetDrivingDistance($destinationAddress["destinationLatitude"],$destinationAddress["destinationLongitude"],$addressAppointment["latitude"],$addressAppointment["longitude"]);
            $categoryTravelDuration = (int)$travelingTime['time'];
            $totalDuration = $totalDuration + $categoryTravelDuration;
        }
        */
        return $totalDuration;
    }


    /* Method to getCarTypeIdFromAppointment
       Created By: Manzz Baria
    */
    public function getCarTypeIdFromAppointment($appointmentId){
      $this->db->select("requestmaster.carTypeId");
      $this->db->from("appoinmentmaster");
      $this->db->join("requestmaster","appoinmentmaster.requestId = requestmaster.Id","inner");
      $this->db->where('appoinmentmaster.Id',$appointmentId);
      $query = $this->db->get();
      $result = $query->result();
      $carTypeId = 0;
      if($result != null){
        foreach ($result as $row) {
          $carTypeId = $row->carTypeId;
            //echo "CarType: ".$row->carTypeId;
        }
      }

      return $carTypeId;
    }

    /* Method to removeServiceForCarTypeById
       Created By: Manzz Baria
    */
    public function removeServiceForCarTypeById($Id){
        $this->db->where('Id', $Id);
        $this->db->delete('servicedetail');
        if($this->db->affected_rows() > 0){
          return true;
        }else{
          return false;
        }
    }

    /* Method get getServiceNameByServiceId
     Created By: Manzz Baria
     */
     public function getServiceNameByServiceId($serviceId){
       $this->db->select('name');
       $this->db->from('servicemaster');
       $this->db->where('Id',$serviceId);
       $query = $this->db->get();
       $result = $query->result();
       $name = null;
       if($result != null){
         foreach ($result as $row) {
           $name = $row->name;
         }
       }
       return $name;
     }

   /* Method to getRemainServicesByVanId
      Created By: Manzz Baria
   */
   public function getRemainServicesByVanId($vanId,$language = LANGUAGE_ENGLISH){
      $this->db->select("Id,name,nameAR,description,descriptionAR,icon,categoryId,isBlocked,datetime");
     $this->db->from("servicemaster");
     $this->db->where('isBlocked',UNBLOCKED);
     $query = $this->db->get();
     return $this->displayRemainServicesByVanId($query->result(),$vanId,$language);
   }

   /* Method to displayServices
        Created By: Manzz Baria
     */
     public function displayRemainServicesByVanId($result,$vanId,$language = LANGUAGE_ENGLISH){
       $carObject = null;
       if($result != null){
         $this->load->model("ApiVanModel","vanModel");
         $this->load->model("ApiCategoryModel","categoryModel");
        $this->load->model("Utility","utility");
         foreach ($result as $row) {
           $isAdded = $this->vanModel->isServiceAlreadyAddedToVan($vanId,$row->Id);
           $isBlocked=$this->categoryModel->isCategoryBlocked($row->categoryId);
           if (!$isAdded) {
                if(!$isBlocked)
                {
                 $datetime = $this->utility->timeAgoFormat($row->datetime,$language);
                 $category = $this->categoryModel->getCategoryById($row->categoryId,$language);
                 $details = $this->getServicesDetailsByServiceId($row->Id);
                 $name = $row->name;
                 if ($language == LANGUAGE_ARABIC) {
                   $name = $row->nameAR;
                 }
                 $description = $row->description;
                 if ($language == LANGUAGE_ARABIC) {
                   $description = $row->descriptionAR;
                 }
                 $Object = array(
                   'Id'=>(int)$row->Id,
                   'name'=>$name,
                   'nameAR'=>$row->nameAR,
                   'description'=>$description,
                   'descriptionAR'=>$row->descriptionAR,
                   'icon'=>IMAGE_URL.$row->icon,
                   'iconThump'=>THUMB_URL.$row->icon,
                   'isBlocked'=>(int)$row->isBlocked,
                   'datetime'=>$datetime,
                   'category'=>$category,
                   'details'=>$details
                 );
                $carObject[]=$Object;
             }
           }
         }
       }
       return $carObject;
     }

     /* Method to getServicesDetailsById
        Created By: Manzz Baria
     */
     public function getServicesDetailsByIdAndCarTypeId($serviceId,$carTypeId){
       $this->db->select("Id,carTypeId,amount,duration,variableAmount");
       $this->db->from("servicedetail");
       $this->db->where('serviceId',$serviceId);
       $this->db->where('carTypeId',$carTypeId);
       $query = $this->db->get();
       return $this->displayServicesDetailsByIdAndCarTypeId($query->result());
     }

     /* Method to displayServicesDetailsById
          Created By: Manzz Baria
       */
       public function displayServicesDetailsByIdAndCarTypeId($result){
         $carObject = null;
         if($result != null){
           $this->load->model("ApiCarModel","carModel");
           foreach ($result as $row) {
             $carType = $this->carModel->getCarTypeById($row->carTypeId);
             $carObject = array(
               'Id'=>(int)$row->Id,
               'amount'=>(float)$row->amount,
               'duration'=>$row->duration,
               'variableAmount'=>(float)$row->variableAmount,
               'carType'=>$carType
             );

           }
         }
         return $carObject;
       }


     /* Method to getServicesByAppointmentId
        Created By: Manzz Baria
     */
     public function getServicesByAppointmentId($appointmentId,$carTypeId,$language = LANGUAGE_ENGLISH){
         $this->db->select("servicemaster.Id,servicemaster.name,servicemaster.nameAR,servicemaster.description,servicemaster.descriptionAR,servicemaster.icon,appoinmentdetail.status,appoinmentdetail.reason,appoinmentdetail.serviceStartTime,appoinmentdetail.serviceEndTime,servicemaster.datetime,servicedetail.amount,servicedetail.duration,servicedetail.variableAmount");
         $this->db->from("servicemaster");
         $this->db->join('appoinmentdetail','servicemaster.Id = appoinmentdetail.serviceId','inner');
         $this->db->join('servicedetail','servicemaster.Id = servicedetail.serviceId','inner');
         $this->db->where('servicedetail.carTypeId',$carTypeId);
         $this->db->where('appoinmentdetail.appointmentId',$appointmentId);
         $query = $this->db->get();
         return $this->displayServiceStatusByServiceId($query->result(),$appointmentId,true,$language);
     }

     /* Method to getServiceStatusByServiceId
        Created By: Manzz Baria
     */
     public function getServiceStatusByServiceId($serviceId,$appointmentId,$language = LANGUAGE_ENGLISH){
         $carTypeId = $this->getCarTypeIdFromAppointment($appointmentId);
         $this->db->select("servicemaster.Id,servicemaster.name,servicemaster.nameAR,servicemaster.description,servicemaster.descriptionAR,servicemaster.icon,appoinmentdetail.status,appoinmentdetail.reason,appoinmentdetail.serviceStartTime,appoinmentdetail.serviceEndTime,servicemaster.datetime,servicedetail.amount,servicedetail.duration,servicedetail.variableAmount");
         $this->db->from("servicemaster");
         $this->db->join('appoinmentdetail','servicemaster.Id = appoinmentdetail.serviceId','inner');
         $this->db->join('servicedetail','servicemaster.Id = servicedetail.serviceId','inner');
         $this->db->where('appoinmentdetail.serviceId',$serviceId);
         $this->db->where('servicedetail.carTypeId',$carTypeId);
         $this->db->where('appoinmentdetail.appointmentId',$appointmentId);
         $query = $this->db->get();
         return $this->displayServiceStatusByServiceId($query->result(),$appointmentId,false,$language);
     }

    /* Method to displayServiceStatusByServiceId
            Created By: Manzz Baria
    */
    public function displayServiceStatusByServiceId($result,$appointmentId,$isArray = true,$language = LANGUAGE_ENGLISH){
           $serviceObject = null;
           $Object = null;
           if($result != null){
             $this->load->model("Utility","utility");
             $this->load->model("ApiCurrencyModel","currencyModel");
             foreach ($result as $row) {
               $datetime = $this->utility->timeAgoFormat($row->datetime,$language);
               $serviceStartTime = $row->serviceStartTime;
               $serviceEndTime = $row->serviceEndTime;
               if ($serviceStartTime == '0000-00-00 00:00:00') {
                  $serviceStartTime = '--';
               }
               if ($serviceEndTime == '0000-00-00 00:00:00') {
                  $serviceEndTime = '--';
               }

               $name = $row->name;
               if ($language == LANGUAGE_ARABIC) {
                 $name = $row->nameAR;
               }
               $description = $row->description;
               if ($language == LANGUAGE_ARABIC) {
                 $description = $row->descriptionAR;
               }
               
               $amount = $this->currencyModel->getAmountByAppointmentCurrency($appointmentId,$row->amount);
               $currency = $this->currencyModel->getAmountCurrencyOfAppointment($appointmentId,$language);
               $currencyAR = $this->currencyModel->getAmountCurrencyAROfAppointment($appointmentId);
               $Object = array(
                 'Id'=>(int)$row->Id,
                 'name'=>$name,
                 'nameAR'=>$row->nameAR,
                 'description'=>$description,
                 'descriptionAR'=>$row->descriptionAR,
                 'icon'=>IMAGE_URL.$row->icon,
                 'iconThump'=>THUMB_URL.$row->icon,
                 'status'=>(int)$row->status,
                 'reason'=>$row->reason,
                 'amount'=>$amount,
                 'currency'=>$currency,
                 'currencyAR'=>$currencyAR,
                 'duration'=>$row->duration,
                 'variableAmount'=>$row->variableAmount,
                 'serviceStartTime'=>$serviceStartTime,
                 'serviceEndTime'=>$serviceEndTime,
                 'datetime'=>$datetime
               );
               $serviceObject[] = $Object;
             }
           }

           if ($isArray) {
                return $serviceObject;
           }else{
             return $Object;
           }
        }



























    /***************************    OLD APIS    ***************************/

    /* Method to getServiceDurationByRequestId
       Created By: Nishit Patel
    */
    public function getServiceDurationByRequestId($requestId){
      $this->db->select("servicedetail.duration");
      $this->db->from("requestmaster");
      $this->db->join("requestdetails","requestmaster.Id = requestdetails.requestId","inner");
      $this->db->join("servicedetail","servicedetail.serviceId = requestdetails.serviceId AND servicedetail.carTypeId = requestmaster.carTypeId","inner");
      $whereStr = "requestmaster.Id = ".$requestId;
      $this->db->where($whereStr);
      $query = $this->db->get();
      $result = $query->result();
      $duration = 0;
      $totalDuration = 0;
      if($result != null){
        foreach ($result as $row) {
          $duration = $duration + (int)$row->duration;
        }
      }
      $additionalServiceDuration = $this->getAdditionalServiceDurationByRequestId($requestId);
      $totalDuration =(int) ($duration + $additionalServiceDuration);
      return $totalDuration;
    }



  /* Method to getAdditionalServiceDurationByRequestId
     Created By: Manzz Baria
  */
  public function getAdditionalServiceDurationByRequestId($requestId){
    $this->db->select("additionalservicedetail.duration");
    $this->db->from("requestadditionalservice");
    $this->db->join("additionalservicedetail","additionalservicedetail.Id = requestadditionalservice.additionalServiceDetailId","inner");
    $whereStr = "requestadditionalservice.requestId = ".$requestId;
    $this->db->where($whereStr);
    $query = $this->db->get();
    $result = $query->result();
    $duration = 0;
    if($result != null){
      foreach ($result as $row) {
        $duration = $duration + (int)$row->duration;
      }
    }
    return $duration;
  }


  /* Method to get total duration of services
     Created By: Nishit Patel
  */
  public function getTotalDurationOfServices($userId){
    $this->db->select("sum(servicedetail.duration) as duration,requestmaster.Id");
    $this->db->from("requestmaster");
    $this->db->join("requestdetails","requestmaster.Id = requestdetails.requestId","inner");
    $this->db->join("servicedetail","requestdetails.serviceId = servicedetail.serviceId AND servicedetail.carTypeId = requestmaster.carTypeId","inner");
    $this->db->where("requestmaster.userId = ".$userId);
    $this->db->group_by("requestmaster.Id");
    $query = $this->db->get();
    $result = $query->result();
    $duration = 0;
    if($result != null){
      foreach ($result as $row) {
        $duration = (int)$row->duration;
      }
    }
    return $duration;
  }

  /* Method to deleteServiceById
     Created By: Manzz Baria
  */
  public function deleteservicedetailById($serviceId){
      $this->db->where('serviceId', $serviceId);
      $this->db->delete('servicedetail');
      if($this->db->affected_rows() > 0){
        return true;
      }else{
        return false;
      }
  }


  /* Method to getServicesByRequestId
     Created By: Manzz Baria
  */
  public function getServicesByRequestId($requestId,$carTypeId){
    $this->db->select("servicemaster.Id,servicemaster.name,servicemaster.description,servicedetail.amount,servicedetail.duration,requestdetails.status,requestdetails.reason,requestdetails.serviceStartTime,requestdetails.serviceEndTime,servicemaster.datetime");
    $this->db->from("servicemaster");
    $this->db->join('requestdetails','servicemaster.Id = requestdetails.serviceId','inner');
    $this->db->join('servicedetail','servicemaster.Id = servicedetail.serviceId','inner');
    $this->db->where('requestdetails.requestId',$requestId);
    $this->db->where('servicedetail.carTypeId',$carTypeId);
    $query = $this->db->get();
    return $this->displayServicesByRequestId($query->result(),$carTypeId,$requestId);
  }

  /* Method to displayServices
       Created By: Manzz Baria
    */
    public function displayServicesByRequestId($result,$carTypeId,$requestId){
      $carObject = null;
      if($result != null){
        $this->load->model("Utility","utility");
        $this->load->model("ApiAdditionalServiceModel","additionalServiceModel");
        foreach ($result as $row) {
          $datetime = $this->utility->timeAgoFormat($row->datetime);
          $serviceStartTime = $row->serviceStartTime;
          $serviceEndTime = $row->serviceEndTime;
          if ($serviceStartTime == '0000-00-00 00:00:00') {
             $serviceStartTime = '--';
          }
          if ($serviceEndTime == '0000-00-00 00:00:00') {
             $serviceEndTime = '--';
          }
          //$additionalService =$this->getAddionalServiceTemp();
          $additionalService =$this->additionalServiceModel->getAdditionalServicesOfRequest($requestId,$row->Id,$carTypeId);
          $duration = $this->getServieDuration($row->Id,$carTypeId);
          $Object = array(
            'Id'=>(int)$row->Id,
            'name'=>$row->name,
            'description'=>$row->description,
            'amount'=>(float)$row->amount,
            'duration'=>(int)$row->duration,
            'status'=>(int)$row->status,
            'reason'=>$row->reason,
            'duration'=>(float)$duration,
            'serviceStartTime'=>$serviceStartTime,
            'serviceEndTime'=>$serviceEndTime,
            'datetime'=>$datetime,
            'additionalService'=>$additionalService
          );
          $carObject[]=$Object;
        }
      }
      return $carObject;
    }


  /* Method to updateService
     Created By: Manzz Baria
  */
  public function updateService($serviceId,$name,$description,$details){
      if ($name != "" && $name != null) {
          $data = array(
            'name' => $name
            );
          $this->db->where('Id', $serviceId);
          $this->db->update('servicemaster', $data);
      }
      if ($description != "" && $description != null) {
          $data = array(
            'description' => $description
            );
          $this->db->where('Id', $serviceId);
          $this->db->update('servicemaster', $data);
      }
      if ($details != "" && $details != null) {
         $this->deleteservicedetailById($serviceId);
         $this->addservicedetail($serviceId,$details);
      }

    return $this->getServiceSmallDetailsById($serviceId);
  }

  /* Method to updateService
     Created By: Manzz Baria
  */
  public function changeServiceStatus($serviceId,$status){

          $data = array(
            'status' => $status
            );
          $this->db->where('Id', $serviceId);
          $this->db->update('servicemaster', $data);

    return true;
  }


          /* Method to getServiceMasterById
               Created By: Manzz Baria
            */
          public function getServiceMasterById($serviceId){
            $this->db->select("Id,name,description,status,datetime");
            $this->db->from("servicemaster");
            $this->db->where("Id",$serviceId);
            $query = $this->db->get();
            return $this->displayServiceMasterById($query->result());
          }

          /* Method to displayServiceMasterById
               Created By: Manzz Baria
            */
            public function displayServiceMasterById($result){
              $Object = null;
              if($result != null){
                $this->load->model("Utility","utility");
                foreach ($result as $row) {
                  $datetime = $this->utility->timeAgoFormat($row->datetime);
                  $Object = array(
                    'Id'=>(int)$row->Id,
                    'name'=>$row->name,
                    'description'=>$row->description,
                    'status'=>(int)$row->status,
                    'datetime'=>$datetime
                  );
                }
              }
              return $Object;
            }



}
?>
