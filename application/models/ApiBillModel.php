<?php if( ! defined("BASEPATH")) exit('No direct script access allowed');

class ApiBillModel extends CI_Model {

  public function _construct(){
		parent::_construct();
	}

    /* Method to check isBillAlreadyAdded
       Created By: Manzz Baria
    */
    public function isBillAlreadyAdded($appointmentId){
      $this->db->select('Id');
      $this->db->from('billmaster');
      $this->db->where("appointmentId",$appointmentId);
      $query = $this->db->get();
      if ($query->result() != null) {
        return true;
      }
      return false;
    }

    /*
       Method get getAmountdOfAppointment
       Created By: Manzz Baria
     */
     public function getAmountdOfAppointment($appointmentId){
       $this->db->select('totalAmount');
       $this->db->from('billmaster');
       $this->db->where('appointmentId',$appointmentId);
       $query = $this->db->get();
       $result = $query->result();
       $totalAmount = null;
       if($result != null){
         foreach ($result as $row) {
           $totalAmount = (float)$row->totalAmount;
		   $totalAmount = round($totalAmount);
         }
       }
       return $totalAmount;
     }
     


/*
       Method to saveBill
       Created By: Manzz Baria
*/
 public function saveBill($appointmentId,$localAmount,$totalAmount,$currencyId,$status=BILL_STATUS_PENDING){
   $isAdded = $this->isBillAlreadyAdded($appointmentId);
   $this->load->model("ApiCurrencyModel","currencyModel");
   $exchangeRate=$this->currencyModel->getExchangeRateOfCurrency($currencyId);
   if (!$isAdded) {
         $data = array(
                     'appointmentId'=>$appointmentId,
					 'localAmount'=>$localAmount,
                     'totalAmount'=>$totalAmount,
                     'currencyId'=>$currencyId,
					 'exchangeRate'=> $exchangeRate,
                     'status'=>$status);
         $this->db->insert('billmaster', $data);
         //$insert_Id = $this->db->insert_id();
         return true;
   }

  }
  /* Method to getBillDetailsByAppointmentId
     Created By: Manzz Baria
  */
  
  public function getBillDetailsByAppointmentId($appointmentId,$language=LANGUAGE_ENGLISH){
    $this->db->select("billmaster.Id,billmaster.appointmentId,billmaster.status,billmaster.paidBy,billmaster.discount,billmaster.tax,billmaster.localAmount,billmaster.totalAmount,billmaster.exchangeRate,billmaster.currencyId,currencymaster.currency,currencymaster.currencyAR");
    $this->db->from("billmaster");
    $this->db->join('currencymaster','billmaster.currencyId = currencymaster.Id','inner');
    $this->db->where('billmaster.appointmentId', $appointmentId);
    $query = $this->db->get();
    return $this->displayBillDetailsByAppointmentId($query->result(),$language);
  }

  /* Method to displayBillDetailsByAppointmentId
       Created By: Manzz Baria
    */
    public function displayBillDetailsByAppointmentId($result,$language=LANGUAGE_ENGLISH){
      $Object = null;
      if($result != null){
        foreach ($result as $row) {
          $currency = $row->currency;
            if($language==LANGUAGE_ARABIC)
            {
            	$currency=$row->currencyAR;
            }
          $Object = array(
            'Id'=>(int)$row->Id,
            'status'=>(int)$row->status,
            'currency'=>$currency,
            'paidBy'=>(int)$row->paidBy,
            'discount'=>(float)$row->discount,
            'tax'=>(float)$row->tax,
			'localAmount'=>(float)$row->localAmount,
            'totalAmount'=>(float)$row->totalAmount,
            'exchangeRate'=>$row->exchangeRate,
          );
        }
      }
      return $Object;
    }
    /* Method to cancelBill
       Created By: Manzz Baria
    */
    public function cancelBill($appointmentId){
        $this->db->where('appointmentId', $appointmentId);
        $this->db->delete('billmaster');
        if($this->db->affected_rows() > 0){
          return true;
        }else{
          return false;
        }
    }

    /*
       Method to updateBillAmount
       Created By: Manzz Baria
    */
    public function updateBillAmount($appointmentId,$localAmount,$amount){
      if ($amount > 0) {
          $data = array(
		             'localAmount' => $localAmount,
                      'totalAmount' => $amount
                      );
      }
      $this->db->where('appointmentId', $appointmentId);
      $this->db->update('billmaster', $data);

    }


}
?>
