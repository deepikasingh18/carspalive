<?php if( ! defined("BASEPATH")) exit('No direct script access allowed');

class ApiAppoinmnetModel extends CI_Model {

  public function _construct(){
    parent::_construct();
  }

/***************************    NEW APIS    ***************************/
  /* Get Appointment by DATe
      Created By: Nishit Pate
  */
  public function getAppointmentByDate($appointmentDate,$vanId){
    $this->load->model("Utility","utility");
    $this->db->select("appoinmentmaster.Id, requestmaster.carTypeId, appoinmentmaster.requestId,appoinmentmaster.vanId, appoinmentmaster.status,appoinmentmaster.reason,
    DATE_FORMAT(appoinmentmaster.appointmentDatetime, '%d-%m-%Y') as date, DATE_FORMAT(appoinmentmaster.appointmentDatetime, '%H') as Hour,
    DATE_FORMAT(appoinmentmaster.appointmentDatetime, '%i') as Mint, DATE_FORMAT(appoinmentmaster.appointmentDatetime, '%H:%i') as Time,
    appoinmentmaster.appointmentDatetime,DATE_FORMAT(appoinmentmaster.operatorDatetime, '%H:%i') as operatorTime,
    appoinmentmaster.operatorDatetime, appoinmentmaster.statusTime,addressmaster.Id as addId,addressmaster.address,
    addressmaster.latitude,addressmaster.longitude,addressmaster.userId");
    $this->db->from("appoinmentmaster");
    $this->db->join("requestmaster","appoinmentmaster.requestId = requestmaster.Id","inner");
    $this->db->join("addressmaster","requestmaster.addressId = addressmaster.Id","inner");
    //$whereStr = "DATE_FORMAT(appoinmentmaster.appointmentDatetime, '%d-%m-%Y') = '".$appointmentDate."'";
    $whereStr = "DATE_FORMAT(appoinmentmaster.appointmentDatetime, '%d-%m-%Y') = '".$appointmentDate."' AND appoinmentmaster.vanId = '".$vanId."'";
    $this->db->where($whereStr);
    $this->db->where_not_in('appoinmentmaster.status',REQUEST_STATUS_REJECTED);
    $this->db->where_not_in('appoinmentmaster.status',REQUEST_STATUS_FINISH);
    $this->db->order_by("appoinmentmaster.appointmentDatetime");
    $query = $this->db->get();
    $result = $query->result();
    if($result != null){
      $appointmentList = [];
      foreach ($result as $row) {
        $statusTime = $this->utility->timeAgoFormat($row->statusTime);
        //$array = array('Id'=>(int)$row->Id,'requestId'=>(int)$row->requestId,'vanId'=>(int)$row->vanId,'status'=>$row->status,'Date'=>$row->date,'Time'=>$row->Time,'dateTime'=>$row->dateTime,'statusTime'=>$statusTime);
        $array = array('appointmentId'=>(int)$row->Id,'requestId'=>(int)$row->requestId,'carTypeId'=>(int)$row->carTypeId,'vanId'=>(int)$row->vanId,'status'=>$row->status,'Date'=>$row->date,'Time'=>$row->Time,'operatorTime'=>$row->operatorTime,
        'Hour'=>$row->Hour,'Mint'=>$row->Mint,'dateTime'=>$row->appointmentDatetime,'operatorDatetime'=>$row->operatorDatetime,'latitude'=>(double)$row->latitude,'longitude'=>(double)$row->longitude);
        $appointmentList [] = $array;
      }
      return $appointmentList;
    }
    return null;

  }

  function getAddedTime($mainTime,$addedMint){
    $mainDateTime = new DateTime($mainTime);
    $finalTime = $mainDateTime->add(new DateInterval('PT' . $addedMint . 'M'));
  //  return $mainDateTime->format('H').":".$mainDateTime->format('i').":00";
     $datetime_from = (new DateTime($mainTime))->add(DateInterval::createFromDateString($addedMint.' minutes'))->format('Y-m-d H:i:s');
     return $datetime_from;
  }

  /*
    Method check isAppointmentPossible
    Created By: Manzz Baria
   */
   public function isAppointmentPossible($appointmentDate,$duration,$vanId){

     //SELECT * FROM `commentmaster` WHERE commentDate >= '2016-07-23 06:36:26'  ORDER BY `commentmaster`.`commentDate`  ASC

     //SELECT * FROM `commentmaster` WHERE commentDate <= '2016-07-23 06:36:26' ORDER BY `commentmaster`.`commentDate` DESC

     $this->load->model("ApiServiceModel","servicePossibleModel");

     $queryGreater = $this->db->query("select Id, appointmentDatetime  FROM appoinmentmaster WHERE appointmentDatetime >= '".$appointmentDate."' and vanId = '".$vanId."' ORDER BY appointmentDatetime  ASC limit 1");
     $queryLess = $this->db->query("select Id, appointmentDatetime  FROM appoinmentmaster WHERE appointmentDatetime <= '".$appointmentDate."' and vanId = '".$vanId."' ORDER BY appointmentDatetime  DESC limit 1");
     $resultGreater = $queryGreater->result();
     $resultLess = $queryLess->result();

     $appointmentId = 0;
     $appointmentDuration = 0;
     $fixAppointmentDatetime = "";
     $newAppointmentDatetime = "";
     if($resultGreater != null){
       foreach ($resultGreater as $rowGreater) {
          $appointmentId = $rowGreater->Id;
          $fixAppointmentDatetime =  $rowGreater->appointmentDatetime;
       }
       $newAppointmentDatetime = $this->getAddedTime($appointmentDate,$duration);

       if(strtotime($fixAppointmentDatetime) <= strtotime($newAppointmentDatetime)){
        // echo "hello";
              return false;
       }

     }

     if($resultLess != null){
       foreach ($resultLess as $rowLess) {
          $appointmentId = $rowLess->Id;
          $fixAppointmentDatetime =  $rowLess->appointmentDatetime;
       }
       $appointmentDuration = $this->servicePossibleModel->getAppointmentDuration($appointmentId);
       $possibleAppointmentDatetime = $this->getAddedTime($fixAppointmentDatetime,$appointmentDuration);
       if(strtotime($possibleAppointmentDatetime) > strtotime($appointmentDate)){
              return false;
       }

     }
     return true;
   }

    /*
     Method check deleyAppointmentList
     Created By: Manzz Baria
    */
    public function deleyAppointmentList(){

        $this->load->model("Utility","utility");
         $this->load->library('mylibrary');
        $currentTime = $this->utility->getCurrentDate('Y-m-d h:i:s');
        
        //echo $currentTime;
//echo "select Id  FROM appoinmentmaster WHERE appointmentDatetime < '".$currentTime."' and  status = 1 ORDER BY appointmentDatetime  ASC";
        $queryDelay = $this->db->query("SELECT Id,requestId,appointmentDateTime FROM `appoinmentmaster` where status=1 order by appointmentDateTime ASC");
        $resultDelay = $queryDelay->result();
//print_r($resultDelay);
        if($resultDelay != null){
          foreach ($resultDelay as $rowDelay) {
          
       //  print_r($rowDelay);
           
           $countrycodegetquery=$this->db->query("SELECT countryCode FROM usermaster as um,requestmaster as rm where rm.Id=".$rowDelay->requestId." and um.Id=rm.userId");
           $resultcountrycode = $countrycodegetquery->row();
         //  print_r( $resultcountrycode );
          $currentTime = $this->utility->getCurrentDate('Y-m-d h:i:s');
      // echo "beforecurrentTime".$currentTime."<br>";
           if($resultcountrycode->countryCode == "974" || $resultcountrycode->countryCode== "965" || $resultcountrycode->countryCode == "973" || $resultcountrycode->countryCode == "961" || $resultcountrycode->countryCode == "966" )
           {
            $currentTime= date("Y-m-d h:i:s", strtotime('+3 hours', strtotime($currentTime)));
           }
           else if($resultcountrycode->countryCode == "971" || $resultcountrycode->countryCode == "968")
           {
            $currentTime= date("Y-m-d h:i:s", strtotime('+4 hours', strtotime($currentTime)));
           }
      //     echo "rowDelay->appointmentDateTime".$rowDelay->appointmentDateTime."<br>";
      //     echo "aftercurrentTime".$currentTime."<br>";
           if($rowDelay->appointmentDateTime < $currentTime)
           {
      //    echo "success email send to rowDelay->Id".$rowDelay->Id."<br>";
            $url = base_url()."Api/Email/appointmentDeley";
           // echo $url;
            $param = array('appointmentId' => "".$rowDelay->Id );
            $this->mylibrary->do_in_background($url, $param);
           }
          }

return true;
        }
        
        return false;

    }


   /*
     Method check isAppointmentPossible
     Created By: Manzz Baria
    */
    public function isAppointmentPossibleNew($appointmentDate,$duration,$vanId){

      //SELECT * FROM `commentmaster` WHERE commentDate >= '2016-07-23 06:36:26'  ORDER BY `commentmaster`.`commentDate`  ASC

      //SELECT * FROM `commentmaster` WHERE commentDate <= '2016-07-23 06:36:26' ORDER BY `commentmaster`.`commentDate` DESC

      $this->load->model("ApiServiceModel","servicePossibleModel");

      $queryGreater = $this->db->query("select Id, appointmentDatetime  FROM appoinmentmaster WHERE appointmentDatetime >= '".$appointmentDate."' and vanId = '".$vanId."' and status !='".REQUEST_STATUS_REJECTED."' and status != '".REQUEST_STATUS_FINISH."' ORDER BY appointmentDatetime  ASC limit 1");
      $queryLess = $this->db->query("select Id, appointmentDatetime  FROM appoinmentmaster WHERE appointmentDatetime <= '".$appointmentDate."' and vanId = '".$vanId."'and status != '".REQUEST_STATUS_REJECTED."' and status != '".REQUEST_STATUS_FINISH."' ORDER BY appointmentDatetime  DESC limit 1");
      $resultGreater = $queryGreater->result();
      $resultLess = $queryLess->result();

      $appointmentId = 0;
      $appointmentDuration = 0;
      $fixAppointmentDatetime = "";
      $newAppointmentDatetime = "";
      if($resultGreater != null){
        foreach ($resultGreater as $rowGreater) {
           $appointmentId = $rowGreater->Id;
           $fixAppointmentDatetime =  $rowGreater->appointmentDatetime;
        //   echo $fixAppointmentDatetime;
        }
        $newAppointmentDatetime = $this->getAddedTime($appointmentDate,$duration);
        //echo $appointmentDate."     next: ";
      //  echo $newAppointmentDatetime;
        if(strtotime($fixAppointmentDatetime) <= strtotime($newAppointmentDatetime)){
         // echo "hello";
               return false;
        }

      }
      if($resultLess != null){
        foreach ($resultLess as $rowLess) {
           $appointmentId = $rowLess->Id;
           $fixAppointmentDatetime =  $rowLess->appointmentDatetime;
        }
        $appointmentDuration = $this->servicePossibleModel->getAppointmentDuration($appointmentId);
        $possibleAppointmentDatetime = $this->getAddedTime($fixAppointmentDatetime,$appointmentDuration);
        if(strtotime($possibleAppointmentDatetime) > strtotime($appointmentDate)){
               return false;
        }

      }
      return true;

    }

  /*
        Method to addAppoinment
        Created By: Manzz Baria
  */
  public function addAppoinment($suffixId,$userId,$requestId,$categoryId,$vanId,$datetime,$serviceIds,$operatorDatetime,$destinationAddress,$destinationLatitude,$destinationLongitude,$duration){

            $this->load->model("Utility","utility");
            $statusTime = $this->utility->getCurrentDate('Y/m/d h:i:s');
            $status = REQUEST_STATUS_PENDING;
            $data = array(
                        'suffixId'=>$suffixId,
                        'requestId'=>$requestId,
                        'categoryId'=>$categoryId,
                        'vanId'=>$vanId,
                        'status'=>$status,
                        'statusTime'=>$statusTime,
                        'operatorDatetime'=>$operatorDatetime,
                        'appointmentDatetime'=>$datetime,
                        'duration'=>$duration,
                        'destinationAddress'=>$destinationAddress,
                        'destinationLatitude'=>$destinationLatitude,
                        'destinationLongitude'=>$destinationLongitude);
            $this->db->insert('appoinmentmaster', $data);
            $insert_Id = $this->db->insert_id();
            $this->saveAppoinmentServices($insert_Id,$serviceIds);
            $this->appointmentAction($insert_Id,$userId,$status,"");
            $this->load->library('mylibrary');
            $url = base_url()."Api/Email/fixAppointmentConfirmation";
            $param = array('appointmentId' => "".$insert_Id );
            $this->mylibrary->do_in_background($url, $param);
            return $insert_Id;

   }

   /*
         Method to saveAppoinmentServices
         Created By: Manzz Baria
   */
   public function saveAppoinmentServices($appointmentId,$serviceIds){
         $serviceIdsArray = explode(',', $serviceIds);
         for ($i=0; $i< sizeof($serviceIdsArray); $i++) {
           $this->addAppoinmentService($appointmentId,$serviceIdsArray[$i]);
         }
    }

    /* Method to addAppoinmentService
       Created By: Manzz Baria
    */
    public function addAppoinmentService($appointmentId,$serviceId){
          $status = REQUEST_STATUS_PENDING;
          $data = array(
                          'appointmentId'=>$appointmentId,
                          'serviceId'=>$serviceId,
                          'status'=>$status);
          $this->db->insert('appoinmentdetail', $data);
          $insert_Id = $this->db->insert_id();
    }

   

    /*
        Method get getUserIdOfAppointment
        Created By: Manzz Baria
     */
     public function getUserIdOfAppointment($appointmentId){

       $this->db->select('requestmaster.userId');
       $this->db->from('requestmaster');
       $this->db->join("appoinmentmaster","requestmaster.Id = appoinmentmaster.requestId","inner");
       $this->db->where('appoinmentmaster.Id',$appointmentId);
       $query = $this->db->get();
       $result = $query->result();

       $userId = null;
       if($result != null){
         foreach ($result as $row) {
           $userId = $row->userId;
         }
       }
       return $userId;
     }

     /*
         Method get getSuffixIdfAppointment
         Created By: Manzz Baria
      */
      public function getSuffixIdfAppointment($appointmentId){
        $this->db->select('suffixId');
        $this->db->from('appoinmentmaster');
        $this->db->where('Id',$appointmentId);
        $query = $this->db->get();
        $result = $query->result();
        $suffixId = null;
        if($result != null){
          foreach ($result as $row) {
            $suffixId = $row->suffixId;
          }
        }
        return $suffixId;
      }
     /*
         Method get getRequestIdOfAppointment
         Created By: Manzz Baria
      */
      public function getRequestIdOfAppointment($appointmentId){
        $this->db->select('requestId');
        $this->db->from('appoinmentmaster');
        $this->db->where('Id',$appointmentId);
        $query = $this->db->get();
        $result = $query->result();
        $requestId = null;
        if($result != null){
          foreach ($result as $row) {
            $requestId = $row->requestId;
          }
        }
        return $requestId;
      }

      /* Method to getTotalPagesOfAdminAppointmentsByTwoDates
         Created By: Manzz Baria
      */
      public function getTotalPagesOfAdminAppointmentsByTwoDates($fromDate,$toDate){
        $fromDate = $fromDate." 00:00:00";
        $toDate = $toDate." 23:59:59";
        $this->db->select("Id,requestId,suffixId,categoryId,vanId,status,reason,appointmentDatetime,operatorDatetime,statusTime,destinationAddress,destinationLatitude,destinationLongitude");
        $this->db->from("appoinmentmaster");
        $this->db->where('appointmentDatetime >=', $fromDate);
        $this->db->where('appointmentDatetime <=', $toDate);
        $this->db->order_by("appointmentDatetime","ASC");
        $query = $this->db->count_all_results();
        $result  = $query / 20;
        return ceil($result);

      }
      /* Method to getAdminAppointmentsByTwoDates
         Created By: Manzz Baria
      */
      public function getAdminAppointmentsByTwoDates($fromDate,$toDate,$pageIndex,$language =LANGUAGE_ENGLISH){
        $fromDate = $fromDate." 00:00:00";
        $toDate = $toDate." 23:59:59";
        $this->db->select("Id,requestId,suffixId,categoryId,vanId,status,reason,appointmentDatetime,operatorDatetime,statusTime,destinationAddress,destinationLatitude,destinationLongitude");
        $this->db->from("appoinmentmaster");
        $this->db->where('appointmentDatetime >=', $fromDate);
        $this->db->where('appointmentDatetime <=', $toDate);
        $this->db->order_by("appointmentDatetime","ASC");
        $pageNo = "00";
        if ($pageIndex > 0) {
          $pageIndex = $pageIndex * 2;
          $pageNo = $pageIndex.'0';
        }
        $this->db->limit(20,$pageNo);
        $query = $this->db->get();
        return $this->displayAppointments($query->result(),true,$language);
      }


      /* Method to getTotalPagesOfAppointmentsByUserIdBetweenTwoDates
         Created By: Manzz Baria
      */
      public function getTotalPagesOfAppointmentsByUserIdBetweenTwoDates($userId,$fromDate,$toDate){
        $fromDate = $fromDate." 00:00:00";
        $toDate = $toDate." 23:59:59";
        $this->db->select("appoinmentmaster.Id,appoinmentmaster.requestId,appoinmentmaster.suffixId,appoinmentmaster.categoryId,appoinmentmaster.vanId,appoinmentmaster.status,appoinmentmaster.reason,appoinmentmaster.appointmentDatetime,appoinmentmaster.operatorDatetime,appoinmentmaster.statusTime,appoinmentmaster.destinationAddress,appoinmentmaster.destinationLatitude,appoinmentmaster.destinationLongitude");
        $this->db->from("appoinmentmaster");
        $this->db->join("requestmaster","appoinmentmaster.requestId = requestmaster.Id","inner");
        $this->db->where('requestmaster.userId', $userId);
        $this->db->where('appoinmentmaster.appointmentDatetime >=', $fromDate);
        $this->db->where('appoinmentmaster.appointmentDatetime <=', $toDate);
        $this->db->where_not_in('appoinmentmaster.status',REQUEST_STATUS_PENDING);
          $this->db->where_not_in('appoinmentmaster.status',REQUEST_STATUS_IN_PROGRESS);
          $this->db->where_not_in('appoinmentmaster.status',REQUEST_STATUS_ON_THE_WAY);
        $this->db->order_by("appoinmentmaster.appointmentDatetime","DESC");
        $query = $this->db->count_all_results();
        $result  = $query / 20;
        return ceil($result);

      }

      /* Method to getAppointmentsByUserIdBetweenTwoDates
         Created By: Manzz Baria
      */
      public function getAppointmentsByUserIdBetweenTwoDates($userId,$fromDate,$toDate,$pageIndex,$language =LANGUAGE_ENGLISH){
        $fromDate = $fromDate." 00:00:00";
        $toDate = $toDate." 23:59:59";
        $this->db->select("appoinmentmaster.Id,appoinmentmaster.requestId,appoinmentmaster.suffixId,appoinmentmaster.categoryId,appoinmentmaster.vanId,appoinmentmaster.status,appoinmentmaster.reason,appoinmentmaster.appointmentDatetime,appoinmentmaster.operatorDatetime,appoinmentmaster.statusTime,appoinmentmaster.destinationAddress,appoinmentmaster.destinationLatitude,appoinmentmaster.destinationLongitude");
        $this->db->from("appoinmentmaster");
        $this->db->join("requestmaster","appoinmentmaster.requestId = requestmaster.Id","inner");
        $this->db->where('requestmaster.userId', $userId);
        $this->db->where('appoinmentmaster.appointmentDatetime >=', $fromDate);
        $this->db->where('appoinmentmaster.appointmentDatetime <=', $toDate);
        $this->db->where_not_in('appoinmentmaster.status',REQUEST_STATUS_PENDING);
          $this->db->where_not_in('appoinmentmaster.status',REQUEST_STATUS_IN_PROGRESS);
          $this->db->where_not_in('appoinmentmaster.status',REQUEST_STATUS_ON_THE_WAY);
        $this->db->order_by("appoinmentmaster.appointmentDatetime","DESC");
        $pageNo = "00";
        if ($pageIndex > 0) {
          $pageIndex = $pageIndex * 2;
          $pageNo = $pageIndex.'0';
        }
        $this->db->limit(20,$pageNo);
        $query = $this->db->get();
        return $this->displayAppointments($query->result(),true,$language);
      }



  public function getRequestsBySupplier($status,$supplierId,$pageIndex,$language=LANGUAGE_ENGLISH){
        $this->db->select("appoinmentmaster.Id,appoinmentmaster.requestId,appoinmentmaster.suffixId,appoinmentmaster.categoryId,appoinmentmaster.vanId,appoinmentmaster.status,appoinmentmaster.reason,appoinmentmaster.appointmentDatetime,appoinmentmaster.operatorDatetime,appoinmentmaster.statusTime,appoinmentmaster.destinationAddress,appoinmentmaster.destinationLatitude,appoinmentmaster.destinationLongitude");
        $this->db->from("appoinmentmaster");
        if ($status == REQUEST_STATUS_IN_PROGRESS) {
          $strWhere = "(appoinmentmaster.status ='".$status."' or appoinmentmaster.status ='".REQUEST_STATUS_ON_THE_WAY."')";
          $this->db->where($strWhere,null,false);
        }else if ($status == REQUEST_STATUS_REFUND) {
            $this->db->join('billmaster','appoinmentmaster.Id = billmaster.appointmentId','inner');
            $this->db->where('billmaster.status',BILL_STATUS_REFUND);
        }else{
            $this->db->where('appoinmentmaster.status',$status);
        }
      $this->db->join('vanmaster','appoinmentmaster.vanId = vanmaster.Id','inner');
        $this->db->where('vanmaster.ownerId', $supplierId);
        $this->db->order_by("appointmentDatetime","ASC");
        $pageNo = "00";
        if ($pageIndex > 0) {
          $pageIndex = $pageIndex * 2;
          $pageNo = $pageIndex.'0';
        }
        $this->db->limit(20,$pageNo);
        $query = $this->db->get();
        return $this->displayAppointments($query->result(),true,$language);
      }

      /* Method to getTotalPagesOfAppointmentsByOperatorBetweenTwodDates
         Created By: Manzz Baria
      */
      public function getTotalPagesOfAppointmentsByOperatorBetweenTwodDates($userId,$fromDate,$toDate){
        $fromDate = $fromDate." 00:00:00";
        $toDate = $toDate." 23:59:59";
        $this->db->select("appoinmentmaster.Id,appoinmentmaster.requestId,appoinmentmaster.suffixId,appoinmentmaster.categoryId,appoinmentmaster.vanId,appoinmentmaster.status,appoinmentmaster.reason,appoinmentmaster.appointmentDatetime,appoinmentmaster.operatorDatetime,appoinmentmaster.statusTime,appoinmentmaster.destinationAddress,appoinmentmaster.destinationLatitude,appoinmentmaster.destinationLongitude");
        $this->db->from("appoinmentmaster");
        $this->db->join("requestmaster","appoinmentmaster.requestId = requestmaster.Id","inner");
        $this->db->join('usermaster','appoinmentmaster.vanId = usermaster.vanId','inner');
        $this->db->where('usermaster.Id', $userId);
        $this->db->where('appoinmentmaster.appointmentDatetime >=', $fromDate);
        $this->db->where('appoinmentmaster.appointmentDatetime <=', $toDate);
        $this->db->where_not_in('appoinmentmaster.status',REQUEST_STATUS_PENDING);
          $this->db->where_not_in('appoinmentmaster.status',REQUEST_STATUS_IN_PROGRESS);
          $this->db->where_not_in('appoinmentmaster.status',REQUEST_STATUS_ON_THE_WAY);
        $this->db->order_by("appoinmentmaster.appointmentDatetime","DESC");
        $query = $this->db->count_all_results();
        $result  = $query / 20;
        return ceil($result);

      }

      /* Method to getAppointmentsByOperatorBetweenTwodDates
         Created By: Manzz Baria
      */
      public function getAppointmentsByOperatorBetweenTwodDates($userId,$fromDate,$toDate,$pageIndex,$language =LANGUAGE_ENGLISH){
        $fromDate = $fromDate." 00:00:00";
        $toDate = $toDate." 23:59:59";
        $this->db->select("appoinmentmaster.Id,appoinmentmaster.requestId,appoinmentmaster.suffixId,appoinmentmaster.categoryId,appoinmentmaster.vanId,appoinmentmaster.status,appoinmentmaster.reason,appoinmentmaster.appointmentDatetime,appoinmentmaster.operatorDatetime,appoinmentmaster.statusTime,appoinmentmaster.destinationAddress,appoinmentmaster.destinationLatitude,appoinmentmaster.destinationLongitude");
        $this->db->from("appoinmentmaster");
        $this->db->join("requestmaster","appoinmentmaster.requestId = requestmaster.Id","inner");
        $this->db->join('usermaster','appoinmentmaster.vanId = usermaster.vanId','inner');
        $this->db->where('usermaster.Id', $userId);
        $this->db->where('appoinmentmaster.appointmentDatetime >=', $fromDate);
        $this->db->where('appoinmentmaster.appointmentDatetime <=', $toDate);
        $this->db->where_not_in('appoinmentmaster.status',REQUEST_STATUS_PENDING);
        $this->db->where_not_in('appoinmentmaster.status',REQUEST_STATUS_IN_PROGRESS);
        $this->db->where_not_in('appoinmentmaster.status',REQUEST_STATUS_ON_THE_WAY);
        $this->db->order_by("appoinmentmaster.appointmentDatetime","DESC");
        $pageNo = "00";
        if ($pageIndex > 0) {
          $pageIndex = $pageIndex * 2;
          $pageNo = $pageIndex.'0';
        }
        $this->db->limit(20,$pageNo);
        $query = $this->db->get();
        return $this->displayAppointments($query->result(),true,$language);
      }

      /* Method to getTotalPagesOfAppointmentsByOperator
         Created By: Manzz Baria
      */
      public function getTotalPagesOfAppointmentsByOperator($userId){
        $this->db->select("appoinmentmaster.Id,appoinmentmaster.requestId,appoinmentmaster.suffixId,appoinmentmaster.categoryId,appoinmentmaster.vanId,appoinmentmaster.status,appoinmentmaster.reason,appoinmentmaster.appointmentDatetime,appoinmentmaster.operatorDatetime,appoinmentmaster.statusTime,appoinmentmaster.destinationAddress,appoinmentmaster.destinationLatitude,appoinmentmaster.destinationLongitude");
        $this->db->from("appoinmentmaster");
        $this->db->join("requestmaster","appoinmentmaster.requestId = requestmaster.Id","inner");
        $this->db->join('usermaster','appoinmentmaster.vanId = usermaster.vanId','inner');
        $this->db->where('usermaster.Id', $userId);
        $this->db->where_not_in('appoinmentmaster.status',REQUEST_STATUS_FINISH);
        $this->db->where_not_in('appoinmentmaster.status',REQUEST_STATUS_REJECTED);
        $this->db->order_by("appoinmentmaster.appointmentDatetime","ASC");
        $query = $this->db->count_all_results();
        $result  = $query / 20;
        return ceil($result);

      }

    /* Method to getAppointmentsByOperator
       Created By: Manzz Baria
    */
    public function getAppointmentsByOperator($userId,$pageIndex,$language=LANGUAGE_ENGLISH){
        $this->db->select("appoinmentmaster.Id,appoinmentmaster.requestId,appoinmentmaster.suffixId,appoinmentmaster.categoryId,appoinmentmaster.vanId,appoinmentmaster.status,appoinmentmaster.reason,appoinmentmaster.appointmentDatetime,appoinmentmaster.operatorDatetime,appoinmentmaster.statusTime,appoinmentmaster.destinationAddress,appoinmentmaster.destinationLatitude,appoinmentmaster.destinationLongitude");
        $this->db->from("appoinmentmaster");
        $this->db->join("requestmaster","appoinmentmaster.requestId = requestmaster.Id","inner");
        $this->db->join('usermaster','appoinmentmaster.vanId = usermaster.vanId','inner');
        $this->db->where('usermaster.Id', $userId);
        $this->db->where_not_in('appoinmentmaster.status',REQUEST_STATUS_FINISH);
        $this->db->where_not_in('appoinmentmaster.status',REQUEST_STATUS_REJECTED);
        $this->db->order_by("appoinmentmaster.appointmentDatetime","ASC");
        $pageNo = "00";
        if ($pageIndex > 0) {
          $pageIndex = $pageIndex * 2;
          $pageNo = $pageIndex.'0';
        }
        $this->db->limit(20,$pageNo);
        $query = $this->db->get();
        return $this->displayAppointments($query->result(),true,$language);
    }

      /* Method to getTotalPagesOfAppointmentsByUserId
         Created By: Manzz Baria
      */
      public function getTotalPagesOfAppointmentsByUserId($userId){
        $this->db->select("appoinmentmaster.Id,appoinmentmaster.requestId,appoinmentmaster.suffixId,appoinmentmaster.categoryId,appoinmentmaster.vanId,appoinmentmaster.status,appoinmentmaster.reason,appoinmentmaster.appointmentDatetime,appoinmentmaster.operatorDatetime,appoinmentmaster.statusTime,appoinmentmaster.destinationAddress,appoinmentmaster.destinationLatitude,appoinmentmaster.destinationLongitude");
        $this->db->from("appoinmentmaster");
        $this->db->join("requestmaster","appoinmentmaster.requestId = requestmaster.Id","inner");
        $this->db->where('requestmaster.userId', $userId);
        $this->db->where_not_in('appoinmentmaster.status',REQUEST_STATUS_FINISH);
        $this->db->where_not_in('appoinmentmaster.status',REQUEST_STATUS_REJECTED);
        $this->db->order_by("appoinmentmaster.appointmentDatetime","ASC");
        $query = $this->db->count_all_results();
        $result  = $query / 20;
        return ceil($result);

      }

      /* Method to getAppointmentsByUserId
         Created By: Manzz Baria
      */
      public function getAppointmentsByUserId($userId,$pageIndex,$language=LANGUAGE_ENGLISH){
      //  $this->load->model("Utility","uty");
      //  $currentDate = $this->uty->getCurrentDate('Y-m-d');
      //  $currentDate = $currentDate ." 00:00:00";
        $this->db->select("appoinmentmaster.Id,appoinmentmaster.requestId,appoinmentmaster.suffixId,appoinmentmaster.categoryId,appoinmentmaster.vanId,appoinmentmaster.status,appoinmentmaster.reason,appoinmentmaster.appointmentDatetime,appoinmentmaster.operatorDatetime,appoinmentmaster.statusTime,appoinmentmaster.destinationAddress,appoinmentmaster.destinationLatitude,appoinmentmaster.destinationLongitude");
        $this->db->from("appoinmentmaster");
        $this->db->join("requestmaster","appoinmentmaster.requestId = requestmaster.Id","inner");
        $this->db->where('requestmaster.userId', $userId);
        $this->db->where_not_in('appoinmentmaster.status',REQUEST_STATUS_FINISH);
        $this->db->where_not_in('appoinmentmaster.status',REQUEST_STATUS_REJECTED);
        $this->db->order_by("appoinmentmaster.appointmentDatetime","ASC");
    //    $this->db->where('appoinmentmaster.appointmentDatetime >=', $currentDate);
        $pageNo = "00";
        if ($pageIndex > 0) {
          $pageIndex = $pageIndex * 2;
          $pageNo = $pageIndex.'0';
        }
        $this->db->limit(20,$pageNo);
        $query = $this->db->get();
        return $this->displayAppointments($query->result(),true,$language);
      }

      /* Method to getTotalPagesOfAppointmentsByStatus
         Created By: Manzz Baria
      */
      public function getTotalPagesOfAppointmentsByStatus($status){
        $this->db->select("appoinmentmaster.Id,appoinmentmaster.requestId,appoinmentmaster.suffixId,appoinmentmaster.categoryId,appoinmentmaster.vanId,appoinmentmaster.status,appoinmentmaster.reason,appoinmentmaster.appointmentDatetime,appoinmentmaster.operatorDatetime,appoinmentmaster.statusTime,appoinmentmaster.destinationAddress,appoinmentmaster.destinationLatitude,appoinmentmaster.destinationLongitude");
        $this->db->from("appoinmentmaster");
        if ($status == REQUEST_STATUS_IN_PROGRESS) {
          $strWhere = "appoinmentmaster.status ='".$status."' or appoinmentmaster.status ='".REQUEST_STATUS_ON_THE_WAY."'";
          $this->db->where($strWhere,null,false);
        }else if ($status == REQUEST_STATUS_REFUND) {
            $this->db->join('billmaster','appoinmentmaster.Id = billmaster.appointmentId','inner');
            $this->db->where('billmaster.status',BILL_STATUS_REFUND);
        }else{
            $this->db->where('appoinmentmaster.status',$status);
        }
        $this->db->order_by("appointmentDatetime","ASC");
        $query = $this->db->count_all_results();
        $result  = $query / 20;
        return ceil($result);
      }

      /* Method to getAppointmentsByStatus
         Created By: Manzz Baria
      */
      public function getAppointmentsByStatus($status,$pageIndex,$language=LANGUAGE_ENGLISH){
        $this->db->select("appoinmentmaster.Id,appoinmentmaster.requestId,appoinmentmaster.suffixId,appoinmentmaster.categoryId,appoinmentmaster.vanId,appoinmentmaster.status,appoinmentmaster.reason,appoinmentmaster.appointmentDatetime,appoinmentmaster.operatorDatetime,appoinmentmaster.statusTime,appoinmentmaster.destinationAddress,appoinmentmaster.destinationLatitude,appoinmentmaster.destinationLongitude");
        $this->db->from("appoinmentmaster");
        if ($status == REQUEST_STATUS_IN_PROGRESS) {
          $strWhere = "appoinmentmaster.status ='".$status."' or appoinmentmaster.status ='".REQUEST_STATUS_ON_THE_WAY."'";
          $this->db->where($strWhere,null,false);
        }else if ($status == REQUEST_STATUS_REFUND) {
            $this->db->join('billmaster','appoinmentmaster.Id = billmaster.appointmentId','inner');
            $this->db->where('billmaster.status',BILL_STATUS_REFUND);
        }else{
            $this->db->where('appoinmentmaster.status',$status);
        }
        $this->db->order_by("appointmentDatetime","ASC");
        $pageNo = "00";
        if ($pageIndex > 0) {
          $pageIndex = $pageIndex * 2;
          $pageNo = $pageIndex.'0';
        }
        $this->db->limit(20,$pageNo);
        $query = $this->db->get();
        return $this->displayAppointments($query->result(),true,$language);
      }

      public function searchRequest($status,$pageIndex,$language=LANGUAGE_ENGLISH,$suffix){
     //    echo "sdf".$suffix;
        $this->db->select("appoinmentmaster.Id,appoinmentmaster.requestId,appoinmentmaster.suffixId,appoinmentmaster.categoryId,appoinmentmaster.vanId,appoinmentmaster.status,appoinmentmaster.reason,appoinmentmaster.appointmentDatetime,appoinmentmaster.operatorDatetime,appoinmentmaster.statusTime,appoinmentmaster.destinationAddress,appoinmentmaster.destinationLatitude,appoinmentmaster.destinationLongitude");
         $this->db->from("appoinmentmaster");
         
       // $this->db->where('suffixId',$suffix);

        if ($status == REQUEST_STATUS_IN_PROGRESS) 
        {
            $strWhere = "appoinmentmaster.status ='".$status."' or appoinmentmaster.status ='".REQUEST_STATUS_ON_THE_WAY."'";
            $this->db->where($strWhere,null,false);
        }
        else if ($status == REQUEST_STATUS_REFUND) 
        {
            $this->db->join('billmaster','appoinmentmaster.Id = billmaster.appointmentId','inner');
            $this->db->where('billmaster.status',BILL_STATUS_REFUND);
        }
        else
        {
            $this->db->where('appoinmentmaster.status',$status);
        }
         $this->db->where('appoinmentmaster.requestId',$suffix); 
        $query = $this->db->get();
        return $this->displayAppointments($query->result(),true,$language);
      }
      
     /*
        Method to getAppointmentsByRequestId
        Created By: Manzz Baria
     */
     public function getAppointmentsByRequestId($requestId,$language=LANGUAGE_ENGLISH){
       $this->db->select("Id,requestId,suffixId,categoryId,vanId,status,reason,appointmentDatetime,operatorDatetime,statusTime,destinationAddress,destinationLatitude,destinationLongitude");
       $this->db->from("appoinmentmaster");
       $this->db->where('requestId', $requestId);
       $this->db->order_by("appointmentDatetime","ASC");
       $query = $this->db->get();
       return $this->displayAppointments($query->result(),true,$language);
     }

     /* Method to getAppointmentDetailsById
        Created By: Manzz Baria
     */
     public function getAppointmentDetailsById($appointmentId,$language=LANGUAGE_ENGLISH){
       $this->db->select("Id,requestId,suffixId,categoryId,vanId,status,reason,appointmentDatetime,operatorDatetime,statusTime,destinationAddress,destinationLatitude,destinationLongitude");
       $this->db->from("appoinmentmaster");
       $this->db->where('Id', $appointmentId);
       $this->db->order_by("appointmentDatetime","ASC");
       $query = $this->db->get();
       return $this->displayAppointments($query->result(),false,$language);
     }


     /* Method to displayAppointments
          Created By: Manzz Baria
      */
    public function displayAppointments($result,$isArray,$language=LANGUAGE_ENGLISH){
       $appointmentObject = null;
       $Object= null;
       if($result != null){
         $this->load->model("Utility","utility");
         $this->load->model("ApiCategoryModel","categoryModel");
         $this->load->model("ApiVanOperatorModel","vanOperatorModel");
         $this->load->model("ApiBillModel","billModel");
         $this->load->model("ApiRateModel","rateModel");

         $this->load->model("ApiAddressModel","addressModel");
         $this->load->model("ApiCarModel","carModel");
         $this->load->model("ApiUserModel","userModel");
         $this->load->model("ApiVanModel","vModel");
         $this->load->model("ApiServiceModel","srvModel");
      //   print_r($result);
         foreach ($result as $row) {
           $statusTime = $this->utility->timeAgoFormat($row->statusTime,$language);
           $categoryObj = $this->categoryModel->getCategoryById($row->categoryId,$language);

           $operatorObj = $this->vanOperatorModel->getVanOperatorsByVanId($row->vanId);
           $billObj = $this->billModel->getBillDetailsByAppointmentId($row->Id,$language);
           $rateObj = $this->rateModel->getRateOfRequestId($row->Id);
           $appointmentStatusObj = $this->getAppointmentActions($row->Id,$language);

           $carTypeObject = $this->carModel->getCarTypeByAppointmentId($row->Id,$language);
           $serviceObj = $this->srvModel->getServicesByAppointmentId($row->Id,$carTypeObject['Id'],$language);
           $carObject = $this->carModel->getCarDetailsByAppointmentId($row->Id);
           $addressObject = $this->addressModel->getAddressByAppointmentId($row->Id);
           $userObject = $this->userModel->getUserDetailsByAppointmentId($row->Id);
           $vanObject = $this->vModel->getVanLocationById($row->vanId);

          // $requestId = $this->getRequestIdOfAppointment($row->Id);
           $suffix = $this->utility->getSuffix($row->requestId,$row->suffixId);
           //'operator'=>$operatorObj,
            //$statusdatetime= $this->utility->changeDateFromat($row->statusTime,'d-m-Y h:i');
    //  $operatorDatetime= $this->utility->changeDateFromat($row->operatorDatetime,'d-m-Y h:i');
    //  $appointmentDatetime= $this->utility->changeDateFromat($row->appointmentDatetime,'d-m-Y h:i');
           $Object = array(
             'suffix'=>$suffix,
             'requestId'=>$row->requestId,
             'Id'=>(int)$row->Id,
             'status'=>(int)$row->status,
             'reason'=>$row->reason,
             'statusTime'=>$statusTime,
             'statusdatetime'=>$row->statusTime,
             'appointmentDatetime'=>$row->appointmentDatetime,
             'operatorDatetime'=>$row->operatorDatetime,
             'destinationAddress'=>$row->destinationAddress,
             'destinationLatitude'=>$row->destinationLatitude,
             'destinationLongitude'=>$row->destinationLongitude,
             'carType'=>$carTypeObject,
             'car'=>$carObject,
             'address'=>$addressObject,
             'user'=>$userObject,
             'category'=>$categoryObj,
             'services'=>$serviceObj,
             'van'=>$vanObject,

             'bill'=>$billObj,
             'rate'=>$rateObj,
             'appointmentStatus'=>$appointmentStatusObj
           );
           $appointmentObject[] = $Object;
         }
       }
       if ($isArray) {
         return $appointmentObject;
       }else{
         return $Object;
       }

     }

   /* Method to getAppointmentServicesByAppointmentId
      Created By: Manzz Baria
   */
   public function getAppointmentServicesByAppointmentId($appointmentId){
     $this->db->select("Id,serviceId,status,reason,serviceStartTime,serviceEndTime");
     $this->db->from("appoinmentdetail");
     $this->db->where('appointmentId', $appointmentId);
     $query = $this->db->get();
     return $this->displayAppointmentServicesByAppointmentId($query->result());
   }

   /* Method to displayAppointmentServicesByAppointmentId
        Created By: Manzz Baria
     */
   public function displayAppointmentServicesByAppointmentId($result){
     $appointmentObject = null;
     if($result != null){
       $this->load->model("ApiServiceModel","serviceModel");
       foreach ($result as $row) {
         $serviceObj = $this->serviceModel->getServiceSmallDetailById($row->serviceId);
         $Object = array(
           'Id'=>(int)$row->Id,
           'status'=>(int)$row->status,
           'reason'=>$row->reason,
           'serviceStartTime'=>$row->serviceStartTime,
           'serviceEndTime'=>$row->serviceEndTime,
           'service'=>$serviceObj
         );
         $appointmentObject[] = $Object;
       }
     }
     return $appointmentObject;
   }


   /* Method to getAppointmentActions
      Created By: Manzz Baria
   */
   public function getAppointmentActions($appointmentId,$language =LANGUAGE_ENGLISH){
     $this->db->select("Id,appointmentId,userId,status,reason,datetime");
     $this->db->from("appointmentactionmaster");
     $this->db->where('appointmentId', $appointmentId);
     $this->db->order_by("Id","DESC");
     $query = $this->db->get();
     return $this->displayAppointmentActions($query->result(),$language );
   }

   /* Method to displayAppointmentActions
        Created By: Manzz Baria
     */
     public function displayAppointmentActions($result,$language =LANGUAGE_ENGLISH){
       $requestObject = null;
       if($result != null){
         $this->load->model("Utility","utility");
         $this->load->model("ApiUserModel","uModel");
         foreach ($result as $row) {
             $displayDate = $this->utility->timeAgoFormat($row->datetime,$language);
             $userObject = $this->uModel->getUserDetail($row->userId);
             $Object = array(
                             'Id'=>(int)$row->Id,
                             'status'=>(int)$row->status,
                             'reason'=>$row->reason,
                             'datetime'=>$displayDate,
                             'ActionBy'=>$userObject
                         );
             $requestObject[]=$Object;
         }
       }
       return $requestObject;
     }

     /* Method to isCancelable
        Created By: Manzz Baria
     */
     public function isCancelable($appointmentId){
         $this->load->model("Utility","utility");
         $dateTime = $this->getTimeOfAppointment($appointmentId);
       //  echo $dateTime;
         $hours = $this->utility->getDifferenceInHour($dateTime);
       //  echo 'Hour: '.$hours;
         if($hours <= 2 ){
           return false;
         }else{
           return true;
         }
     }

     /* Method getTimeOfAppointment
      Created By: Manzz Baria
      */
      public function getTimeOfAppointment($appointmentId){
        $this->db->select('appointmentDatetime');
        $this->db->from('appoinmentmaster');
        $this->db->where('Id',$appointmentId);
        $query = $this->db->get();
        $result = $query->result();
        $datetime = null;
        if($result != null){
          foreach ($result as $row) {
            $datetime = $row->appointmentDatetime;
          }
        }
    // $this->load->model("Utility","utility");
    //$datetime=$statusdatetime= $this->utility->changeDateFromat($datetime,'d-m-Y h:i');
            return $datetime;
      }

      /* Method getTimeOfAppointmentOperator
       Created By: Manzz Baria
       */
        public function getTimeOfAppointmentOperator($appointmentId){
         $this->db->select('operatorDatetime');
         $this->db->from('appoinmentmaster');
         $this->db->where('Id',$appointmentId);
         $query = $this->db->get();
         $result = $query->result();
         $datetime = null;
         if($result != null){
           foreach ($result as $row) {
             $datetime = $row->operatorDatetime;
           }
         }
     //$this->load->model("Utility","utility");
    //$datetime=$statusdatetime= $this->utility->changeDateFromat($datetime,'d-m-Y h:i');
         return $datetime;
       }


   /* Method to check isAppoinmentAlreadyDone
       Created By: Manzz Baria
    */
    public function isAppoinmentAlreadyDone($appointmentId,$status){
   
        $this->db->select('Id');
        $this->db->from('appointmentactionmaster');
        $this->db->where("appointmentId",$appointmentId);
        $this->db->where("status",$status);
        $query = $this->db->get();
        if ($query->result() != null) {
          return true;
        }
     
      return false;
    }



      /*
         Method to changeAppointmentStatus
         Created By: Manzz Baria
      */


        public function getOperatorStatus($userid)
        {
            $status="";
             $this->db->select('status');
             $this->db->from('usermaster');
             $this->db->where("Id",$userid);
              $this->db->where("userType",2);
              $this->db->where("isActive",1);
             $query = $this->db->get();
            // print_r($query->result());
             if ($query->result() != null) {
              foreach ($query->result() as $row) {
                $status=$row->status;
                
              }
            }
             return $status;
        }

      public function changeAppointmentStatus($appointmentId,$status,$reason,$userId){
        $isDone = $this->isAppoinmentAlreadyDone($appointmentId,$status);
             if (!$isDone) {
                $this->load->model("Utility","utility");
                $datetime = $this->utility->getCurrentDate('Y/m/d h:i:s');
                      $data = array(
                        'status' => $status,
                        'statusTime' => $datetime,
                        'reason' => $reason
                        );
                $this->db->where('Id', $appointmentId);
                $this->db->update('appoinmentmaster', $data);
                $this->appointmentAction($appointmentId,$userId,$status,$reason);
                return true;
              }
              return false; 
      }

      /*
         Method to changeAppointmentVan
         Created By: Manzz Baria
      */
       public function changeAppointmentVan($appointmentId,$vanId){
          $oldVanId = $this->getAppointmentVanId($appointmentId);
          $this->load->model("Utility","utility");
          $this->load->model("ApiNotificationModel","notificationModel");
          $statusTime = $this->utility->getCurrentDate('Y/m/d h:i:s');
          $data = array(
                          'vanId' => $vanId
                        );
          $this->db->where('Id', $appointmentId);
          $this->db->update('appoinmentmaster', $data);
         
          $url = base_url()."Api/Email/sendEmailForChangeRequestVan";
         $param = array('appointmentId' => "".$appointmentId,'oldVanId' => "".$oldVanId,'newVanId' => "".$vanId);
         $this->mylibrary->do_in_background($url, $param);
          $this->notificationModel->sendNotificationUserToOperatorForChangeRequestVan($appointmentId,$oldVanId,$vanId);

          return true;

      }
      
       /*
       Method to appointmentAction
       Created By: Manzz Baria
    */
    
      public function appointmentAction($appointmentId,$userId,$status,$reason){
            $this->load->model("Utility","utilityModel");
            $this->load->model("ApiNotificationModel","notiModel");
            $this->load->library('mylibrary');
            $datetime = $this->utilityModel->getCurrentDate('Y/m/d h:i:s');
            $data = array(
                        'appointmentId'=>$appointmentId,
                        'userId'=>$userId,
                        'status'=>$status,
                        'reason'=>$reason,
                        'datetime'=>$datetime);
            $this->db->insert('appointmentactionmaster', $data);
            $requestOwnerId = $this->getUserIdOfAppointment($appointmentId);
            if ($userId == $requestOwnerId) {
                $this->notiModel->sendNotificationUserToOperator($appointmentId,$status,$reason);
            }else{
                $this->notiModel->sendNotificationOperaterToUserForRequestAction($appointmentId,$status,$reason,0);
                if ($status == REQUEST_STATUS_REJECTED) {
                  $this->notiModel->sendNotificationUserToOperator($appointmentId,$status,$reason);
                }
                 

              
            }
              if ($status == REQUEST_STATUS_REJECTED) {
                    $url = base_url()."Api/Email/rejectRequestEmail";
                    $param = array('appointmentId' => "".$appointmentId, 'reason' => "".$reason );
                    $this->mylibrary->do_in_background($url, $param);

                }

            if ($status == REQUEST_STATUS_ON_THE_WAY) {
                $url = base_url()."Api/Email/operatorSendEmail";
                $param = array('appointmentId' => "".$appointmentId);
                $this->mylibrary->do_in_background($url, $param);

            }
            if ($status == REQUEST_STATUS_FINISH) {

              $url = base_url()."Api/Email/completedRequest";
              $param = array('appointmentId' => "".$appointmentId);
              $this->mylibrary->do_in_background($url, $param);

            }
            return true;
        }


         /* Method to check isAppoinmentServiceAlreadyDone
       Created By: Manzz Baria
    */
    public function isAppoinmentServiceAlreadyDone($appointmentId,$serviceId,$status){
   
        $this->db->select('Id');
        $this->db->from('appoinmentdetail');
        $this->db->where("appointmentId",$appointmentId);
        $this->db->where("serviceId",$serviceId);
        $this->db->where("status",$status);
        $query = $this->db->get();
        if ($query->result() != null) {
          return true;
        }
     
      return false;
    }


      /*
         Method to changeRequestServiceStatus
         Created By: Manzz Baria
      */
      public function changeRequestServiceStatus($appointmentId,$serviceId,$status,$reason,$userId,$language=LANGUAGE_ENGLISH){

         $isDone = $this->isAppoinmentServiceAlreadyDone($appointmentId,$serviceId,$status);
             if (!$isDone) {
                    $this->load->model("Utility","utility");
                    $this->load->model("ApiServiceModel","serviceModel");
                    $this->load->model("ApiNotificationModel","notificationModel");
                    $datetime = $this->utility->getCurrentDate('Y/m/d h:i:s');
                   if ($status == REQUEST_STATUS_IN_PROGRESS) {
                      $data = array(
                        'status' => $status,
                        'serviceStartTime' => $datetime
                        );
                    }else if($status == REQUEST_STATUS_FINISH){
                      $data = array(
                        'status' => $status,
                        'serviceEndTime' => $datetime
                        );
                    }else if($status == REQUEST_STATUS_REJECTED){
                       $data = array(
                        'status' => $status,
                        'serviceEndTime' => $datetime,
                        'reason' => $reason
                        );
                    }
                    $this->db->where('appointmentId', $appointmentId);
                    $this->db->where('serviceId', $serviceId);
                    $this->db->update('appoinmentdetail', $data);

                    $appoitnmentStatus=  $this->getAppointmentStatus($appointmentId);
                    if ($status == REQUEST_STATUS_IN_PROGRESS) {
                       if ($appoitnmentStatus != REQUEST_STATUS_IN_PROGRESS) {
                          $this->changeAppointmentStatus($appointmentId,$status,$reason,$userId);
                       }
                    }
                    if($status == REQUEST_STATUS_REJECTED){
                        $this->notificationModel->sendNotificationOperaterToUserForRequestAction($appointmentId,NOTIFICATION_TYPE_SERVICE_REJECTED,$reason,$serviceId);
                    }
                    $serviceObject =  $this->serviceModel->getServiceStatusByServiceId($serviceId,$appointmentId,$language);
                    $Object = array(
                      'status'=>(int)$appoitnmentStatus,
                      'service'=>$serviceObject
                    );
                    return $Object;
             }
             return null;
         
      }

      /*
        Method getTimeOfAppointment
        Created By: Manzz Baria
       */
       public function getAppointmentStatus($appointmentId){
         $this->db->select('status');
         $this->db->from('appoinmentmaster');
         $this->db->where('Id',$appointmentId);
         $query = $this->db->get();
         $result = $query->result();
         $status = null;
         if($result != null){
           foreach ($result as $row) {
             $status = (int)$row->status;
           }
         }
         return $status;
       }

   /*
     Method getRequestVanOwnerInfo
     Created By: Manzz Baria
    */
    public function getRequestVanOwnerInfo($aapointmentId){
      $this->db->select('vanprovidercontact.email as contactEmail,vanprovidercontact.fullName as contactName,vanmaster.Id,vanmaster.name as vanName,vanmaster.type,vanmaster.ownerId,vanownermaster.name,vanownermaster.email,vanprovidercontact.countryCode,vanprovidercontact.mobileNumber,vanownermaster.address');
      $this->db->from('appoinmentmaster');
      $this->db->join("vanmaster","appoinmentmaster.vanId = vanmaster.Id","inner");
      $this->db->join("vanownermaster","vanmaster.ownerId = vanownermaster.Id","inner");
      $this->db->join("vanprovidercontact","vanownermaster.Id = vanprovidercontact.vanOwnerId","inner");
      $this->db->where('appoinmentmaster.Id',$aapointmentId);
      $this->db->where('vanprovidercontact.isPrimary',1);
      $query = $this->db->get();
      return $this->displayRequestVanOwnerInfo($query->result());
    }

   /*
       Method to displayRequestVanOwnerInfo
       Created By: Manzz Baria
    */
    public function displayRequestVanOwnerInfo($result){
      if($result != null){
        foreach ($result as $row) {
          $Object = array(
            'Id'=>(int)$row->Id,
            'vanName'=>$row->vanName,
            'type'=>(int)$row->type,
            'ownerId'=>(int)$row->ownerId,
            'name'=>$row->name,
            'email'=>$row->email,
            'contactEmail'=>$row->contactEmail,
            'contactName'=>$row->contactName,
            'countryCode'=>$row->countryCode,
            'mobileNumber'=>$row->mobileNumber,
            'address'=>$row->address
          );
          return $Object;
        }
      }

    }

    /*
      Method getAppointmentVanId
      Created By: Manzz Baria
     */
     public function getAppointmentVanId($appointmentId){
       $this->db->select('vanId');
       $this->db->from('appoinmentmaster');
       $this->db->where('Id',$appointmentId);
       $query = $this->db->get();
       $result = $query->result();
       $vanId = null;
       if($result != null){
         foreach ($result as $row) {
           $vanId = (int)$row->vanId;
         }
       }
       return $vanId;
     }

     /*
        Method to updateRequest
        Created By: Manzz Baria
     */
       public function updateAppointment($appointmentId,$addressId,$carId,$vanId,$appoinmentDateTime,$appointmentTravelingTime,$destinationAddress,$destinationLatitude,$destinationLongitude,$localAmount,$amount,$duration,$language =LANGUAGE_ENGLISH){
         $this->load->model("Utility","utility");
         $this->load->model("ApiAppoinmnetModel","appoinmnetModel");
         $this->load->model("ApiNotificationModel","notificationModel");
         $this->load->model("ApiBillModel","billModel");
         $datetime = $this->utility->getCurrentDate('Y/m/d h:i:s');
         $requestId = $this->getRequestIdOfAppointment($appointmentId);
         $oldVanId = $this->appoinmnetModel->getAppointmentVanId($appointmentId);
         if ($addressId != "") {
             $data = array(
                         'addressId'=>$addressId,
                         'datetime'=>$datetime);
             $this->db->where('Id', $requestId);
             $this->db->update('requestmaster', $data);
         }
         if ($carId != "") {
           $data = array(
                       'carId'=>$carId);
           $this->db->where('Id', $requestId);
           $this->db->update('requestmaster', $data);
         }
         if ($destinationAddress != "") {
             $this->billModel->updateBillAmount($appointmentId,$localAmount,$amount);
             $data = array(
               'destinationAddress' => $destinationAddress,
               'destinationLatitude'=>$destinationLatitude,
               'destinationLongitude'=>$destinationLongitude
               );
             $this->db->where('Id', $appointmentId);
             $this->db->update('appoinmentmaster', $data);
         }
         if ($vanId != "" && $appoinmentDateTime != "") {
               $statusTime = $this->utility->getCurrentDate('Y/m/d h:i:s');
               $data = array(
                 'vanId' => $vanId,
                 'duration' => $duration,
                 'statusTime' => $statusTime,
                 'appointmentDatetime'=>$appoinmentDateTime,
                 'operatorDatetime'=>$appointmentTravelingTime
                 );
               $this->db->where('Id', $appointmentId);
               $this->db->update('appoinmentmaster', $data);
         }
         if ($addressId != "" || $appoinmentDateTime != "") {

           if ($oldVanId != $vanId && $appoinmentDateTime != "") {
             $this->notificationModel->sendNotificationUserToOperatorForChangeRequestVan($appointmentId,$oldVanId,$vanId);

           }else{
             $this->notificationModel->sendNotificationUserToOperatorForUpdateRequest($appointmentId,$addressId,$appoinmentDateTime);
           }
           $this->load->library('mylibrary');
           $url = base_url()."Api/Email/updateAppointmentConfirmation";
          $param = array('appointmentId' => "".$appointmentId );
          $this->mylibrary->do_in_background($url, $param);

         }
         return $this->getAppointmentDetailsById($appointmentId,$language);
     }

     /* Method to getAppointmentDestinationAddress
        Created By: Manzz Baria
     */
     public function getAppointmentDestinationAddress($appointmentId){
       $this->db->select("destinationAddress,destinationLatitude,destinationLongitude");
       $this->db->from("appoinmentmaster");
       $this->db->where('Id', $appointmentId);
       $query = $this->db->get();
       return $this->displayAppointmentDestinationAddress($query->result());
     }

     /* Method to displayAppointmentDestinationAddress
          Created By: Manzz Baria
       */
       public function displayAppointmentDestinationAddress($result){
         $requestObject = null;
         if($result != null){
           foreach ($result as $row) {
               $Object = array(
                               'destinationAddress'=>$row->destinationAddress,
                               'destinationLatitude'=>$row->destinationLatitude,
                               'destinationLongitude'=>$row->destinationLongitude
                           );
                           return $Object;
              // $requestObject[]=$Object;
           }
         }
        // return $requestObject;
       }

  /***************************    OLD APIS    ***************************/


  /* Method to getPendingAppoinmentsByVanId
   Created By: Manzz Baria
*/
public function getPendingAppoinmentsByVanId($vanId){
  $this->db->select("Id,requestId,status,reason,appointmentDatetime,statusTime");
  $this->db->from("appoinmentmaster");
  $this->db->where("vanId",$vanId);
  $this->db->where("status",REQUEST_STATUS_PENDING);
  $query = $this->db->get();
  return $this->displayPendingAppoinmentsByVanId($query->result());
}

/* Method to displayPendingAppoinmentsByVanId
     Created By: Manzz Baria
  */
  public function displayPendingAppoinmentsByVanId($result){
    $carObject = null;
    if($result != null){
      $this->load->model("Utility","utility");
      $this->load->model("ApiRequestModel","requestModel");
      foreach ($result as $row) {
        $statusTime = $this->utility->timeAgoFormat($row->statusTime);
        $requestObject = $this->getAppointmentDetailsById($row->Id);
        $Object = array(
          'Id'=>(int)$row->Id,
          'status'=>(int)$row->status,
          'statusTime'=>$statusTime,
          'reason'=>$row->reason,
          'datetime'=>$row->appointmentDatetime,
          'request'=>$requestObject
        );
        $carObject[]=$Object;
      }
    }
    return $carObject;
  }


    /* Method to check isAddressAlreadyAdded
       Created By: Manzz Baria
    */
    public function isAppoinmentAlreadyFixed($datetime,$vanId){
      if ($datetime != "") {
        $this->db->select('Id');
        $this->db->from('appoinmentmaster');
        $this->db->where("appointmentDatetime",$datetime);
        $this->db->where("vanId",$vanId);
        $query = $this->db->get();
        if ($query->result() != null) {
          return true;
        }
      }
      return false;
    }

  /* Method to cancelAppointment
     Created By: Manzz Baria
  */
  public function cancelAppointment($requestId){
      $this->db->where('requestId', $requestId);
      $this->db->delete('appoinmentmaster');
      if($this->db->affected_rows() > 0){
        return true;
      }else{
        return false;
      }
  }

  /*
     Method to changeAppointmentTime
     Created By: Manzz Baria
  */
  public function changeAppointmentTime($requestId,$vanId,$datetime){
    $isAdded = $this->isAppoinmentAlreadyFixed($datetime,$vanId);
    if (!$isAdded) {
      $this->load->model("Utility","utility");
      $statusTime = $this->utility->getCurrentDate('Y/m/d h:i:s');
            $data = array(
              'vanId' => $vanId,
              'statusTime' => $statusTime,
              'appointmentDatetime'=>$datetime
              );
            $this->db->where('requestId', $requestId);
            $this->db->update('appoinmentmaster', $data);
      return true;
    }
    return false;

  }

  //$this->db->join('usermaster','appoinmentmaster.vanId = usermaster.vanId','inner');
  //$this->db->where('usermaster.Id', $userId);

/********     reports      ********/

/* Method to getReports
   Created By: Manzz Baria
*/
public function getReports($fromDate,$toDate,$supplierId,$serviceId,$categoryId,$status,$operatorId){
  $fromDate = $fromDate." 00:00:00";
  $toDate = $toDate." 23:59:59";
  $this->db->select("appoinmentmaster.Id,appoinmentmaster.requestId,appoinmentmaster.suffixId,appoinmentmaster.categoryId,appoinmentmaster.vanId,appoinmentmaster.status,appoinmentmaster.reason,appoinmentmaster.appointmentDatetime,appoinmentmaster.operatorDatetime,appoinmentmaster.statusTime,appoinmentmaster.destinationAddress,appoinmentmaster.destinationLatitude,appoinmentmaster.destinationLongitude");
  $this->db->from("appoinmentmaster");

  if ($supplierId > 0 ) {
    $this->db->join('vanmaster','appoinmentmaster.vanId = vanmaster.Id','inner');
    $this->db->where('vanmaster.ownerId', $supplierId);
  }

  if ($serviceId > 0 ) {
    $this->db->join('appoinmentdetail','appoinmentmaster.Id = appoinmentdetail.appointmentId','inner');
    $this->db->where('appoinmentdetail.serviceId', $serviceId);
  }

  if ($categoryId > 0 ) {
      $this->db->where('appoinmentmaster.categoryId', $categoryId);
  }

  if ($status > 0 ) {
      $this->db->where('appoinmentmaster.status', $status);
  }

  if ($operatorId > 0 ) {
    //$this->db->join('vanmaster','appoinmentmaster.vanId = vanmaster.Id','inner');
    $this->db->join('usermaster','appoinmentmaster.vanId = usermaster.vanId','inner');
    $this->db->where('usermaster.Id', $operatorId);
  }

  $this->db->where('appoinmentmaster.appointmentDatetime >=', $fromDate);
  $this->db->where('appoinmentmaster.appointmentDatetime <=', $toDate);
  $this->db->order_by("appoinmentmaster.appointmentDatetime","ASC");

  $query = $this->db->get();
  return $this->displayAppointments($query->result(),true);
}


public function getCustomerReport($fromDate,$toDate,$customerId,$countrycode){
  $fromDate = $fromDate." 00:00:00";
  $toDate = $toDate." 23:59:59";
  $this->db->select("usermaster.Id,usermaster.fullName,usermaster.mobileNumber,usermaster.email,usermaster.datetime,usermaster.countryCode,usermaster.latitude,usermaster.longitude");
  $this->db->from("usermaster");

  if ($customerId > 0 ) {
    $this->db->where('usermaster.Id', $customerId);
  }

  if ($countrycode > 0 ) {
    $this->db->join('countrymaster','countrymaster.countrycode = usermaster.countrycode','inner');
    $this->db->where('usermaster.countryCode', $countrycode);
  }
  $this->db->where("isActive",1);
  $this->db->where("userType",USER_TYPE_CUSTOMER);
  $this->db->where_not_in("status",USER_STATUS_BLOCK);
  $this->db->where('usermaster.datetime >=', $fromDate);
  $this->db->where('usermaster.datetime <=', $toDate);
  //$this->db->order_by("usermaster.datetime","ASC");

  $query = $this->db->get();
 // print_r($query->result());
  return $this->displayCustomerReport($query->result(),$fromDate,$toDate);
}

public function displayCustomerReport($result,$fromDate,$toDate){
       $CustomerObject = null;
       $Object= null;

       if($result != null){
        $this->load->model("Utility","utility");
          $this->load->model("ApiContactModel","ContactModel");
          $this->load->model("ApiRequestModel","RequestModel");
          $this->load->model("ApiVanOperatorModel","vanOperatorModel");

         foreach ($result as $row) {
          //  $CustomerDate=$this->utility->getCurrentDate('Y-m-d h:i:s');

           $CustomerDate = $this->utility->changeDateFromat($row->datetime,'d-m-Y h:i:s');
           $CustomerDate= date("Y-m-d h:i:s", strtotime('+3 hours', strtotime($CustomerDate)));
           $country = $this->ContactModel->getcountry($row->countryCode);
           $lastjobrequestedDate = $this->RequestModel->getrequestdate($row->Id);
           $sumofjob = $this->RequestModel->getsumofrequestjob($fromDate,$toDate,$row->Id);
           $loginaddress = $this->vanOperatorModel->getaddress($row->latitude,$row->longitude);
          
           $Object = array(
             'Id'=>(int)$row->Id,
             'fullName'=>$row->fullName,
             'mobileno'=>$row->mobileNumber,
             'email'=>$row->email,
             'registerDate'=>$CustomerDate,
             'country'=>$country,
             'loginaddress'=>$loginaddress,
             'lastjobdate'=> $lastjobrequestedDate,
             'sumofjob'=>$sumofjob
           );
           $CustomerObject[] = $Object;
         }
       }
        return $CustomerObject ;

     }

}
?>
