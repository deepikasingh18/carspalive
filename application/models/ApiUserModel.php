<?php if( ! defined("BASEPATH")) exit('No direct script access allowed');

class ApiUserModel extends CI_Model {

  public function _construct(){
    parent::_construct();
  }

  /* Method to getTotalCustomers
  Created By: Manzz Baria
  */
  public function getTotalCustomers(){
    $this->db->select("Id");
    $this->db->from("usermaster");
    $this->db->where('userType',USER_TYPE_CUSTOMER);
    $this->db->where('isActive',1);
    $query = $this->db->count_all_results();
    return ceil($query);
  }

  /*
       Method getUserVanId
       Created By: Manzz Baria
   */
   public function getUserVanId($userId){
     $this->db->select('vanId');
     $this->db->from('usermaster');
     $this->db->where('Id',$userId);
     $query = $this->db->get();
     $result = $query->result();
     $vanId = 0;
     if($result != null){
       foreach ($result as $row) {
         $vanId = (int)$row->vanId;
       }
     }
     return $vanId;
   }

  /* Method get fullname from id
   Created By: Manzz Baria
   */
   public function getFullName($userId){
     $this->db->select('fullName');
     $this->db->from('usermaster');
     $this->db->where('Id',$userId);
     $query = $this->db->get();
     $result = $query->result();
     $fullName = null;
     if($result != null){
       foreach ($result as $row) {
         $fullName = $row->fullName;
       }
     }
     return $fullName;
   }

   /* Method get fullname from id
    Created By: Manzz Baria
    */
    public function getEmail($userId){
      $this->db->select('email');
      $this->db->from('usermaster');
      $this->db->where('Id',$userId);
      $query = $this->db->get();
      $result = $query->result();
      $email = null;
      if($result != null){
        foreach ($result as $row) {
          $email = $row->email;
        }
      }
      return $email;
    }

    public function getCountryCode($userId){
      
      $this->db->select('countryCode');
      $this->db->from('usermaster');
      $this->db->where('Id',$userId);
      $query = $this->db->get();
      $result = $query->result();
      $count=$query->num_rows();
     
      $email = null;
     
      if($count>0){
        foreach ($result as $row) {
          $countryCode = $row->countryCode;
        }
          return $countryCode;
      }
    }

  public function searchUser($isActive,$blocked,$name){
    $this->db->select("Id,fullName,email,socialType,socialId,countryCode,mobileNumber,fileUrl,status,latitude,longitude,datetime");
    $this->db->from("usermaster");
    $this->db->where("isActive",$isActive);
    $this->db->where("userType",USER_TYPE_CUSTOMER);
    if ($blocked) {
        $this->db->where("status",USER_STATUS_BLOCK);
    }else{
        $this->db->where_not_in("status",USER_STATUS_BLOCK);
    }
    $this->db->like("fullName",$name);
    $query = $this->db->get();
    return $this->displayCustomers($query->result());
}
    /* Method getFullMobileNumber
     Created By: Manzz Baria
     */
     public function getFullMobileNumber($userId){
       $this->db->select('countryCode,mobileNumber');
       $this->db->from('usermaster');
       $this->db->where('Id',$userId);
       $query = $this->db->get();
       $result = $query->result();
       $mobileNumber = null;
       if($result != null){
         foreach ($result as $row) {
           $mobileNumber = $row->countryCode .''. $row->mobileNumber;
         }
       }
       return $mobileNumber;
     }


    /* Method getMobileNumber
     Created By: Manzz Baria
     */
     public function getMobileNumber($userId){
       $this->db->select('mobileNumber');
       $this->db->from('usermaster');
       $this->db->where('Id',$userId);
       $query = $this->db->get();
       $result = $query->result();
       $mobileNumber = null;
       if($result != null){
         foreach ($result as $row) {
           $mobileNumber = $row->mobileNumber;
         }
       }
       return $mobileNumber;
     }

     /*
          Method getUserIdFromMobileNumber
          Created By: Manzz Baria
      */
      public function getUserIdFromMobileNumber($mobileNumber){
        $this->db->select('Id');
        $this->db->from('usermaster');
        $this->db->where('mobileNumber',$mobileNumber);
        $query = $this->db->get();
        $result = $query->result();
        $Id = null;
        if($result != null){
          foreach ($result as $row) {
            $Id = $row->Id;
          }
        }
        return $Id;
      }
      
      
   /* Method getEmailFromMobileNumber
    Created By: Manzz Baria
    */
    public function getEmailFromMobileNumber($mobileNumber){
      $this->db->select('email');
      $this->db->from('usermaster');
      $this->db->where('mobileNumber',$mobileNumber);
      $query = $this->db->get();
      $result = $query->result();
      $email = null;
      if($result != null){
        foreach ($result as $row) {
          $email = $row->email;
        }
      }
      return $email;
    }


    /* Method getUserIdFromEmailORMobile
     Created By: Manzz Baria
     */
     public function getUserIdFromEmail($email){
       $this->db->select('Id');
       $this->db->from('usermaster');
       if (strpos($email, '@') === FALSE) {
           $this->db->where("mobileNumber",$email);
       }else{
           $this->db->where("email",$email);
       }
       $this->db->where("userType",USER_TYPE_CUSTOMER);
       $query = $this->db->get();
       $result = $query->result();
       $Id = null;
       if($result != null){
         foreach ($result as $row) {
           $Id = $row->Id;
         }
       }
       return $Id;
     }

    /* Method get fullname from id
     Created By: Manzz Baria
     */
     public function getUserIdFromSocialId($socialId){
       $this->db->select('Id');
       $this->db->from('usermaster');
       $this->db->where('socialId',$socialId);
       $query = $this->db->get();
       $result = $query->result();
       $Id = null;
       if($result != null){
         foreach ($result as $row) {
           $Id = $row->Id;
         }
       }
       return $Id;
     }

  /*
      Method to check isUserAlreadyRegister
      Created By: Manzz Baria
  */
  public function isUserAlreadyRegister($email,$mobileNumber){
    $this->db->select('Id');
    $this->db->from('usermaster');
    $strWhere = "email='".$email."' or mobileNumber='".$mobileNumber."'";
    $this->db->where($strWhere,null,false);
    $this->db->where_not_in("status",USER_STATUS_BLOCK);
   //  $this->db->where("userType",USER_TYPE_CUSTOMER);
    $query = $this->db->get();
    if ($query->result() != null) {
      return true;
    }
    return false;
  }

  /*
    Method to check isUserEmailAlreadyRegister
    Created By: Manzz Baria
  */
  public function isUserEmailAlreadyRegister($email){
    $this->db->select('Id');
    $this->db->from('usermaster');
    if (strpos($email, '@') === FALSE) {
        $this->db->where("mobileNumber",$email);
    }else{
        $this->db->where("email",$email);
    }
    $query = $this->db->get();
    if ($query->result() != null) {
      return true;
    }
    return false;
  }

  /*
    Method to check isUserEmailAlreadyRegister
    Created By: Manzz Baria
  */
  public function isUserEmailIsOwn($email,$userId){
    $this->db->select('Id');
    $this->db->from('usermaster');
    if (strpos($email, '@') === FALSE) {
        $this->db->where("mobileNumber",$email);
    }else{
        $this->db->where("email",$email);
    }
    $this->db->where('Id',$userId);
    $query = $this->db->get();
    if ($query->result() != null) {
      return true;
    }
    return false;
  }

  /*
    Method to check isUserMobileNumberIsOwn
    Created By: Manzz Baria
  */
  public function isUserMobileNumberIsOwn($mobileNumber,$userId){
    $this->db->select('Id');
    $this->db->from('usermaster');
    $this->db->where('mobileNumber',$mobileNumber);
    $this->db->where('Id',$userId);
    $query = $this->db->get();
    if ($query->result() != null) {
      return true;
    }
    return false;
  }


  /*
    Method to check isUserMobileNumberAlreadyRegister
    Created By: Manzz Baria
  */
  public function isUserMobileNumberAlreadyRegister($mobileNumber){
    $this->db->select('Id');
    $this->db->from('usermaster');
    $this->db->where('mobileNumber',$mobileNumber);
    $query = $this->db->get();
    if ($query->result() != null) {
      return true;
    }
    return false;
  }

  /* Method to check isSocilAlreadyRegister
  Created By: Manzz Baria
  */
  public function isSocilAlreadyRegister($socialId,$socialType){
    $this->db->select('Id');
    $this->db->from('usermaster');
    $strWhere = "socialId='".$socialId."' and socialType='".$socialType."'";
    $this->db->where($strWhere,null,false);
    $query = $this->db->get();
    if ($query->result() != null) {
      return true;
    }
    return 0;
  }

  /* Method to check isAccountActive
  Created By: Manzz Baria
  */
  public function isAccountActive($email){
    $isActive = 1;
    $this->db->select('Id');
    $this->db->from('usermaster');
    $this->db->where("isActive",$isActive);
    if (strpos($email, '@') === FALSE) {
        $this->db->where("mobileNumber",$email);
    }else{
        $this->db->where("email",$email);
    }
    $query = $this->db->get();
    if ($query->result() != null) {
      return true;
    }
    return false;
  }
/*  Method to check isAccountAvailable
    Created By: Manzz Baria
*/
public function isAccountAvailable($email){
  $this->load->model("Utility","utility");
//  $securePassword = $this->utility->encrypt($password);
  $this->db->select('Id');
  $this->db->from('usermaster');
//  $this->db->where("password",$securePassword);
  if (strpos($email, '@') === FALSE) {
      $this->db->where("mobileNumber",$email);
  }else{
      $this->db->where("email",$email);
  }
  $query = $this->db->get();
  if ($query->result() != null) {
    return true;
  }
  return false;
}

/* Method save accessToken
Created By: Manzz Baria
*/
public function saveAccessToken($accessToken,$userId){
    $data = array(
      'accessToken' => $accessToken
    );
    $this->db->where('Id', $userId);
    $this->db->update('usermaster', $data);
    return true;
}

/* Method to registration
   Created By: Manzz Baria
*/
public function registration($fullName,$email,$cityId,$countryCode,$mobileNumber,$password,$fileUrl){
  $isAdded = $this->isUserAlreadyRegister($email,$mobileNumber);
    if (!$isAdded) {
      $this->load->model("Utility","utility");
      $this->load->model("ApiSMSModel","sMSModel");
      $datetime = $this->utility->getCurrentDate('Y/m/d h:i:s');
      $securePassword = $this->utility->encrypt($password);
      $isActive = 1;
      $data = array(
        'fullName'=>$fullName,
        'email'=>$email,
        'cityId'=>$cityId,
        'countryCode'=>$countryCode,
        'mobileNumber'=>$mobileNumber,
        'userType'=>USER_TYPE_CUSTOMER,
        'password'=>$securePassword,
        'isActive'=>$isActive,
        'fileUrl'=>$fileUrl,
        'datetime'=>$datetime);
      $this->db->insert('usermaster', $data);
      $insert_Id = $this->db->insert_id();
      $this->sMSModel->sendOTPSMS($insert_Id);
    //  $this->utility->sendEmailForApproved($insert_Id);
      return true;
    }else{
      return false;
    }
}

/* Method to socialRegistration
   Created By: Manzz Baria
*/
public function socialRegistration($fullName,$email,$mobileNumber,$socialId,$socialType){
  $isAdded = $this->isSocilAlreadyRegister($socialId,$socialType);
    if (!$isAdded) {
      $this->load->model("Utility","utility");
      $datetime = $this->utility->getCurrentDate('Y/m/d h:i:s');
      $isActive = 1;
      $data = array(
        'fullName'=>$fullName,
        'email'=>$email,
        'mobileNumber'=>$mobileNumber,
        'userType'=>USER_TYPE_CUSTOMER,
        'isActive'=>$isActive,
        'socialId'=>$socialId,
        'socialType'=>$socialType,
        'datetime'=>$datetime);
      $this->db->insert('usermaster', $data);
      $insert_Id = $this->db->insert_id();
      return $insert_Id;
    }else{
      return 0;
    }
}


/* Method to getVanOperatorById
 Created By: Manzz Baria
*/
public function getSocialUserById($socialId){
    $this->db->select("Id,fullName,email,socialType,socialId,fileUrl,countryCode,mobileNumber,status,latitude,longitude,datetime");
    $this->db->from("usermaster");
    $this->db->where_not_in("status",USER_STATUS_BLOCK);
    $this->db->where("socialId",$socialId);
    $query = $this->db->get();
    return $this->displaySocialUserById($query->result());
}

/* Method to displayVanOperatorById
   Created By: Manzz Baria
*/
public function displaySocialUserById($result){
  $Object = null;
  if($result != null){
    $this->load->model("Utility","utility");
    foreach ($result as $row) {
      $displayDate = $this->utility->timeAgoFormat($row->datetime);
      $Object = array(
        'Id'=>(int)$row->Id,
        'fullName'=>$row->fullName,
        'email'=>$row->email,
        'status'=>(int)$row->status,
        'socialType'=>(int)$row->socialType,
        'socialId'=>$row->socialId,
        'fileUrl'=>IMAGE_URL.$row->fileUrl,
        'thumbUrl'=>THUMB_URL.$row->fileUrl,
        'countryCode'=>(int)$row->countryCode,
        'mobileNumber'=>$row->mobileNumber,
        'latitude'=>(double)$row->latitude,
        'longitude'=>(double)$row->longitude,
        'datetime'=>$displayDate
      );
      return $Object;
    }
  }
  return $Object;
}

/* Method to testLogin
 Created By: Manzz Baria
*/
public function testLogin($email,$password){
    $this->load->model("Utility","utility");
    $isActive = 1;
    $securePassword = $this->utility->encrypt($password);
    $this->db->select("Id,fullName,email,socialType,socialId,countryCode,mobileNumber,fileUrl,status,latitude,longitude,datetime");
    $this->db->from("usermaster");
    $this->db->where("password",$securePassword);
    if (strpos($email, '@') === FALSE) {
        $this->db->where("mobileNumber",$email);
    }else{
        $this->db->where("email",$email);
    }
    $query = $this->db->get();
    return $this->displayLoginUser($query->result());
}

/*
    Method to isOTPVerified
    Created By: Manzz Baria
*/
public function isOTPVerified($mobileNumber,$OTPCode){
  $this->db->select('Id');
  $this->db->from('usermaster');
  $this->db->where("accessToken",$OTPCode);
  if (strpos($mobileNumber, '@') === FALSE) {
      $this->db->where("mobileNumber",$mobileNumber);
  }else{
      $this->db->where("email",$mobileNumber);
  }
  $query = $this->db->get();
  if ($query->result() != null) {
     $accessToken = "";
     $userId = $this->getUserIdFromEmail($mobileNumber);
     $this->updateUserActive($userId,1);
     $this->saveAccessToken($accessToken,$userId);
    return true;
  }
  return false;
}

/*
    Method to verifyOTP
    Created By: Manzz Baria
*/
public function verifyOTP($mobileNumber,$OTPCode){
  $isVerifyOTP = $this->isOTPVerified($mobileNumber,$OTPCode);
    if ($isVerifyOTP) {
       $userId = $this->getUserIdFromEmail($mobileNumber);
       return $this->getUserDetail($userId);
    }
    return null;
}

/* Method to getVanOperatorById
 Created By: Manzz Baria
*/
public function loginUser($email,$password){
    $this->load->model("Utility","utility");
    $isActive = 1;
    $securePassword = $this->utility->encrypt($password);
    $this->db->select("Id,fullName,email,socialType,socialId,countryCode,mobileNumber,fileUrl,status,latitude,longitude,datetime");
    $this->db->from("usermaster");
    $this->db->where("isActive",$isActive);
    $this->db->where("userType",USER_TYPE_CUSTOMER);
    $this->db->where_not_in("status",USER_STATUS_BLOCK);
    if (strpos($email, '@') === FALSE) {
        $this->db->where("mobileNumber",$email);
    }else{
        $this->db->where("email",$email);
    }
    $this->db->where("password",$securePassword);
    $query = $this->db->get();
    return $this->displayLoginUser($query->result());
}
/* Method to displayVanOperatorById
   Created By: Manzz Baria
*/
public function displayLoginUser($result){
  $Object = null;
  if($result != null){
    $this->load->model("Utility","utility");
    foreach ($result as $row) {
      $displayDate = $this->utility->timeAgoFormat($row->datetime);
      $Object = array(
        'Id'=>(int)$row->Id,
        'fullName'=>$row->fullName,
        'email'=>$row->email,
        'status'=>(int)$row->status,
        'socialType'=>(int)$row->socialType,
        'socialId'=>$row->socialId,
        'countryCode'=>(int)$row->countryCode,
        'mobileNumber'=>$row->mobileNumber,
        'fileUrl'=>IMAGE_URL.$row->fileUrl,
        'thumbUrl'=>THUMB_URL.$row->fileUrl,
        'latitude'=>(double)$row->latitude,
        'longitude'=>(double)$row->longitude,
        'datetime'=>$displayDate
      );
      return $Object;
    }
  }
  return $Object;
}

/* Method to change password
   Created By: Manzz Baria
*/
public function changePassword($userId,$oldPassword,$newPassword){
  $this->load->model("Utility","utility");
  $isMatched = $this->isPasswordMatched($userId,$oldPassword);
  $securePassword = $this->utility->encrypt($newPassword);
  if ($isMatched) {
    $data = array(
      'password' => $securePassword,
      );
    $this->db->where('Id', $userId);
    $this->db->update('usermaster', $data);
    return true;
  }else{
    return false;
  }
}

/* Method to check isPasswordMatched
Created By: Manzz Baria
*/
public function isPasswordMatched($userId,$password){
  $this->load->model("Utility","utility");
  $securePassword = $this->utility->encrypt($password);
  $this->db->select('Id');
  $this->db->from('usermaster');
  $strWhere = "Id='".$userId."' and password='".$securePassword."'";
  $this->db->where($strWhere,null,false);
  $query = $this->db->get();
  if ($query->result() != null) {
    return true;
  }
  return false;
}

/* Method to updateProfile
   Created By: Manzz Baria
*/
public function updateProfile($userId,$fullName,$email,$countryCode,$mobileNumber,$fileUrl){
    if ($fullName != "" && $fullName != null) {
        $data = array(
          'fullName' => $fullName
          );
        $this->db->where('Id', $userId);
        $this->db->update('usermaster', $data);
    }
    if ($email != "" && $email != null) {
        $data = array(
          'email' => $email
          );
        $this->db->where('Id', $userId);
        $this->db->update('usermaster', $data);
    }
    if ($mobileNumber != "" && $mobileNumber != null) {
        $data = array(
          'countryCode' => $countryCode,
          'mobileNumber' => $mobileNumber
          );
        $this->db->where('Id', $userId);
        $this->db->update('usermaster', $data);
    }
    if ($fileUrl != "" && $fileUrl != null) {
        $data = array(
          'fileUrl' => $fileUrl
          );
        $this->db->where('Id', $userId);
        $this->db->update('usermaster', $data);
    }
  return $this->getUserDetail($userId);
}

/*
   Method to getUserDetailsByAppointmentId
   Created By: Manzz Baria
*/
public function getUserDetailsByAppointmentId($appointmentId){
  $this->db->select("usermaster.Id,usermaster.fullName,usermaster.email,usermaster.socialType,usermaster.socialId,usermaster.countryCode,usermaster.mobileNumber,usermaster.fileUrl,usermaster.status,usermaster.latitude,usermaster.longitude,usermaster.datetime");
  $this->db->from("usermaster");
  $this->db->join('requestmaster','usermaster.Id = requestmaster.userId','inner');
  $this->db->join('appoinmentmaster','requestmaster.Id = appoinmentmaster.requestId','inner');
  $this->db->where('appoinmentmaster.Id',$appointmentId);
  $query = $this->db->get();
  return $this->displayLoginUser($query->result());

}

/* Method to getUserDetail
 Created By: Manzz Baria
*/
public function getUserDetail($userId){
    $isActive = 1;
    $this->db->select("Id,fullName,email,socialType,socialId,countryCode,mobileNumber,fileUrl,status,latitude,longitude,datetime");
    $this->db->from("usermaster");
  //  $this->db->where("isActive",$isActive);
    $this->db->where_not_in("status",USER_STATUS_BLOCK);
    $this->db->where("Id",$userId);
    $query = $this->db->get();
    return $this->displayLoginUser($query->result());
}


/* Method to updateUserLocation
   Created By: Manzz Baria
*/
public function updateUserLocation($userId,$latitude,$longitude,$logindatetime){

  $this->load->model("Utility","util");
    $this->util->dumpTester('updateUserLocation',$latitude,$longitude,$userId);
    

    if ($latitude > 0 && $longitude > 0) {
     
       $data = array(
      'status' => USER_STATUS_ONLINE
      );

    $this->db->where('Id', $userId);
    $this->db->where('status', USER_STATUS_DISCONNECT);
    $this->db->update('usermaster', $data);
    }

    $data = array(
      'latitude' => $latitude,
      'longitude' => $longitude,
      'logindate'=>$logindatetime
      );

    $this->db->where('Id', $userId);
    $this->db->update('usermaster', $data);

    

    $vanId = $this->getUserVanId($userId);
    if ($vanId != null && $vanId > 0) {
      $this->load->model("ApiVanModel","vanModel");
      $this->vanModel->updateVanLocation($vanId,$latitude,$longitude);
    }
    return true;
}

/* Method to updateUserActive
   Created By: Manzz Baria
*/
public function updateUserActive($userId,$isActive){
    $data = array(
      'isActive' => $isActive
      );
    $this->db->where('Id', $userId);
    $this->db->update('usermaster', $data);
    return true;
}


/* Method to updateUserSatus
   Created By: Manzz Baria
*/
public function updateUserSatus($userId,$status){
    $data = array(
      'status' => $status
      );
    $this->db->where('Id', $userId);
    $this->db->update('usermaster', $data);
    return true;
}


/* Method save addOperatorToVan
Created By: Manzz Baria
*/
public function addOperatorToVan($vanId,$userId){
    $data = array(
      'vanId' => $vanId
    );
    $this->db->where('Id', $userId);
    $this->db->update('usermaster', $data);
    return true;
}

/* Method save removeOperatorFromVanById
   Created By: Manzz Baria
*/
public function removeOperatorFromVanById($vanId,$userId){
      $data = array(
        'vanId' => $vanId
      );
      $this->db->where('Id', $userId);
      $this->db->update('usermaster', $data);
      return true;
}


  /* Method to getTotalPagesOfCustomers
     Created By: Manzz Baria
  */
  public function getTotalPagesOfCustomers($isActive,$blocked){
    $this->db->select("Id,fullName,email,socialType,socialId,countryCode,mobileNumber,fileUrl,status,latitude,longitude,datetime");
    $this->db->from("usermaster");
    $this->db->where("isActive",$isActive);
    $this->db->where("userType",USER_TYPE_CUSTOMER);
    if ($blocked) {
        $this->db->where("status",USER_STATUS_BLOCK);
    }else{
        $this->db->where_not_in("status",USER_STATUS_BLOCK);
    }
    $query = $this->db->count_all_results();
    $result  = $query / 20;
    return ceil($result);
  }


/* Method to getCustomers
 Created By: Manzz Baria
*/
public function getCustomers($pageIndex,$isActive,$blocked){
    $this->db->select("Id,fullName,email,socialType,socialId,countryCode,mobileNumber,fileUrl,status,latitude,longitude,datetime");
    $this->db->from("usermaster");
    $this->db->where("isActive",$isActive);
    $this->db->where("userType",USER_TYPE_CUSTOMER);
    if ($blocked) {
        $this->db->where("status",USER_STATUS_BLOCK);
    }else{
        $this->db->where_not_in("status",USER_STATUS_BLOCK);
    }
    $pageNo = "00";
    if ($pageIndex > 0) {
      $pageIndex = $pageIndex * 2;
      $pageNo = $pageIndex.'0';
    }
    $this->db->limit(20,$pageNo);
    $query = $this->db->get();
    return $this->displayCustomers($query->result());
}

public function getuserlist(){
    $this->db->select("Id,fullName,email,socialType,socialId,countryCode,mobileNumber,fileUrl,status,latitude,longitude,datetime");
    $this->db->from("usermaster");
    $this->db->where("isActive",1);
    $this->db->where("userType",USER_TYPE_CUSTOMER);
    $this->db->where_not_in("status",USER_STATUS_BLOCK);
    
    $query = $this->db->get();
    return $this->displayCustomers($query->result());
}

public function getCustomersbyCountry($pageIndex,$isActive,$blocked,$type,$countrycode){
    $this->db->select("Id,fullName,email,countryCode,fileUrl,datetime,mobileNumber");
    $this->db->from("usermaster");
    $this->db->where("isActive",$isActive);
  if($countrycode!=null && $countrycode!="")
  { 
    $this->db->where("countryCode",$countrycode);
  }
  if($type==1)
  {
    $this->db->where("userType",USER_TYPE_CUSTOMER);
    
  }
  else
  {
     $this->db->where("userType",USER_TYPE_OPERATOR);
    
  }
    $this->db->where_not_in("status",USER_STATUS_BLOCK);
    
    /*$pageNo = "00";
    if ($pageIndex > 0) {
      $pageIndex = $pageIndex * 2;
      $pageNo = $pageIndex.'0';
    }
    $this->db->limit(20,$pageNo);*/
    $query = $this->db->get();
    return $this->displayCustomersbycountry($query->result());
}


/* Method to displayCustomers
   Created By: Manzz Baria
*/
public function displayCustomers($result){
  $customerObject = null;
  if($result != null){
    $this->load->model("Utility","utility");
    $this->load->model("ApiAddressModel","addressModel");
    foreach ($result as $row) {
      $displayDate = $this->utility->timeAgoFormat($row->datetime);
      $addressObject = $this->addressModel->getAddresses($row->Id);
      $Object = array(
        'Id'=>(int)$row->Id,
        'fullName'=>$row->fullName,
        'email'=>$row->email,
        'status'=>(int)$row->status,
        'socialType'=>(int)$row->socialType,
        'socialId'=>$row->socialId,
        'countryCode'=>(int)$row->countryCode,
        'mobileNumber'=>$row->mobileNumber,
        'fileUrl'=>IMAGE_URL.$row->fileUrl,
        'thumbUrl'=>THUMB_URL.$row->fileUrl,
        'latitude'=>(double)$row->latitude,
        'longitude'=>(double)$row->longitude,
        'datetime'=>$displayDate,
        'address'=>$addressObject
      );
      $customerObject[]= $Object;
    }
  }
  return $customerObject;
}

/* Method isValidTokens
 Created By: Manzz Baria
 */
 public function isValidTokens($userId,$accessToken){
   $this->db->select('Id');
   $this->db->from('usermaster');
   $this->db->where("accessToken",$accessToken);
   $this->db->where("Id",$userId);
   $query = $this->db->get();
   if ($query->result() != null) {
     return true;
   }
   return false;
 }



 /* Method to change password
    Created By: Manzz Baria
 */
 public function changeRestPassword($userId,$newPassword){
   $this->load->model("Utility","utility");
   $securePassword = $this->utility->encrypt($newPassword);
     $data = array(
       'password' => $securePassword,
       );
     $this->db->where('Id', $userId);
     $this->db->update('usermaster', $data);
     return true;

 }


/* Method reset password
Created By: Manzz Baria
*/
public function resetPassword($validationToken,$accessToken,$newPassword){
  $userId = $validationToken;
  $isValid = $this->isValidTokens($userId,$accessToken);
  if ($isValid) {
    $this->changeRestPassword($userId,$newPassword);
    $this->saveAccessToken('',$userId);
    return true;
  }
  return false;
}


/* Method Forget password (send email for reset password)
Created By: Manzz Baria
*/
public function forgetPassword($email){
  $isRegistered = $this->isUserEmailAlreadyRegister($email);
  if ($isRegistered) {
      $this->load->model("ApiEmailModel","emailModel");
    //  $this->emailModel->sendResetPasswordLink($email);
    $this->load->library('mylibrary');
    $url = base_url()."Api/Email/sendResetPasswordLink";
    $param = array('email' => "".$email );
    $this->mylibrary->do_in_background($url, $param);
      return true;
    }
    return false;

  }


public function displayCustomersbycountry($result){
  $customerObject = null;
  if($result != null){
    $this->load->model("Utility","utility");
    foreach ($result as $row) {
      $displayDate = $this->utility->timeAgoFormat($row->datetime);

      $Object = array(
        'Id'=>(int)$row->Id,
        'fullName'=>$row->fullName,
        'mobileNumber'=>$row->mobileNumber,
        'email'=>$row->email,
        'countryCode'=>(int)$row->countryCode,
        'fileUrl'=>IMAGE_URL.$row->fileUrl,
        'thumbUrl'=>THUMB_URL.$row->fileUrl,
        'datetime'=>$displayDate
      );
      $customerObject[]= $Object;
    
    }
  }
  return $customerObject;
}
public function updateLanguage($deviceToken,$languagecode)
{
  $data = array(
      'language' => $languagecode
    );
    $this->db->where('deviceToken', $deviceToken);
    $this->db->update('devicemaster', $data);
    return true;
}

public function callapilog($userId=0,$type=0,$apiname,$ip,$requestBody,$result)
{
//print_r($requestBody);
$data = array(
        'userId'=>$userId,
        'type'=>$type,
        'apiName'=>$apiname,
        'requestBody'=>json_encode($requestBody),
        'responseBody'=>json_encode($result),
        'ip'=>$ip);
      
      $this->db->insert('apilog', $data);
      $insert_Id = $this->db->insert_id();
    //  $this->sMSModel->sendOTPSMS($insert_Id);
    //  $this->utility->sendEmailForApproved($insert_Id);
      return true;
}

}
?>
