<?php if( ! defined("BASEPATH")) exit('No direct script access allowed');

class ApiEvaluationModel extends CI_Model {

  public function _construct(){
		parent::_construct();
	}

  /* Method to addDeviceToken
     Created By: Manzz Baria
  */
  public function addEvaluation($evaluation){
    $data = array('evaluation'=>$evaluation);
    $this->db->insert('evaluationmaster', $data);
    $insert_Id = $this->db->insert_id();
    return $this->getEvaluation($insert_Id);
  }

  /* Method to getEvaluation
   Created By: Manzz Baria
  */
  public function getEvaluation($evaluationId){
      $this->db->select("Id,evaluation,datetime");
      $this->db->from("evaluationmaster");
      $this->db->where('Id', $evaluationId);
      $query = $this->db->get();
      return $this->displayEvaluation($query->result());
  }

  /* Method to displayEvaluation
     Created By: Manzz Baria
  */
  public function displayEvaluation($result){
    $Object = null;
    if($result != null){
      $this->load->model("Utility","utility");
      foreach ($result as $row) {
        $displayDate = $this->utility->timeAgoFormat($row->datetime);
        $Object = array(
          'Id'=>(int)$row->Id,
          'evaluation'=>$row->evaluation,
          'datetime'=>$displayDate
        );
         return $Object;
      }
    }
  }

  /* Method to getEvaluations
   Created By: Manzz Baria
  */
  public function getEvaluations(){
      $this->db->select("Id,evaluation,datetime");
      $this->db->from("evaluationmaster");
      $query = $this->db->get();
      return $this->displayEvaluations($query->result());
  }

  /* Method to getEvaluationByRequestId
   Created By: Manzz Baria
  */
  public function getEvaluationByRequestId($requestId){
      $this->db->select("evaluationmaster.Id,evaluationmaster.evaluation,evaluationmaster.datetime");
      $this->db->from("evaluationmaster");
      $this->db->join('requestevaluationmaster','evaluationmaster.Id = requestevaluationmaster.evaluationId','inner');
      $this->db->where('requestevaluationmaster.requestId',$requestId);
      $query = $this->db->get();
      return $this->displayEvaluations($query->result());
  }

  /* Method to displayEvaluations
     Created By: Manzz Baria
  */
  public function displayEvaluations($result){
    $companyObject = null;
    $Object = null;
    if($result != null){
      $this->load->model("Utility","utility");
      foreach ($result as $row) {
        $displayDate = $this->utility->timeAgoFormat($row->datetime);
        $Object = array(
          'Id'=>(int)$row->Id,
          'evaluation'=>$row->evaluation,
          'datetime'=>$displayDate
        );
         $companyObject[]=$Object;
      }
    }
    return $companyObject;
  }

  /* Method to addEvaluationOnRequest
     Created By: Manzz Baria
  */
public function addEvaluationOnRequest($requestId,$evaluationIds){

    $this->db->where('requestId', $requestId);
    $this->db->delete('requestevaluationmaster');
    $this->db->affected_rows();

    $evaluationIds = str_replace(' ', '', $evaluationIds);
    $evaluationId = explode(',', $evaluationIds);
    $this->load->model("Utility","utility");
    //$this->load->model("ApiEmailModel","emailModel");
    $datetime = $this->utility->getCurrentDate('Y/m/d h:i:s');
    for ($i=0; $i< sizeof($evaluationId); $i++) {
          $isAdded = $this->isEvaluationAlreadyAdded($requestId,$evaluationId[$i]);
          if (!$isAdded) {
              $data = array(
                          'requestId'=>$requestId,
                          'evaluationId'=>$evaluationId[$i],
                          'datetime'=>$datetime);
              $this->db->insert('requestevaluationmaster', $data);
              $insert_Id = $this->db->insert_id();
        }
   }
   $this->load->library('mylibrary');
   $url = base_url()."Api/Email/sendEvaluationEmailToAdmin";
   $param = array('appointmentId' => "".$appointmentId );
   $this->mylibrary->do_in_background($url, $param);
  // $this->emailModel->sendEvaluationEmailToAdmin($requestId);
   return $this->getEvaluationByRequestId($requestId);
}
/* Method to check isRequestDetailAlreadyAdded
   Created By: Manzz Baria
*/
public function isEvaluationAlreadyAdded($requestId,$evaluationId){
  $this->db->select('Id');
  $this->db->from('requestevaluationmaster');
  $this->db->where("requestId",$requestId);
  $this->db->where("evaluationId",$evaluationId);
  $query = $this->db->get();
  if ($query->result() != null) {
    return true;
  }
  return false;
}



}
?>
