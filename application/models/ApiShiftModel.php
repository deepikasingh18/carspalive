<?php if( ! defined("BASEPATH")) exit('No direct script access allowed');

class ApiShiftModel extends CI_Model {

  public function _construct(){
		parent::_construct();
	}


  /* Method to getTestShift
     Created By: Manzz Baria
  */
  public function getTestShift($date){
    $this->db->select("shiftmaster.Id,shiftmaster.title,shiftmaster.startDate,shiftmaster.endDate,shiftmaster.datetime,vanownermaster.name");
    $this->db->from("shiftmaster");
    $this->db->join("vanownermaster","vanownermaster.shiftId = shiftmaster.Id","inner");
    $whereStr = "('".$date." 00:00:00' BETWEEN shiftmaster.startDate AND shiftmaster.endDate)";
    $this->db->where("shiftmaster.isBlock",UNBLOCKED);
    $this->db->where($whereStr);
    $query = $this->db->get();
    return $query->result();
}
  /*
     Method to getShiftsByDate
     Created By: Nishit Patel
  */

  public function getShiftsByDate($date){
    $this->db->select("Id,title,startDate,endDate,datetime");
    $this->db->from("shiftmaster");
    $whereStr = "('".$date." 00:00:00' BETWEEN startDate AND endDate)";
    $this->db->where("isBlock",UNBLOCKED);
    $this->db->where($whereStr);
    $query = $this->db->get();
    $result = $query->result();
    $shiftIdList = [];
    if($result != null){
      foreach ($result as $row) {
        //$shiftTime = $this->getShiftDetailsByShiftId($row->Id);
        $array = array("Id"=>(int)$row->Id);
        $shiftIdList[] = $array;
      }
    }
    return $shiftIdList;
  }

  /* Method to get shift details by shift id
     Created By: Nishit Patel
  */
  public function getShiftDetailsByShiftId($shiftId){
    $this->db->select("Id,shiftId,startTime,endTime");
    $this->db->from("shiftdetails");
    $this->db->where("shiftId",$shiftId);
    $this->db->where("isBlock",UNBLOCKED);
    $this->db->order_by("startTime,endTime");
    $query = $this->db->get();
    $result = $query->result();
    $shiftList = [];
    if($result != null){
        foreach ($result as $row) {
          $array = array("Id"=>(int)$row->Id,"startTime"=>$row->startTime,"endTime"=>$row->endTime);
          $shiftList[] = $array;
        }
    }
    return $shiftList;
  }

  /* Method to updateOwnerShift
     Created By: Manzz Baria
  */
  public function updateOwnerShift($shiftId,$ownerIds){

      $this->db->query('update vanownermaster set shiftId = '.$shiftId.' where Id in ('.$ownerIds.')');
      return $this->getShiftDate($shiftId);
  }


    /*
     Method to removeownerFromShift
     Created By: Manzz Baria
  */
  public function removeownerFromShift($shiftId){

     $query='update vanownermaster set shiftId = 0 where shiftId = '. $shiftId;
     $this->db->query($query);
      return $this->getShiftDate($shiftId);
  }

  /*
     Method to removeProviderFromShift
     Created By: Manzz Baria
  */
  public function removeProviderFromShift($shiftId,$ownerIds){

      $this->db->query('update vanownermaster set shiftId = 0 where Id in ('.$ownerIds.')');
      return $this->getShiftDate($shiftId);
  }

  /*
    Method to addShiftDate
     Created By: Manzz Baria
  */

  public function addShiftDate($title,$startDate,$endDate,$ownerIds,$days,$dates){
    $startDate = $startDate ." 00:00:00";
    $endDate = $endDate ." 23:59:59";
    $this->load->model("Utility","utility");
    $this->load->model("ApiHolidayModel","holidayModel");
    $datetime = $this->utility->getCurrentDate('Y/m/d h:i:s');
    $data = array(
                    'title'=>$title,
                    'startDate'=>$startDate,
                    'endDate'=>$endDate,
                    'datetime'=>$datetime
                 );
    $this->db->insert('shiftmaster', $data);
    $shiftId = $this->db->insert_id();
    $this->updateOwnerShift($shiftId,$ownerIds);
    $this->holidayModel->addShiftHolidays($shiftId,$days,$dates);
    return $this->getShiftDate($shiftId);

  }

  /*
    Method to getShiftDate
    Created By: Manzz Baria
  */

  public function getShiftDate($shiftId){
      $this->db->select("Id,title,startDate,endDate,isBlock,datetime");
      $this->db->from("shiftmaster");
      $this->db->where('Id', $shiftId);
      $query = $this->db->get();
      return $this->displayShiftDate($query->result());
  }

  /* Method to displayShiftDate
     Created By: Manzz Baria
  */
  public function displayShiftDate($result){
    $Object = null;
    if($result != null){
      $this->load->model("Utility","utility");
      $this->load->model("ApiHolidayModel","holiModel");
      foreach ($result as $row) {
        $displayDate = $this->utility->timeAgoFormat($row->datetime);
        $provides = $this->getProvidersOfshift($row->Id);
        $holidays = $this->holiModel->getHolidays($row->Id);
        $Object = array(
          'Id'=>(int)$row->Id,
          'title'=>$row->title,
          'startDate'=>$row->startDate,
          'endDate'=>$row->endDate,
          'isBlock'=>(bool)$row->isBlock,
          'datetime'=>$displayDate,
          'holidays'=>$holidays,
          'provider'=>$provides
        );
         return $Object;
      }
    }
  }

  /* Method to addShiftTime
     Created By: Manzz Baria
  */
  public function addShiftTime($shiftId,$startTime,$endTime){
    $this->load->model("Utility","utility");
    $datetime = $this->utility->getCurrentDate('Y/m/d h:i:s');
    $data = array(
                    'shiftId'=>$shiftId,
                    'startTime'=>$startTime,
                    'endTime'=>$endTime,
                    'datetime'=>$datetime
                 );
    $this->db->insert('shiftdetails', $data);
    $shiftTimeId = $this->db->insert_id();
    return $this->getShiftTime($shiftTimeId);
  }

  /*
    Method to getShiftTime
    Created By: Manzz Baria
  */
  public function getShiftTime($Id){
      $this->db->select("Id,shiftId,startTime,endTime,isBlock,datetime");
      $this->db->from("shiftdetails");
      $this->db->where('Id', $Id);
      $query = $this->db->get();
      return $this->displayShiftTime($query->result());
  }

  /* Method to displayShiftDate
     Created By: Manzz Baria
  */
  public function displayShiftTime($result){
    $Object = null;
    if($result != null){
      $this->load->model("Utility","utility");
      foreach ($result as $row) {
        $displayDate = $this->utility->timeAgoFormat($row->datetime);
        $Object = array(
          'Id'=>(int)$row->Id,
          'shiftId'=>(int)$row->shiftId,
          'startTime'=>$row->startTime,
          'endTime'=>$row->endTime,
          'isBlock'=>(bool)$row->isBlock,
          'datetime'=>$displayDate
        );
         return $Object;
      }
    }
  }


  /* Method to getShiftTimes
   Created By: Manzz Baria
  */
  public function getShiftTimes($shiftId){
      $this->db->select("Id,shiftId,startTime,endTime,isBlock,datetime");
      $this->db->from("shiftdetails");
      $this->db->where('shiftId', $shiftId);
      $this->db->where('isBlock', UNBLOCKED);
      $query = $this->db->get();
      return $this->displayShiftTimes($query->result());
  }

  /* Method to displayShiftTimes
     Created By: Manzz Baria
  */
  public function displayShiftTimes($result){
    $shiftObject = null;
    $Object = null;
    if($result != null){
      $this->load->model("Utility","utility");
      foreach ($result as $row) {
        $displayDate = $this->utility->timeAgoFormat($row->datetime);
        $Object = array(
          'Id'=>(int)$row->Id,
          'shiftId'=>$row->shiftId,
          'startTime'=>$row->startTime,
          'endTime'=>$row->endTime,
          'isBlock'=>(bool)$row->isBlock,
          'datetime'=>$displayDate
        );
         $shiftObject[]=$Object;
      }
    }
    return $shiftObject;
  }

  /*
    Method to getShifts
    Created By: Manzz Baria
  */
  public function getShifts(){
      $this->db->select("Id,title,startDate,endDate,isBlock,datetime");
      $this->db->from("shiftmaster");
      $this->db->where('isBlock', UNBLOCKED);
      $query = $this->db->get();
      return $this->displayShifts($query->result());
  }


  /*
     Method to displayShifts
     Created By: Manzz Baria
  */

  public function displayShifts($result){
    $companyObject = null;
    $Object = null;
    if($result != null){
      $this->load->model("Utility","utility");
      $this->load->model("ApiHolidayModel","holiModel");
      foreach ($result as $row) {
        $displayDate = $this->utility->timeAgoFormat($row->datetime);
        $times = $this->getShiftTimes($row->Id);
        $provides = $this->getProvidersOfshift($row->Id);
        $holidays = $this->holiModel->getHolidays($row->Id);
        $Object = array(
          'Id'=>(int)$row->Id,
          'title'=>$row->title,
          'startDate'=>$row->startDate,
          'endDate'=>$row->endDate,
          'isBlock'=>(bool)$row->isBlock,
          'datetime'=>$displayDate,
          'Times'=>$times,
          'holidays'=>$holidays,
          'provider'=>$provides
        );
         $companyObject[]=$Object;
      }
    }
    return $companyObject;
  }

  /*
     Method to updateShiftDate
     Created By: Manzz Baria
  */
  public function updateShiftDate($shiftId,$title,$startDate,$endDate,$days,$dates,$ownerIds){
    if ($startDate != "") {
          $data = array(
                      'startDate'=>$startDate
                   );
         $this->db->where('Id', $shiftId);
         $this->db->update('shiftmaster', $data);
    }
    if ($title != "") {
          $data = array(
                      'title'=>$title
                   );
         $this->db->where('Id', $shiftId);
         $this->db->update('shiftmaster', $data);
    }
    if ($endDate != "") {
        $data = array(
                      'endDate'=>$endDate
                   );
         $this->db->where('Id', $shiftId);
         $this->db->update('shiftmaster', $data);
    }
    $this->load->model("ApiHolidayModel","holModel");
    $this->holModel->addShiftHolidays($shiftId,$days,$dates);
    if ($ownerIds != "") {
      $this->removeownerFromShift($shiftId);
      $this->updateOwnerShift($shiftId,$ownerIds);
    }
    return $this->getShiftDate($shiftId);

  }

  /*
     Method to updateShiftTime
     Created By: Manzz Baria
  */
  public function updateShiftTime($shiftTimeId,$startTime,$endTime){
    if ($startTime != "") {
      $data = array(
                      'startTime'=>$startTime
                   );
      $this->db->where('Id', $shiftTimeId);
      $this->db->update('shiftdetails', $data);
    }

    if ($endTime != "") {
      $data = array(
                      'endTime'=>$endTime
                   );
      $this->db->where('Id', $shiftTimeId);
      $this->db->update('shiftdetails', $data);
    }

    return $this->getShiftTime($shiftTimeId);

  }

  /*
     Method to blockedShiftDate
     Created By: Manzz Baria
  */
  public function blockedShiftDate($shiftId){
    $data = array(
                'isBlock'=>BLOCKED);
    $this->db->where('Id', $shiftId);
    $this->db->update('shiftmaster', $data);

    $this->db->where('shiftId', $shiftId);
    $this->db->update('shiftdetails', $data);

    return true;

  }

  /*
     Method to blockedShiftTime
     Created By: Manzz Baria
  */
  public function blockedShiftTime($shiftId){
    $data = array(
                'isBlock'=>BLOCKED);
    $this->db->where('Id', $shiftId);
    $this->db->update('shiftdetails', $data);

    return true;

  }


  /* Method to getProvidersOfshift
     Created By: Manzz Baria
  */
  public function getProvidersOfshift($shiftId){
    $this->db->select("Id,name,email,mobileNumber,address,shiftId");
    $this->db->from("vanownermaster");
    $this->db->where('shiftId',$shiftId);
    $query = $this->db->get();
    return $this->displayProviders($query->result());
  }

  /* Method to getProvidersOfshift
     Created By: Manzz Baria
  */
  public function getProviders(){
    $this->db->select("Id,name,email,mobileNumber,address,shiftId");
    $this->db->from("vanownermaster");
    $query = $this->db->get();
    return $this->displayProviders($query->result());
  }


  /*
      Method to displayProviders
       Created By: Manzz Baria
  */
    public function displayProviders($result){
      $shiftObject = null;
      if($result != null){
        foreach ($result as $row) {
          $Object = array(
            'Id'=>(int)$row->Id,
            'name'=>$row->name,
            'email'=>$row->email,
            'mobileNumber'=>$row->mobileNumber,
            'address'=>$row->address,
            'shiftId'=>(int)$row->shiftId,
          );
          $shiftObject[]=$Object;
        }
      }
      return $shiftObject;
    }


}
?>
