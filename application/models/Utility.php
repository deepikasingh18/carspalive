<?php

if( ! defined("BASEPATH")) exit('No direct script access allowed');

class Utility extends CI_Model {

  public function _construct(){
		parent::_construct();
	}


  /* Method to getSuffix
    Created By: Manzz Baria
  */
  public function getSuffix($requestId,$appointmentId){
    $suffix = '#'.$requestId.'-'.$appointmentId;
    return $suffix;
  }

    public function addHourTimes($operatorId, $mainTime,$format = 'Y/m/d H:i:s'){
    $addedHour = 3;
    $mainDateTime = new DateTime($mainTime);
    $mainDateTimeR = $mainDateTime->add(new DateInterval('PT' . $addedHour . 'H'))->format($format);
    return $mainTime;
  }

  /* Method to dumpTester
     Created By: Manzz Baria
  */
  public function dumpTester($url,$params,$result,$userId){
    $datetime = $this->getCurrentDate('Y/m/d h:i:s');

    $data = array(
    		'userId'=>$userId,
    		'url'=>$url,
                  'params'=>$params,
                  'result'=>$result);
    $this->db->insert('tester', $data);
  }

  /* Method to show Message
    Created By: Manzz Baria
  */
  public function showMessage($status,$message){
    $this->response([
      'Status' => $status,
      'Message' => $message
    ], REST_Controller::HTTP_OK);
  }
  /* Method to calcular distance between two coordinates
     Created By: Nishit Patel
  */
  public function calculetDistance($fromLat,$fromLong,$toLat,$toLog,$unit){
    $theta =(double) $fromLong - $toLog;
    $dist = sin(deg2rad($fromLat)) * sin(deg2rad($toLat)) +  cos(deg2rad($fromLat)) * cos(deg2rad($toLat)) * cos(deg2rad($theta));
    $dist = acos($dist);
    $dist = rad2deg($dist);
    $miles = $dist * 60 * 1.1515;
    $unit = strtoupper($unit);

    if ($unit == "K") {
       return ($miles * 1.609344);
    } else if ($unit == "N") {
       return ($miles * 0.8684);
    } else {
       return $miles;
    }
  }


  /* Method to get traveling distance between two coordinates
     Created By: Nishit Patel
  */
  function GetDrivingDistance($lat1,$long1,$lat2,$long2){
    $url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=".$lat1.",".$long1."&destinations=".$lat2.",".$long2."&mode=driving&language=pl-PL";
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    $response = curl_exec($ch);
    curl_close($ch);

    if ($response != null) {
      $response_a = json_decode($response, true);

      if ($response_a != null && $response_a['status'] == 'OK' && $response_a['rows'][0]['elements'][0]['status'] == 'OK') {
          $dist = $response_a['rows'][0]['elements'][0]['distance']['text'];
          $time = $response_a['rows'][0]['elements'][0]['distance']['text'];
          $distMiter = (float)$response_a['rows'][0]['elements'][0]['distance']['value'];

          $timeValue = (float)$response_a['rows'][0]['elements'][0]['duration']['value'];
          $timeMint = $timeValue / 60;
          $finalMint = round($timeMint);

          $distKM = $distMiter / 1000;
          $distFinalKM = round($distKM,1);

      }else{
        $distFinalKM = 100;
        $finalMint = 200;
      }
    }else{
      $distFinalKM = 100;
      $finalMint = 200;
    }



    return array('distance' => $distFinalKM, 'time' => $finalMint);
  }

  /* Method to change date format
     Created By: Nishit Patel
  */
  public function changeDateFromat($date,$format){
    return  date($format, strtotime($date));
  }

  /* Method to get Current date
    Created By: Nishit Patel
  */
  public function getCurrentDate($format){
      $date = date($format, time());
      return $date;
  }

  /* Method to get datetime in arabic
    Created By: Manzz Baria
  */
  public function getDateTimeInArabic($dateTime){
    $standard = array("0","1","2","3","4","5","6","7","8","9");
    $eastern_arabic_symbols = array("٠","١","٢","٣","٤","٥","٦","٧","٨","٩");
    $arabicDate = str_replace($standard , $eastern_arabic_symbols , $dateTime);
    return $arabicDate;
  }

  /* Method to getDayNameByDate
    Created By: Manzz Baria
  */
  public function getDayNameByDate($date){

    $datetime = $date.'00:00:00';
    $stamp = strtotime($datetime);
     $day = date("l", $stamp);

      return $day;
  }

  /* Method to add days in date
     Created By: Nishit Patel
  */
  public function addDaysToDate($date,$days,$format){
    $date = strtotime("+".$days." days", strtotime($date));
    return  date($format, $date);
  }

  /* Method to convert strign to date
     Crated By: Nishit patel
  */
  public function convertStringToDate($dateStr,$format){
    return DateTime::createFromFormat('d-m-Y', $requestDate)->format('d-m-Y');
  }

  /* Method to generate rondom string
    Created By: Manzz Baria
  */
  public function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, strlen($characters) - 1)];
    }
    return $randomString;
}

/* Method to generate rondom number
  Created By: Manzz Baria
*/
public function generateRandomNumber($length = 4) {
  $characters = '0123456789';
  $randomString = '';
  for ($i = 0; $i < $length; $i++) {
      $randomString .= $characters[rand(0, strlen($characters) - 1)];
  }
  return $randomString;
}


  /* Method to get Readable time format
    Created By: Nishit Patel
  */
  public function timeAgoFormat($time_ago,$language =LANGUAGE_ENGLISH){

      $time_ago = strtotime($time_ago);
		  $cur_time   = time();

      $d = $this->getCurrentDate("Y/m/d h:i:s");

			$cur_time = strtotime($d);

		  $time_elapsed   = $cur_time - $time_ago;
		  $seconds    = $time_elapsed ;
		  $minutes    = round($time_elapsed / 60 );
		  $hours      = round($time_elapsed / 3600);
		  $days       = round($time_elapsed / 86400 );
		  $weeks      = round($time_elapsed / 604800);
		  $months     = round($time_elapsed / 2600640 );
		  $years      = round($time_elapsed / 31207680 );
		  // Seconds
      $message = "";
		  if($seconds <= 60){
		      $message = "just now";
          if ($language == LANGUAGE_ARABIC) {
            $message = 'الآن';
          }
		  }
		  //Minutes
		  else if($minutes <=60){
		      if($minutes==1){
		          $message = "one minute ago";
              if ($language == LANGUAGE_ARABIC) {
                $message = ' منذ دقيقة واحدة';
              }
		      }else{
		          $message = $minutes." minutes ago";
              if ($language == LANGUAGE_ARABIC) {
                $message = "منذ ".$minutes."دقيقة ";
              }
		      }
		  }
		  //Hours
		  else if($hours <=24){
		      if($hours==1){
		          $message = "an hour ago";
              if ($language == LANGUAGE_ARABIC) {
                $message = " منذ ساعة واحدة";
              }
		      }else{
		          $message = $hours." hrs ago";
              if ($language == LANGUAGE_ARABIC) {
                $message ="منذ  ".$hours." ساعة";
              }
		      }
		  }
		  //Days
		  else if($days <= 7){
		      if($days==1){
		          $message = "yesterday";
              if ($language == LANGUAGE_ARABIC) {
                $message = 'في الامس';
              }
		      }else{
		          $message = $days." days ago";
              if ($language == LANGUAGE_ARABIC) {
                $message = "قبل ".$days." أيام";
              }
		      }
		  }
		  //Weeks
		  else if($weeks <= 4.3){
		      if($weeks==1){
		          $message = "a week ago";
              if ($language == LANGUAGE_ARABIC) {
                $message = ' منذ أسبوع';
              }
		      }else{
		          $message = $weeks." weeks ago";
              if ($language == LANGUAGE_ARABIC) {
                $message = " منذ ".$weeks." أسبوع";
              }
		      }
		  }
		  //Months
		  else if($months <=12){
		      if($months==1){
		          $message = "a month ago";
              if ($language == LANGUAGE_ARABIC) {
                $message = 'قبل شهر';
              }
		      }else{
		          $message = $months." months ago";
              if ($language == LANGUAGE_ARABIC) {
                $message = "منذ ".$months." اشهر";
              }
		      }
		  }
		  //Years
		  else{
		      if($years==1){
		          $message = "one year ago";
              if ($language == LANGUAGE_ARABIC) {
                $message = "قبل عام واحد";
              }
		      }else{
		          $message = $years." years ago";
              if ($language == LANGUAGE_ARABIC) {
                $message = "منذ ".$years." سنة";
              }
		      }
		  }
      return $message;
	}

  /* Method to getDifferenceInHour
    Created By: Nishit Patel
  */
  public function getDifferenceInHour($time_ago){

      $time_ago = strtotime($time_ago);
		  $cur_time   = time();

      $d = $this->getCurrentDate("Y/m/d h:i:s");

			$cur_time = strtotime($d);

		  $time_elapsed   = $time_ago - $cur_time;
		  $seconds    = $time_elapsed ;
		  $minutes    = round($time_elapsed / 60 );
		  $hours      = round($time_elapsed / 3600);
		  $days       = round($time_elapsed / 86400 );
		  $weeks      = round($time_elapsed / 604800);
		  $months     = round($time_elapsed / 2600640 );
		  $years      = round($time_elapsed / 31207680 );



		  return $hours;


	}

  /* Method to Convert bytes into Readable size form
     Created By: Nishit Patel
  */
  function formatSizeUnits($bytes){

      if ($bytes >= 1073741824){
        $bytes = number_format($bytes / 1073741824, 2) . ' GB';
      }elseif ($bytes >= 1048576){
        $bytes = number_format($bytes / 1048576, 2) . ' MB';
      }elseif ($bytes >= 1024){
        $bytes = number_format($bytes / 1024, 2) . ' KB';
      }elseif ($bytes > 1){
        $bytes = $bytes . ' bytes';
      }elseif ($bytes == 1){
        $bytes = $bytes . ' byte';
      }else{
        $bytes = '0 bytes';
      }
      return $bytes;
	}



  /* ENCRYPT String
			Return enycrypted string
			Created By: Nishit Patel
		*/
		function encrypt($text)
		{
			  $SALT ='whateveryouwantwhateveryouwant'.'\0';
		    $password = trim(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $SALT, $text, MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND))));
        //$password = str_replace(' ', '', $password);
        return $password;
		}

		/* DECRYPT String
			Return decrypted string
			Created By: Nishit Patel
		*/
		function decrypt($text)
		{
			$SALT ='whateveryouwantwhateveryouwant'.'\0';
		    return trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $SALT, base64_decode($text), MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND)));
		}

    /* Method to get distance bitween two lat lon pass with unit = 'K' for killometer
       Created By: Nishit Patel
    */
    function distance($lat1,$lon1,$lat2,$lon2,$unit){
        $theta=$lon1-$lon2;
        $dist=sin(deg2rad($lat1))*sin(deg2rad($lat2))+cos(deg2rad($lat1))*cos(deg2rad($lat2))*cos(deg2rad($theta));
        $dist=acos($dist);
        $dist=rad2deg($dist);
        $miles=$dist*60*1.1515;
        $unit=strtoupper($unit);
        if ($unit=="K"){
          return ($miles*1.609344);
        }
        else if ($unit=="N"){
          return ($miles*0.8684);
        }
        else{
          return $miles;
      }
    }

function sentTestEmail($email){
  // Email configuration
  $body = "";
  $password = "testing email";
  $language = 'en';

								  $this->load->library('email');
								  if ($language == 'ar') {
							            $body = 'رمز التفعيل هو '.$password;
							      }else{
							            $body = 'Your access code was ' .$password;
							      }
								    $result = $this->email
					                ->from('support@carspaco.com','CarSpa')
					                ->to($email)
					                ->subject('Thank you for contacting us')
					                ->message($body)
					                ->send();

								return $result;
}


    /* Method send email
     Created By: Manzz Baria
     */
     public function sendEmailForApproved($userId){
       $this->load->model("ApiUserModel","userModel");
       $email = $this->userModel->getEmail($userId);
         $username = $this->userModel->getFullName($userId);
         $accessToken = $this->generateRandomString(16);
         $this->userModel->saveAccessToken($accessToken,$userId);

         $body=' <div style="max-width:100%"  align="center"><div style="max-width:100%;
         "align="center"><div style="border:1px solid teal;
         "align="center" id="emb-email-header"><img src="http://dev.mobileartsme.com/carcleanapi/electra.png
         " alt="" height="50px" align="middle"></div><br>
         <div align="left"><p style="Margin-top: 0;color: #565656;font-family: Georgia,serif;font-size:
           20px;line-height: 25px;Margin-bottom: 25px;margin-left:125px;;Margin-top: 30px">Hi, '.$username.'</p></div>
           <p style="Margin-top: 0;color: #565656;font-family: Georgia,serif;font-size: 18px;
           line-height: 10px;Margin-bottom: 40px;">Here is your carclean account verification link</p>
           <p style="Margin-bottom: 35px;"><a href="'.base_url().'Tdgdgdgfhankscntrl?userId='.$userId.'&accessToken='.$accessToken.'"
           style="text-decoration:none;border:solid 1px;color:teal;padding:10px;font-size: 14px;">Verify</a></p>
           <p style="Margin-top: 0;color: #565656;font-family: Georgia,serif;font-size: 18px;
           line-height: 25px;Margin-bottom: 25px;margin-left:80px;">If you ignore this message, your carclean account wont be active.</p>
           <p style="Margin-top: 0;color: #565656;font-family: Georgia,serif;font-size: 18px;
           line-height: 25px;Margin-bottom: 25px;margin-left:80px;">Thanks for using CarClean!</p></div>
           <p style="Margin-top: 0;color: #565656;font-family: Georgia,serif;font-size: 18px;
           line-height: 25px;Margin-bottom: 25px;margin-left:80px;">The CarClean Team</p></div></div>';
           $config = Array(
           'protocol' => 'smtp',
           'smtp_host' => 'ssl://smtp.googlemail.com',
           'smtp_port' => 465,
           'smtp_user' => 'teamcontactme@gmail.com',
           'smtp_pass' => 'ashapura228',
           'mailtype' => 'html',
           'charset' => 'iso-8859-1',
           'wordwrap' => TRUE
         );
         $this->load->library('email', $config);
         $this->email->initialize($config);

         $this->email->set_newline("\r\n");
         $this->email->from('teamcontactme@gmail.com', 'Team CarClean');
         $this->email->to($email);
         $this->email->subject('Here is link to active your CarClean account');
         $this->email->message($body);
         if (!$this->email->send()) {
           //	show_error($this->email->print_debugger());
           $msg='Sorry Unable to send email...'; }
           else {

             $msg='Your e-mail has been sent!';
           }
           return true;
       }


  function sendEmailToAdminForNewRequest($requestId){


      $this->load->model("ApiAppoinmnetModel","appoinmnetModel");
      $this->load->model("ApiRequestModel","requestModel");
      $this->load->model("ApiUserModel","userModel");

      $userId = $this->requestModel->getUserIdOfRequest($requestId);
      $userName = $this->userModel->getFullName($userId);
      $address = $this->requestModel->getAddressOfRequest($requestId);
      $carType = $this->requestModel->getCarTypeOfRequest($requestId);
      $category = $this->requestModel->getCategoryOfRequest($requestId);
      $appointmentTime = $this->appoinmnetModel->getTimeOfAppointment($requestId);

      $body = "Hi";
    /*  $body ="Hi,".$userName." has fix new appointment on ".$appointmentTime.". Address of the appointment is ".$address.
      " CarType : ".$carType". Categoty: ".$category;
      */
      $this->load->library('email');
      $result = $this->email
              ->from('support@carspaco.com','CarSpa')
              ->to('bariamanojfr@gmfdfgail.com')
              ->subject("New appointment on ".$appointmentTime)
              ->message($body)
              ->send();

      return $result;

  }

  function sendFeedbackEmail($feedback,$name,$email,$phoneNumber){

      $body = "Hi, Here is new feedback from ".$name ." Email: ".$email ." Contact number : ".$phoneNumber."Feedback: ".$feedback;
      $this->load->library('email');
      $result = $this->email
              ->from('support@carspaco.com','CarSpa')
              ->to('bariamanfsoj@gmadsffil.com')
              ->subject('Feedback')
              ->message($body)
              ->send();

      return $result;
  }


}
?>
