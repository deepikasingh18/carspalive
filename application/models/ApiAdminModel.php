<?php if( ! defined("BASEPATH")) exit('No direct script access allowed');

class ApiAdminModel extends CI_Model {

  public function _construct(){
		parent::_construct();
	}


  /* Method to updateMaxDistance
     Created By: Manzz Baria
  */
  public function updateMaxDistance($maxDistance){
        $data = array(
                'maxdistance' => $maxDistance
                         );
        $this->db->where('Id', 1);
        $this->db->update('vanownermaster', $data);
        return $maxDistance;

  }

  /*
      Method getMaxDistance
      Created By: Manzz Baria
   */
   public function getMaxDistance(){
     $this->db->select('maxDistance');
     $this->db->from('support');
     $this->db->where('Id',1);
     $query = $this->db->get();
     $result = $query->result();
     $maxDistance = null;
     if($result != null){
       foreach ($result as $row) {
         $maxDistance = $row->maxDistance;
       }
     }
     return $maxDistance;
   }


  /* Method to updateContacts
     Created By: Manzz Baria
  */
  public function updateContacts($name,$email,$countryCode,$phoneNumber,$address){
    $this->load->model("Utility","utility");
    $datetime = $this->utility->getCurrentDate('Y/m/d h:i:s');
        $data = array(
                'name' => $name,
                'email' => $email,
                'countryCode' => $countryCode,
                'phoneNumber' => $phoneNumber,
                'address' => $address,
                'datetime' => $datetime
                         );
        $this->db->where('Id', 1);
        $this->db->update('support', $data);
        return $this->getCustomerServiceSupport();

  }

  /* Method to getCustomerServiceSupport
     Created By: Manzz Baria
  */
  public function getCustomerServiceSupport(){
    $this->load->model("Utility","utility");
    $this->db->select("Id,name,maxDistance,email,countryCode,phoneNumber,address,datetime");
    $this->db->from("support");
    $this->db->where("Id",1);
    $query = $this->db->get();
    return $this->displayCustomerServiceSupport($query->result());
  }

  /* Method to displayCustomerServiceSupport
   Created By: Manzz Baria
*/
public function displayCustomerServiceSupport($result){
  $Object = null;
  if($result != null){
      foreach ($result as $row) {
      $Object = array(
        'Id'=>(int)$row->Id,
        'maxDistance'=>$row->maxDistance,
        'name'=>$row->name,
        'email'=>$row->email,
        'countryCode'=>$row->countryCode,
        'phoneNumber'=>$row->phoneNumber,
        'address'=>$row->address,
        'datetime'=>$row->datetime
      );
      return $Object;
    }
  }
  return $Object;
}


  /* Method to adminLogin
     Created By: Manzz Baria
  */
  public function adminLogin($email,$password){
    $this->load->model("Utility","utility");
    $encryptedPassword = $this->utility->encrypt($password);
    $this->db->select("Id,fullName,email");
    $this->db->from("usermaster");
    $this->db->where("email",$email);
    $this->db->where("password",$encryptedPassword);
    $query = $this->db->get();
    return $this->displayAdminLogin($query->result());
  }

  /* Method to displayAdminLogin
   Created By: Manzz Baria
*/
public function displayAdminLogin($result){
  $Object = null;
  if($result != null){
      foreach ($result as $row) {
      $Object = array(
        'Id'=>(int)$row->Id,
        'fullName'=>$row->fullName,
        'email'=>$row->email
      );
      return $Object;
    }
  }
  return $Object;
}

  /* Method to adminDashboard
     Created By: Manzz Baria
  */
  public function adminDashboard($adminId,$todayDate){
    $this->db->select("Id,fullName,email");
    $this->db->from("usermaster");
    $this->db->where("Id",$adminId);
    $query = $this->db->get();
    return $this->displayAdmin($query->result(),$todayDate);
  }
    /* Method to displayAdmin
     Created By: Manzz Baria
  */
  public function displayAdmin($result,$todayDate){
    $Object = null;
    if($result != null){
      $this->load->model("ApiVanModel","vanModel");
      $this->load->model("ApiUserModel","userModel");
      $this->load->model("ApiRequestModel","requestModel");
      $this->load->model("ApiAppoinmnetModel","appoinmnetModel");

      $requests = $this->appoinmnetModel->getAdminAppointmentsByTwoDates($todayDate,$todayDate,0);
      $TotalVans = $this->vanModel->getTotalVans();
      $TotalCustomer = $this->userModel->getTotalCustomers();
      $TotalAppoinments = $this->getTotalAppoinments();
      $TotalCompletedRequests = $this->getTotalRequestsByStatus(REQUEST_STATUS_FINISH);
      $TotalPendingRequests = $this->getTotalRequestsByStatus(REQUEST_STATUS_PENDING);
      $TotalRejectedRequests = $this->getTotalRequestsByStatus(REQUEST_STATUS_REJECTED);
      $TotalInProgressRequests = $this->getTotalRequestsByStatus(REQUEST_STATUS_IN_PROGRESS);

      foreach ($result as $row) {
        $Object = array(
          'Id'=>(int)$row->Id,
          'fullName'=>$row->fullName,
          'email'=>$row->email,
          'totalCustomer'=>(int)$TotalCustomer,
          'totalAppoinments'=>(int)$TotalAppoinments,
          'completedRequests'=>(int)$TotalCompletedRequests,
          'pendingRequests'=>(int)$TotalPendingRequests,
          'rejectedRequests'=>(int)$TotalRejectedRequests,
          'inProgressRequests'=>(int)$TotalInProgressRequests,
          'totalVan'=>(int)$TotalVans,
          'requests'=>$requests
        );
        return $Object;
      }
    }
    return $Object;
  }

  /* Method to getAllVanStatusInfo
     Created By: Manzz Baria
  */
  public function getAllVanStatusInfo(){
    $this->db->select("Id,name,type,status,datetime");
    $this->db->from("vanmaster");
    $this->db->order_by("Id","DESC");
    $query = $this->db->get();
    return $this->displayAllVanStatusInfo($query->result());
  }

  /* Method to displayAllVanStatusInfo
   Created By: Manzz Baria
*/
public function displayAllVanStatusInfo($result){
  $adminObject = null;
  if($result != null){
    $this->load->model("Utility","utility");
    foreach ($result as $row) {
      $datetime = $this->utility->timeAgoFormat($row->datetime);
      $Object = array(
        'Id'=>(int)$row->Id,
        'name'=>$row->name,
        'type'=>(int)$row->type,
        'status'=>(int)$row->status,
        'datetime'=>$datetime
      );
      $adminObject[]=$Object;
      return $Object;
    }
  }
  return $adminObject;
}

/* Method to getVanAppoinmentInfo
   Created By: Manzz Baria
*/
public function getVanAppoinmentInfo($vanId){
  $this->db->select("Id,requestId,reason,status,datetime");
  $this->db->from("appoinmentmaster");
  $this->db->where('vanId',$vanId);
  $query = $this->db->get();
  return $this->displayVanAppoinmentInfo($query->result());
}

/* Method to displayVanAppoinmentInfo
 Created By: Manzz Baria
*/
public function displayVanAppoinmentInfo($result){
$Object = null;
if($result != null){
  $this->load->model("Utility","utility");

  foreach ($result as $row) {
    $datetime = $this->utility->timeAgoFormat($row->datetime);
    $request = $this->getRequestLocationInfo($row->requestId);
    $Object = array(
      'Id'=>(int)$row->Id,
      'reason'=>$row->fullName,
      'status'=>$row->email,
      'datetime'=>$datetime,
      'requestAddress'=>$request
    );
    return $Object;
  }
}
return $Object;
}

/* Method to getRequestLocationInfo
   Created By: Manzz Baria
*/
public function getRequestLocationInfo($requestId){
  $this->db->select("addressmaster.Id,addressmaster.address,addressmaster.latitude,addressmaster.longitude");
  $this->db->from("requestmaster");
  $this->db->join("addressmaster","requestmaster.addressId = addressmaster.Id","inner");
  $this->db->where('requestmaster.Id',$requestId);
  $query = $this->db->get();
  return $this->displayRequestLocationInfo($query->result());
}

/* Method to displayRequestLocationInfo
 Created By: Manzz Baria
*/
public function displayRequestLocationInfo($result){
$Object = null;
if($result != null){
  foreach ($result as $row) {
    $Object = array(
      'Id'=>(int)$row->Id,
      'address'=>$row->address,
      'latitude'=>$row->latitude,
      'longitude'=>$row->longitude
    );
    return $Object;
  }
}
return $Object;
}

/* Method to getTotalRequested
Created By: Manzz Baria
*/
public function getTotalAppoinments(){
  $this->db->select("Id");
  $this->db->from("appoinmentmaster");
  $query = $this->db->count_all_results();
  return ceil($query);
}

/* Method to getTotalRequestsByStatus
Created By: Manzz Baria
*/
public function getTotalRequestsByStatus($status){
  $this->db->select("Id");
  $this->db->from("appoinmentmaster");
  $this->db->where("status",$status);
  $query = $this->db->count_all_results();
  return ceil($query);
}


  /* Method to change password
     Created By: Manzz Baria
  */
  public function changePassword($adminId,$newPassword){
    $this->load->model("Utility","utility");
    $decryptedPassword = $this->utility->encrypt($newPassword);
        $data = array(
                'password' => $decryptedPassword
                         );
        $this->db->where('Id', $adminId);
        $this->db->update('adminmaster', $data);
        return true;

  }
  /* Method to check currentpassword
     Created By: Manzz Baria
  */
  public function isCurrentPasswordMatched($Id,$password){
    $this->load->model("Utility","utility");
    $encryptedPassword = $this->utility->encrypt($password);
    $this->db->select('Id');
    $this->db->from('adminmaster');
    $strWhere = "Id='".$Id."' AND password='".$encryptedPassword."'";
    $this->db->where($strWhere,null,false);
    $query = $this->db->get();
    if ($query->result() != null) {
      return true;
    }
    return false;
  }







}
?>
