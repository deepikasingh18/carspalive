<?php if( ! defined("BASEPATH")) exit('No direct script access allowed');

class ApiAddressModel extends CI_Model {

  public function _construct(){
    parent::_construct();
  }

/* Method to check isAddressAlreadyAdded
   Created By: Manzz Baria
*/
public function isAddressAlreadyAdded($userId,$address){
  $this->db->select('Id');
  $this->db->from('addressmaster');
  $this->db->where("userId",$userId);
  $this->db->where("address",$address);
  $this->db->where("isBlocked",UNBLOCKED);
  $query = $this->db->get();
  if ($query->result() != null) {
    return true;
  }
  return false;
}

  /* Method to addAddress
     Created By: Manzz Baria
  */
  public function addAddress($userId,$address,$addresstitle,$latitude,$longitude){
    $isAdded = $this->isAddressAlreadyAdded($userId,$address);
      if (!$isAdded) {
        $data = array(
                    'userId'=>$userId,
                    'address'=>$address,
                    'addresstitle'=>$addresstitle,
                    'latitude'=>$latitude,
                    'longitude'=>$longitude,
                     'isBlocked'=>UNBLOCKED);
        $this->db->insert('addressmaster', $data);
        $insert_Id = $this->db->insert_id();
        return $this->getAddressById($insert_Id);
      }else{
        return null;
      }
  }

/* Method to getAddressByAppointmentId
   Created By: Manzz Baria
*/
public function getAddressByAppointmentId($appointmentId){
  $this->db->select("addressmaster.Id,addressmaster.userId,addressmaster.address,addressmaster.addresstitle,addressmaster.latitude,addressmaster.longitude");
  $this->db->from("addressmaster");
  $this->db->join('requestmaster','addressmaster.Id = requestmaster.addressId','inner');
  $this->db->join('appoinmentmaster','requestmaster.Id = appoinmentmaster.requestId','inner');
  $this->db->where('appoinmentmaster.Id',$appointmentId);
  $query = $this->db->get();
  return $this->displayAddressById($query->result());
}

  /* Method to getAddressById
   Created By: Manzz Baria
*/
public function getAddressById($addressId){
  $this->db->select("Id,userId,address,addresstitle,latitude,longitude");
  $this->db->from("addressmaster");
  $this->db->where("Id",$addressId);
  $query = $this->db->get();
  return $this->displayAddressById($query->result());
}

/* Method to displayAddressById
     Created By: Manzz Baria
  */
  public function displayAddressById($result){
    $Object = null;
    if($result != null){
      foreach ($result as $row) {
        $Object = array(
          'Id'=>(int)$row->Id,
          'userId'=>(int)$row->userId,
          'address'=>$row->address,
          'addresstitle'=>$row->addresstitle,
          'latitude'=>(double)$row->latitude,
          'longitude'=>(double)$row->longitude
        );
        return $Object;
      }
    }
    return $Object;
  }

  /* Method to updateAddress
     Created By: Manzz Baria
  */
  public function updateAddress($addressId,$userId,$address,$addresstitle,$latitude,$longitude){
      $isAdded = $this->isAddressAlreadyAdded($userId,$address);
      if (!$isAdded) {
          $this->load->model("Utility","utility");
          $datetime = $this->utility->getCurrentDate('Y/m/d h:i:s');
          $data = array(
                      'userId'=>$userId,
                      'address'=>$address,
                      'addresstitle'=>$addresstitle,
                      'latitude'=>$latitude,
                      'longitude'=>$longitude);
          $this->db->where('Id', $addressId);
          $this->db->update('addressmaster', $data);
          return $this->getAddressById($addressId);
      }
      return null;
  }


  /* Method to delete bids by bidId
     Created By: Manzz Baria
  */
  public function deleteAddressId($addressId){
    $data = array(
                'isBlocked'=>BLOCKED);
    $this->db->where('Id', $addressId);
    $this->db->update('addressmaster', $data);
    return true;

  }
  /* Method to getAddresses
   Created By: Manzz Baria
*/
public function getAddresses($userId){
  $this->db->select("Id,address,addresstitle,latitude,longitude");
  $this->db->from("addressmaster");
  $this->db->where('userId', $userId);
  $this->db->where('isBlocked', UNBLOCKED);
  $query = $this->db->get();
  return $this->displayAddresses($query->result());
}
/* Method to displayAddresses
     Created By: Manzz Baria
  */
  public function displayAddresses($result){
    $carObject = null;
    if($result != null){
      $this->load->model("Utility","utility");
      foreach ($result as $row) {
        $Object = array(
          'Id'=>(int)$row->Id,
          'address'=>$row->address,
          'addresstitle'=>$row->addresstitle,
          'latitude'=>(double)$row->latitude,
          'longitude'=>(double)$row->longitude
        );
        $carObject[]=$Object;
      }
    }
    return $carObject;
  }



}
?>
