<?php if( ! defined("BASEPATH")) exit('No direct script access allowed');

class ApiVanOperatorModel extends CI_Model {

  public function _construct(){
    parent::_construct();
  }



/* Method to check isVanOperatorAlreadyAdded
   Created By: Manzz Baria
*/
public function isVanOperatorAlreadyAdded($email,$mobileNumber = "12345"){
  $this->db->select('Id');
  $this->db->from('usermaster');
   //$this->db->where("email",$email);
    $strWhere = "email='".$email."' and  userType=2";
    $this->db->where($strWhere,null,false);
    $this->db->where_not_in("status",USER_STATUS_BLOCK);
  $query = $this->db->get();

  if ($query->result() != null) {
  
    return true;
   }

  return false;
   
}

 /*
    Method to check isUserMobileNumberIsOwn
    Created By: Manzz Baria
  */
  public function isUserMobileNumberIsOwn($mobileNumber,$userId){
    $this->db->select('Id');
    $this->db->from('usermaster');
    $this->db->where('mobileNumber',$mobileNumber);
    $this->db->where('Id',$userId);
    $this->db->where("userType",USER_TYPE_OPERATOR);
    $query = $this->db->get();
    if ($query->result() != null) {
      return true;
    }
    return false;
  }


  /*
    Method to check isUserMobileNumberAlreadyRegister
    Created By: Manzz Baria
  */
  public function isUserMobileNumberAlreadyRegister($mobileNumber){
    $this->db->select('Id');
    $this->db->from('usermaster');
    $this->db->where('mobileNumber',$mobileNumber);
    $this->db->where("userType",USER_TYPE_OPERATOR);
    $query = $this->db->get();

    if ($query->result() != null) {

      return true;
    }

    return false;
  }

/* Method to check isVanOperatorOnlineForVan
   Created By: Manzz Baria
*/
public function isVanOperatorOnlineForVan($vanId){
  $this->db->select('Id');
  $this->db->from('usermaster');
  $this->db->where("vanId",$vanId);
  $this->db->where("status",USER_STATUS_ONLINE);
   $this->db->where("isActive",1);
  $query = $this->db->get();
  if ($query->result() != null) {
    return true;
  }
  return false;
}

/* Method to check isVanOperatorAlreadyAdded
   Created By: Manzz Baria
*/
public function isMyEmailAddress($email,$userId){
  $this->db->select('Id');
  $this->db->from('usermaster');
  $this->db->where("email",$email);
  $this->db->where("Id",$userId);
   $this->db->where_not_in("status",USER_STATUS_BLOCK);
  $this->db->where("userType",2);
  $query = $this->db->get();
  if ($query->result() != null) {
    return true;
  }
  return false;
}
 /* Method to isVanOperatorAvailable
    Created By: Manzz Baria
 */
 public function isVanOperatorAvailable($vanId){
   $this->db->select("Id,fullName,email,fileUrl,countryCode,mobileNumber,latitude,longitude,status,datetime");
   $this->db->from("usermaster");
   $this->db->where("userType",USER_TYPE_OPERATOR);
   $this->db->where("isActive",1);
   $this->db->where("vanId",$vanId);
   $this->db->where_not_in("status",USER_STATUS_BLOCK);
   $this->db->order_by("Id","DESC");
   $query = $this->db->get();
  if ($query->result() != null) {
    return true;
  }
  return false;
 }


/* Method to check isVanOperatorAlreadyAddedORItsOwnEmail
   Created By: Manzz Baria
*/
public function isVanOperatorAlreadyAddedORItsOwnEmail($email,$userId){
  $isMy = $this->isMyEmailAddress($email,$userId);
  if (!$isMy) {
    return $this->isVanOperatorAlreadyAdded($email);
  }else{
    return false;
  }
}

/* Method to addVanOperator
   Created By: Manzz Baria
*/
public function addVanOperator($fullName,$address,$email,$countryCode,$mobileNumber,$password,$fileUrl){
  $isAdded = $this->isVanOperatorAlreadyAdded($email,$mobileNumber);
    if (!$isAdded) {
      $this->load->model("Utility","utility");
      $datetime = $this->utility->getCurrentDate('Y/m/d h:i:s');
      $securePassword = $this->utility->encrypt($password);
       
      $data = array(
        'fullName'=>$fullName,
        'email'=>$email,
        'countryCode'=>$countryCode,
        'mobileNumber'=>$mobileNumber,
        'userType'=>USER_TYPE_OPERATOR,
        'password'=>$securePassword,
        'isActive'=>1,
        'fileUrl'=>$fileUrl,
        'datetime'=>$datetime);
      $this->db->insert('usermaster', $data);
      $insert_Id = $this->db->insert_id();
      $this->addVanOperatorAddress($insert_Id,$address);

     return $this->getVanOperatorById($insert_Id);
       
    }else{
 
      return null;
    }
}

/* Method to addVanOperatorAddress
   Created By: Manzz Baria
*/
public function addVanOperatorAddress($userId,$address){
      $this->load->model("Utility","utility");
      $datetime = $this->utility->getCurrentDate('Y/m/d h:i:s');
      $data = array(
        'userId'=>$userId,
        'address'=>$address);
      $this->db->insert('addressmaster', $data);
      $insert_Id = $this->db->insert_id();
      return $userId;
}

/* Method to getVanOperatorById
 Created By: Manzz Baria
*/
public function getVanOperatorById($userId){
    $this->db->select("Id,fullName,email,fileUrl,countryCode,mobileNumber,latitude,longitude,status,datetime");
    $this->db->from("usermaster");
    $this->db->where_not_in("status",USER_STATUS_BLOCK);
    $this->db->where("Id",$userId);
    $query = $this->db->get();
    return $this->displayVanOperatorById($query->result());
}

/* Method to displayVanOperatorById
   Created By: Manzz Baria
*/
public function displayVanOperatorById($result){
  $Object = null;
  if($result != null){
    $this->load->model("Utility","utility");
    foreach ($result as $row) {
      $displayDate = $this->utility->timeAgoFormat($row->datetime);
      $address = $this->getVanOperatorAddress($row->Id);
      $Object = array(
        'Id'=>(int)$row->Id,
        'fullName'=>$row->fullName,
        'email'=>$row->email,
        'status'=>(int)$row->status,
        'fileUrl'=>IMAGE_URL.$row->fileUrl,
        'thumbUrl'=>THUMB_URL.$row->fileUrl,
        'countryCode'=>(int)$row->countryCode,
        'mobileNumber'=>$row->mobileNumber,
        'latitude'=>(double)$row->latitude,
        'longitude'=>(double)$row->longitude,
        'datetime'=>$displayDate,
        'address'=>$address
      );
      return $Object;
    }
  }
  return $Object;
}

/* Method get getVanOperatorAddress
 Created By: Manzz Baria
 */
 public function getVanOperatorAddress($userId){
   $this->db->select('address');
   $this->db->from('addressmaster');
   $this->db->where('userId',$userId);
   $query = $this->db->get();
   $result = $query->result();
   $address = null;
   if($result != null){
     foreach ($result as $row) {
       $address = $row->address;
     }
   }
   return $address;
 }

/* Method get getVanNamebyId
 Created By: Henal Bhandari
 */
 public function getVanNamebyId($vanId){
   $this->db->select('name');
   $this->db->from('vanmaster');
   $this->db->where('Id',$vanId);
   $query = $this->db->get();
   $result = $query->result();
   $address = null;
   if($result != null){
     foreach ($result as $row) {
       $name= $row->name;
     }
   }
   return $name;
 }


/*
     Method getRequestVanOwnerInfo
     Created By: Henal Bhandari
    */
    public function getVanOwnerInfoByVanId($vanId){
      $this->db->select('vanprovidercontact.email as contactEmail,vanprovidercontact.fullName as contactName,vanmaster.Id,vanmaster.name as vanName,vanmaster.type,vanmaster.ownerId,vanownermaster.name,vanownermaster.email,vanprovidercontact.countryCode,vanprovidercontact.mobileNumber,vanownermaster.address');
      $this->db->from('vanmaster');
      $this->db->join("vanownermaster","vanmaster.ownerId = vanownermaster.Id","inner");
      $this->db->join("vanprovidercontact","vanownermaster.Id = vanprovidercontact.vanOwnerId","inner");
      $this->db->where('vanmaster.Id',$vanId);
      $this->db->where('vanprovidercontact.isPrimary',1);
      $query = $this->db->get();
      return $this->displayVanOwnerInfo($query->result());
    }

   /*
       Method to displayVanOwnerInfo
       Created By: Henal Bhandari
    */
    public function displayVanOwnerInfo($result){
      if($result != null){
        foreach ($result as $row) {
          $Object = array(
            'Id'=>(int)$row->Id,
            'vanName'=>$row->vanName,
            'type'=>(int)$row->type,
            'ownerId'=>(int)$row->ownerId,
            'name'=>$row->name,
            'email'=>$row->email,
            'contactEmail'=>$row->contactEmail,
            'contactName'=>$row->contactName,
            'countryCode'=>$row->countryCode,
            'mobileNumber'=>$row->mobileNumber,
            'address'=>$row->address
          );
          return $Object;
        }
      }

    }



 /* Method to updateCategory
    Created By: Manzz Baria
 */
 public function updateVanOperator($userId,$fullName,$address,$email,$countryCode,$mobileNumber,$fileUrl,$password){

     if ($address != "" && $address != null) {
         $data = array(
           'address' => $address
           );
         $this->db->where('userId', $userId);
         $this->db->update('addressmaster', $data);
     }
     if ($fullName != "" && $fullName != null) {
         $data = array(
           'fullName' => $fullName
           );
         $this->db->where('Id', $userId);
         $this->db->update('usermaster', $data);
     }
     if ($email != "" && $email != null) {
         $data = array(
           'email' => $email
           );
         $this->db->where('Id', $userId);
         $this->db->update('usermaster', $data);
     }
     if ($mobileNumber != "" && $mobileNumber != null) {
         $data = array(
           'countryCode' => $countryCode,
           'mobileNumber' => $mobileNumber
           );
         $this->db->where('Id', $userId);
         $this->db->update('usermaster', $data);
     }
     if ($password != "" && $password != null) {
         $this->load->model("Utility","utility");
         $securePassword = $this->utility->encrypt($password);
         $data = array(
                'password' => $securePassword
           );
         $this->db->where('Id', $userId);
         $this->db->update('usermaster', $data);
     }
     if ($fileUrl != "" && $fileUrl != null) {
         $data = array(
           'fileUrl' => $fileUrl
           );
         $this->db->where('Id', $userId);
         $this->db->update('usermaster', $data);
     }
   return $this->getVanOperatorById($userId);

 }

 /* Method to deleteVanOperatorId
    Created By: Manzz Baria
 */
 public function blockVanOperatorId($userId){
   $status = USER_STATUS_BLOCK;
   $data = array(
     'status' => $status
     );
   $this->db->where('Id', $userId);
   $this->db->update('usermaster', $data);
   return true;
 }

 public function unBlockVanOperatorId($userId){
   $status = USER_STATUS_ONLINE;
   $data = array(
     'status' => $status
     );
   $this->db->where('Id', $userId);
   $this->db->update('usermaster', $data);
   return true;
 }

 /* Method to getTotalPagesForEnquiriesOnMyElectraByType
    Created By: Manzz Baria
 */
 public function getTotalPagesForVanOperators(){
   $this->db->select("Id,fullName,email,fileUrl,countryCode,mobileNumber,status,latitude,longitude,datetime");
   $this->db->from("usermaster");
   $this->db->where("userType",USER_TYPE_OPERATOR);
   $this->db->where_not_in("status",USER_STATUS_BLOCK);
   $query = $this->db->count_all_results();
   $result  = $query / 20;
   return ceil($result);
 }

 /* Method to getVanOperators
    Created By: Manzz Baria
 */
 public function getVanOperators($pageIndex){
   $this->db->select("Id,fullName,email,fileUrl,countryCode,mobileNumber,latitude,longitude,status,datetime");
   $this->db->from("usermaster");
   $this->db->where("userType",USER_TYPE_OPERATOR);
  //  $this->db->where("isActive",1);
   $this->db->where_not_in("status",USER_STATUS_BLOCK);
   $this->db->order_by("Id","DESC");
   $pageNo = "00";
   if ($pageIndex > 0) {
     $pageIndex = $pageIndex * 2;
     $pageNo = $pageIndex.'0';
   }
   $this->db->limit(20,$pageNo);
   $query = $this->db->get();
   return $this->displayVanOperators($query->result());
 }

  public function getAllVanOperators($pageIndex){
   $this->db->select("Id,fullName,email,fileUrl,countryCode,mobileNumber,latitude,longitude,status,datetime");
   $this->db->from("usermaster");
   $this->db->where("userType",USER_TYPE_OPERATOR);
  //  $this->db->where("isActive",1);
   $this->db->where_not_in("status",USER_STATUS_BLOCK);
   
   $query = $this->db->get();
   return $this->displayVanOperators($query->result());
 }
  
 public function searchOperators($name){
   $this->db->select("Id,fullName,email,fileUrl,countryCode,mobileNumber,latitude,longitude,status,datetime");
   $this->db->from("usermaster");
   $this->db->where("userType",USER_TYPE_OPERATOR);
  //  $this->db->where("isActive",1);
   $this->db->like('fullName', $name);
   $this->db->where_not_in("status",USER_STATUS_BLOCK);
   $query = $this->db->get();
   return $this->displayVanOperators($query->result());
 }
 
 /* Method to display Enquiry master
    Created By: Manzz Baria
 */
 public function displayVanOperators($result){
   $VanOperatorObject = null;
   if($result != null){
     $this->load->model("Utility","utility");
     foreach ($result as $row) {
       $displayDate = $this->utility->timeAgoFormat($row->datetime);
       $address = $this->getVanOperatorAddress($row->Id);
       $Object = array(
         'Id'=>(int)$row->Id,
         'fullName'=>$row->fullName,
         'status'=>(int)$row->status,
         'email'=>$row->email,
         'fileUrl'=>IMAGE_URL.$row->fileUrl,
         'thumbUrl'=>THUMB_URL.$row->fileUrl,
         'countryCode'=>(int)$row->countryCode,
         'mobileNumber'=>$row->mobileNumber,
         'latitude'=>(double)$row->latitude,
         'longitude'=>(double)$row->longitude,
         'address'=>$address,
         'datetime'=>$displayDate
       );
       $VanOperatorObject[]=$Object;

     }
   }
   return $VanOperatorObject;
 }

 /* Method to loginOperator
  Created By: Manzz Baria
 */
 public function loginOperator($email,$password){
     $this->load->model("Utility","utility");
     $isActive = 1;
     $securePassword = $this->utility->encrypt($password);
     $this->db->select("Id,fullName,email,countryCode,mobileNumber,fileUrl,status,latitude,longitude,datetime,vanId");
     $this->db->from("usermaster");
     $this->db->where("isActive",$isActive);
     $this->db->where("userType",USER_TYPE_OPERATOR);
     $this->db->where_not_in("status",USER_STATUS_BLOCK);
     if (strpos($email, '@') === FALSE) {
         $this->db->where("mobileNumber",$email);
     }else{
         $this->db->where("email",$email);
     }
     $this->db->where("password",$securePassword);
     $query = $this->db->get();
     return $this->displayloginOperator($query->result());
 }

 /* Method to displayVanOperatorById
    Created By: Manzz Baria
 */
 public function displayloginOperator($result){
   $Object = null;
   if($result != null){
     $this->load->model("Utility","utility");
     foreach ($result as $row) {
       $displayDate = $this->utility->timeAgoFormat($row->datetime);
       $address = $this->getVanOperatorAddress($row->Id);
      // $vanObject = $this->getVanDetails($row->vanId);
       $Object = array(
         'Id'=>(int)$row->Id,
         'fullName'=>$row->fullName,
         'email'=>$row->email,
         'countryCode'=>(int)$row->countryCode,
         'mobileNumber'=>$row->mobileNumber,
         'fileUrl'=>IMAGE_URL.$row->fileUrl,
         'thumbUrl'=>THUMB_URL.$row->fileUrl,
         'status'=>(int)$row->status,
         'latitude'=>(double)$row->latitude,
         'longitude'=>(double)$row->longitude,
         'address'=>$address,
         'datetime'=>$displayDate
       );
       return $Object;
     }
   }
   return $Object;
 }
 
     /* Method getOperatorIdFromEmail
     Created By: Manzz Baria
     */
     public function getOperatorIdFromEmail($email){
       $this->db->select('Id');
       $this->db->from('usermaster');
       if (strpos($email, '@') === FALSE) {
           $this->db->where("mobileNumber",$email);
       }else{
           $this->db->where("email",$email);
       }
       $this->db->where("userType",USER_TYPE_OPERATOR);
       $query = $this->db->get();
       $result = $query->result();
       $Id = null;
       if($result != null){
         foreach ($result as $row) {
           $Id = $row->Id;
         }
       }
       return $Id;
     }


 /* Method to getVanOperatorsByVanId
    Created By: Manzz Baria
 */
 public function getVanOperatorsByVanId($vanId){
   $this->db->select("Id,fullName,email,fileUrl,countryCode,mobileNumber,latitude,longitude,status,datetime");
   $this->db->from("usermaster");
   $this->db->where("userType",USER_TYPE_OPERATOR);
   $this->db->where("vanId",$vanId);
   $this->db->where_not_in("status",USER_STATUS_BLOCK);
   $this->db->order_by("Id","DESC");
   $query = $this->db->get();
   return $this->displayVanOperators($query->result());
 }

 /* Method to getVanOperatorsByVanId
    Created By: Manzz Baria
 */
 public function getVanOperatorsByVanIdForReport($vanId,$operatorStatus){
   $this->db->select("Id,fullName,email,fileUrl,countryCode,mobileNumber,latitude,longitude,status,datetime");
   $this->db->from("usermaster");
   $this->db->where("userType",USER_TYPE_OPERATOR);
   $this->db->where("vanId",$vanId);
   $this->db->where_not_in("status",USER_STATUS_BLOCK);
   if ($operatorStatus > 0) {
      $this->db->where("status",$operatorStatus);
   }
   $this->db->order_by("Id","DESC");
   $query = $this->db->get();
   return $this->displayVanOperators($query->result());
 }


 /* Method to getRemainVanOperators
    Created By: Manzz Baria
 */
 public function getRemainVanOperators($vanId){
   $this->db->select("Id,fullName,email,fileUrl,countryCode,mobileNumber,status,latitude,longitude,datetime");
   $this->db->from("usermaster");
   $this->db->where("userType",USER_TYPE_OPERATOR);
   $this->db->where_not_in("status",USER_STATUS_BLOCK);
   $this->db->where_not_in("vanId",$vanId);
   $this->db->order_by("Id","DESC");
   $query = $this->db->get();
   return $this->displayVanOperators($query->result());
 }


/* Method to updateUserSatusByVanId
   Created By: Manzz Baria
*/
public function updateUserSatusByVanId($vanId,$status){
    $data = array(
      'status' => $status
      );
    $this->db->where('vanId', $vanId);
    $this->db->update('usermaster', $data);
    return true;
}


public function getOperatorReport($operatorstatus,$customerId,$serviceId,$country){
  $this->db->select("usermaster.Id,usermaster.fullName,usermaster.mobileNumber,usermaster.email,usermaster.status,usermaster.datetime,usermaster.countryCode,usermaster.latitude,usermaster.longitude,usermaster.vanId,usermaster.logindate");
  $this->db->from("usermaster");

  if ($customerId > 0 ) {
    $this->db->where('usermaster.Id', $customerId);
  }

  if ($country > 0 ) {
    $this->db->join('countrymaster','countrymaster.countrycode = usermaster.countrycode','inner');
    $this->db->where('usermaster.countryCode', $country);
  }

  if ($operatorstatus > 0 ) {
    $this->db->where('usermaster.status', $operatorstatus);
  }

  if ($serviceId > 0 ) {
    $this->db->join('vanservicemaster','vanservicemaster.vanId=usermaster.vanId');
    $this->db->where('vanservicemaster.serviceId', $serviceId);
  }
  $this->db->where("isActive",1);
  $this->db->where("userType",USER_TYPE_OPERATOR);
  $this->db->where_not_in("status",USER_STATUS_BLOCK);

  //$this->db->order_by("usermaster.datetime","ASC");

  $query = $this->db->get();
 // print_r($query->result());
  return $this->displayOperatorReport($query->result(),$serviceId);
}

public function displayOperatorReport($result,$serviceId){
       $OperatorObject = null;
       $Object= null;
       $loginDate="";
       $lastaddress=null;
       if($result != null){
        $this->load->model("Utility","utility");
         $this->load->model("ApiContactModel","ContactModel");
         $this->load->model("ApiRequestModel","RequestModel");
          $this->load->model("ApiVanModel","VanModel");


         foreach ($result as $row) {
          $CustomerDate = $this->utility->changeDateFromat($row->datetime,'d-m-Y h:i:s');
           $CustomerDate= date("Y-m-d h:i:s", strtotime('+3 hours', strtotime($CustomerDate)));

           $loginDate= $this->utility->changeDateFromat($row->logindate,'d-m-Y h:i:s');
           $loginDate= date("Y-m-d h:i:s", strtotime('+3 hours', strtotime($loginDate)));
        //   $CustomerDate = $this->utility->changeDateFromat($row->datetime,'d-m-Y h:i');
           $country = $this->ContactModel->getcountry($row->countryCode);
           $Supplier = $this->VanModel->getVanByvanId($row->vanId,$serviceId);
            $lastaddress = $this->getaddress($row->latitude,$row->longitude);
          
         /*   if($lastaddress)
            {
              echo $lastaddress;
            }
            else
            {
              echo "Not found";
            }*/


           $Object = array(
             'Id'=>(int)$row->Id,
             'fullName'=>$row->fullName,
             'mobileno'=>$row->mobileNumber,
             'email'=>$row->email,
             'createdDate'=>$CustomerDate,
             'logindate'=>$loginDate,
             'country'=>$country,
             'status'=>$row->status,
              'loginaddress'=>$lastaddress,
             'Supplier'=>$Supplier
           );
           $OperatorObject[] = $Object;
         }
       }
    //   $lastaddress = $this->getaddress(20.3633436,72.9173457);
      // echo  $lastaddress;
       return $OperatorObject ;

     }

  public function getaddress($lat,$lng)
  {
    $response=null;
    $obj=null;
      $url = 'https://maps.googleapis.com/maps/api/geocode/json?latlng='.trim($lat).','.trim($lng).'&sensor=false&key=AIzaSyAPJvZVaE8b1oWO0jULOeEtO_Zn8ms0IyE';
     $json = @file_get_contents($url);
     $data=json_decode($json);
   //  print_r($data);
   //  $json_decode = json_decode($file_contents);
     if($data->results !=null) {
        $response = array();
        //print_r($data->results[0]->address_components);
        foreach($data->results[0]->address_components as $addressComponet) {
        // $response[] = $addressComponet->long_name; 
            if(in_array('political', $addressComponet->types)) {
              $response[] = $addressComponet->short_name; 
             }
      }
            return $response;
       }
       
          
  }

}
?>
