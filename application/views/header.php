<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Electra</title>

<link href="<?php echo base_url().'/assets/css/bootstrap.css'?>" rel="stylesheet">
<link href="<?php echo base_url().'/assets/css/datepicker3.css'?>" rel="stylesheet">
<link href="<?php echo base_url().'/assets/css/styles.css'?>" rel="stylesheet">
<link rel="stylesheet" href="<?php echo base_url('assets/css/w3.css');?>">
<link rel="stylesheet" href="<?php echo base_url().'/assets/css/font-awesome.min.css'?>">
<link href='http://fonts.googleapis.com/css?family=Special+Elite' rel='stylesheet' type='text/css'>
<!--Icons-->
<script src="<?php echo base_url().'/assets/js/lumino.glyphs.js'?>"></script>
<script src="<?php echo base_url('assets/js/jquery-1.12.0.min.js')?>"></script>
<style>
.headertext
{
	font-family : 'Special Elite', cursive;
	font-size : 38px; 
	font-weight : 700;
	
}
</style>
<!--[if lt IE 9]>
<script src="js/html5shiv.js"></script>
<script src="js/respond.min.js"></script>
<![endif]-->

</head>

<body>
	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#" style="color:teal;"><span class="headertext w3-text-white">Electra</span></a>
				<ul class="user-menu">
					<li class="dropdown pull-right">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg> User <span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="<?php echo base_url('ChangePassword');?>"><svg class="glyph stroked gear"><use xlink:href="#stroked-gear"></use></svg> Change password</a></li>
							<li><a href="<?php echo base_url('Login/logout');?>"><svg class="glyph stroked cancel"><use xlink:href="#stroked-cancel"></use></svg> Logout</a></li>
						</ul>
					</li>
				</ul>
			</div>
							
		</div><!-- /.container-fluid -->
	</nav>
		
	<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
		<form role="search">
			<div class="form-group">
				<input type="text" class="form-control" placeholder="Search">
			</div>
		</form>
		<ul class="nav menu">
			<li><a href="<?php echo  base_url()."Admindashboard";?>"><i class="fa fa-tachometer" aria-hidden="true"></i><svg class="glyph stroked line-graph"></svg> Dashboard</a></li>
			<li><a href="<?php echo  base_url()."User";?>"><i class="fa fa-user" aria-hidden="true"></i><svg class="glyph stroked line-graph"></svg> Approved Users</a></li>			<li><a href="<?php echo  base_url()."Electrician";?>"><i class="fa fa-bolt" aria-hidden="true"></i><svg class="glyph stroked line-graph"></svg> Approved Electrician </a></li>
			<li><a href="<?php echo  base_url()."Enquiry";?>"><i class="fa fa-phone" aria-hidden="true"></i><svg class="glyph stroked table"></svg> Enquries</a></li>
		</ul>

	</div><!--/.sidebar-->

<script>
$(document).ready(function() {
    $("[href]").each(function() {
    if (this.href == window.location.href) 
	{
        $(this).addClass("w3-pale-green");
     }
    });
});
</script>