
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
		<div class="row">
			<ol class="breadcrumb w3-pale-green">
				<li class="active">Approved Electrician</li>
			</ol>
		</div><!--/.row-->
		
		<br/>
		
		<div class="row">
		
			
		</div><!--/.row-->
			<div class="row">
			<div class="col-md-8">
			<div class="panel panel-blue">
					<div class="panel-heading dark-overlay"><svg class="glyph stroked clipboard-with-paper"><use xlink:href="#stroked-clipboard-with-paper"></use></svg>Electrician</div>
					<div class="panel-body">
						<ul class="todo-list">
						<li class="todo-list-item">
								<div class="checkbox">
									<input type="checkbox" id="checkbox1" style="margin:0px"/>
									<label for="checkbox1"> <a href="#" id="elect1"><img src="<?php echo base_url()?>img_avatar3.png" alt="Avatar" class="w3-left w3-circle w3-margin-right" style="width:20px"> Alok Singh </a></label>
								</div>
								<div class="pull-right action-buttons">
									
									<svg class="glyph stroked flag"></svg>
									<a href="#" class="trash"><svg class="glyph stroked trash"><use xlink:href="#stroked-cancel"></use></svg></a>
								</div>
							</li>
							<li class="todo-list-item">
								<div class="checkbox">
									<input type="checkbox" id="checkbox2" style="margin:0px"/>
									<label for="checkbox2"> <img src="<?php echo base_url()?>img_avatar3.png" alt="Avatar" class="w3-left w3-circle w3-margin-right" style="width:20px"> Abhinav Singh</label>
								</div>
								<div class="pull-right action-buttons">
									
									<svg class="glyph stroked flag"></svg>
									<a href="#" class="trash"><svg class="glyph stroked trash"><use xlink:href="#stroked-cancel"></use></svg></a>
								</div>
							</li>
							<li class="todo-list-item">
								<div class="checkbox">
									<input type="checkbox" id="checkbox3" style="margin:0px"/>
									<label for="checkbox3"> <img src="<?php echo base_url()?>img_avatar3.png" alt="Avatar" class="w3-left w3-circle w3-margin-right" style="width:20px"> Anurag Singh</label>
								</div>
								<div class="pull-right action-buttons">
									
											<svg class="glyph stroked flag"></svg>
									<a href="#" class="trash"><svg class="glyph stroked trash"><use xlink:href="#stroked-cancel"></use></svg></a>
								</div>
							</li>
							<li class="todo-list-item">
								<div class="checkbox">
									<input type="checkbox" id="checkbox4" style="margin:0px"/>
									<label for="checkbox4"> <img src="<?php echo base_url()?>img_avatar3.png" alt="Avatar" class="w3-left w3-circle w3-margin-right" style="width:20px"> Abhishek Singh</label>
								</div>
								<div class="pull-right action-buttons">
									
									<svg class="glyph stroked flag"></svg>
									<a href="#" class="trash"><svg class="glyph stroked trash"><use xlink:href="#stroked-cancel"></use></svg></a>
								</div>
							</li>
							<li class="todo-list-item">
								<div class="checkbox">
									<input type="checkbox" id="checkbox5" style="margin:0px"/>
									<label for="checkbox5"> <img src="<?php echo base_url()?>img_avatar3.png" alt="Avatar" class="w3-left w3-circle w3-margin-right" style="width:20px"> Anand Singh</label>
								</div>
								<div class="pull-right action-buttons">
								
									<svg class="glyph stroked flag"></svg>
									<a href="#" class="trash"><svg class="glyph stroked trash"><use xlink:href="#stroked-cancel"></use></svg></a>
								</div>
							</li>
							<li class="todo-list-item">
								<div class="checkbox">
									<input type="checkbox" id="checkbox6" style="margin:0px"/>
									<label for="checkbox6"> <img src="<?php echo base_url()?>img_avatar3.png" alt="Avatar" class="w3-left w3-circle w3-margin-right" style="width:20px"> Awanish Singh</label>
								</div>
								<div class="pull-right action-buttons">
									
										<svg class="glyph stroked flag"></svg>
									<a href="#" class="trash"><svg class="glyph stroked trash"><use xlink:href="#stroked-cancel"></use></svg></a>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div><!--/.col-->
			<div class="col-md-4" id="electprofile" tabindex='1'>
				<div class="panel panel-default chat">
					<div class="panel-heading  w3-blue" id="accordion"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg> Electrician Profile</div>
					<div class="panel-body">
						<ul>
							<li class="left clearfix">
								<span class="chat-img pull-left">
									<img src="<?php echo base_url()?>img_avatar3.png" alt="User Avatar" class="img-circle" style="width:90px"/>
								</span>
								<div class="chat-body clearfix">
									<div class="header">
										<strong class="primary-font">Alok Singh</strong> <small class="text-muted">32 mins ago</small>
									</div>
									<p>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ante turpis, rutrum ut ullamcorper sed, dapibus ac nunc. Vivamus luctus convallis mauris, eu gravida tortor aliquam ultricies. 
									</p>
								</div>
							</li>
						</ul>
					</div>
				</div>
								
			</div><!--/.col-->
		</div><!--/.row-->		
			
</div>	<!--/.main-->

<script src="<?php echo base_url('assets/js/jquery-1.12.0.min.js')?>"></script>
<script>
$('#electprofile').hide();
$('#elect1').click(function()
{
	$('#electprofile').show();
	 //$('#userprofile').focus();
});
</script>