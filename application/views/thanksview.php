<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Thanks for registration</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url('/assets/css/bootstrap.min.css')?>" rel="stylesheet"/>
    <!-- Custom CSS -->
    <style>
     body
    {
        padding-top: 70px;
    }
   .separator {
    border-right: 1px solid #dfdfe0;
}
.icon-btn-save {
    padding-top: 0;
    padding-bottom: 0;
}
.input-group {
    margin-bottom:10px;
}
.btn-save-label {
    position: relative;
    left: -12px;
    display: inline-block;
    padding: 6px 12px;
    background: rgba(0,0,0,0.15);
    border-radius: 3px 0 0 3px;
}
.btn-success
{
    background-color: teal;
	border-color: teal;
}
    </style>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
 <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation" style="background-color:#0776BC">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header" style="background-color:#0776BC">

               <a href="<?php echo base_url();?>"> <img src="<?php echo base_url()?>.\electra.png" height="50px" style="background-color:#0776BC"></a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

                </ul>
            </div>

            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
	    <div class="container bootstrap snippet">
    <div class="row"  id="forgetform">
        <div class="col-xs-12 col-sm-12 col-md-12 col-md-12 col-md-offset-2">
          <span style="font-size:36px;color:#347FBF;font-weight:600">  Thanks for Registration with <img src="<?php echo base_url()?>.\electra.png" height="50px" style="vertical-align:baseline"/></span>
        </div>
    </div>
</div>
<div class="panel-body" id="msgforgetform"></div>
<script src="<?php echo base_url('assets/js/jquery-1.12.0.min.js')?>"></script>
<script src="<?php echo base_url('assets/js/jquery.dataTables.min.js')?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap.min.js')?>"></script>
<script>
var userid = getParameterByName('userId');
var accesstoken = getParameterByName('accessToken');
$(document).ready(function()
{
			 $.ajax({
						url : "http://dev.mobileartsme.com/electra/Api/User/verifylink?userId="+userid+"&accessToken="+accesstoken,
						type : "GET",
						success:function(data)
						{
							alert(data.Status);
						}
				});
});

function getParameterByName(name)
   {
	    name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
	    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
	    results = regex.exec(location.search);
	    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
   }
</script>
</body>
</html>
