<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Welcome to Electra</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="<?php echo base_url().'/assets/css/bootstrap.css'?>" rel='stylesheet' type='text/css'/>
<style type="text/css">

body, html 
{
    height: 100%;
    background-repeat: no-repeat;
    background: radial-gradient(circle, black, white);
}

.card-container.card {
    max-width: 350px;
    padding: 40px 40px;
}

.btn {
    font-weight: 700;
    height: 36px;
    -moz-user-select: none;
    -webkit-user-select: none;
    user-select: none;
    cursor: default;
}

/*
 * Card component
 */
.card {
    background-color: #F7F7F7;
    /* just in case there no content*/
    padding: 20px 25px 30px;
    margin: 0 auto 25px;
    margin-top: 50px;
    /* shadows and rounded borders */
    -moz-border-radius: 2px;
    -webkit-border-radius: 2px;
    border-radius: 2px;
    -moz-box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
    -webkit-box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
    box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
}

.profile-img-card {
    width: 96px;
    height: 96px;
    margin: 0 auto 10px;
    display: block;
    -moz-border-radius: 50%;
    -webkit-border-radius: 50%;
    border-radius: 50%;
}
.profile-name-card {
    font-size: 16px;
    font-weight: bold;
    text-align: center;
    margin: 10px 0 0;
    min-height: 1em;
}

.reauth-email {
    display: block;
    color: #404040;
    line-height: 2;
    margin-bottom: 10px;
    font-size: 14px;
    text-align: center;
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
    -moz-box-sizing: border-box;
    -webkit-box-sizing: border-box;
    box-sizing: border-box;
}

.form-signin #inputEmail,
.form-signin #inputPassword {
    direction: ltr;
    height: 44px;
    font-size: 16px;
}

.form-signin input[type=email],
.form-signin input[type=password],
.form-signin input[type=text],
.form-signin button {
    width: 100%;
    display: block;
    margin-bottom: 10px;
    z-index: 1;
    position: relative;
    -moz-box-sizing: border-box;
    -webkit-box-sizing: border-box;
    box-sizing: border-box;
}

.form-signin .form-control:focus {
    border-color: rgb(104, 145, 162);
    outline: 0;
    -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075),0 0 8px rgb(104, 145, 162);
    box-shadow: inset 0 1px 1px rgba(0,0,0,.075),0 0 8px rgb(104, 145, 162);
}

.btn.btn-signin {
    /*background-color: #4d90fe; */
    background-color: rgb(104, 145, 162);
    /* background-color: linear-gradient(rgb(104, 145, 162), rgb(12, 97, 33));*/
    padding: 0px;
    font-weight: 700;
    font-size: 14px;
    height: 36px;
    -moz-border-radius: 3px;
    -webkit-border-radius: 3px;
    border-radius: 3px;
    border: none;
    -o-transition: all 0.218s;
    -moz-transition: all 0.218s;
    -webkit-transition: all 0.218s;
    transition: all 0.218s;
}

.btn.btn-signin:hover,
.btn.btn-signin:active,
.btn.btn-signin:focus {
    background-color: rgb(12, 97, 33);
}

.forgot-password {
    color: rgb(104, 145, 162);
}

.forgot-password:hover,
.forgot-password:active,
.forgot-password:focus{
    color: rgb(12, 97, 33);
}
	</style>
</head>
<body>
   <div class="container">
        <div class="card card-container">
            <img id="profile-img" class="profile-img-card" src="//ssl.gstatic.com/accounts/ui/avatar_2x.png" />
	            <p id="profile-name" class="profile-name-card"></p>
				<?php 
					$sessdat=$this->session->userdata('data');
					
					if($sessdat['loginuser']==1)
					{
						$id=$sessdat['id'];
					}
				?>
    	    	    <form class="form-signin">
        	    	    <span id="reauth-email" class="reauth-email"></span>
			                <input type="text" id="inputEmail" class="form-control" placeholder="User Name" required autofocus>
            			    <input type="password" id="inputPassword" class="form-control" placeholder="Password" required>
							<!--<input type="hidden" id="userid" value="<?php echo $id;?>">-->
			            	    <div id="remember" class="checkbox">
            			    	    <label id="ErrorMsg" style="background-color:#CC9999;color:#FF0000;" class="form-control">
			                	     
            				        </label>
				                </div>
                					<input type="button" class="btn btn-lg btn-primary btn-block btn-signin" type="button" id="loginbtn" value="Sign in"/>
		            </form>
        						    <a href="#" class="forgot-password">
						                Forgot the password?
						            </a>
   	     </div>
    </div>
</body>
<script src="<?php echo base_url('assets/js/jquery-1.12.0.min.js')?>"></script>
<script>
$(document).ready(function()
{
	
	var checkerror='';
	$('#ErrorMsg').hide();
	var userid=$('#userid').val();
	
	$('#inputEmail').keypress(function(e) 
	{
		if (e.which == '13') 
		{
			$('#loginbtn').click();
		}
	});
	
	$('#inputPassword').keypress(function(e)
	 {
		if (e.which == '13')
		 {
			$('#loginbtn').click();
		}
	});
	
	$('#loginbtn').click(function()
	{
		var username =  $('#inputEmail').val();
		var password =  $('#inputPassword').val();
		if(username=="")
		{
			$('#ErrorMsg').html('Please Enter UserName!').fadeIn('slow');
			$('#ErrorMsg').delay(1000).fadeOut('slow');
			return false;
		}
		if(password=="")
		{
			$('#ErrorMsg').html('Please Enter Password!').fadeIn('slow');
			$('#ErrorMsg').delay(1000).fadeOut('slow');
			return false;
		}
		else
		{
			$.ajax({
					url : "<?php echo base_url('Login/login');?>",
					type : "POST",
					data:
					{
						uname:username,	
						password:password
					},
					success:function(response)
					{
						var obj = JSON.parse(response);
						
						if(obj.Status==0)
						{
							$('#ErrorMsg').html(obj.Message).fadeIn('slow');
							$('#ErrorMsg').delay(1000).fadeOut('slow');
						}
						
						if(obj.Status==1)
						{
							$('#loginbtn').addClass('disabled');
							window.location ="<?php echo base_url('Admindashboard')?>";
						}

					},
					error:function()
					{
						alert('error');
					}
			});
			
		}
	});
});
</script>
</html>