<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Reset Password</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url('/assets/css/bootstrap.min.css')?>" rel="stylesheet"/>
    <!-- Custom CSS -->
    <style>
     body
    {
        padding-top: 70px;
    }
   .separator {
    border-right: 1px solid #dfdfe0;
}
.icon-btn-save {
    padding-top: 0;
    padding-bottom: 0;
}
.input-group {
    margin-bottom:10px;
}
.btn-save-label {
    position: relative;
    left: -12px;
    display: inline-block;
    padding: 6px 12px;
    background: rgba(0,0,0,0.15);
    border-radius: 3px 0 0 3px;
}
.btn-success
{
    background-color: teal;
	border-color: teal;
}
    </style>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
 <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation" style="background-color:#0776BC">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header" style="background-color:#0776BC">

               <a href="<?php echo base_url();?>"> <img src="<?php echo base_url()?>.\electra.png" height="50px" style="background-color:#0776BC"></a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

                </ul>
            </div>

            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
	    <div class="container bootstrap snippet">
    <div class="row"  id="forgetform">
        <div class="col-xs-12 col-sm-12 col-md-6 col-md-offset-2">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <span class="glyphicon glyphicon-th"></span>
                        Reset Password
                    </h3>
                </div>
                <div class="panel-body">
                    <div class="row">

						<form role="form" method="post" class="registration-form"  id="chngePwd_form">
                        <div class="col-md-12 login-box">
                         <div class="form-group">
                            <div class="input-group">
                              <div class="input-group-addon"><span class="glyphicon glyphicon-new-window"></span></div>
                              <input class="form-control" type="password" placeholder="New Password" name="newPwd" id="newPwd">
                            </div>
                          </div>
                          <div class="form-group">
                            <div class="input-group">
                              <div class="input-group-addon"><span class="glyphicon glyphicon-edit"></span></div>
                              <input class="form-control" type="password" placeholder="Conform Password" name="confirmPwd" id="confirmPwd">
                            </div>
							 <span id="msg"></span>
                          </div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-6">
                            <input type="button" class="btn btn-success" id="changePwd" name="changePwd" value="Reset Password"/>
                        </div>
						<div class="col-xs-6 col-sm-6 col-md-6" id="sess">
                        </div>
                    </div>
                </div>
				</form>
            </div>

        </div>
    </div>
</div>
<div class="panel-body" id="msgforgetform"></div>
<script src="<?php echo base_url('assets/js/jquery-1.12.0.min.js')?>"></script>
<script src="<?php echo base_url('assets/js/jquery.dataTables.min.js')?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap.min.js')?>"></script>
<script>
var validationtoken = getParameterByName('validationToken');
//validationtoken= encodeURIComponent(validationtoken);
var accesstoken = getParameterByName('accessToken');
$(document).ready(function()
{
   		$('#changePwd').click(function()
		{
			var newPwd=$('#newPwd').val();
			var confirmPwd=$('#confirmPwd').val();
			var id=$('#id').val();

			 if(newPwd=="")
			 {
			 	//$("#myModal").modal('show');
				$('#msg').html("<font style='color:#FF0000;background-color:#FFCC99;padding:1px;'>Please Enter new Password</font>");
				return false;
			 }
			 else
			 {
			 	$('#msg').html("");
			 }
			 if(confirmPwd=="")
			 {
				$('#msg').html("<font style='color:#FF0000;background-color:#FFCC99;padding:1px;'>Please Enter Confirm Password</font>");
				return false;
			 }

			 else
			 {
				 	if(confirmPwd==newPwd)
					{
						$('#msg').html("");
					}
					 else
					 {
							$('#msg').html("<font style='color:#FF0000;background-color:#FFCC99;padding:1px;'>Password and Confirm Password does not match</font>");
							return false;
					 }
			}

			 $.ajax({
						url : "http://dev.mobileartsme.com/electra/Api/User/resetPassword?",
						type : "POST",
						data :
						{
							validationToken : validationtoken,
							accessToken  : accesstoken,
							newPassword : newPwd
						},
						success:function(data)
						{
								//var res = JSON.parse(data);
								if(data.Status==0)
								{
									var str="<center><span style='font-size:20px;color:red'>"+data.Message+"</span></center>"
									$('#sess').html(str);
								}
								if(data.Status==1)
								{
									var str="<center><span style='font-size:3.0em;color:teal'>Your Password Successfully Changed Please login <br><center><a href='<?php echo base_url();?>' style='font-size:2.6em;color:black;text-decoration:none'> Electra </a></center></span></center>"
									$('#forgetform').hide();
									$('#msgforgetform').html(str);
								}
						}
				});
		});

		$('.close').click(function()
		{
			//alert('close');
			$('#oldPwd').val("");
			$('#newPwd').val("");
			$('#confirmPwd').val("");
		});
});
function getParameterByName(name)
   {
	    name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
	    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
	    results = regex.exec(location.search);
	    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
   }
</script>
</body>
</html>
