
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
		<div class="row">
			<ol class="breadcrumb w3-pale-green">
				<li class="active">Dashboard</li>
			</ol>
		</div><!--/.row-->
		
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header"></h1>
			</div>
		</div><!--/.row-->
		
		<div class="row">
		<div class="col-xs-12 col-md-6 col-lg-3">
				<div class="panel panel-teal panel-widget">
					<div class="row no-padding">
						<div class="col-sm-3 col-lg-5 widget-left">
							<svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>
						</div>
						<div class="col-sm-9 col-lg-7 widget-right">
							<div class="large">24</div>
							<div class="text-muted">Users</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-md-6 col-lg-3" id="electrician">
				<div class="panel panel-blue panel-widget ">
					<div class="row no-padding">
						<div class="col-sm-3 col-lg-5 widget-left">
							<svg class="glyph stroked bag"><use xlink:href="#stroked-bag"></use></svg>
						</div>
						<div class="col-sm-9 col-lg-7 widget-right">
							<div class="large">120</div>
							<div class="text-muted"> Electrician</div>
						</div>
					</div>
				</div>
			</div>
			
		</div><!--/.row-->
			<div class="row">
			<div class="col-md-8">
			<div class="panel panel-blue">
					<div class="panel-heading dark-overlay"><svg class="glyph stroked clipboard-with-paper"><use xlink:href="#stroked-clipboard-with-paper"></use></svg>Recently Registered Users</div>
					<div class="panel-body">
						<ul class="todo-list">
						<li class="todo-list-item">
								<div class="checkbox">
									<input type="checkbox" id="checkbox1" style="margin:0px"/>
									<label for="checkbox1"> <img src="<?php echo base_url()?>img_avatar3.png" alt="Avatar" class="w3-left w3-circle w3-margin-right" style="width:20px">Renuka Singh</label>
								</div>
								<div class="pull-right action-buttons">
									<a href="#"><svg class="glyph stroked pencil"><use xlink:href="#stroked-pencil"></use></svg></a>
									<svg class="glyph stroked flag"></svg>
									<a href="#" class="trash"><svg class="glyph stroked trash"><use xlink:href="#stroked-cancel"></use></svg></a>
								</div>
							</li>
							<li class="todo-list-item">
								<div class="checkbox">
									<input type="checkbox" id="checkbox2" style="margin:0px"/>
									<label for="checkbox2"> <img src="<?php echo base_url()?>img_avatar3.png" alt="Avatar" class="w3-left w3-circle w3-margin-right" style="width:20px">Deepika Singh</label>
								</div>
								<div class="pull-right action-buttons">
									<a href="#"><svg class="glyph stroked pencil"><use xlink:href="#stroked-pencil"></use></svg></a>
									<svg class="glyph stroked flag"></svg>
									<a href="#" class="trash"><svg class="glyph stroked trash"><use xlink:href="#stroked-cancel"></use></svg></a>
								</div>
							</li>
							<li class="todo-list-item">
								<div class="checkbox">
									<input type="checkbox" id="checkbox3" style="margin:0px"/>
									<label for="checkbox3"> <img src="<?php echo base_url()?>img_avatar3.png" alt="Avatar" class="w3-left w3-circle w3-margin-right" style="width:20px">Priyanka Singh</label>
								</div>
								<div class="pull-right action-buttons">
									<a href="#"><svg class="glyph stroked pencil"><use xlink:href="#stroked-pencil"></use></svg></a>
											<svg class="glyph stroked flag"></svg>
									<a href="#" class="trash"><svg class="glyph stroked trash"><use xlink:href="#stroked-cancel"></use></svg></a>
								</div>
							</li>
							<li class="todo-list-item">
								<div class="checkbox">
									<input type="checkbox" id="checkbox4" style="margin:0px"/>
									<label for="checkbox4"> <img src="<?php echo base_url()?>img_avatar3.png" alt="Avatar" class="w3-left w3-circle w3-margin-right" style="width:20px"> Nidhi Singh</label>
								</div>
								<div class="pull-right action-buttons">
									<a href="#"><svg class="glyph stroked pencil"><use xlink:href="#stroked-pencil"></use></svg></a>
									<svg class="glyph stroked flag"></svg>
									<a href="#" class="trash"><svg class="glyph stroked trash"><use xlink:href="#stroked-cancel"></use></svg></a>
								</div>
							</li>
							<li class="todo-list-item">
								<div class="checkbox">
									<input type="checkbox" id="checkbox5" style="margin:0px"/>
									<label for="checkbox5"> <img src="<?php echo base_url()?>img_avatar3.png" alt="Avatar" class="w3-left w3-circle w3-margin-right" style="width:20px">Shikha Singh</label>
								</div>
								<div class="pull-right action-buttons">
									<a href="#"><svg class="glyph stroked pencil"><use xlink:href="#stroked-pencil"></use></svg></a>
									<svg class="glyph stroked flag"></svg>
									<a href="#" class="trash"><svg class="glyph stroked trash"><use xlink:href="#stroked-cancel"></use></svg></a>
								</div>
							</li>
							<li class="todo-list-item">
								<div class="checkbox">
									<input type="checkbox" id="checkbox6" style="margin:0px"/>
									<label for="checkbox6"> <img src="<?php echo base_url()?>img_avatar3.png" alt="Avatar" class="w3-left w3-circle w3-margin-right" style="width:20px"> Baby Singh </label>
								</div>
								<div class="pull-right action-buttons">
									<a href="#"><svg class="glyph stroked pencil"><use xlink:href="#stroked-pencil"></use></svg></a>
										<svg class="glyph stroked flag"></svg>
									<a href="#" class="trash"><svg class="glyph stroked trash"><use xlink:href="#stroked-cancel"></use></svg></a>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div><!--/.col-->
			
			<div class="col-md-4">
				<div class="panel panel-default chat">
					<div class="panel-heading  w3-blue" id="accordion"><svg class="glyph stroked two-messages"><use xlink:href="#stroked-two-messages"></use></svg> Enquries</div>
					<div class="panel-body">
						<ul>
							<li class="left clearfix">
								<span class="chat-img pull-left">
									<img src="http://placehold.it/80/30a5ff/fff" alt="User Avatar" class="img-circle" />
								</span>
								<div class="chat-body clearfix">
									<div class="header">
										<strong class="primary-font">John Doe</strong> <small class="text-muted">32 mins ago</small>
									</div>
									<p>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ante turpis, rutrum ut ullamcorper sed, dapibus ac nunc. Vivamus luctus convallis mauris, eu gravida tortor aliquam ultricies. 
									</p>
								</div>
							</li>
							<li class="right clearfix">
								<span class="chat-img pull-right">
									<img src="http://placehold.it/80/dde0e6/5f6468" alt="User Avatar" class="img-circle" />
								</span>
								<div class="chat-body clearfix">
									<div class="header">
										<strong class="pull-left primary-font">Jane Doe</strong> <small class="text-muted">6 mins ago</small>
									</div>
									<p>
										Mauris dignissim porta enim, sed commodo sem blandit non. Ut scelerisque sapien eu mauris faucibus ultrices. Nulla ac odio nisl. Proin est metus, interdum scelerisque quam eu, eleifend pretium nunc. Suspendisse finibus auctor lectus, eu interdum sapien.
									</p>
								</div>
							</li>
							<li class="left clearfix">
								<span class="chat-img pull-left">
									<img src="http://placehold.it/80/30a5ff/fff" alt="User Avatar" class="img-circle" />
								</span>
								<div class="chat-body clearfix">
									<div class="header">
										<strong class="primary-font">John Doe</strong> <small class="text-muted">32 mins ago</small>
									</div>
									<p>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ante turpis, rutrum ut ullamcorper sed, dapibus ac nunc. Vivamus luctus convallis mauris, eu gravida tortor aliquam ultricies. 
									</p>
								</div>
							</li>
						</ul>
					</div>
					
				
				</div>
								
			</div><!--/.col-->
		</div><!--/.row-->		
			<div class="row">
			<div class="col-md-8">
			<div class="panel panel-blue" id="elect" tabindex='1'>
					<div class="panel-heading dark-overlay"><svg class="glyph stroked clipboard-with-paper"><use xlink:href="#stroked-clipboard-with-paper"></use></svg>Recently Registered Electrician	</div>
					<div class="panel-body">
						<ul class="todo-list">
						<li class="todo-list-item">
								<div class="checkbox">
									<input type="checkbox" id="checkbox1" style="margin:0px"/>
									<label for="checkbox1"> <img src="<?php echo base_url()?>img_avatar3.png" alt="Avatar" class="w3-left w3-circle w3-margin-right" style="width:20px"> Alok Singh</label>
								</div>
								<div class="pull-right action-buttons">
									<a href="#"><svg class="glyph stroked pencil"><use xlink:href="#stroked-pencil"></use></svg></a>
									<svg class="glyph stroked flag"></svg>
									<a href="#" class="trash"><svg class="glyph stroked trash"><use xlink:href="#stroked-cancel"></use></svg></a>
								</div>
							</li>
							<li class="todo-list-item">
								<div class="checkbox">
									<input type="checkbox" id="checkbox2" style="margin:0px"/>
									<label for="checkbox2"> <img src="<?php echo base_url()?>img_avatar3.png" alt="Avatar" class="w3-left w3-circle w3-margin-right" style="width:20px"> Abhinav Singh</label>
								</div>
								<div class="pull-right action-buttons">
									<a href="#"><svg class="glyph stroked pencil"><use xlink:href="#stroked-pencil"></use></svg></a>
									<svg class="glyph stroked flag"></svg>
									<a href="#" class="trash"><svg class="glyph stroked trash"><use xlink:href="#stroked-cancel"></use></svg></a>
								</div>
							</li>
							<li class="todo-list-item">
								<div class="checkbox">
									<input type="checkbox" id="checkbox3" style="margin:0px"/>
									<label for="checkbox3"> <img src="<?php echo base_url()?>img_avatar3.png" alt="Avatar" class="w3-left w3-circle w3-margin-right" style="width:20px"> Anurag Singh</label>
								</div>
								<div class="pull-right action-buttons">
									<a href="#"><svg class="glyph stroked pencil"><use xlink:href="#stroked-pencil"></use></svg></a>
											<svg class="glyph stroked flag"></svg>
									<a href="#" class="trash"><svg class="glyph stroked trash"><use xlink:href="#stroked-cancel"></use></svg></a>
								</div>
							</li>
							<li class="todo-list-item">
								<div class="checkbox">
									<input type="checkbox" id="checkbox4" style="margin:0px"/>
									<label for="checkbox4"> <img src="<?php echo base_url()?>img_avatar3.png" alt="Avatar" class="w3-left w3-circle w3-margin-right" style="width:20px"> Abhishek Singh</label>
								</div>
								<div class="pull-right action-buttons">
									<a href="#"><svg class="glyph stroked pencil"><use xlink:href="#stroked-pencil"></use></svg></a>
									<svg class="glyph stroked flag"></svg>
									<a href="#" class="trash"><svg class="glyph stroked trash"><use xlink:href="#stroked-cancel"></use></svg></a>
								</div>
							</li>
							<li class="todo-list-item">
								<div class="checkbox">
									<input type="checkbox" id="checkbox5" style="margin:0px"/>
									<label for="checkbox5"> <img src="<?php echo base_url()?>img_avatar3.png" alt="Avatar" class="w3-left w3-circle w3-margin-right" style="width:20px"> Anand Singh</label>
								</div>
								<div class="pull-right action-buttons">
									<a href="#"><svg class="glyph stroked pencil"><use xlink:href="#stroked-pencil"></use></svg></a>
									<svg class="glyph stroked flag"></svg>
									<a href="#" class="trash"><svg class="glyph stroked trash"><use xlink:href="#stroked-cancel"></use></svg></a>
								</div>
							</li>
							<li class="todo-list-item">
								<div class="checkbox">
									<input type="checkbox" id="checkbox6" style="margin:0px"/>
									<label for="checkbox6"> <img src="<?php echo base_url()?>img_avatar3.png" alt="Avatar" class="w3-left w3-circle w3-margin-right" style="width:20px"> Awanish Singh</label>
								</div>
								<div class="pull-right action-buttons">
									<a href="#"><svg class="glyph stroked pencil"><use xlink:href="#stroked-pencil"></use></svg></a>
										<svg class="glyph stroked flag"></svg>
									<a href="#" class="trash"><svg class="glyph stroked trash"><use xlink:href="#stroked-cancel"></use></svg></a>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div><!--/.col-->
			</div>
					</div>
				</div>
			</div>
			
		</div><!--/.row-->
		
			
</div>	<!--/.main-->

<script src="<?php echo base_url('assets/js/jquery-1.12.0.min.js')?>"></script>
<script>
$('#electrician').click(function()
{
	 $('#elect').focus();
});
</script>