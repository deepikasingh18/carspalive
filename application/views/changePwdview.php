
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
		<div class="row">
			<ol class="breadcrumb w3-pale-green">
				<li class="active">Change Password</li>
			</ol>
		</div><!--/.row-->
		
			<br />
		
			<div class="row">
			<div class="col-md-8">
			<div class="panel panel-blue">
					<div class="panel-heading dark-overlay"><svg class="glyph stroked clipboard-with-paper"><use xlink:href="#stroked-clipboard-with-paper"></use></svg>Change Password</div>
					<div class="panel-body">
						<ul class="todo-list">
						<li class="todo-list-item">
								 <div class="form-bottom">
			                    <form role="form" action="" method="post" class="registration-form"  id="chngePwd_form">
														 <div class="form-group">
			                    		<label  for="form-first-name" style="color:#0197d8;">Old Password :</label>
			                        	<input type="password" name="oldPwd" id="oldPwd" placeholder="Old Password" class="form-first-name form-control">
			                        </div>
															<input type="hidden" name="id" id="id" value="<?=$id;?>" />
															<div class="form-group">
			                    		<label  for="form-first-name" style="color:#0197d8;">New Password :</label>
			                        	<input type="password" name="newPwd" id="newPwd" placeholder="New Password" class="form-first-name form-control">
			                        </div>
															
															<div class="form-group">
			                    		<label  for="form-first-name" style="color:#0197d8;">Confirm Password :</label>
			                        	<input type="password" name="confirmPwd" id="confirmPwd" placeholder="Confirm Password" class="form-first-name form-control">
			                        </div>
															
								<div align="left" style="padding-top:5px;">
			                        <input type="button" class="btn btn-primary" value="Save" id="changePwd" name="changePwd" data-toggle="modal" data-target="#myModal">
			      
								</div>
								
								</form>
		                    </div>
							</li>
						</ul>
					</div>
				</div>
			</div><!--/.col-->
		</div><!--/.row-->		
			
</div>	<!--/.main-->
 <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <span id="msg"></span>
        </div>
      </div>
      
    </div>
  </div>
    </div>
<script src="<?php echo base_url('/assets/js/jquery-1.12.0.min.js')?>"></script>
<script>
$(document).ready(function() 
{
   	$('#changePwd').click(function()
		{
			var oldPwd=$('#oldPwd').val();
			var newPwd=$('#newPwd').val();
			var confirmPwd=$('#confirmPwd').val(); 
			var id=$('#id').val();
			 if(oldPwd=="")
			 {
				$("#myModal").modal('show');
				$('#msg').html("Please Enter Old Password");
				return false;
			 }
			 if(newPwd=="")
			 {
			 	$("#myModal").modal('show');
				$('#msg').html("Please Enter new Password");
				return false;
			 }
			 if(confirmPwd=="")
			 {
			 	$("#myModal").modal('show');
				$('#msg').html("Please Enter Confirm Password");
				return false;
			 }
			 else
			 {
				 	if(confirmPwd==newPwd) {}
					 else 
					 {
						 	$("#myModal").modal('show');
							$('#msg').html("Password and Confirm Password does not match");
							return false;
					 }
			}			
			 $.ajax({
						type :  "POST",
						datatype : "JSON",
						url: "<?php echo site_url('ChangePassword/chngePassword');?>",
						data	: {oldPwd,newPwd,confirmPwd,id},
						success:function(data)
						{
								if(data==0)
								{
									$('#msg').html("Please enter valid Username and Password");
								}
								if(data==1)
								{
									$('#msg').html("Password Successfully Changed");
									$('#oldPwd').val("");
									$('#newPwd').val("");
									$('#confirmPwd').val(""); 
								}	
						}
				});
		});
		
	/*	$('.close').click(function()
		{
			$('#oldPwd').val("");
			$('#newPwd').val("");
			$('#confirmPwd').val(""); 
		});*/
}); 
</script>
