<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ChangePassword extends CI_Controller 
{
	public $data=array();
	public function __construct()
	{
		  parent::__construct();
          $this->load->library('session');
          $this->load->helper(array('form','url'));
          $this->load->library('form_validation');
		  $this->load->model('loginmodel');
		 //  $this->no_cache();
	}
	public function index()
	{
		$sesscheck=$this->session->userdata('data');	
			if($sesscheck['loginuser']==1)
			{
				$data['id']=$sesscheck['id'];
				$this->load->view('header');
				$this->load->view('changePwdview',$data);
				$this->load->view('footer');
			}
			else
			{
				redirect('login');
			} 
	
	}
	
	public function chngePassword()
	{	
			$oldPwd=$this->input->post('oldPwd');
			$newPwd=$this->input->post('newPwd');
			$id=$this->input->post('id');
			
		   	$check_value=$this->loginmodel->chngePwd($oldPwd,$newPwd,$id);	
			
			if($check_value==1)
			{	
				echo 1;
			}
			else
			{
				echo 0;
			}
	}
	
	
	
}
?>