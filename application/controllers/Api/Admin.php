<?php

defined('BASEPATH') OR exit('No direct script access allowed');
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class Admin extends REST_Controller
{
	public function __construct()
	{
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		$method = $_SERVER['REQUEST_METHOD'];
		if($method == "OPTIONS") {
				die();
		}
		  parent::__construct();
      $this->load->library('session');
      $this->load->helper(array('form','url'));
      $this->load->library('form_validation');
			$this->load->library('mylibrary');
			 $this->load->database();
	}
	    /* Method to display default error message
	    		 Created By: Manzz Baria
	    	*/
	    	private function displayDefaultJsonWithoutData($status,$message){
	    		if($status == JSON_SUCCESS_STATUS){
	    			$this->response([
	    				'Status' => $status,
	    				'Message' => $message
	    			], REST_Controller::HTTP_OK);
	    		}else{
	    			$this->response([
	    				'Status' => $status,
	    				'Message' => $message
	    			], REST_Controller::HTTP_OK);
	    		}
	    	}

				/* Method to show Message
					Created By: Manzz Baria
				*/
				public function showMessage($status,$message){
					$this->response([
						'Status' => $status,
						'Message' => $message
					], REST_Controller::HTTP_OK);
				}

				/* Method to displayMaxDistanceJsonWithData
					 Created By: Manzz Baria
				*/
				private function displayMaxDistanceJsonWithData($status,$mesage,$maxDistance){
					if($status == JSON_SUCCESS_STATUS){
						$this->response([
							'Status' => $status,
							'Message' => $mesage,
							'maxDistance' => $maxDistance
						], REST_Controller::HTTP_OK);
					}else{
						$this->response([
							'Status' => $status,
							'Message' => $mesage,
							'maxDistance' => $maxDistance
						], REST_Controller::HTTP_OK);
					}
				}

	    	/* Method to display default success with Result data and totals
	    		 Created By: Manzz Baria
	    	*/
	    	private function displayDefaultJsonWithData($status,$mesage,$result,$totalPages,$currentPages){
	    		if($status == JSON_SUCCESS_STATUS){
	    			$this->response([
	    				'Status' => $status,
	    				'Message' => $mesage,
	    				'TotalPage' => $totalPages,
	    				'CurrentPage' => $currentPages,
	    				'Data' => $result
	    			], REST_Controller::HTTP_OK);
	    		}else{
	    			$this->response([
	    				'Status' => $status,
	    				'Message' => $mesage,
	    				'TotalPage' => $totalPages,
	    				'CurrentPage' => $currentPages,
	    				'Data' => $result
	    			], REST_Controller::HTTP_OK);
	    		}
	    	}

	    /* Method to check parameters is null or empty
	        Created By: Manzz Baria
	    */
	    public function verifyRequiredParams($required_fields,$type){
	        $error = false;
	        $error_fields = "";
	        foreach ($required_fields as $field) {
	    			if($type == POST_TYPE){
	    				if ($this->post($field) == null) {
	    	      	$error = true;
	    	        $error_fields .= $field . ', ';
	    	      }
	    	    }else if($type == GET_TYPE){
	    				if ($this->get($field) == null) {
	    	      	$error = true;
	    	        $error_fields .= $field . ', ';
	    	      }
	    	    }else if($type == PUT_TYPE){
	    				if ($this->put($field) == null) {
	    	      	$error = true;
	    	        $error_fields .= $field . ', ';
	    	      }
	    	    }else{
	    				if ($this->delete($field) == null) {
	    	      	$error = true;
	    	        $error_fields .= $field . ', ';
	    	      }
	    			}
	        }
	        if ($error) {
	        	$message = 'Required field(s) ' . substr($error_fields, 0, -2) . ' is missing or empty';
	          $this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$message);
	        }
	      }
	      
	      
	      		/* Method to checkDelayAppointments
					Created By: Manzz Baria
				*/
				function checkDelayAppointments_get(){
					$data=json_decode(file_get_contents('php://input'));

					/***** getting params *****/
					$language = $this->get('language');
					if (empty($language)){
						$language = LANGUAGE_ENGLISH;
					}
					$this->load->model("ApiAppoinmnetModel","appoinmnetModel");
					$result = $this->appoinmnetModel->deleyAppointmentList();
					if($result){
								$mesage = 'Sending email to operator for delay appointments';
								$this->displayDefaultJsonWithoutData(JSON_SUCCESS_STATUS,$mesage);

					}else{
						$mesage = 'No delay appointments found' ;
						
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
					}
				}


				/* Method to updateContacts
					Created By: Manzz Baria
				*/
				function updateContacts_post(){
					$data=json_decode(file_get_contents('php://input'));
					$this->verifyRequiredParams(array('name','email','countryCode','phoneNumber','address'),POST_TYPE);

					/***** getting params *****/
					$name = $_POST['name'];
					$email = $_POST['email'];
					$countryCode = $_POST['countryCode'];
					$phoneNumber = $_POST['phoneNumber'];
					$address = $_POST['address'];

					$language = LANGUAGE_ENGLISH;
					if (!empty($_POST['language'])){
						 $language = $_POST['language'];
					}

					$this->load->model("ApiAdminModel","adminModel");
					$result = $this->adminModel->updateContacts($name,$email,$countryCode,$phoneNumber,$address);
					if($result != null){
								$mesage = 'Contact successfully updated';
								if ($language == LANGUAGE_ARABIC) {
										$mesage = 'تم التحديث بنجاح';
								}
								$totalPages = 1;
								$currentPages = 1;
								$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

					}else{
						$mesage = 'Something wrong,please try again later' ;
						if ($language == LANGUAGE_ARABIC) {
								$mesage = 'يرجى المحاولة مرة أخرى في وقت لاحقق';
						}
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
					}
				}

				/* Method to updateMaxDistance
					Created By: Manzz Baria
				*/
				function updateMaxDistance_post(){
					$data=json_decode(file_get_contents('php://input'));
					$this->verifyRequiredParams(array('maxDistance'),POST_TYPE);

					/***** getting params *****/
					$maxDistance = $_POST['maxDistance'];
					$language = LANGUAGE_ENGLISH;
					if (!empty($_POST['language'])){
						 $language = $_POST['language'];
					}
					$this->load->model("ApiAdminModel","adminModel");
					$result = $this->adminModel->updateMaxDistance($maxDistance);
					if($result != null){
								$mesage = 'Contact successfully updated';
								if ($language == LANGUAGE_ARABIC) {
										$mesage = 'تم التحديث بنجاح';
								}
								$this->displayMaxDistanceJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result);

					}else{
						$mesage = 'Something wrong,please try again later' ;
						if ($language == LANGUAGE_ARABIC) {
								$mesage = 'يرجى المحاولة مرة أخرى في وقت لاحقق';
						}
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
					}
				}

				/* Method to getMaxDistance
					Created By: Manzz Baria
				*/
				function getMaxDistance_get(){
					$data=json_decode(file_get_contents('php://input'));

					/***** getting params *****/
					$language = $this->get('language');
					if (empty($language)){
						$language = LANGUAGE_ENGLISH;
					}
					$this->load->model("ApiAdminModel","adminModel");
					$result = $this->adminModel->getMaxDistance();
					if($result != null){
								$mesage = 'Max distance found';
								$this->displayMaxDistanceJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result);

					}else{
						$mesage = 'Something wrong,please try again later' ;
						if ($language == LANGUAGE_ARABIC) {
								$mesage = 'يرجى المحاولة مرة أخرى في وقت لاحق';
						}
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
					}
				}

				function getMaxDistances_get(){
					$data=json_decode(file_get_contents('php://input'));

					/***** getting params *****/
					$language = $this->get('language');
					if (empty($language)){
						$language = LANGUAGE_ENGLISH;
					}
					 $this->load->model("ApiVanModel","vanModel");
					$result = $this->vanModel->getmaxdistanceByAdminId(1);
					if($result != null){
								$mesage = 'Max distance found';
								$this->displayMaxDistanceJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result);

					}else{
						$mesage = 'Something wrong,please try again later' ;
						if ($language == LANGUAGE_ARABIC) {
								$mesage = 'يرجى المحاولة مرة أخرى في وقت لاحق';
						}
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
					}
				}




				/* Method to admin login
					Created By: Manzz Baria
				*/
				function adminLogin_post(){
					$data=json_decode(file_get_contents('php://input'));
					$this->verifyRequiredParams(array('email','password'),POST_TYPE);

					/***** getting params *****/
					$email = $_POST['email'];
					$password = $_POST['password'];

					$language = LANGUAGE_ENGLISH;
					if (!empty($_POST['language'])){
						 $language = $_POST['language'];
					}

					$this->load->model("ApiAdminModel","adminModel");
					$result = $this->adminModel->adminLogin($email,$password);
					if($result != null){
								$mesage = 'Successfully login';
								$totalPages = 1;
								$currentPages = 1;
								$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

					}else{
						$mesage = 'Wrong email or pasword' ;
						if ($language == LANGUAGE_ARABIC) {
								$mesage = 'البريد الإلكتروني أو كلمة المرور غير صحيحين ';
						}
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
					}
				}
				/* Method to adminDashboard
					Created By: Manzz Baria
				*/
				function adminDashboard_get(){
					$data=json_decode(file_get_contents('php://input'));
					$this->verifyRequiredParams(array('adminId','todayDate'),GET_TYPE);

					/***** getting params *****/
				  $adminId = (int)$this->get('adminId');
					$todayDate = $this->get('todayDate');
					$language = $this->get('language');
					if (empty($language)){
						$language = LANGUAGE_ENGLISH;
					}

					$this->load->model("ApiAdminModel","adminModel");
					$result = $this->adminModel->adminDashboard($adminId,$todayDate);
					if($result != null){
								$mesage = 'Data found';
								$totalPages = 1;
								$currentPages = 1;
								$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

					}else{
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"No data found");
					}
				}

				/* Method to getCustomerServiceSupport
					Created By: Manzz Baria
				*/
				function getCustomerServiceSupport_get(){
					$data=json_decode(file_get_contents('php://input'));
					$language = $this->get('language');
					if (empty($language)){
						$language = LANGUAGE_ENGLISH;
					}
					$this->load->model("ApiAdminModel","adminModel");
					$result = $this->adminModel->getCustomerServiceSupport();
					if($result != null){
								$mesage = 'Data found';
								$totalPages = 1;
								$currentPages = 1;
								$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

					}else{
						$mesage = 'No customer service support found' ;
						if ($language == LANGUAGE_ARABIC) {
								$mesage = 'لم نتمكن من العثور على أي دعم من خدمة العملاء';
						}
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
					}
				}

/* Method to getCustomerServiceSupport
					Created By: Manzz Baria
				*/
				function sendNotificationTest_get(){
					$data=json_decode(file_get_contents('php://input'));

					$this->load->model("ApiNotificationModel","notificationModel");
					$this->load->model("ApiDeviceModel","deviceModel");
					$deviceObject[]="fFil9IZvHGg:APA91bHcWPnWpU5zym1F8J9nOo6pYAmlzfX5tIP8f5oFUrbSzML-l5Tfw_mBq8NNieQvXj6QBdCxlJKLFjjjjd3ff_9pj96RuDCkrDRwzTfPI1YTdSHB_601RHEpKJxbe1_Q_JO27wbI";
					$deviceId = $deviceObject;
					$title = "Test";
					$message = "Hello Krishnu";
					$userId = "3";
					$appointmentId = "3";
					$serviceId = "0";
					$notificationType = "1";
$result = $this->notificationModel->sendNotificationToAndroidDevice($deviceId,$title,$message,$userId,$appointmentId,$serviceId,$notificationType);
					if($result != null){
								$mesage = 'Data found';
								$totalPages = 1;
								$currentPages = 1;
								$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

					}else{
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"No data found");
					}
				}


				/* Method to sendSMSTEST
					Created By: Manzz Baria
				*/
				function sendSMSTEST_get(){
					$data=json_decode(file_get_contents('php://input'));
					$this->verifyRequiredParams(array('message','mobileNumber'),GET_TYPE);

					/***** getting params *****/
				  $message = $this->get('message');
				   $mobileNumber = $this->get('mobileNumber');
				  
							$this->load->model("ApiSMSModel","smsModel");

							

						$result = $this->smsModel->sendSMSByMobileNumber($message,$mobileNumber);
					if($result != null){
								$mesage = 'Data found';
								$totalPages = 1;
								$currentPages = 1;
								$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

					}else{
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"No data found");
					}
				}


				/* Method to sendTestIphoneNotification
					Created By: Manzz Baria
				*/
				function sendTestIphoneNotification_get(){
					$data=json_decode(file_get_contents('php://input'));
					
					/***** getting params *****/
					$this->load->model("ApiNotificationModel","notificationModel");
					$result = $this->notificationModel->sendTestIphoneNotification();
					if($result){
								$mesage = 'Notification successfully sent';
								$this->displayMaxDistanceJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result);

					}else{
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"Failed to send notification");
					}
				}
				
				
				
				/* Method to adminSendMessage
					Created By: Manzz Baria
				*/
				function adminSendMessage_post(){
					$data=json_decode(file_get_contents('php://input'));
					$this->verifyRequiredParams(array('type','userType','userIds'),POST_TYPE);

					/***** getting params *****/
					$type = $_POST['type'];
					$userType = $_POST['userType'];
					
                                         $userIds = $_POST['userIds'];
                                         
                                         //echo $userIds;
					
					$mesaageEn = "";
					$mesaageAr = "";
					if (!empty($_POST['messageEn'])){
						 $mesaageEn = $_POST['messageEn'];
					}
					
					if (!empty($_POST['mesaageAr'])){
						 $mesaageAr = $_POST['mesaageAr'];
					}
					
					if (!empty($_POST['countryCode'])){
						$countryCode = $_POST['countryCode'];
					}else{
							$countryCode = "";
					}
					if ($countryCode == "undefined"){
						$countryCode = "";
					}
					
					
					
					if(empty($_POST['messageEn']) && empty($_POST['mesaageAr']))
					{
						$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"Please provide message");
					}

					$this->load->model("ApiNotificationModel","notificationModel");
					$this->load->model("ApiDeviceModel","deviceModel");
					$this->load->model("ApiSMSModel","sModel");
					
				//	$result = $this->adminModel->adminLogin($email,$password);
						
						if($type==1)
						{
							$DeviceTokenEn=	$this->deviceModel->getDeviceTokenbyCountry($userType,$countryCode,LANGUAGE_EN,$userIds);
							$DeviceTokenAr=	$this->deviceModel->getDeviceTokenbyCountry($userType,$countryCode,LANGUAGE_AR,$userIds);
							$titleAr = "كارسبا";
							$titleEn  = "CarSpa";
							if (empty($_POST['messageEn'])){
								
								$this->notificationModel->sendNotificationFromAdmin($DeviceTokenEn,$titleAr,$mesaageAr);
								$this->notificationModel->sendNotificationFromAdmin($DeviceTokenAr,$titleAr,$mesaageAr);
							}
							else if (empty($_POST['mesaageAr']))
							{
								$this->notificationModel->sendNotificationFromAdmin($DeviceTokenEn,$titleEn,$mesaageEn);
								$this->notificationModel->sendNotificationFromAdmin($DeviceTokenAr,$titleEn,$mesaageEn);
							}
							else
							{
								$this->notificationModel->sendNotificationFromAdmin($DeviceTokenEn,$titleEn,$mesaageEn);
								$this->notificationModel->sendNotificationFromAdmin($DeviceTokenAr,$titleAr,$mesaageAr);
							}
				
						}
						else
						{
							$MobileNoEn=$this->sModel->getNumberbyCountry($userType,$countryCode,LANGUAGE_EN,$userIds);
							$MobileNoAr=$this->sModel->getNumberbyCountry($userType,$countryCode,LANGUAGE_AR,$userIds);
							
							
							//echo "$mesaageAr".$mesaageAr;
							//echo "$mesaageEn".$mesaageEn;
							
							if ($mesaageEn=="undefined")
							{
								
								//$this->sModel->sendSMSforAdmin($mesaageAr,$MobileNoEn);
								$this->sModel->sendSMSforAdmin($mesaageAr,$MobileNoAr);
							}
							else if ($mesaageAr=="undefined")
							{
								
								$this->sModel->sendSMSforAdmin($mesaageEn,$MobileNoEn);
								//$this->sModel->sendSMSforAdmin($mesaageEn,$MobileNoAr);
							}
							else
							{
								
								$this->sModel->sendSMSforAdmin($mesaageEn,$MobileNoEn);
								$this->sModel->sendSMSforAdmin($mesaageAr,$MobileNoAr);
							}
				
						}
				
								$mesage = 'Successfully sent';
								$totalPages = $userIds;
								$currentPages = $countryCode;
								$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$mesage,$totalPages,$currentPages);

				}





}
?>
