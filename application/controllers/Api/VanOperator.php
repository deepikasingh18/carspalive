<?php

defined('BASEPATH') OR exit('No direct script access allowed');
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class VanOperator extends REST_Controller
{
	public function __construct()
	{
			 header('Access-Control-Allow-Origin: *');
			 header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
			 header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
			 $method = $_SERVER['REQUEST_METHOD'];
			 if($method == "OPTIONS") {
					 die();
			 }

		  parent::__construct();
      $this->load->library('session');
      $this->load->helper(array('form','url'));
      $this->load->library('form_validation');
			$this->load->library('mylibrary');
			 $this->load->database();


	}
	    /* Method to display default error message
	    		 Created By: Manzz Baria
	    	*/
	    	private function displayDefaultJsonWithoutData($status,$message){
	    		if($status == JSON_SUCCESS_STATUS){
	    			$this->response([
	    				'Status' => $status,
	    				'Message' => $message
	    			], REST_Controller::HTTP_OK);
	    		}else{
	    			$this->response([
	    				'Status' => $status,
	    				'Message' => $message
	    			], REST_Controller::HTTP_OK);
	    		}
	    	}

				/* Method to show Message
					Created By: Manzz Baria
				*/
				public function showMessage($status,$message){
					$this->response([
						'Status' => $status,
						'Message' => $message
					], REST_Controller::HTTP_OK);
				}

	    	/* Method to display default success with Result data and totals
	    		 Created By: Manzz Baria
	    	*/
	    	private function displayDefaultJsonWithData($status,$mesage,$result,$totalPages,$currentPages){
	    		if($status == JSON_SUCCESS_STATUS){
	    			$this->response([
	    				'Status' => $status,
	    				'Message' => $mesage,
	    				'TotalPage' => $totalPages,
	    				'CurrentPage' => $currentPages,
	    				'Data' => $result
	    			], REST_Controller::HTTP_OK);
	    		}else{
	    			$this->response([
	    				'Status' => $status,
	    				'Message' => $mesage,
	    				'TotalPage' => $totalPages,
	    				'CurrentPage' => $currentPages,
	    				'Data' => $result
	    			], REST_Controller::HTTP_OK);
	    		}
	    	}

	    /* Method to check parameters is null or empty
	        Created By: Manzz Baria
	    */
	    public function verifyRequiredParams($required_fields,$type){
	        $error = false;
	        $error_fields = "";
	        foreach ($required_fields as $field) {
	    			if($type == POST_TYPE){
	    				if ($this->post($field) == null) {
	    	      	$error = true;
	    	        $error_fields .= $field . ', ';
	    	      }
	    	    }else if($type == GET_TYPE){
	    				if ($this->get($field) == null) {
	    	      	$error = true;
	    	        $error_fields .= $field . ', ';
	    	      }
	    	    }else if($type == PUT_TYPE){
	    				if ($this->put($field) == null) {
	    	      	$error = true;
	    	        $error_fields .= $field . ', ';
	    	      }
	    	    }else{
	    				if ($this->delete($field) == null) {
	    	      	$error = true;
	    	        $error_fields .= $field . ', ';
	    	      }
	    			}
	        }
	        if ($error) {
	        	$message = 'Required field(s) ' . substr($error_fields, 0, -2) . ' is missing or empty';
	          $this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$message);
	        }
	      }


				/* Method to addVanOperator
					Created By: Manzz Baria
				*/
				function addVanOperator_post(){
					$data=json_decode(file_get_contents('php://input'));
					$this->verifyRequiredParams(array('fullName','address','email','countryCode','mobileNumber','password'),POST_TYPE);

					/***** getting params *****/
					$fullName = $_POST['fullName'];
					$address = $_POST['address'];
					$email = $_POST['email'];
					$mobileNumber = $_POST['mobileNumber'];
                                       
                                       $this->load->model("ApiVanOperatorModel","vanOperatorModel");

                                        if ($mobileNumber){
                                       
						//$mobileNumber = $_POST['mobileNumber'];
						
						$isMobileNumberRegistered = $this->vanOperatorModel->isUserMobileNumberAlreadyRegister($mobileNumber);
  						 if($isMobileNumberRegistered ){
								 $mesage = 'User already registered, please use different email or mobile number';
                                                               
  								$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
  						 
						 }
				 }
					$password = $_POST['password'];
					$countryCode = 0;
					$fileUrl = "";
					$countryCode = $_POST['countryCode'];
				/*	if (!empty($_POST['countryCode'])){
 						$countryCode = $_POST['countryCode'];
 				 }
				 */
					if(isset($_FILES['file']['name'])){
						 $file=$_FILES['file']['name'];

						 $image_path='./uploads/files/';
						 $thumb_path='./uploads/files/thumb/';
						 $data = $this->uploadFileContent();
						 $filename=$data['upload_data']['file_name'];
		 				 $videofile=$_FILES["file"]["tmp_name"];

						 // Images create thumbnail and upload
						 $imagefile = $filename;
						 $imagefile = $this->generateImageThumbContent($imagefile,$data);
						 $fileUrl = $filename;

					}
					
					$result = $this->vanOperatorModel->addVanOperator($fullName,$address,$email,$countryCode,$mobileNumber,$password,$fileUrl);
					if($result != null){
								$mesage = 'Operator successfully added';
								$totalPages = 1;
								$currentPages = 1;
								$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

					}else{
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"User already registered, please use different email or mobile number");
					}

				}

				/* Method to updateVanOperator
					Created By: Manzz Baria
				*/
				function updateVanOperator_post(){
					$data=json_decode(file_get_contents('php://input'));
					$this->verifyRequiredParams(array('userId'),POST_TYPE);

					/***** getting params *****/
					$userId = $_POST['userId'];
					$countryCode = 0;
					$password = "";
					$language = LANGUAGE_ENGLISH;
					if (!empty($_POST['language'])){
						 $language = $_POST['language'];
					}
					$this->load->model("ApiVanOperatorModel","vanOperatorModel");
					 $this->load->model("ApiUserModel","userModel");


						if (!empty($_POST['fullName'])){
							$fullName = $_POST['fullName'];
					 }
					 if (!empty($_POST['address'])){
						 $address = $_POST['address'];
					}

					if (!empty($_POST['email'])){
						$email = $_POST['email'];
						$isRegistered = $this->vanOperatorModel->isVanOperatorAlreadyAddedORItsOwnEmail($email,$userId);
						if($isRegistered){
							$mesage = 'Email already registered';
						 if ($language == LANGUAGE_ARABIC) {
							 $mesage = 'البريد الإلكتروني مسجل مسبقا ';
						 }
							 $this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
						}
				 }
				 if (!empty($_POST['mobileNumber'])){
						$mobileNumber = $_POST['mobileNumber'];
						
						 $isOwnNumber = $this->userModel->isUserMobileNumberIsOwn($mobileNumber,$userId);
						 if (!$isOwnNumber) {
							 $isMobileNumberRegistered = $this->userModel->isUserMobileNumberAlreadyRegister($mobileNumber);
  						 if($isMobileNumberRegistered){
								 $mesage = 'Mobile number already registered';
		 						if ($language == LANGUAGE_ARABIC) {
		 							$mesage = ' تم تسجيل رقم الجوال سابقا ';
		 						}
  								$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
  						 }
						 }
				 }
				 if (!empty($_POST['countryCode'])){
					 $countryCode = $_POST['countryCode'];
				  }
					if (!empty($_POST['password'])){
								$password = $_POST['password'];
					}


					$fileUrl = "";
					if(isset($_FILES['file']['name'])){
						 $file=$_FILES['file']['name'];

						 $image_path='./uploads/files/';
						 $thumb_path='./uploads/files/thumb/';
						 $data = $this->uploadFileContent();
						 $filename=$data['upload_data']['file_name'];
						 $videofile=$_FILES["file"]["tmp_name"];

						 // Images create thumbnail and upload
						 $imagefile = $filename;
						 $imagefile = $this->generateImageThumbContent($imagefile,$data);
						 $fileUrl = $filename;

					}

					$result = $this->vanOperatorModel->updateVanOperator($userId,$fullName,$address,$email,$countryCode,$mobileNumber,$fileUrl,$password);
					if($result != null){
								$mesage = 'Operator successfully updated';
								if ($language == LANGUAGE_ARABIC) {
		 							$mesage = 'تم تحديث المشغل بنجاح ';
		 						}
								$totalPages = 1;
								$currentPages = 1;
								$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

					}else{
						$mesage = 'Unable to update operator';
						if ($language == LANGUAGE_ARABIC) {
							$mesage = ' غير قادر على تحديث المشغل';
						}
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
					}

				}

				/* Method to blockVanOperator
					Created By: Manzz Baria
				*/
				function blockVanOperator_post(){
					$data=json_decode(file_get_contents('php://input'));
					$this->verifyRequiredParams(array('userId'),POST_TYPE);

					/***** getting params *****/
					$userId = $_POST['userId'];

					$this->load->model("ApiVanOperatorModel","vanOperatorModel");
					$result = $this->vanOperatorModel->blockVanOperatorId($userId);
					if($result){
								$mesage = 'Operator successfully deleted';
								$totalPages = 1;
								$currentPages = 1;
								$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

					}else{
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"Unable to delete operator");
					}
				}

				/* Method to blockVanOperator
					Created By: Manzz Baria
				*/
				function UnblockVanOperator_post(){
					$data=json_decode(file_get_contents('php://input'));
					$this->verifyRequiredParams(array('userId'),POST_TYPE);

					/***** getting params *****/
					$userId = $_POST['userId'];

					$this->load->model("ApiVanOperatorModel","vanOperatorModel");
					$result = $this->vanOperatorModel->unBlockVanOperatorId($userId);
					if($result){
								$mesage = 'Operator successfully unblocked';
								$totalPages = 1;
								$currentPages = 1;
								$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

					}else{
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"Unable to unblock operator");
					}
				}

				/* Method to Upload content
					Created By: Nishit Patel
				*/
				public function uploadFileContent(){
					$time = time();
					$new_name = 'carspa_'.$time;
					$config['upload_path'] = './uploads/files/';
					$config['allowed_types']        = '*';
					$config['max_size']             = 0;
					$config['max_width']            = 9000;
					$config['max_height']           = 8000;
					$config['file_name'] = $new_name;

					$this->load->library('upload', $config);
					$this->upload->initialize($config);
					if(!$this->upload->do_upload('file')){
						$error = array('error' => $this->upload->display_errors());
						$msg=$error['error'];
						$status=FALSE;
						$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$msg);
					}else{
						$data = array('upload_data' => $this->upload->data());
						$filename=$data['upload_data']['file_name'];
					}
					$data = $this->upload->data();
					$data=array('upload_data' => $this->upload->data());
					return $data;
				}
				public function generateImageThumbContent($imagefile,$data){
					$config['image_library'] = 'gd2';
					$config['source_image'] =  $data['upload_data']['full_path'];
					$config['new_image'] =  $data['upload_data']['full_path'].'/thumb/'.$imagefile;
					$config['maintain_ratio'] = FALSE;
					$config['overwrite'] = TRUE;
					$config['allowed_types'] = '*';
					$config['max_size'] = 0;
					$config['width'] = 120;
					$config['height'] = 100;
					$this->load->library('image_lib', $config);
					if($this->image_lib->resize()){}
					return $imagefile;
				}


			/* Method to getEnquiriesOnMyElectraById_get
				Created By: Manzz Baria
			*/
				function getVanOperators_get(){
					$data=json_decode(file_get_contents('php://input'));
				//	$this->verifyRequiredParams(array('electraId'),GET_TYPE);
					$pageIndex = (int)$this->get('pageIndex');
					if (empty($pageIndex)){
						$pageIndex = 0;
					}
					$this->load->model("ApiVanOperatorModel","vanOperatorModel");
					$result = $this->vanOperatorModel->getVanOperators($pageIndex);
					$totalPage = $this->vanOperatorModel->getTotalPagesForVanOperators();
					if ($result != null) {
								$mesage = 'Data found';
								$totalPages = $totalPage;
								$currentPages = $pageIndex + 1;
								$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);
						}else{
						$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"No operator found");
					//	return;
					}
				}

				function getAllVanOperators_get(){
					$data=json_decode(file_get_contents('php://input'));
				//	$this->verifyRequiredParams(array('electraId'),GET_TYPE);
					$pageIndex = (int)$this->get('pageIndex');
					if (empty($pageIndex)){
						$pageIndex = 0;
					}
					$this->load->model("ApiVanOperatorModel","vanOperatorModel");
					$result = $this->vanOperatorModel->getAllVanOperators($pageIndex);
					//$totalPage = $this->vanOperatorModel->getTotalPagesForVanOperators();
					if ($result != null) {
								$mesage = 'Data found';
								$totalPages = 1;
								$currentPages = 0;
								$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);
						}else{
						$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"No operator found");
					//	return;
					}
				}

				function searchOperators_get()
				{
						$data=json_decode(file_get_contents('php://input'));
					//	$this->verifyRequiredParams(array('electraId'),GET_TYPE);
						$name = $this->get('name');
						
						$this->load->model("ApiVanOperatorModel","vanOperatorModel");

						$pageIndex = (int)$this->get('pageIndex');
						
						if (empty($pageIndex))
						{	
							$pageIndex = 0;
						}
				
					   
					   if($name=="")
					   {
					   		$result = $this->vanOperatorModel->getVanOperators($pageIndex);
					   		$totalPage = $this->vanOperatorModel->getTotalPagesForVanOperators();
					   		$mesage = 'Found data';
									$totalPages = $totalPage;
									$currentPages = $pageIndex+1;
									$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);
					   }
					   else
					   {
					 	 
							$result = $this->vanOperatorModel->searchOperators($name);
								if($result != null)
								{
											$mesage = 'Found data';
											$totalPages = 1;
											$currentPages = 1;
											$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

								}
								else
								{
									$mesage = 'No Operator found';
									$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
								}
					 	}

				}

				/*
				  Method to customer login
					Created By: Manzz Baria
				*/

				function operatorLogin_post(){
					$data=json_decode(file_get_contents('php://input'));
					$this->verifyRequiredParams(array('email','password','deviceType','deviceToken','latitude','longitude'),POST_TYPE);
					/***** getting params *****/
					$email = $_POST['email'];
					$password = $_POST['password'];
					$deviceType = $_POST['deviceType'];
					$deviceToken = $_POST['deviceToken'];
					$latitude = $_POST['latitude'];
					$longitude = $_POST['longitude'];
					$logindatetime= date("Y-m-d h:i:sa");
					$type="";

					$apiName="operatorLoginApi";
					$ip =  $_SERVER['REMOTE_ADDR'];
					


                     if (strpos($email, '@') === FALSE) 
                     {
						  $email=substr($email,3);
					 }


					if ($latitude <= 0 || $longitude <= 0) {
						$latitude = VAN_DEFAULT_LATITUDE;
						$longitude = VAN_DEFAULT_LONGITUDE;
					}
					$language = LANGUAGE_ENGLISH;
					if (!empty($_POST['language'])){
						 $language = $_POST['language'];
					}
					$this->load->model("Utility","util");
					

					$this->load->model("ApiUserModel","usModel");
					$this->load->model("ApiVanOperatorModel","vanOperatorModel");
					$this->load->model("ApiDeviceModel","deviceModel");
					 $this->load->model("ApiUserModel","userModel");

					$isAvailable = $this->usModel->isAccountAvailable($email);

					$requestBody=array("email"=>$email,"password"=>$password,"deviceType"=>$deviceType,"deviceToken"=>$deviceToken,"latitude"=>$latitude
					,"longitude"=>$longitude,"isAvailable"=>$isAvailable);

					if ($isAvailable) {
							//$isActive = $this->usModel->isAccountActive($email);
							//if ($isActive) {
								    $userId = $this->vanOperatorModel->getOperatorIdFromEmail($email);
								    $this->util->dumpTester('operatorLogin',$latitude,$longitude,$userId);
								    $this->deviceModel->addDeviceToken($userId,$deviceToken,$deviceType);
								    $this->usModel->updateUserLocation($userId,$latitude,$longitude,$logindatetime);
								    $this->usModel->updateUserSatus($userId,USER_STATUS_ONLINE);
								     $this->usModel->updateUserActive($userId ,1);
								    $this->usModel->updateLanguage($deviceToken ,$language );
										$result = $this->vanOperatorModel->loginOperator($email,$password);

										$response=$this->userModel->callapilog($userId,$type,$apiName,$ip,$requestBody,$result);
										if($result != null){
													$mesage = 'Successfully login';
													$totalPages = 1;
													$currentPages = 1;
													$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

										}else{
											$mesage = 'unable to login';
											if ($language == LANGUAGE_ARABIC) {
												$mesage = 'غير قادر على تسجيل الدخول ';
											}
												$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
										}
							//}else{
							//	$mesage = 'Your account is not activated yet, to active your account check your registered email account';
							//	if ($language == LANGUAGE_ARABIC) {
 							//	 $mesage = ' لم يتم تنشيط الحساب الخاص بك حتى الآن، لتنشيط حسابك تحقق حساب البريد الإلكتروني مسجل';
 							// }
							//	$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
							//}
					}else{
						$mesage = 'Invalid email or password';
						if ($language == LANGUAGE_ARABIC) {
							$mesage = 'البريد الإلكتروني أو كلمة السر خاطئة ';
						}
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
					}

				}

				/* Method to getVanOperatorDetails
					Created By: Manzz Baria
				*/
					function getVanOperatorDetails_get(){
						$data=json_decode(file_get_contents('php://input'));
						$this->verifyRequiredParams(array('userId'),GET_TYPE);
						$userId = (int)$this->get('userId');

						$this->load->model("ApiVanOperatorModel","vanOperatorModel");
						$result = $this->vanOperatorModel->getVanOperatorById($userId);
						if ($result != null) {
									$mesage = 'Data found';
									$totalPages = 1;
									$currentPages = 1;
									$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);
							}else{
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"No operator detail found");
						//	return;
						}
					}

		/* Method to updateLocation
			Created By: Manzz Baria
		*/
		function updateLocation_post(){
			$data=json_decode(file_get_contents('php://input'));
			$this->verifyRequiredParams(array('userId','latitude','longitude'),POST_TYPE);

			/***** getting params *****/

			$userId = $_POST['userId'];
			$latitude = $_POST['latitude'];
			$longitude = $_POST['longitude'];
			$logindatetime= date("Y-m-d h:i:sa");
			$type="";
			$apiName="updateLocationApi";
			$ip =  $_SERVER['REMOTE_ADDR'];


			if ($latitude <= 0 || $longitude <= 0) {
				 $latitude = VAN_DEFAULT_LATITUDE;
				 $longitude = VAN_DEFAULT_LONGITUDE;

			}

			$this->load->model("ApiUserModel","userModel");

			$requestBody=array("latitude"=>$latitude,"longitude"=>$longitude,"logindatetime"=>$logindatetime);


			$result = $this->userModel->updateUserLocation($userId,$latitude,$longitude,$logindatetime);

			$response=$this->userModel->callapilog($userId,$type,$apiName,$ip,$requestBody,$result);

			if($result){
						$mesage = 'Successfully updated';
						$totalPages = 1;
						$currentPages = 1;
						$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

			}else{
					$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"Unable to update location");
			}
		}

		/* Method to updateUserStatus
			Created By: Manzz Baria
		*/
		function updateUserStatus_post(){
			$data=json_decode(file_get_contents('php://input'));
			$this->verifyRequiredParams(array('userId','status'),POST_TYPE);

			/***** getting params *****/
			$userId = $_POST['userId'];
			$status = $_POST['status'];
			$type="";
			$apiName="updateUserStatusApi";
			$ip =  $_SERVER['REMOTE_ADDR'];

			$this->load->model("ApiUserModel","userModel");
			$requestBody=array("status"=>$status);

			$result = $this->userModel->updateUserSatus($userId,$status);

			$response=$this->userModel->callapilog($userId,$type,$apiName,$ip,$requestBody,$result);


			if($result){
						$mesage = 'Successfully changed';
						$totalPages = 1;
						$currentPages = 1;
						$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

			}else{
					$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"Unable to change user status");
			}
		}

		function getOperatorReport_get(){
		$data=json_decode(file_get_contents('php://input'));
		$this->load->model("ApiVanOperatorModel","VanOperatorModel");
		$operatorstatus = $this->get('operatorstatus');
		if (empty($operatorstatus)){
			$operatorstatus = '-1';
		}
		$customerId = $this->get('customerId');
		if (empty($customerId)){
			$customerId = '-1';
		}
		$serviceId = $this->get('serviceId');
		if (empty($serviceId)){
			$serviceId = '-1';
		}
		$country = $this->get('countryId');
		if (empty($country)){
			$country = '-1';
		}
		$result = $this->VanOperatorModel->getOperatorReport($operatorstatus,$customerId,$serviceId,$country);
		if ($result != null) {
					$mesage = 'Data found';
					$totalPages = 1;
					$currentPages = 1;
					$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);
			}else{
			$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"No report found");
		//	return;
		}
	}
	/*function getaddress_get()
	{
		$geolocation = $latitude.','.$longitude;
		$request = 'http://maps.googleapis.com/maps/api/geocode/json?latlng='.$geolocation.'&sensor=false'; 
		$file_contents = file_get_contents($request);
		$json_decode = json_decode($file_contents);
		if(isset($json_decode->results[0])) {
		    $response = array();
		    foreach($json_decode->results[0]->address_components as $addressComponet) {
		        if(in_array('political', $addressComponet->types)) {
		                $response[] = $addressComponet->long_name; 
		        }
		    }

		    if(isset($response[0])){ $first  =  $response[0];  } else { $first  = 'null'; }
		    if(isset($response[1])){ $second =  $response[1];  } else { $second = 'null'; } 
		    if(isset($response[2])){ $third  =  $response[2];  } else { $third  = 'null'; }
		    if(isset($response[3])){ $fourth =  $response[3];  } else { $fourth = 'null'; }
		    if(isset($response[4])){ $fifth  =  $response[4];  } else { $fifth  = 'null'; }

		    if( $first != 'null' && $second != 'null' && $third != 'null' && $fourth != 'null' && $fifth != 'null' ) {
		        echo "<br/>Address:: ".$first;
		        echo "<br/>City:: ".$second;
		        echo "<br/>State:: ".$fourth;
		        echo "<br/>Country:: ".$fifth;
		    }
		    else if ( $first != 'null' && $second != 'null' && $third != 'null' && $fourth != 'null' && $fifth == 'null'  ) {
		        echo "<br/>Address:: ".$first;
		        echo "<br/>City:: ".$second;
		        echo "<br/>State:: ".$third;
		        echo "<br/>Country:: ".$fourth;
		    }
		    else if ( $first != 'null' && $second != 'null' && $third != 'null' && $fourth == 'null' && $fifth == 'null' ) {
		        echo "<br/>City:: ".$first;
		        echo "<br/>State:: ".$second;
		        echo "<br/>Country:: ".$third;
		    }
		    else if ( $first != 'null' && $second != 'null' && $third == 'null' && $fourth == 'null' && $fifth == 'null'  ) {
		        echo "<br/>State:: ".$first;
		        echo "<br/>Country:: ".$second;
		    }
		    else if ( $first != 'null' && $second == 'null' && $third == 'null' && $fourth == 'null' && $fifth == 'null'  ) {
		        echo "<br/>Country:: ".$first;
		    }
		  }
	}*/

}
?>
