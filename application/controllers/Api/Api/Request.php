<?php

defined('BASEPATH') OR exit('No direct script access allowed');
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class Request extends REST_Controller
{
	public function __construct()
	{
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		$method = $_SERVER['REQUEST_METHOD'];
		if($method == "OPTIONS") {
				die();
		}
		  parent::__construct();
      $this->load->library('session');
      $this->load->helper(array('form','url'));
      $this->load->library('form_validation');
			$this->load->library('mylibrary');
			 $this->load->database();
	}
	    /* Method to display default error message
	    		 Created By: Manzz Baria
	    	*/
	    	private function displayDefaultJsonWithoutData($status,$message){
	    		if($status == JSON_SUCCESS_STATUS){
	    			$this->response([
	    				'Status' => $status,
	    				'Message' => $message
	    			], REST_Controller::HTTP_OK);
	    		}else{
	    			$this->response([
	    				'Status' => $status,
	    				'Message' => $message
	    			], REST_Controller::HTTP_OK);
	    		}
	    	}

				/* Method to display default error message
						 Created By: Manzz Baria
					*/
					private function displayDefaultJsonWithoutDataError($status,$categoryId,$message){
						if($status == JSON_SUCCESS_STATUS){
							$this->response([
								'Status' => $status,
								'categoryId' => $categoryId,
								'Message' => $message
							], REST_Controller::HTTP_OK);
						}else{
							$this->response([
								'Status' => $status,
								'categoryId' => $categoryId,
								'Message' => $message
							], REST_Controller::HTTP_OK);
						}
					}


	    	/* Method to display default success with Result data and totals
	    		 Created By: Manzz Baria
	    	*/
	    	private function displayDefaultJsonWithData($status,$mesage,$result,$totalPages,$currentPages){
	    		if($status == JSON_SUCCESS_STATUS){
	    			$this->response([
	    				'Status' => $status,
	    				'Message' => $mesage,
	    				'TotalPage' => $totalPages,
	    				'CurrentPage' => $currentPages,
	    				'Data' => $result
	    			], REST_Controller::HTTP_OK);
	    		}else{
	    			$this->response([
	    				'Status' => $status,
	    				'Message' => $mesage,
	    				'TotalPage' => $totalPages,
	    				'CurrentPage' => $currentPages,
	    				'Data' => $result
	    			], REST_Controller::HTTP_OK);
	    		}
	    	}

	    /* Method to check parameters is null or empty
	        Created By: Manzz Baria
	    */
	    public function verifyRequiredParams($required_fields,$type){
	        $error = false;
	        $error_fields = "";
	        foreach ($required_fields as $field) {
	    			if($type == POST_TYPE){
	    				if ($this->post($field) == null) {
	    	      	$error = true;
	    	        $error_fields .= $field . ', ';
	    	      }
	    	    }else if($type == GET_TYPE){
	    				if ($this->get($field) == null) {
	    	      	$error = true;
	    	        $error_fields .= $field . ', ';
	    	      }
	    	    }else if($type == PUT_TYPE){
	    				if ($this->put($field) == null) {
	    	      	$error = true;
	    	        $error_fields .= $field . ', ';
	    	      }
	    	    }else{
	    				if ($this->delete($field) == null) {
	    	      	$error = true;
	    	        $error_fields .= $field . ', ';
	    	      }
	    			}
	        }
	        if ($error) {
	        	$message = 'Required field(s) ' . substr($error_fields, 0, -2) . ' is missing or empty';
	          $this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$message);
	        }
	      }

		/* Method to show Message
			Created By: Manzz Baria
		*/
		public function showMessage($status,$message){
			$this->response([
				'Status' => $status,
				'Message' => $message
			], REST_Controller::HTTP_OK);
		}



	/***************************    NEW APIS    ***************************/
		function getTime_get(){
			$minutes_to_add = 30;
			$time = new DateTime('05:05');
			$time->add(new DateInterval('PT' . $minutes_to_add . 'M'));
			$hours = $time->format('H');
			$time = $time->format('i');
			echo "Time: ".$hours.":".$time;
		}
		
		function getTimeDifference_get(){
			$timeOne = "2017-02-03 12:29:42";
			$timeTwo = "2017-02-03 12:19:42";
			$one = strtotime($timeOne);
			$two = strtotime($timeTwo);
			$f = $this->getMinusTimes($timeOne,20);
			echo "TimeOne: ".$one ." TimeTwo: ".$two;
			
		}

		function getTimeByAddingMint($time){
			$time = new DateTime($time);
			$time->add(new DateInterval('PT' . ADD_MINT . 'M'));
			$hours = $time->format('H');
			$time = $time->format('i');
			return $hours.":".$time;
		}

		function getAddedTime($mainTime,$addedMint){
			$mainDateTime = new DateTime($mainTime);
			$mainDateTime->add(new DateInterval('PT' . $addedMint . 'M'));
			return $mainDateTime->format('H').":".$mainDateTime->format('i').":00";
		}

		function getMinusTimes($mainTime,$minusMint){
		//	$mainDateTime = new DateTime($mainTime);
			$datetime_from = (new DateTime($mainTime))->sub(DateInterval::createFromDateString($minusMint.' minutes'))->format('Y-m-d H:i:s');
			return $datetime_from;
		}
			function sendDemoEmail_get(){
			$url = base_url()."Api/Email/testEmailDemo";
			$param = array('appointmentId' => "1");
			$this->mylibrary->do_in_background($url, $param);
			echo $url;
			//$ch = curl_init();
			//		curl_setopt($ch, CURLOPT_URL, $url);
			//		curl_setopt( $ch, CURLOPT_POST, false );
					//curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
			//		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
			//		curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
				//	curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $fields ) );
			//		$result = curl_exec($ch);
			//		curl_close($ch);

					

		}

		function getTestShift_get(){
			$data=json_decode(file_get_contents('php://input'));
			//$this->verifyRequiredParams(array('requestDate'),GET_TYPE);

			/***** getting params *****/

		//	$requestDate = $this->get('requestDate');
$this->load->model("ApiServiceModel","servicemodel");
			$this->load->model("Utility","utility");
			$this->load->model("ApiShiftModel","shiftmodel");
			$this->load->model("ApiAppoinmnetModel","appointmentmodel");

		//	$appointmentDate = $this->utility->addDaysToDate($requestDate,0,'d-m-Y');
		//	$shiftDate = $this->utility->addDaysToDate($appointmentDate,0,'Y-m-d');
		//	$result = $this->shiftmodel->getTestShift($shiftDate);
	//		$dayName = $this->utility->getDayNameByDate($shiftDate);



	  $appointmentDateS = '2017-02-10';
			$appointmentDate = '08:00:00';
			$appointmentENdDate = '08:30:00';
		//$dayName = $this->getMinusTimes($appointmentDate,8);
		$this->appointmentmodel->getRequestVanOwnerInfo(1);
		$formet = "Y-m-d";
		$appointmentDateS = $this->utility->addDaysToDate($appointmentDateS,0,'d-m-Y');
		$dayName = $this->appointmentmodel->getAppointmentByDate($appointmentDateS,4);
		//$dayName = $this->getTimeDifference($appointmentENdDate,$appointmentDate);
		//$dayName = $this->getAddedTime($appointmentDate,$lastAppointmentStartTime);
//$dayName = $lastAppointmentStartTime;


$pass = "welcome";
 $pass =  "6QXCHPo6ROMyTq2+V/ZU84a0YACp14O79vKd76ingGY=";
$pass = '2017-02-17 01:36:00';
$dayName = $this->utility->getDifferenceInHour($pass);
$dayName = $this->utility->getCurrentDate('Y-m-d');
			if($dayName != null){
						$mesage = 'Successfully';
						$totalPages = $dayName;
						$currentPages = 1;
						$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$dayName,$totalPages,$currentPages);

			}else{
					$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"Error");
			}

		}

		function getRequestTime_get(){
			$data=json_decode(file_get_contents('php://input'));
			$this->verifyRequiredParams(array('requestDate','userId','addressId','duration','serviceIds'),GET_TYPE);
			/***** getting params *****/
			$requestDate = $this->get('requestDate');
			$userId = (int)$this->get('userId');
			$addressId = (int)$this->get('addressId');
			$serviceDuration = (int)$this->get('duration');
			$serviceIds = $this->get('serviceIds');
			$language = $this->get('language');
			if (empty($language)){
				$language = 'en';
			}

			$this->load->model("ApiServiceModel","servicemodel");
			$this->load->model("Utility","utility");
			$this->load->model("ApiAppoinmnetModel","appointmentmodel");
			$this->load->model("ApiShiftModel","shiftmodel");
			$this->load->model("ApiAddressModel","addressmodel");
			$this->load->model("ApiVanModel","vanModel");
			$this->load->model("ApiHolidayModel","holidayModel");
			$this->load->model("ApiAdminModel","adminModel");
      		//$maxDistance = $this->adminModel->getMaxDistance();
		//	echo $maxDistance;

			$newAppointmentTimeList = null;
			$totalTimeRequired = $serviceDuration;
			$newAppointmentTravelingTime = ADD_MINT;

			$addLatitude = 0;
			$addLongitude = 0;
			$isHoliday =false;
			$isOutsideArea =false;
			$isBreakPoint = false;
			$isVanAvailable = false;
			$isShiftAvailable = false;

			$addressObject = $this->addressmodel->getAddressById($addressId);
			if($addressObject != null){
					$addLatitude = $addressObject['latitude'];
					$addLongitude = $addressObject['longitude'];

			}

			//Generate Appointment Date & get appointsments

			$appointmentDate = $this->utility->addDaysToDate($requestDate,0,'d-m-Y');
			$currentAppointmentDate = $this->utility->changeDateFromat($appointmentDate,'Y-m-d');

			//echo json_encode($appointments);

			$shiftDate = $this->utility->addDaysToDate($appointmentDate,0,'Y-m-d');
			$shiftIdList = $this->shiftmodel->getShiftsByDate($shiftDate);
			$dayName = $this->utility->getDayNameByDate($shiftDate);
		//	$shiftTimeList = $this->shiftmodel->getShiftDetailsByShiftId($shiftId);
		//  $vans = $this->vanModel->getAllVanByAvailableServices($serviceIds);


			if($shiftIdList != null){
					//echo json_encode($shiftIdList);
		 	foreach ($shiftIdList as $shiftId) {
					$shiftOwners = $this->vanModel->getOwnerListByShift($shiftId["Id"]);

					$holidays = $this->holidayModel->getHolidaysByType($shiftId["Id"],DAYS);
					$holidates = $this->holidayModel->getHolidaysByType($shiftId["Id"],DATES);

					if ($holidays != null) {
						foreach ($holidays as $holiday) {
									if($holiday["day"] == $dayName){
										$isHoliday = true;
									}
						}
					}
					if ($holidates != null) {
						foreach ($holidates as $holidate) {
							$shiftHolidateDate = $this->utility->addDaysToDate($holidate["date"],0,'Y-m-d');
									if($shiftHolidateDate == $shiftDate){
										$isHoliday = true;
									}
						}
					}
					if ($isHoliday) {
						continue;
					}

					//echo json_encode($shiftOwners);
						if ($shiftOwners != null) {
						foreach ($shiftOwners as $shiftOwner) {
										$isShiftAvailable = true;
								    $shiftTimeList = $this->shiftmodel->getShiftDetailsByShiftId($shiftId["Id"]);
										$vans = $this->vanModel->getAllVanByAvailableServices($serviceIds,$shiftOwner["Id"],$requestDate);

									if ($vans != null) {
										foreach ($vans as $van) {
											$isVanAvailable = true;
											   //echo $van["Id"];
													if($addressObject != null){
														if ($van["latitude"] > 0 && $addLatitude > 0 && $van["longitude"] > 0 && $addLongitude > 0) {

															$maxDistance=$this->vanModel->getmaxdistanceById($van['Id']);
															$travelingTime = $this->utility->GetDrivingDistance($van["latitude"],$van["longitude"],$addLatitude,$addLongitude);
															$distanceBWvanAndAdd = $travelingTime['distance'];
														//	echo $distanceBWvanAndAdd;
															if ($maxDistance < $distanceBWvanAndAdd) {
																$isOutsideArea = true;
																continue;
															}
															$newAppointmentTravelingTime = (int)$travelingTime['time'];
															$totalTimeRequired = $newAppointmentTravelingTime + $serviceDuration;

														}else{
															$totalTimeRequired = $serviceDuration;
															$newAppointmentTravelingTime = ADD_MINT;
														}
													}

													if ($newAppointmentTravelingTime <= 0) {
															$newAppointmentTravelingTime = ADD_MINT;
													}
													//echo $newAppointmentTravelingTime;
												$appointments = $this->appointmentmodel->getAppointmentByDate($appointmentDate,$van["Id"]);

											foreach ($shiftTimeList as $shift) {
													$shiftStartTime = new DateTime($shift["startTime"]);
													$shiftEndTime  = new DateTime($shift["endTime"]);
													$shiftStartHour = (int)$shiftStartTime->format('H');
													$shiftStartmint = (int)$shiftStartTime->format('i');
													$shiftEndHour = (int)$shiftEndTime->format('H');
													$shiftEndMint = (int)$shiftEndTime->format('i');
													$shiftStartDateTime = $shiftStartTime->format('H').":".$shiftStartTime->format('i').":00";
													$originShiftStartDateTime = $shiftStartTime->format('H').":".$shiftStartTime->format('i').":00";
													$shiftEndDateTime = $shiftEndTime->format('H').":".$shiftEndTime->format('i').":00";
													$newRequestAppointmentTime = "";
													$newRequestFreeTime = "";

													if($appointments != null){
														foreach ($appointments as $appoint) {

															$fixAppointmentDateTime = $appoint['Time'].":00";
															$fixAppointmentDuration = (int)$this->servicemodel->getAppointmentDuration($appoint['appointmentId']);
															$lastAppointmentFinishTime = $this->getAddedTime($fixAppointmentDateTime,$fixAppointmentDuration);

															//$timeDifference = (float)$this->getTimeDifference($fixAppointmentDateTime,$shiftStartDateTime);
															//$lastAppointmentStartTime = $this->getAddedTime($shiftStartDateTime,$timeDifference);
															$lastAppointmentStartTime = $appoint['operatorTime'].":00";
														//	echo $lastAppointmentStartTime;
														//	14:40:00 15:55:00 14:40:00 15:55:00
														// 14:30:00 15:45:00 14:30:00 15:45:00
															if(strtotime($fixAppointmentDateTime) > strtotime($originShiftStartDateTime) && strtotime($fixAppointmentDateTime) < strtotime($shiftEndDateTime)){
															 $startTempH = $shiftStartTime->format('H');


															while (strtotime($shiftStartDateTime) < strtotime($lastAppointmentStartTime)) {

																	$newRequestAppointmentTime = $this->getAddedTime($shiftStartDateTime,$newAppointmentTravelingTime);
																	$newRequestFreeTime  = $this->getAddedTime($shiftStartDateTime,$totalTimeRequired);

																	$shiftStartDateTimeTemp  = new DateTime($newRequestAppointmentTime);
																	$startH = $shiftStartDateTimeTemp->format('H');

																	if ($startTempH > $startH) {
																		break;
																	}

																	if(strtotime($newRequestFreeTime) < strtotime($lastAppointmentStartTime)){
																		$aDateTime = $currentAppointmentDate." ".$newRequestAppointmentTime;
																		$isPossible = $this->appointmentmodel->isAppointmentPossibleNew($aDateTime,$serviceDuration,$van["Id"]);
																		if ($isPossible) {
																			$newAppointmentTimeList[] = array("vanId"=>$van["Id"],"travelingTime"=>$newAppointmentTravelingTime,"status"=>0,"Date"=>$currentAppointmentDate,"Time"=>$newRequestAppointmentTime,"dateTime"=>$currentAppointmentDate." ".$newRequestAppointmentTime);
																		}

																		$shiftStartDateTime = $newRequestAppointmentTime;
																	}else{
																		if(strtotime($newRequestFreeTime) > strtotime($lastAppointmentFinishTime) && strtotime($newRequestFreeTime) < strtotime($shiftEndDateTime)){
																			$newRequestAppointmentTime = $this->getAddedTime($lastAppointmentFinishTime,$newAppointmentTravelingTime);
																			$aDateTime = $currentAppointmentDate." ".$newRequestAppointmentTime;
																			$isPossible = $this->appointmentmodel->isAppointmentPossibleNew($aDateTime,$serviceDuration,$van["Id"]);
																			if ($isPossible) {
																					$newAppointmentTimeList[] = array("vanId"=>$van["Id"],"travelingTime"=>$newAppointmentTravelingTime,"status"=>0,"Date"=>$currentAppointmentDate,"Time"=>$newRequestAppointmentTime,"dateTime"=>$currentAppointmentDate." ".$newRequestAppointmentTime);
																			}

																			$shiftStartDateTime = $newRequestAppointmentTime;
																		}else{
																			$newRequestAppointmentTime = $this->getAddedTime($lastAppointmentFinishTime,$newAppointmentTravelingTime);
																			$shiftStartDateTime = $newRequestAppointmentTime;
																		}
																	}

																}

															}

														}//End Appoint ment for each loop
														//echo "OURSIDE: ".$shiftStartDateTime;
														$startTempH = $shiftStartTime->format('H');

														while (strtotime($shiftStartDateTime) < strtotime($shiftEndDateTime)) {
															# code..
															$newRequestAppointmentTime = $this->getAddedTime($shiftStartDateTime,ADD_MINT);
															$newRequestFreeTime = $this->getAddedTime($shiftStartDateTime,$serviceDuration);
															$shiftStartDateTimeTemp  = new DateTime($newRequestAppointmentTime);
															$startH = $shiftStartDateTimeTemp->format('H');

															if ($startTempH > $startH) {
																# code...
																break;
															}
														//	echo "  shiftStartDateTime: ".$shiftStartDateTime."  newRequestFreeTime: ".$newRequestFreeTime."  shiftEndDateTime: ".$shiftEndDateTime;
															if(strtotime($newRequestFreeTime) < strtotime($shiftEndDateTime)){
																$aDateTime =  $currentAppointmentDate." ".$newRequestAppointmentTime;
																$isPossible = $this->appointmentmodel->isAppointmentPossibleNew($aDateTime,$serviceDuration,$van["Id"]);
																if ($isPossible) {
																	$newAppointmentTimeList[] = array("vanId"=>$van["Id"],"travelingTime"=>$newAppointmentTravelingTime,"status"=>0,"Date"=>$currentAppointmentDate,"Time"=>$newRequestAppointmentTime,"dateTime"=>$currentAppointmentDate." ".$newRequestAppointmentTime);

																}

															}
															$shiftStartDateTime = $newRequestAppointmentTime;


														}
													}else{
														$count = 0;
														$newRequestAppointmentTime = "";
														$newRequestFreeTime = "";

														$startTempH = $shiftStartTime->format('H');

														while (strtotime($shiftStartDateTime) < strtotime($shiftEndDateTime)) {

															if($count == 0){
																$newRequestAppointmentTime = $this->getAddedTime($shiftStartDateTime,$newAppointmentTravelingTime);
															}else{
																$newRequestAppointmentTime = $this->getAddedTime($shiftStartDateTime,ADD_MINT);
															}
															$count ++;

															$shiftStartDateTimeTemp  = new DateTime($newRequestAppointmentTime);
															$startH = $shiftStartDateTimeTemp->format('H');

															if ($startTempH > $startH) {
																# code...
																break;
															}
															$newRequestFreeTime = $this->getAddedTime($newRequestAppointmentTime,$serviceDuration);
															if(strtotime($newRequestFreeTime) < strtotime($shiftEndDateTime)){
																$aDateTime =  $currentAppointmentDate." ".$newRequestAppointmentTime;
																$isPossible = $this->appointmentmodel->isAppointmentPossibleNew($aDateTime,$serviceDuration,$van["Id"]);
																if ($isPossible) {
																		$newAppointmentTimeList[] = array("vanId"=>$van["Id"],"travelingTime"=>$newAppointmentTravelingTime,"status"=>0,"Date"=>$currentAppointmentDate,"Time"=>$newRequestAppointmentTime,"dateTime"=>$currentAppointmentDate." ".$newRequestAppointmentTime);
																}

															}
															$shiftStartDateTime = $newRequestAppointmentTime;


														}

													}

												}
										}//van for
									}//van null
						}//owners for
					}// owner null
				}//$shiftIdList for

			} // $shiftIdList if
			if($newAppointmentTimeList != null){
						$mesage = 'Success';
						$totalPages = 1;
						$currentPages = 1;
						$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,array('AppointmentDate'=>$currentAppointmentDate,'Dates'=>$newAppointmentTimeList),$totalPages,$currentPages);
			}else{
					if ($isHoliday) {
							$mesage = 'We cannot server you in the selected date, kindly, select another date';
					}else if($isOutsideArea){
						$mesage = 'Sorry this service not yet available in your area, will be coming soon';
				  }else if(!$isShiftAvailable){
						$mesage = 'We cannot find any provider with working shift for the selected date, kindly, select another date';
				  }else if(!$isVanAvailable){
						$mesage = 'Sorry no van available in your area, will be coming soon';
				  }else{
						$mesage = 'Something went wrong, please try again later';
					}
					$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
			}
		}



		function getTravellingTime_get(){
			$this->load->model("Utility","utility");

			$fromLat = 20.39931;
			$fromLong = 72.892346;

			$toLat = 20.36325;
			$toLong = 72.91818;

			$result = $this->utility->GetDrivingDistance($fromLat,$fromLong,$toLat,$toLong);

			echo json_encode($result);

		}

		function getTimeDifference($from,$to){
			$to_time = strtotime($to);
			$from_time = strtotime($from);
		//	echo "to_time ".$to_time."   from_time ".$from_time;

			return round(abs($to_time - $from_time) / 60,2);
		}

		/* Method to addRequestJob
			Created By: Manzz Baria
		*/
		function addRequestJob_post(){
/*
			{
			  "userId": 5,
			  "carTypeId": 5,
			  "addressId": 5,
			  "carId": 5,
				"language": "en",
			  "Appointments": [
						{
							"categoryId": 1,
							"serviceIds": "1,3,5,4,7,2",
							"duration": 35,
							"localAmount": 65.34,
							"amount": 65.34,
							"vanId": 5,
							"appointmentDatetime": "2017-02-04 08:40:00",
							"travelingTime": 25,
							"paymentStatus": 1,
							"destinationAddress": "My address",
							"destinationLatitude": 26.1967493,
							"destinationLongitude": 50.1972873
						}
					]
			}

*/

//SELECT * FROM `commentmaster` WHERE commentDate >= '2016-07-23 06:36:26'  ORDER BY `commentmaster`.`commentDate`  ASC

//SELECT * FROM `commentmaster` WHERE commentDate <= '2016-07-23 06:36:26' ORDER BY `commentmaster`.`commentDate` DESC


				$userId = 0;
				$type="";
				$carTypeId = 0;
				$addressId = 0;
				$carId = 0;
				$categoryId = 0;
				$serviceIds = "";
				$amount = -1;
				$localAmount = -1;
				$currencyId = 0;
				$vanId = 0;
				$duration = 0;
				$appointmentDatetime = "";
				$paymentStatus = 0;
				$destinationAddress = "";
				$destinationLatitude = 0;
				$destinationLongitude = 0;
				$language = LANGUAGE_ENGLISH;

				$request = file_get_contents('php://input');
				$input = json_decode($request);
				if($input != null)
				{
						$request = null;
						$isPossible = true;
						$appointmentTravelingTime = ADD_MINT;
						$travelingTime = ADD_MINT;

						if(!property_exists($input, 'userId') || $input->userId == null){
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"userId is required");
						}
						if(!property_exists($input, 'carTypeId') || $input->carTypeId == null){
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"carTypeId is required");
						}
						if(!property_exists($input, 'addressId') || $input->addressId == null){
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"addressId is required");
						}
						if(property_exists($input, 'carId')){
							$carId = $input->carId;
						}
						if(property_exists($input, 'language')){
							$language = $input->language;
						}
						if(!property_exists($input, 'Appointments') || $input->Appointments == null ){
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"Appointments is required");
						}
						$userId = $input->userId;
						$carTypeId = $input->carTypeId;
						$addressId = $input->addressId;
						$appointments = $input->Appointments;

						$apiName="addRequestJobApi";
						$ip =  $_SERVER['REMOTE_ADDR'];
						

						$this->load->model("ApiAppoinmnetModel","appoinmnetModel");
						if ($appointments != null) {
							// Check validation
								foreach($appointments as $appointment)
								{

										if(!property_exists($appointment, 'categoryId') || $appointment->categoryId == null){
											$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"categoryId is required");
										}else{
											$categoryId = $appointment->categoryId;
										}
										if(!property_exists($appointment, 'serviceIds') || $appointment->serviceIds == null){
											$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"serviceIds is required");
										}
										if(!property_exists($appointment, 'duration') || $appointment->duration == null && $appointment->duration < 0){
											$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"duration is required");
										}
										if(!property_exists($appointment, 'amount') || $appointment->amount == null  && $appointment->amount < 0){
											$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"amount is required");
										}
										if(!property_exists($appointment, 'localAmount') || $appointment->localAmount == null  && $appointment->localAmount < 0){
											$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"localAmount is required");
										}
										if(!property_exists($appointment, 'currencyId') || $appointment->currencyId == null  && $appointment->currencyId < 0){
											$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"currencyId is required");
										}
										if(!property_exists($appointment, 'vanId') || $appointment->vanId == null){
											$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"vanId is required");
										}else{
											$vanId = $appointment->vanId;
										}
										if(!property_exists($appointment, 'appointmentDatetime') || $appointment->appointmentDatetime == null){
											$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"appointmentDatetime is required");
										}
										if(!property_exists($appointment, 'paymentStatus') || $appointment->paymentStatus == null){
											$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"paymentStatus is required");
										}
										if ($categoryId == GATEGORY_TOW) {
											if(!property_exists($appointment, 'destinationAddress') || $appointment->destinationAddress == null){
												$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"destinationAddress is required");
											}
											if(!property_exists($appointment, 'destinationLatitude') || $appointment->destinationLatitude == null){
												$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"destinationLatitude is required");
											}
											if(!property_exists($appointment, 'destinationLongitude') || $appointment->destinationLongitude == null){
												$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"destinationLongitude is required");
											}
										}
										if(!property_exists($appointment, 'appointmentDatetime') || $appointment->appointmentDatetime == null){
											$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"appointmentDatetime is required");
										}else{
											$appointmentDatetime = $appointment->appointmentDatetime;
											$isPossible = $this->appoinmnetModel->isAppointmentPossibleNew($appointmentDatetime,$duration,$vanId);
											if (!$isPossible) {
													$message = "Sorry to say that someone just booked appointment on ".$appointmentDatetime .", please select another time.";
													if ($language == LANGUAGE_ARABIC) {
														$mesage = "آسف أن أقول أن شخصا حجز مجرد تعيين على ".$appointmentDatetime ."، يرجى تحديد وقت آخر.";
													}
													$this->displayDefaultJsonWithoutDataError(JSON_ERROR_STATUS,$categoryId,$message);
											}
										}

								}

								// Save entries on tables
								$this->load->model("ApiRequestModel","requestModel");
								$this->load->model("ApiBillModel","billModel");
								$requestId = $this->requestModel->addRequestJobs($userId,$addressId,$carTypeId,$carId);

								$suffixId = 0;
								foreach($appointments as $appointment)
								{
										$categoryId = $appointment->categoryId;
										$serviceIds = $appointment->serviceIds;
										$duration = $appointment->duration;
										$amount = $appointment->amount;
										$localAmount = $appointment->localAmount;
										$currencyId = $appointment->currencyId;
										$vanId = $appointment->vanId;
										$appointmentDatetime = $appointment->appointmentDatetime;
										$paymentStatus = $appointment->paymentStatus;
										if ($categoryId == GATEGORY_TOW) {
											$destinationAddress = $appointment->destinationAddress;
											$destinationLatitude = $appointment->destinationLatitude;
											$destinationLongitude = $appointment->destinationLongitude;
										}
										$suffixId = $suffixId +1;
										$travelingTime = $appointment->travelingTime;
										$appointmentTravelingTime = $this->getMinusTimes($appointmentDatetime,$travelingTime);
										$appointmentId = $this->appoinmnetModel->addAppoinment($suffixId,$userId,$requestId,$categoryId,$vanId,$appointmentDatetime,$serviceIds,$appointmentTravelingTime,$destinationAddress,$destinationLatitude,$destinationLongitude,$duration);
										$this->billModel->saveBill($appointmentId,$localAmount,$amount,$currencyId,$paymentStatus);

								}

								$requestBody=array("carTypeId"=>$carTypeId,"addressId"=>$addressId,"appointments"=>$appointments,"categoryId"=>$categoryId,"vanId"=>$vanId,"appointmentDatetime"=>$appointmentDatetime,"serviceIds"=>$serviceIds,"duration"=>$duration,"amount"=>$amount,"localAmount"=>$localAmount,"paymentStatus"=>$paymentStatus,"suffixId"=>$suffixId,"requestId"=>$requestId,"currencyId"=>$currencyId,"travelingTime"=>$travelingTime,"appointmentTravelingTime"=>$appointmentTravelingTime);

								$this->load->model("ApiUserModel","userModel");
								$result = $this->appoinmnetModel->getAppointmentsByRequestId($requestId,$language);
								$response=$this->userModel->callapilog($userId,$type,$apiName,$ip,$requestBody,$result);


								if($result != null){
											$mesage = 'Successfully Added';
											$totalPages = 1;
											$currentPages = 1;
											$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

								}else{
									$mesage = 'Unable to fix appointment, please try again later';
									if ($language == LANGUAGE_ARABIC) {
										$mesage = 'غير قادر على تعيين موعد، يرجى المحاولة مرة أخرى في وقت لاحق ';
									}
										$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
								}

						}else{
							$mesage = 'Please provide Appointment JSON Object';
							if ($language == LANGUAGE_ARABIC) {
								$mesage = 'Please provide Appointment JSON Object';
							}
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
						}

				}else{
					$mesage = 'Please provide data';
					if ($language == LANGUAGE_ARABIC) {
						$mesage = 'Please provide data';
					}
					$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
				}

		}
		/* Method to getRequests
			Created By: Manzz Baria
		*/
		function getRequests_get(){
			$data=json_decode(file_get_contents('php://input'));
			$this->verifyRequiredParams(array('userId'),GET_TYPE);
			$userId = (int)$this->get('userId');
			$pageIndex = (int)$this->get('pageIndex');
			if (empty($pageIndex)){
				$pageIndex = 0;
			}
			$language = $this->get('language');
			if (empty($language)){
				$language = LANGUAGE_ENGLISH;
			}
		  $this->load->model("ApiAppoinmnetModel","appoinmnetModel");
			$result = $this->appoinmnetModel->getAppointmentsByUserId($userId,$pageIndex,$language);
			$totalPage = $this->appoinmnetModel->getTotalPagesOfAppointmentsByUserId($userId);
			if($result != null){
						$mesage = 'Found data';
						$totalPages = $totalPage;
						$currentPages = $pageIndex + 1;
						$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

			}else{
				$mesage = 'No appointment found';
				if ($language == LANGUAGE_ARABIC) {
					$mesage = ' لم يتم العثور على موعد';
				}
					$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
			}
		}

		/* Method to getRequestDetails
			Created By: Manzz Baria
		*/
		function getRequestDetails_get(){
			$data=json_decode(file_get_contents('php://input'));
			$this->verifyRequiredParams(array('appointmentId'),GET_TYPE);
			$appointmentId = (int)$this->get('appointmentId');
			$language = $this->get('language');
			if (empty($language)){
				$language = LANGUAGE_ENGLISH;
			}
			$this->load->model("ApiAppoinmnetModel","appoinmnetModel");
			$result = $this->appoinmnetModel->getAppointmentDetailsById($appointmentId,$language);
			if($result != null){
						$mesage = 'Found data';
						$totalPages = 1;
						$currentPages = 1;
						$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

			}else{
				$mesage = 'No appointment found';
				if ($language == LANGUAGE_ARABIC) {
					$mesage = ' لم يتم العثور على تعيين';
				}
					$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
			}
		}

		/* Method to getRequestsByStatus
			Created By: Manzz Baria
		*/
		function getRequestsByStatus_get(){
			$data=json_decode(file_get_contents('php://input'));
			$this->verifyRequiredParams(array('status'),GET_TYPE);
			$status = (int)$this->get('status');
			$pageIndex = (int)$this->get('pageIndex');
			if (empty($pageIndex)){
				$pageIndex = 0;
			}
			$language = $this->get('language');
			if (empty($language)){
				$language = LANGUAGE_ENGLISH;
			}
			$this->load->model("ApiAppoinmnetModel","appoinmnetModel");
			$result = $this->appoinmnetModel->getAppointmentsByStatus($status,$pageIndex,$language);
			$totalPage = $this->appoinmnetModel->getTotalPagesOfAppointmentsByStatus($status);
			if($result != null){
						$mesage = 'Found data';
						$totalPages = $totalPage;
						$currentPages = $pageIndex + 1;
						$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

			}else{
				$mesage = 'No appointment found';
				if ($language == LANGUAGE_ARABIC) {
					$mesage = ' لم يتم العثور على تعيين';
				}
					$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
			}
		}
		
		
		
		function searchRequest_get()
		{
			$data=json_decode(file_get_contents('php://input'));
		
			$status = (int)$this->get('status');

			$pageIndex = (int)$this->get('pageIndex');
			if (empty($pageIndex)){
				$pageIndex = 0;
			}
			$language = $this->get('language');
			$suffix = $this->get('suffix');

			if (empty($language))
			{
				$language = LANGUAGE_ENGLISH;
			}
			$this->load->model("ApiAppoinmnetModel","appoinmnetModel");
			   //
			if(empty($suffix))
			{
				//echo "if".$suffix;
				$result = $this->appoinmnetModel->getAppointmentsByStatus($status,$pageIndex,$language);
				$totalPage = $this->appoinmnetModel->getTotalPagesOfAppointmentsByStatus($status);
						$mesage = 'Found data';
						$totalPages = $totalPage;
						$currentPages = $pageIndex + 1;
						$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);
			}
			else
			{
				//echo "else".$suffix;
				$result = $this->appoinmnetModel->searchRequest($status,$pageIndex,$language,$suffix);
							if($result != null)
							{
										$mesage = 'Found data';
										$totalPages = 1;
										$currentPages = 1;
										$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

							}
							else
							{
								$mesage = 'No Request found';
								$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
							}

			}

		}
		
		
		

		/* Method to getRequestsByOperator
			Created By: Manzz Baria
		*/
		function getRequestsByOperator_get(){
			$data=json_decode(file_get_contents('php://input'));
			$this->verifyRequiredParams(array('operatorId'),GET_TYPE);
			$userId = (int)$this->get('operatorId');
			$pageIndex = (int)$this->get('pageIndex');
			if (empty($pageIndex)){
				$pageIndex = 0;
			}
			$language = $this->get('language');
			if (empty($language)){
				$language = LANGUAGE_ENGLISH;
			}
			$this->load->model("ApiAppoinmnetModel","appoinmnetModel");
			$result = $this->appoinmnetModel->getAppointmentsByOperator($userId,$pageIndex,$language);
			$totalPage = $this->appoinmnetModel->getTotalPagesOfAppointmentsByOperator($userId);
			if($result != null){
						$mesage = 'Found data';
						$totalPages = $totalPage;
						$currentPages = $pageIndex + 1;
						$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

			}else{
				$mesage = 'No appointment found';
				if ($language == LANGUAGE_ARABIC) {
					$mesage = ' لم يتم العثور على تعيين';
				}
					$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
			}
		}

		/* Method to getOperatorRequestsByTwoDates
			Created By: Manzz Baria
		*/
		function getOperatorRequestsByTwoDates_get(){
			$data=json_decode(file_get_contents('php://input'));
			$this->verifyRequiredParams(array('operatorId','fromDate','toDate'),GET_TYPE);
			$userId = (int)$this->get('operatorId');
			$fromDate = $this->get('fromDate');
			$toDate = $this->get('toDate');
			$pageIndex = (int)$this->get('pageIndex');
			if (empty($pageIndex)){
				$pageIndex = 0;
			}
			$language = $this->get('language');
			if (empty($language)){
				$language = LANGUAGE_ENGLISH;
			}
			$this->load->model("ApiAppoinmnetModel","appoinmnetModel");
			$result = $this->appoinmnetModel->getAppointmentsByOperatorBetweenTwodDates($userId,$fromDate,$toDate,$pageIndex,$language);
			$totalPage = $this->appoinmnetModel->getTotalPagesOfAppointmentsByOperatorBetweenTwodDates($userId,$fromDate,$toDate);
			if($result != null){
						$mesage = 'Found data';
						$totalPages = $totalPage;
						$currentPages = $pageIndex + 1;
						$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

			}else{
				$mesage = 'No appointment found';
				if ($language == LANGUAGE_ARABIC) {
					$mesage = ' لم يتم العثور على تعيين';
				}
					$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
			}
		}

		/* Method to getUserRequestsByTwoDates
			Created By: Manzz Baria
		*/
		function getUserRequestsByTwoDates_get(){
			$data=json_decode(file_get_contents('php://input'));
			$this->verifyRequiredParams(array('userId','fromDate','toDate'),GET_TYPE);
			$userId = (int)$this->get('userId');
			$fromDate = $this->get('fromDate');
			$toDate = $this->get('toDate');
			$pageIndex = (int)$this->get('pageIndex');
			if (empty($pageIndex)){
				$pageIndex = 0;
			}
			$language = $this->get('language');
			if (empty($language)){
				$language = LANGUAGE_ENGLISH;
			}
			$this->load->model("ApiAppoinmnetModel","appoinmnetModel");
			$result = $this->appoinmnetModel->getAppointmentsByUserIdBetweenTwoDates($userId,$fromDate,$toDate,$pageIndex,$language);
			$totalPage = $this->appoinmnetModel->getTotalPagesOfAppointmentsByUserIdBetweenTwoDates($userId,$fromDate,$toDate);
			if($result != null){
						$mesage = 'Found data';
						$totalPages = $totalPage;
						$currentPages = $pageIndex + 1;
						$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

			}else{
				$mesage = 'No appointment found';
				if ($language == LANGUAGE_ARABIC) {
					$mesage = ' لم يتم العثور على تعيين';
				}
					$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
			}
		}

		/* Method to getAdminRequestsByTwoDates
			Created By: Manzz Baria
		*/
		function getAdminRequestsByTwoDates_get(){
			$data=json_decode(file_get_contents('php://input'));
			$this->verifyRequiredParams(array('fromDate','toDate'),GET_TYPE);
			$fromDate = $this->get('fromDate');
			$toDate = $this->get('toDate');
			$pageIndex = (int)$this->get('pageIndex');
			if (empty($pageIndex)){
				$pageIndex = 0;
			}
			$language = $this->get('language');
			if (empty($language)){
				$language = LANGUAGE_ENGLISH;
			}
		 $this->load->model("ApiAppoinmnetModel","appoinmnetModel");
			$result = $this->appoinmnetModel->getAdminAppointmentsByTwoDates($fromDate,$toDate,$pageIndex,$language);
			$totalPage = $this->appoinmnetModel->getTotalPagesOfAdminAppointmentsByTwoDates($fromDate,$toDate);
			if($result != null){
						$mesage = 'Found data';
						$totalPages = $totalPage;
						$currentPages = $pageIndex + 1;
						$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

			}else{
				$mesage = 'No appointment found';
				if ($language == LANGUAGE_ARABIC) {
					$mesage = ' لم يتم العثور على تعيين';
				}
					$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
			}
		}

		/* Method to cancelRequest
			Created By: Manzz Baria
		*/
		function cancelRequest_get(){
			$data=json_decode(file_get_contents('php://input'));
			$this->verifyRequiredParams(array('appointmentId','reason','userId'),GET_TYPE);
			$appointmentId = (int)$this->get('appointmentId');
			$reason = $this->get('reason');
			$userId = $this->get('userId');
			$language = $this->get('language');
			$type="";
			$apiName="cancelRequestApi";
			$ip =  $_SERVER['REMOTE_ADDR'];

			if (empty($language)){
				$language = LANGUAGE_ENGLISH;
			}
			$this->load->model("ApiAppoinmnetModel","appoinmnetModel");
			$this->load->model("ApiUserModel","userModel");

		  $isCancelable = $this->appoinmnetModel->isCancelable($appointmentId);

		  $requestBody=array("appointmentId"=>$appointmentId,"reason"=>$reason,"language"=>$language,"isCancelable"=>$isCancelable);


		  if ($isCancelable) 
		  {
				$result = $this->appoinmnetModel->changeAppointmentStatus($appointmentId,REQUEST_STATUS_REJECTED,$reason,$userId);
				$response=$this->userModel->callapilog($userId,$type,$apiName,$ip,$requestBody,$result);
				
				if($result){
							$mesage = 'Request successfully canceled';
							if ($language == LANGUAGE_ARABIC) {
								$mesage = 'تم إلغاء الطلب بنجاح';
							}
							$totalPages = 1;
							$currentPages = 1;
							$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

				}else{
					$mesage = 'Unable to cancel request';
					if ($language == LANGUAGE_ARABIC) {
						$mesage = 'غير قادر على إلغاء الطلب';
					}
						$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
				}
			}else{
				$mesage = 'You can not cancel your request now, you can cancel your request before 2 hour of appointment time';
				if ($language == LANGUAGE_ARABIC) {
					$mesage = ' لا يمكنك تحديث أو إلغاء طلبك الآن، يمكنك تحديث وإلغاء طلبك قبل 2 ساعة من وقت الموعد';
				}

					$returnresult=array("STATUS"=>JSON_ERROR_STATUS,"mesage"=>$mesage);

					$response=$this->userModel->callapilog($userId,$type,$apiName,$ip,$requestBody,$returnresult);
					$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
			}

		}

		/* Method to changeRequestStatus_get
			Created By: Manzz Baria
		*/
		function changeRequestStatus_get(){
			$data=json_decode(file_get_contents('php://input'));
			$this->verifyRequiredParams(array('appointmentId','status','userId'),GET_TYPE);
			$appointmentId = (int)$this->get('appointmentId');
			$status = (int)$this->get('status');
			$userId = $this->get('userId');
			$reason = "";
			$reason = $this->get('reason');
			$language = $this->get('language');
			$type="";

			$apiName="changeRequestStatusApi";
			$ip =  $_SERVER['REMOTE_ADDR'];

			if (empty($language)){
				$language = LANGUAGE_ENGLISH;
			}
			if ($status == REQUEST_STATUS_REJECTED) {
					if (empty($reason)){
						$mesage = 'Please provide reason';
						if ($language == LANGUAGE_ARABIC) {
							$mesage = 'يرجى تقديم سبب';
						}
						$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
					}
			}else{
					if (empty($reason)){
						$reason ="";
					}
			}
				$this->load->model("ApiAppoinmnetModel","appoinmnetModel");
				$this->load->model("ApiUserModel","userModel");
                 $this->load->model("ApiUserModel","userModel");   

              

                        $userstatus=$this->appoinmnetModel->getOperatorStatus($userId);
                        //echo $userstatus; 	                       

                          $requestBody=array("appointmentId"=>$appointmentId,"status"=>$status,"reason"=>$reason,"language"=>$language,"userstatus"=>$userstatus);

                        if($userstatus==USER_STATUS_OFFLINE) 
                        {
                                $mesage = 'You cannot perform job while you are offline';
                              
				                if ($language == LANGUAGE_ARABIC) {
					             $mesage = 'لا يمكنك تنفيذ الطلب بحالة غير متوفر';
				                }
                                 $this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage); 
                        }
                        else 
                        { 
 
			    $result = $this->appoinmnetModel->changeAppointmentStatus($appointmentId,$status,$reason,$userId);
			    $response=$this->userModel->callapilog($userId,$type,$apiName,$ip,$requestBody,$result);
			      if($result){
						$mesage = 'Successfully';
                                                 if ($language == LANGUAGE_ARABIC) {
					               $mesage = ' بنجاح ';
				                 }
						$totalPages = 1;
						$currentPages = 1;
						$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

			                 }else{
				                  $mesage = 'This order is not active anymore';
				                if ($language == LANGUAGE_ARABIC) {
					             $mesage = ' هذا الطلب  لم يعد متاح';
				                }
					           $this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
			                 }
                      }
		}

		/* Method to changeRequestVan
			Created By: Manzz Baria
		*/
		function changeRequestVan_post(){
					$data=json_decode(file_get_contents('php://input'));
					$this->verifyRequiredParams(array('appointmentId','vanId'),POST_TYPE);

					/***** getting params *****/
					$appointmentId = $_POST['appointmentId'];
					$vanId = $_POST['vanId'];
					$language = LANGUAGE_ENGLISH;
					if (!empty($_POST['language'])){
						 $language = $_POST['language'];
					}

					$this->load->model("ApiAppoinmnetModel","appoinmnetModel");

					$result = $this->appoinmnetModel->changeAppointmentVan($appointmentId,$vanId);
					if($result){
								$mesage = 'Successfully changed';
								$totalPages = 1;
								$currentPages = 1;
								$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

					}else{
						$mesage = 'Unable to change van,please try again later';
						if ($language == LANGUAGE_ARABIC) {
							$mesage = 'غير قادر على تغيير الشاحنة، يرجى المحاولة مرة أخرى في وقت لاحق  ';
						}
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
					}

		}

		/* Method to changeRequestServiceStatus
			Created By: Manzz Baria
		*/
		function changeRequestServiceStatus_get(){
			$data=json_decode(file_get_contents('php://input'));
			$this->verifyRequiredParams(array('appointmentId','serviceId','status','userId'),GET_TYPE);
			$appointmentId = (int)$this->get('appointmentId');
			$serviceId = (int)$this->get('serviceId');
			$userId = $this->get('userId');
			$status = (int)$this->get('status');
			$reason = "";
			$reason = $this->get('reason');
			$language = $this->get('language');
			$type="";
			$apiName="changeRequestServiceStatusApi";
			$ip =  $_SERVER['REMOTE_ADDR'];



			if (empty($language)){
				$language = LANGUAGE_ENGLISH;
			}
			if ($status == REQUEST_STATUS_REJECTED) {
					if (empty($reason)){
						$mesage = 'Please provide reason';
						if ($language == LANGUAGE_ARABIC) {
							$mesage = 'يرجى تقديم سبب';
						}
						$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
					}
			}
			if (empty($reason)){
				$reason ="";
			}
			$this->load->model("ApiAppoinmnetModel","appoinmnetModel");
 			 $this->load->model("ApiUserModel","userModel");


                      $userstatus=$this->appoinmnetModel->getOperatorStatus($userId);

                      $requestBody=array("appointmentId"=>$appointmentId,"serviceId"=>$serviceId,"status"=>$status,"reason"=>$reason,"language"=>$language
					,"userstatus"=>$userstatus);


                       if($userstatus==USER_STATUS_OFFLINE) 
                        {
                                $mesage = 'You cannot perform job while you are offline';
                              
				                if ($language == LANGUAGE_ARABIC) {
					             $mesage = 'لا يمكنك تنفيذ الطلب بحالة غير متوفر';
				                }
                                 $this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage); 
                        }
                        else 
                        { 
			   $result = $this->appoinmnetModel->changeRequestServiceStatus($appointmentId,$serviceId,$status,$reason,$userId,$language);

			   $response=$this->userModel->callapilog($userId,$type,$apiName,$ip,$requestBody,$result);

			   if($result != null){
						$mesage = 'Successfully';
                                                if ($language == LANGUAGE_ARABIC) {
							$mesage = 'بنجاح';
						}
						$totalPages = 1;
						$currentPages = 1;
						$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

			}else{
				$mesage = 'This order is not active anymore';
				if ($language == LANGUAGE_ARABIC) {
					$mesage = 'هذا الطلب  لم يعد متاح ';
				}
					$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
			}
                   }
		}

		/* Method to updateRequest
			Created By: Manzz Baria
		*/
		function updateRequest_post(){
					$data=json_decode(file_get_contents('php://input'));
					$this->verifyRequiredParams(array('appointmentId'),POST_TYPE);

					/***** getting params *****/
					$appointmentId = $_POST['appointmentId'];

					$addressId = "";
					$vanId = "";
					$carId = "";
					$appoinmentDateTime = "";
					$travelingTime = ADD_MINT;
					$appointmentTravelingTime = ADD_MINT;
					$destinationAddress = "";
					$destinationLatitude = 0;
					$destinationLongitude = 0;
					$amount = 0;
					$localAmount = 0;
					$duration = 0;
					$type="";
					$apiName="updateRequestApi";
					$ip =  $_SERVER['REMOTE_ADDR'];
					$userId="";

					$language = LANGUAGE_ENGLISH;
					if (!empty($_POST['language'])){
						 $language = $_POST['language'];
					}

					if (!empty($_POST['carId'])){
						 $carId = $_POST['carId'];
					}
					
					$requestBody=array("appointmentId"=>$appointmentId,"language"=>$language);


					if (!empty($_POST['addressId'])){
						 $addressId = $_POST['addressId'];
						 if (!empty($_POST['appoinmentDateTime'])){
								$appoinmentDateTime = $_POST['appoinmentDateTime'];
								if (!empty($_POST['vanId'])){
									$vanId = $_POST['vanId'];
							  }else{
									 $this->verifyRequiredParams(array('vanId'),POST_TYPE);
							  }
								if (!empty($_POST['duration'])){
								 $duration = $_POST['duration'];
								}else{
									$this->verifyRequiredParams(array('duration'),POST_TYPE);
								}
								if (!empty($_POST['travelingTime'])){
									$travelingTime = $_POST['travelingTime'];
							  }else{
									 $this->verifyRequiredParams(array('travelingTime'),POST_TYPE);
							  }
							  $requestBody+=array("addressId"=>$addressId,"appoinmentDateTime"=>$appoinmentDateTime,"vanId"=>$vanId,"duration"=>$duration,"travelingTime"=>$travelingTime);
						 }else{
							 	$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"You must need to select appoinmentDateTime");
						 }
					}
					if (!empty($_POST['destinationAddress'])){
						 $destinationAddress = $_POST['destinationAddress'];
						 if (!empty($_POST['destinationLatitude'])){
							 $destinationLatitude = $_POST['destinationLatitude'];
						 }else{
								$this->verifyRequiredParams(array('destinationLatitude'),POST_TYPE);
						 }
						 if (!empty($_POST['destinationLongitude'])){
							 $destinationLongitude = $_POST['destinationLongitude'];
						 }else{
								$this->verifyRequiredParams(array('destinationLongitude'),POST_TYPE);
						 }
						 if (!empty($_POST['amount'])){
							  $amount = $_POST['amount'];
							  
						 }else{
								$this->verifyRequiredParams(array('amount'),POST_TYPE);
						 }
						 
						 if (!empty($_POST['localAmount'])){
							  $localAmount = $_POST['localAmount'];

						 }else{
								$this->verifyRequiredParams(array('localAmount'),POST_TYPE);
						 }
						 
						 
						 if (!empty($_POST['appoinmentDateTime'])){
								$appoinmentDateTime = $_POST['appoinmentDateTime'];
								if (!empty($_POST['vanId'])){
									$vanId = $_POST['vanId'];
							  }else{
									 $this->verifyRequiredParams(array('vanId'),POST_TYPE);
							  }
								if (!empty($_POST['duration'])){
								 $duration = $_POST['duration'];
								}else{
									$this->verifyRequiredParams(array('duration'),POST_TYPE);
								}
								if (!empty($_POST['travelingTime'])){
									$travelingTime = $_POST['travelingTime'];
							  }else{
									 $this->verifyRequiredParams(array('travelingTime'),POST_TYPE);
							  }
						 }else{
							 	$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"You must need to select appoinmentDateTime");
						 }
						  $requestBody+=array("destinationAddress"=>$destinationAddress,"destinationLatitude"=>$destinationLatitude,"destinationLongitude"=>$destinationLongitude,"amount"=>$amount,"localAmount"=>$localAmount,"appoinmentDateTime"=>$appoinmentDateTime,"vanId"=>$vanId,"duration"=>$duration,"travelingTime"=>$travelingTime);
					}
					if (!empty($_POST['appoinmentDateTime'])){
						 	$appoinmentDateTime = $_POST['appoinmentDateTime'];
						 	if (!empty($_POST['vanId'])){
							 $vanId = $_POST['vanId'];
							}else{
								$this->verifyRequiredParams(array('vanId'),POST_TYPE);
							}
							if (!empty($_POST['duration'])){
							 $duration = $_POST['duration'];
							}else{
								$this->verifyRequiredParams(array('duration'),POST_TYPE);
							}
							$requestBody+=array("appoinmentDateTime"=>$appoinmentDateTime,"vanId"=>$vanId,"duration"=>$duration);
					}

					$this->load->model("ApiAppoinmnetModel","appoinmnetModel");
					$this->load->model("ApiServiceModel","serviceModel");
					$this->load->model("ApiUserModel","userModel");
			
					$isCancelable = $this->appoinmnetModel->isCancelable($appointmentId);
					$requestBody+=array("isCancelable"=>$isCancelable);
					if ($isCancelable) {
					// $duration = $this->serviceModel->getAppointmentDuration($appointmentId);
					 	  if ($appoinmentDateTime != "") {
								$isPossible = $this->appoinmnetModel->isAppointmentPossibleNew($appoinmentDateTime,$duration,$vanId);
								$requestBody+=array("isPossible"=>$isPossible);
							  if (!$isPossible) {
									$mesage = 'Unable to update request,please try again later';
									if ($language == LANGUAGE_ARABIC) {
										$mesage = ' غير قادر على تحديث الطلب، يرجى المحاولة مرة أخرى في وقت لاحق ';
									}

										$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
								}
					 	  }
							$appointmentTravelingTime = $this->getMinusTimes($appoinmentDateTime,$travelingTime);
							
							$requestBody+=array("appointmentTravelingTime"=>$appointmentTravelingTime);
							
							$result = $this->appoinmnetModel->updateAppointment($appointmentId,$addressId,$carId,$vanId,$appoinmentDateTime,$appointmentTravelingTime,$destinationAddress,$destinationLatitude,$destinationLongitude,$localAmount,$amount,$duration,$language);
							
							$response=$this->userModel->callapilog($userId,$type,$apiName,$ip,$requestBody,$result);
							if($result != null){
										$mesage = 'Successfully updated';
										$totalPages = 1;
										$currentPages = 1;
										$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

						}else{
							$mesage = "Sorry to say that someone just booked appointment on ".$appoinmentDateTime . ", please select another time.";
							if ($language == LANGUAGE_ARABIC) {
								$mesage = "آسف أن أقول أن شخصا حجز مجرد تعيين على ".$appointmentDatetime ."، يرجى تحديد وقت آخر. ";
							}
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
						}

					}else{
						$mesage = 'You can not update or cancel your request now, you can update and cancel your request before 2 hour of appointment time';
						$response=$this->userModel->callapilog($userId,$type,$apiName,$ip,$requestBody,$result);
						if ($language == LANGUAGE_ARABIC) {
							$mesage = ' لا يمكنك تحديث أو إلغاء طلبك الآن، يمكنك تحديث وإلغاء طلبك قبل 2 ساعة من وقت الموعد';
						}
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
					}
		}




















	/***************************    OLD APIS    ***************************/

		/* Method to getTime
			Created By: Manzz Baria
		*/
		function getTimeR_get(){
			$data=json_decode(file_get_contents('php://input'));
			$this->verifyRequiredParams(array('requestId'),GET_TYPE);
			$requestId = (int)$this->get('requestId');
			$language = $this->get('language');
			if (empty($language)){
				$language = 'en';
			}
			$this->load->model("ApiRequestModel","requestModel");
			$this->load->model("ApiAppoinmnetModel","appoinmnetModel");
			$isCancelable = $this->requestModel->isCancelable($requestId);
			if ($isCancelable) {
					$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"Done");
			}else{
					$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"You can not update and cancel your request now, you can update and cancel your request before 2 hour of appointment time");
			}
	}

		/* Method to getDeviceTokensOfOperators
			Created By: Manzz Baria
		*/
		function getDeviceTokensOfOperators_get(){
			$data=json_decode(file_get_contents('php://input'));
			$this->verifyRequiredParams(array('requestId','deviceType'),GET_TYPE);
			$requestId = (int)$this->get('requestId');
			$deviceType = (int)$this->get('deviceType');
			if (empty($pageIndex)){
				$pageIndex = 0;
			}
			$language = $this->get('language');
			if (empty($language)){
				$language = 'en';
			}
			$this->load->model("ApiDeviceModel","deviceModel");
			$result = $this->deviceModel->getDeviceTokensOfOperators($requestId,$deviceType);
			if($result != null){
						$mesage = 'Found data';
						$totalPages = 1;
						$currentPages = 1;
						$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

			}else{
					$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"No data found");
			}
		}



		/* Method to getRequestAdditionalServices
			Created By: Manzz Baria
		*/
		function getRequestAdditionalServices_get(){
			$data=json_decode(file_get_contents('php://input'));
			$this->verifyRequiredParams(array('requestId','serviceId','carTypeId'),GET_TYPE);
			$requestId = (int)$this->get('requestId');
			$serviceId = (int)$this->get('serviceId');
			$carTypeId = (int)$this->get('carTypeId');
			$language = $this->get('language');
			if (empty($language)){
				$language = 'en';
			}

			$this->load->model("ApiAdditionalServiceModel","additionalServiceModel");
			$result = $this->additionalServiceModel->getAdditionalServicesOfRequest($requestId,$serviceId,$carTypeId);
			if($result != null){
						$mesage = 'Found data';
						$totalPages = 1;
						$currentPages = 1;
						$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

			}else{
					$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"No data found");
			}
		}

/**********     REPORTS      **********/

/* Method to getReports
	Created By: Manzz Baria
*/
function getReports_get(){
	$data=json_decode(file_get_contents('php://input'));
	$this->verifyRequiredParams(array('fromDate','toDate'),GET_TYPE);
	$fromDate = $this->get('fromDate');
	$toDate = $this->get('toDate');

	$language = $this->get('language');
	if (empty($language)){
		$language = 'en';
	}
	$supplierId = $this->get('supplierId');
	if (empty($supplierId)){
		$supplierId = '-1';
	}
	$serviceId = $this->get('serviceId');
	if (empty($serviceId)){
		$serviceId = '-1';
	}
	$categoryId = $this->get('categoryId');
	if (empty($categoryId)){
		$categoryId = '-1';
	}
	$status = $this->get('status');
	if (empty($status)){
		$status = '-1';
	}
	$operatorId = $this->get('operatorId');
	if (empty($operatorId)){
		$operatorId = '-1';
	}
 $this->load->model("ApiAppoinmnetModel","appoinmnetModel");
	$result = $this->appoinmnetModel->getReports($fromDate,$toDate,$supplierId,$serviceId,$categoryId,$status,$operatorId);
	if($result != null){
				$mesage = 'Found data';
				$totalPages = 1;
				$currentPages = 1;
				$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

	}else{
			$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"No data found");
	}
}

function getCustomerReport_get(){
	$data=json_decode(file_get_contents('php://input'));
	$this->verifyRequiredParams(array('fromDate','toDate'),GET_TYPE);
	$fromDate = $this->get('fromDate');
	$toDate = $this->get('toDate');

	
	$customerId = $this->get('customerId');
	if (empty($customerId)){
		$customerId = '-1';
	}
	$countrycode = $this->get('countrycode');
	if (empty($countrycode)){
		$countrycode = '-1';
	}
	
 $this->load->model("ApiAppoinmnetModel","appoinmnetModel");
	$result = $this->appoinmnetModel->getCustomerReport($fromDate,$toDate,$customerId,$countrycode);
	if($result != null){
				$mesage = 'Found data';
				$totalPages = 1;
				$currentPages = 1;
				$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

	}else{
			$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"No data found");
	}
}












/***********       New Reqest Time API               ***********/



		function getRequestDate_get(){
			$data=json_decode(file_get_contents('php://input'));
			$this->verifyRequiredParams(array('requestDate','currentTime','userId','addressId','duration','serviceIds'),GET_TYPE);
			/***** getting params *****/
			$requestDate = $this->get('requestDate');
			$currentTime = $this->get('currentTime');
			$userId = (int)$this->get('userId');
			$addressId = (int)$this->get('addressId');
			$serviceDuration = (int)$this->get('duration');
			$serviceIds = $this->get('serviceIds');
			$language = $this->get('language');
			$type="";
			$apiName="getRequestDateApi";
			$ip =  $_SERVER['REMOTE_ADDR'];
					
			if (empty($language)){
				$language = LANGUAGE_ENGLISH;
			}

			$this->load->model("ApiServiceModel","servicemodel");
			$this->load->model("Utility","utility");
			$this->load->model("ApiAppoinmnetModel","appointmentmodel");
			$this->load->model("ApiShiftModel","shiftmodel");
			$this->load->model("ApiAddressModel","addressmodel");
			$this->load->model("ApiVanModel","vanModel");
			$this->load->model("ApiHolidayModel","holidayModel");
			$this->load->model("ApiAdminModel","adminModel");
			$this->load->model("ApiUserModel","userModel");

			$requestBody=array("requestDate"=>$requestDate,"currentTime,"=>$currentTime,"addressId"=>$addressId,"duration"=>$serviceDuration,"serviceIds"=>$serviceIds);
			
			
			//$response=$this->userModel->callapilog($userId,$type,$apiName,$ip,$requestBody,$result);
     		//$maxDistance = $this->adminModel->getMaxDistance();
			$newAppointmentTimeList = null;
			$totalTimeRequired = $serviceDuration;
			$newAppointmentTravelingTime = ADD_MINT;

			$addLatitude = 0;
			$addLongitude = 0;
			$isHoliday =false;
			$isOutsideArea =false;
			$isBreakPoint = false;
			$isVanAvailable = false;
			$isShiftAvailable = false;

			$addressObject = $this->addressmodel->getAddressById($addressId);
			if($addressObject != null){
					$addLatitude = $addressObject['latitude'];
					$addLongitude = $addressObject['longitude'];

			}

			//Generate Appointment Date & get appointsments
			$appointmentDate = $this->utility->addDaysToDate($requestDate,0,'d-m-Y');
			$currentAppointmentDate = $this->utility->changeDateFromat($appointmentDate,'Y-m-d');

			$shiftDate = $this->utility->addDaysToDate($appointmentDate,0,'Y-m-d');
			$shiftIdList = $this->shiftmodel->getShiftsByDate($shiftDate);
			$dayName = $this->utility->getDayNameByDate($shiftDate);

			$requestBody+=array("addressObject"=>$addressObject,"appointmentDate"=>$appointmentDate,"currentAppointmentDate"=>$currentAppointmentDate,"shiftDate"=>$shiftDate,"shiftIdList"=>$shiftIdList,"dayName"=>$dayName);
			

			if($shiftIdList != null){
		 	foreach ($shiftIdList as $shiftId) {
					$shiftOwners = $this->vanModel->getOwnerListByShift($shiftId["Id"]);

					$holidays = $this->holidayModel->getHolidaysByType($shiftId["Id"],DAYS);
					$holidates = $this->holidayModel->getHolidaysByType($shiftId["Id"],DATES);

					$requestBody+=array("shiftOwners"=>$shiftOwners,"holidays"=>$holidays,"holidates"=>$holidates);

					if ($holidays != null) {
						foreach ($holidays as $holiday) {
									if($holiday["day"] == $dayName){
										$isHoliday = true;
									}
						}
					}
					if ($holidates != null) {
						foreach ($holidates as $holidate) {
							$shiftHolidateDate = $this->utility->addDaysToDate($holidate["date"],0,'Y-m-d');
									if($shiftHolidateDate == $shiftDate){
										$isHoliday = true;
									}
						}
					}
					if ($isHoliday) {
						continue;
					}

						if ($shiftOwners != null) {
						foreach ($shiftOwners as $shiftOwner) {
										$isShiftAvailable = true;
								    $shiftTimeList = $this->shiftmodel->getShiftDetailsByShiftId($shiftId["Id"]);
										$vans = $this->vanModel->getAllVanByAvailableServices($serviceIds,$shiftOwner["Id"],$requestDate,$addLatitude,$addLongitude);
											$requestBody+=array("shiftTimeList"=>$shiftTimeList,"vans"=>$vans);
									if ($vans != null) {
										foreach ($vans as $van) {
											$isVanAvailable = true;
													if($addressObject != null){
														if ($van["latitude"] != 0 && $addLatitude != 0 && $van["longitude"] != 0 && $addLongitude != 0) {

															//echo $addLatitude;
															//$travelingTime = $this->utility->GetDrivingDistance($van["latitude"],$van["longitude"],$addLatitude,$addLongitude);
															$maxDistance=$this->vanModel->getmaxdistanceById($van['Id']);
															
															$distanceBWvanAndAdd = $van['distance'];
															if ($maxDistance < $distanceBWvanAndAdd) {
																$isOutsideArea = true;
																continue;
															}
															$newAppointmentTravelingTime = (int)$van['travelTime'];
															//echo $distanceBWvanAndAdd;
															$totalTimeRequired = $newAppointmentTravelingTime + $serviceDuration;

														}else{
															$totalTimeRequired = $serviceDuration;
															$newAppointmentTravelingTime = ADD_MINT;
														}
													}
													if ($newAppointmentTravelingTime <= 0) {
															$newAppointmentTravelingTime = ADD_MINT;
													}


											foreach ($shiftTimeList as $shift) {

													$shiftStartTime = new DateTime($shift["startTime"]);
													$shiftEndTime  = new DateTime($shift["endTime"]);
													$shiftStartDateTime = $shiftStartTime->format('H').":".$shiftStartTime->format('i').":00";
													$shiftEndDateTime = $shiftEndTime->format('H').":".$shiftEndTime->format('i').":00";
													$newRequestFreeTime = "";
													$count = 0;
													$newRequestAppointmentTime = "";
													$newRequestFreeTime = "";

													if ($currentTime == "00:00:00") {
																# code...
																$currentTime = $shiftStartDateTime;
															}

														while (strtotime($shiftDate." ".$shiftStartDateTime) < strtotime($shiftDate." ".$shiftEndDateTime)) {

															if($count == 0){
																$newRequestAppointmentTime = $this->getAddedTime($shiftStartDateTime,$newAppointmentTravelingTime);
																$currentTime  = new DateTime($currentTime);
																$currentTime = $currentTime->format('H').":".$currentTime->format('i').":00";
																$currentTime = $this->getAddedTime($currentTime,$newAppointmentTravelingTime);

															}else{
																$newRequestAppointmentTime = $this->getAddedTime($shiftStartDateTime,ADD_MINT);
															}
															$count ++;
															$shiftStartDateTimeTemp  = new DateTime($newRequestAppointmentTime);
															//echo "Current: ".$currentTime . "  newRequestAppointmentTime: ".$newRequestAppointmentTime;

															if(strtotime($shiftDate." ".$currentTime) < strtotime($shiftDate." ".$newRequestAppointmentTime)){

																$newRequestFreeTime = $this->getAddedTime($newRequestAppointmentTime,$serviceDuration);
																if(strtotime($shiftDate." ".$newRequestFreeTime) < strtotime($shiftDate." ".$shiftEndDateTime)){
																	$aDateTime =  $currentAppointmentDate." ".$newRequestAppointmentTime;
																	$isPossible = $this->appointmentmodel->isAppointmentPossibleNew($aDateTime,$serviceDuration,$van["Id"]);
																	if ($isPossible) {
																		// $dt  = $this->getDateTimeFormet($currentAppointmentDate." ".$newRequestAppointmentTime);
																		$maxdistance=$this->vanModel->getmaxdistanceById($van['Id']);
																		  $dt  = $currentAppointmentDate." ".$newRequestAppointmentTime;
																			$date = new DateTime($dt);
														 				 	$dt = $date->format('Y-m-d H:i:s');
																			$sizeArray  = sizeof($newAppointmentTimeList);
																			if ($sizeArray > 0) {
																				$isAlready = false;
																				for ($i = 0 ; $i < $sizeArray ;$i++) {
																						if ($newAppointmentTimeList[$i]['dateTime'] == $dt) {
																							$isAlready = true;
																						}
																					}
																					if (!$isAlready) {
																						$newAppointmentTimeList[] = array("vanId"=>$van["Id"],"travelingTime"=>$newAppointmentTravelingTime,"status"=>0,"Date"=>$currentAppointmentDate,"maxDistance"=>$maxdistance,"Time"=>$newRequestAppointmentTime,"dateTime"=>$dt);
																					}

																			}else{
																				$newAppointmentTimeList[] = array("vanId"=>$van["Id"],"travelingTime"=>$newAppointmentTravelingTime,"status"=>0,"Date"=>$currentAppointmentDate,"maxDistance"=>$maxdistance,"Time"=>$newRequestAppointmentTime,"dateTime"=>$dt);
																			}

																	}
																}

															}
															$shiftStartDateTime = $newRequestAppointmentTime;

															if ($count == 400) {
																 break;
															}
														}
												}
										}//van for
									}//van null
						}//owners for
					}// owner null
				}//$shiftIdList for

			} // $shiftIdList if
			if($newAppointmentTimeList != null){
				 //usort($newAppointmentTimeList, 'date_compare');
				 usort($newAppointmentTimeList, array($this, "date_compare"));
					$count = 0;
					foreach ($newAppointmentTimeList as $value) {
   								// unset($newAppointmentTimeList[$count]);
									 $count = $count + 1;
						}
					$mesage = 'Success';
					$totalPages = 1;
					$currentPages = 1;
					$requestBody+=array("currentAppointmentDate"=>$currentAppointmentDate,"newAppointmentTimeList"=>$newAppointmentTimeList);
					$result=null;
					$response=$this->userModel->callapilog($userId,$type,$apiName,$ip,$requestBody,$result);
					$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,array('AppointmentDate'=>$currentAppointmentDate,'Dates'=>$newAppointmentTimeList),$totalPages,$currentPages);
			}else{
					if ($isHoliday) {
							$mesage = 'We cannot server you in the selected date, kindly, select another date';
							if ($language == LANGUAGE_ARABIC) {
								$mesage = 'لا يمكن أن نخدمك في التاريخ المحدد، يرجى، تحديد موعد آخر';
							}
					}else if($isOutsideArea){
						$mesage = 'Sorry this service not yet available in your area, will be coming soon';
						if ($language == LANGUAGE_ARABIC) {
							$mesage = 'عذرا هذه الخدمة غير متوفرة في منطقتك، سوف تتوفر قريبا';
						}
				  }else if(!$isShiftAvailable){
						$mesage = 'We cannot find any provider with working shift for the selected date, kindly, select another date';
						if ($language == LANGUAGE_ARABIC) {
							$mesage = ' لا يمكن العثور على أي مزود للخدمة نشط للتاريخ المحدد، يرجى، تحديد موعد آخر';
						}
				  }else if(!$isVanAvailable){
						$mesage = 'Sorry no van available in your area, will be coming soon';
						if ($language == LANGUAGE_ARABIC) {
							$mesage = ' عذرا لا شاحنة متاحة الآن في منطقتك ';
						}
				  }else{
						$mesage = 'Something went wrong, please try again later';
						if ($language == LANGUAGE_ARABIC) {
							$mesage = 'هناك شئ خاطئ، يرجى المحاولة فى وقت لاحق';
						}
					}
					//$requestBody+=array("currentAppointmentDate"=>$currentAppointmentDate,"newAppointmentTimeList"=>$newAppointmentTimeList);
					$response=$this->userModel->callapilog($userId,$type,$apiName,$ip,$requestBody,$mesage);
					$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
			}
		}

		function date_compare($a, $b)
		{
				//echo $a['dateTime'];
		    $t1 = strtotime($a['dateTime']);
		    $t2 = strtotime($b['dateTime']);
		    return $t1 - $t2;
		}


		/* Method to getTravellTimeAndDuration
			Created By: Manzz Baria
		*/
		function getTravellTimeAndDuration_get(){
			$data=json_decode(file_get_contents('php://input'));
			$originLatitude = $this->get('originLatitude');
			if (empty($originLatitude)){
				$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"originLatitude is required");
			}
			$originLongitude = $this->get('originLongitude');
			if (empty($originLongitude)){
				$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"originLongitude is required");
			}
			$destinationLatitude = $this->get('destinationLatitude');
			if (empty($destinationLatitude)){
				$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"destinationLatitude is required");
			}
			$dastinationLongitude = $this->get('dastinationLongitude');
			if (empty($dastinationLongitude)){
				$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"dastinationLongitude is required");
			}
			$this->load->model("Utility","utility");
			$result = $this->utility->GetDrivingDistance($originLatitude,$originLongitude,$destinationLatitude,$dastinationLongitude);
			if($result != null){
						$mesage = 'Data found';
						$totalPages = 1;
						$currentPages = 1;
						$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

			}else{
					$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"No location distance found");
			}
		}


                function getRequestsBySupplier_get(){
			$data=json_decode(file_get_contents('php://input'));
			$this->verifyRequiredParams(array('status','supplierId'),GET_TYPE);
			$status = (int)$this->get('status');
			$supplierId = (int)$this->get('supplierId');
			$pageIndex = (int)$this->get('pageIndex');
			if (empty($pageIndex)){
				$pageIndex = 0;
			}
			$language = $this->get('language');
			if (empty($language)){
				$language = LANGUAGE_ENGLISH;
			}
			$this->load->model("ApiAppoinmnetModel","appoinmnetModel");
			$result = $this->appoinmnetModel->getRequestsBySupplier($status,$supplierId,$pageIndex,$language);
			$totalPage = $this->appoinmnetModel->getTotalPagesOfAppointmentsByStatus($status);
			if($result != null){
						$mesage = 'Found data';
						$totalPages = $totalPage;
						$currentPages = $pageIndex + 1;
						$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

			}else{
				$mesage = 'No appointment found';
				if ($language == LANGUAGE_ARABIC) {
					$mesage = ' لم يتم العثور على تعيين';
				}
					$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
			}
		}
		








}
?>
