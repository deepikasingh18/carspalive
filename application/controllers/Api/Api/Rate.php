<?php

defined('BASEPATH') OR exit('No direct script access allowed');
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class Rate extends REST_Controller
{
	public function __construct()
	{
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		$method = $_SERVER['REQUEST_METHOD'];
		if($method == "OPTIONS") {
				die();
		}

		  parent::__construct();
      $this->load->library('session');
      $this->load->helper(array('form','url'));
      $this->load->library('form_validation');
			$this->load->library('mylibrary');
			 $this->load->database();
	}
	    /* Method to display default error message
	    		 Created By: Manzz Baria
	    	*/
	    	private function displayDefaultJsonWithoutData($status,$message){
	    		if($status == JSON_SUCCESS_STATUS){
	    			$this->response([
	    				'Status' => $status,
	    				'Message' => $message
	    			], REST_Controller::HTTP_OK);
	    		}else{
	    			$this->response([
	    				'Status' => $status,
	    				'Message' => $message
	    			], REST_Controller::HTTP_OK);
	    		}
	    	}

	    	/* Method to display default success with Result data and totals
	    		 Created By: Manzz Baria
	    	*/
	    	private function displayDefaultJsonWithData($status,$mesage,$result,$totalPages,$currentPages){
	    		if($status == JSON_SUCCESS_STATUS){
	    			$this->response([
	    				'Status' => $status,
	    				'Message' => $mesage,
	    				'TotalPage' => $totalPages,
	    				'CurrentPage' => $currentPages,
	    				'Data' => $result
	    			], REST_Controller::HTTP_OK);
	    		}else{
	    			$this->response([
	    				'Status' => $status,
	    				'Message' => $mesage,
	    				'TotalPage' => $totalPages,
	    				'CurrentPage' => $currentPages,
	    				'Data' => $result
	    			], REST_Controller::HTTP_OK);
	    		}
	    	}

	    /* Method to check parameters is null or empty
	        Created By: Manzz Baria
	    */
	    public function verifyRequiredParams($required_fields,$type){
	        $error = false;
	        $error_fields = "";
	        foreach ($required_fields as $field) {
	    			if($type == POST_TYPE){
	    				if ($this->post($field) == null) {
	    	      	$error = true;
	    	        $error_fields .= $field . ', ';
	    	      }
	    	    }else if($type == GET_TYPE){
	    				if ($this->get($field) == null) {
	    	      	$error = true;
	    	        $error_fields .= $field . ', ';
	    	      }
	    	    }else if($type == PUT_TYPE){
	    				if ($this->put($field) == null) {
	    	      	$error = true;
	    	        $error_fields .= $field . ', ';
	    	      }
	    	    }else{
	    				if ($this->delete($field) == null) {
	    	      	$error = true;
	    	        $error_fields .= $field . ', ';
	    	      }
	    			}
	        }
	        if ($error) {
	        	$message = 'Required field(s) ' . substr($error_fields, 0, -2) . ' is missing or empty';
	          $this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$message);
	        }
	      }

				/* Method to show Message
					Created By: Manzz Baria
				*/
				public function showMessage($status,$message){
					$this->response([
						'Status' => $status,
						'Message' => $message
					], REST_Controller::HTTP_OK);
				}

				/* Method to getRateQuestions
					Created By: Manzz Baria
				*/
				function getRateQuestions_get(){
					$data=json_decode(file_get_contents('php://input'));
					$this->load->model("ApiRateModel","rateModel");
					$language = $this->get('language');
					if (empty($language)){
						$language = LANGUAGE_ENGLISH;
					}
					$result = $this->rateModel->getRateQuestions($language);
					if($result != null){
								$mesage = 'Data found';
								if ($language == LANGUAGE_ARABIC) {
									$mesage = 'تم العثور على البيانات';
								}
								$totalPages = 1;
								$currentPages = 1;
								$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

					}else{
						$mesage = 'No rate found';
						if ($language == LANGUAGE_ARABIC) {
							$mesage = 'لم نتمكن من العثور على أي تصنيف ';
						}
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
					}
				}

				/* Method to addRateQuestion
					Created By: Manzz Baria
				*/
				function addRateQuestion_post(){
					$data=json_decode(file_get_contents('php://input'));
					$this->verifyRequiredParams(array('question'),POST_TYPE);
					$language = LANGUAGE_ENGLISH;
					if (!empty($_POST['language'])){
						 $language = $_POST['language'];
					}
					/***** getting params *****/
					$question = $_POST['question'];
					$this->load->model("ApiRateModel","rateModel");
					$result = $this->rateModel->addRateQuestion($question);
					if($result){
								$mesage = 'Successfully';
								$totalPages = 1;
								$currentPages = 1;
								$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

					}else{
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"Unable to add rate question");
					}
				}

				/* Method to addRateOnRequest
					Created By: Manzz Baria
				*/
				function addRateOnRequest_post(){
					$data=json_decode(file_get_contents('php://input'));
					$this->verifyRequiredParams(array('userId','appointmentId','rateDetail'),POST_TYPE);

					/***** getting params *****/
					$userId = $_POST['userId'];
					$appointmentId = $_POST['appointmentId'];
					$rateDetail = $_POST['rateDetail'];
					$comment = "";
					$language = LANGUAGE_ENGLISH;
					if (!empty($_POST['language'])){
						 $language = $_POST['language'];
					}
					if (!empty($_POST['comment'])){
						 $comment = $_POST['comment'];
						 $length = strlen($comment);
		 					if ($length > 1000) {
								$mesage = 'You can not add more then 1000 characters';
								if ($language == LANGUAGE_ARABIC) {
									$mesage = 'لا يمكنك إضافة أكثر من 1000 تشاراكتورس';
								}
		 							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
		 					}
					}

					$this->load->model("ApiRateModel","rateModel");
					$isAdded = $this->rateModel->isRateAlreadyAddedOnRequestByUser($userId,$appointmentId);
					if (!$isAdded) {
							$result = $this->rateModel->addRateOnRequest($userId,$appointmentId,$rateDetail,$comment);
							if($result != null){
										$mesage = 'Thank you for your rate';
										if ($language == LANGUAGE_ARABIC) {
											$mesage = 'شكرا لتقييمك';
										}
										$totalPages = 1;
										$currentPages = 1;
										$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

							}else{
								$mesage = 'Unable to add rate';
								if ($language == LANGUAGE_ARABIC) {
									$mesage = "يتعذر إضافة معدل";
								}
								$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
							}
					}else{
						$mesage = 'Rate already added for this appointment';
						if ($language == LANGUAGE_ARABIC) {
							$mesage = 'تم إضافة التصنيف بالفعل لهذا التعيين';
						}
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
					}

				}

				/* Method to getRateOfRequest
					Created By: Manzz Baria
				*/

				function getRateOfRequest_get(){
					$data=json_decode(file_get_contents('php://input'));
					$this->verifyRequiredParams(array('appointmentId'),GET_TYPE);
					$appointmentId = (int)$this->get('appointmentId');
					$language = $this->get('language');
					if (empty($language)){
						$language = LANGUAGE_ENGLISH;
					}
					$this->load->model("ApiRateModel","rateModel");
					$result = $this->rateModel->getRateOfRequestId($appointmentId);
					if($result != null){
								$mesage = 'Found data';
								$totalPages = 1;
								$currentPages = 1;
								$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

					}else{
						$mesage = 'No rate found';
						if ($language == LANGUAGE_ARABIC) {
							$mesage = 'لم نتمكن من العثور على أي تقييم';
						}
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
					}
				}



}
?>
