<?php

defined('BASEPATH') OR exit('No direct script access allowed');
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class InformationMaster extends REST_Controller
{
	public function __construct()
	{
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		$method = $_SERVER['REQUEST_METHOD'];
		if($method == "OPTIONS") {
				die();
		}
		  parent::__construct();
      $this->load->library('session');
      $this->load->helper(array('form','url'));
      $this->load->library('form_validation');
			$this->load->library('mylibrary');
			 $this->load->database();
	}
	    /* Method to display default error message
	    		 Created By: Manzz Baria
	    	*/
	    	private function displayDefaultJsonWithoutData($status,$message){
	    		if($status == JSON_SUCCESS_STATUS){
	    			$this->response([
	    				'Status' => $status,
	    				'Message' => $message
	    			], REST_Controller::HTTP_OK);
	    		}else{
	    			$this->response([
	    				'Status' => $status,
	    				'Message' => $message
	    			], REST_Controller::HTTP_OK);
	    		}
	    	}

	    	/* Method to display default success with Result data and totals
	    		 Created By: Manzz Baria
	    	*/
	    	private function displayDefaultJsonWithData($status,$mesage,$result,$totalPages,$currentPages){
	    		if($status == JSON_SUCCESS_STATUS){
	    			$this->response([
	    				'Status' => $status,
	    				'Message' => $mesage,
	    				'TotalPage' => $totalPages,
	    				'CurrentPage' => $currentPages,
	    				'Data' => $result
	    			], REST_Controller::HTTP_OK);
	    		}else{
	    			$this->response([
	    				'Status' => $status,
	    				'Message' => $mesage,
	    				'TotalPage' => $totalPages,
	    				'CurrentPage' => $currentPages,
	    				'Data' => $result
	    			], REST_Controller::HTTP_OK);
	    		}
	    	}

	    /* Method to check parameters is null or empty
	        Created By: Manzz Baria
	    */
	    public function verifyRequiredParams($required_fields,$type){
	        $error = false;
	        $error_fields = "";
	        foreach ($required_fields as $field) {
	    			if($type == POST_TYPE){
	    				if ($this->post($field) == null) {
	    	      	$error = true;
	    	        $error_fields .= $field . ', ';
	    	      }
	    	    }else if($type == GET_TYPE){
	    				if ($this->get($field) == null) {
	    	      	$error = true;
	    	        $error_fields .= $field . ', ';
	    	      }
	    	    }else if($type == PUT_TYPE){
	    				if ($this->put($field) == null) {
	    	      	$error = true;
	    	        $error_fields .= $field . ', ';
	    	      }
	    	    }else{
	    				if ($this->delete($field) == null) {
	    	      	$error = true;
	    	        $error_fields .= $field . ', ';
	    	      }
	    			}
	        }
	        if ($error) {
	        	$message = 'Required field(s) ' . substr($error_fields, 0, -2) . ' is missing or empty';
	          $this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$message);
	        }
	      }

				/* Method to show Message
					Created By: Manzz Baria
				*/
				public function showMessage($status,$message){
					$this->response([
						'Status' => $status,
						'Message' => $message
					], REST_Controller::HTTP_OK);
				}

				
				/* Method to add car type
					Created By: Manzz Baria
				*/
				function updateaboutus_post(){
					$data=json_decode(file_get_contents('php://input'));
					$this->verifyRequiredParams(array('aboutusId','flag','aboutustitle','aboutusdetail'),POST_TYPE);

					/***** getting params *****/

					$aboutusId = $_POST['aboutusId'];
					$flag = $_POST['flag'];
					$aboutusdetail = $_POST['aboutusdetail'];
					$aboutustitle = $_POST['aboutustitle'];
					
					$language = LANGUAGE_ENGLISH;
					if (!empty($_POST['language'])){
						 $language = $_POST['language'];
					}
					
					$this->load->model("ApiInformationModel","InformationModel");
					$result = $this->InformationModel->updateAboutus($aboutusId,$aboutusdetail,$aboutustitle,$flag,$language);
					if($result != null){
								$mesage = 'Detail successfully updated';
								if ($language == LANGUAGE_ARABIC) {
									$mesage = 'قد تم تحديث نوع السيارة بنجاح ';
								}
								$totalPages = 1;
								$currentPages = 1;
								$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

					}else{
						$mesage = 'Unable to update Detail';
						if ($language == LANGUAGE_ARABIC) {
							$mesage = 'غير قادر على تحديث نوع السيارة';
						}
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
					}
				}


				function updatepolicy_post(){
					$data=json_decode(file_get_contents('php://input'));
					$this->verifyRequiredParams(array('policyId','flag','policytitle','policydetail'),POST_TYPE);

					/***** getting params *****/

					$policyId = $_POST['policyId'];
					$flag = $_POST['flag'];
					$policydetail = $_POST['policydetail'];
					$policytitle = $_POST['policytitle'];
					
					$language = LANGUAGE_ENGLISH;
					if (!empty($_POST['language'])){
						 $language = $_POST['language'];
					}
					
					$this->load->model("ApiInformationModel","InformationModel");
					$result = $this->InformationModel->updatepolicy($policyId,$policydetail,$policytitle,$flag,$language);
					if($result != null){
								$mesage = 'Policy successfully updated';
								if ($language == LANGUAGE_ARABIC) {
									$mesage = 'قد تم تحديث نوع السيارة بنجاح ';
								}
								$totalPages = 1;
								$currentPages = 1;
								$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

					}else{
						$mesage = 'Unable to update Policy';
						if ($language == LANGUAGE_ARABIC) {
							$mesage = 'غير قادر على تحديث نوع السيارة';
						}
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
					}
				}
				

				/* Method to add car type
					Created By: Manzz Baria
				*/
				function getAboutUs_get(){
					$data=json_decode(file_get_contents('php://input'));
					$this->load->model("ApiInformationModel","InformationModel");
					$language = $this->get('language');
					if (empty($language)){
						$language = LANGUAGE_ENGLISH;
					}
					$result = $this->InformationModel->getAboutUs($language);
					if($result != null){
								$mesage = 'Found data';
								$totalPages = 1;
								$currentPages = 1;
								$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

					}else{
						$mesage = 'No cartype found';
						if ($language == LANGUAGE_ARABIC) {
							$mesage = 'لم نتمكن من العثور على أي نوع السيارة ';
						}
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
					}
				}
				
				function getPolicy_get(){
					$data=json_decode(file_get_contents('php://input'));
					$this->load->model("ApiInformationModel","InformationModel");
					$language = $this->get('language');
					if (empty($language)){
						$language = LANGUAGE_ENGLISH;
					}
					$result = $this->InformationModel->getPolicy($language);
					if($result != null){
								$mesage = 'Found data';
								$totalPages = 1;
								$currentPages = 1;
								$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

					}else{
						$mesage = 'No cartype found';
						if ($language == LANGUAGE_ARABIC) {
							$mesage = 'لم نتمكن من العثور على أي نوع السيارة ';
						}
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
					}
				}
}
?>
