<?php

defined('BASEPATH') OR exit('No direct script access allowed');
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class AdditionalService extends REST_Controller
{
	public function __construct()
	{
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		$method = $_SERVER['REQUEST_METHOD'];
		if($method == "OPTIONS") {
				die();
		}
		  parent::__construct();
      $this->load->library('session');
      $this->load->helper(array('form','url'));
      $this->load->library('form_validation');
			$this->load->library('mylibrary');
			 $this->load->database();
	}
	    /* Method to display default error message
	    		 Created By: Manzz Baria
	    	*/
	    	private function displayDefaultJsonWithoutData($status,$message){
	    		if($status == JSON_SUCCESS_STATUS){
	    			$this->response([
	    				'Status' => $status,
	    				'Message' => $message
	    			], REST_Controller::HTTP_OK);
	    		}else{
	    			$this->response([
	    				'Status' => $status,
	    				'Message' => $message
	    			], REST_Controller::HTTP_OK);
	    		}
	    	}

	    	/* Method to display default success with Result data and totals
	    		 Created By: Manzz Baria
	    	*/
	    	private function displayDefaultJsonWithData($status,$mesage,$result,$totalPages,$currentPages){
	    		if($status == JSON_SUCCESS_STATUS){
	    			$this->response([
	    				'Status' => $status,
	    				'Message' => $mesage,
	    				'TotalPage' => $totalPages,
	    				'CurrentPage' => $currentPages,
	    				'Data' => $result
	    			], REST_Controller::HTTP_OK);
	    		}else{
	    			$this->response([
	    				'Status' => $status,
	    				'Message' => $mesage,
	    				'TotalPage' => $totalPages,
	    				'CurrentPage' => $currentPages,
	    				'Data' => $result
	    			], REST_Controller::HTTP_OK);
	    		}
	    	}

	    /* Method to check parameters is null or empty
	        Created By: Manzz Baria
	    */
	    public function verifyRequiredParams($required_fields,$type){
	        $error = false;
	        $error_fields = "";
	        foreach ($required_fields as $field) {
	    			if($type == POST_TYPE){
	    				if ($this->post($field) == null) {
	    	      	$error = true;
	    	        $error_fields .= $field . ', ';
	    	      }
	    	    }else if($type == GET_TYPE){
	    				if ($this->get($field) == null) {
	    	      	$error = true;
	    	        $error_fields .= $field . ', ';
	    	      }
	    	    }else if($type == PUT_TYPE){
	    				if ($this->put($field) == null) {
	    	      	$error = true;
	    	        $error_fields .= $field . ', ';
	    	      }
	    	    }else{
	    				if ($this->delete($field) == null) {
	    	      	$error = true;
	    	        $error_fields .= $field . ', ';
	    	      }
	    			}
	        }
	        if ($error) {
	        	$message = 'Required field(s) ' . substr($error_fields, 0, -2) . ' is missing or empty';
	          $this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$message);
	        }
	      }

				/* Method to show Message
					Created By: Manzz Baria
				*/
				public function showMessage($status,$message){
					$this->response([
						'Status' => $status,
						'Message' => $message
					], REST_Controller::HTTP_OK);
				}

				/* Method to addAdditionalService
					Created By: Manzz Baria
				*/
				function addAdditionalService_post(){
					$data=json_decode(file_get_contents('php://input'));
					$this->verifyRequiredParams(array('name','additionalDetail'),POST_TYPE);

					/***** getting params *****/
					$name = $_POST['name'];
					$additionalDetail = $_POST['additionalDetail'];

					$this->load->model("ApiAdditionalServiceModel","additionalServiceModel");
					$result = $this->additionalServiceModel->addAdditionalService($name,$additionalDetail);
					if($result != null){
								$mesage = 'Successfully Added';
								$totalPages = 1;
								$currentPages = 1;
								$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

					}else{
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"Additional service already added with same name");
					}
				}

				/* Method to updateAdditionalService
					Created By: Manzz Baria
				*/
				function updateAdditionalService_post(){
					$data=json_decode(file_get_contents('php://input'));
					if (!empty($_POST['additionalServiceId'])){
								$additionalServiceId = $_POST['additionalServiceId'];
					 }else{
								$this->showMessage("0","additionalServiceId is required");
								return;
					}
					$name = "";
					$additionalDetail = "";
					if (!empty($_POST['name'])){
						$name = $_POST['name'];
				 }

				 if (!empty($_POST['additionalDetail'])){
						$additionalDetail = $_POST['additionalDetail'];
				 }
					$this->load->model("ApiAdditionalServiceModel","additionalServiceModel");
					$result = $this->additionalServiceModel->updateAdditionalService($additionalServiceId,$name,$additionalDetail);
					if($result){
								$mesage = 'Successfully updated';
								$totalPages = 1;
								$currentPages = 1;
								$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

					}else{
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"Unable to update additional service");
					}
				}

				/* Method to deleteAdditionalService
					Created By: Manzz Baria
				*/
				function deleteAdditionalService_post(){
					$data=json_decode(file_get_contents('php://input'));
					$this->verifyRequiredParams(array('additionalServiceId'),POST_TYPE);

					/***** getting params *****/
					$additionalServiceId = $_POST['additionalServiceId'];

					$this->load->model("ApiAdditionalServiceModel","additionalServiceModel");
					$result = $this->additionalServiceModel->deleteAdditionalServiceById($additionalServiceId);
					if($result){
								$mesage = 'Successfully deleted';
								$totalPages = 1;
								$currentPages = 1;
								$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

					}else{
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"Unable to delete additional service");
					}
				}

				/* Method to getAdditionalServices
					Created By: Manzz Baria
				*/
				function getAdditionalServices_get(){
					$data=json_decode(file_get_contents('php://input'));
					$this->load->model("ApiAdditionalServiceModel","additionalServiceModel");
					$result = $this->additionalServiceModel->getAdditionalServices();
					if($result != null){
								$mesage = 'Found data';
								$totalPages = 1;
								$currentPages = 1;
								$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

					}else{
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"No additional service found");
					}
				}

				/* Method to updateAdditionaServiceDetail
					 Created By: Manzz Baria
				*/
				function updateAdditionaServiceDetail_post(){
					$data=json_decode(file_get_contents('php://input'));
					$this->verifyRequiredParams(array('Id'),POST_TYPE);

					/***** getting params *****/
					$Id = $_POST['Id'];
					$amount= "";
					$duration ="";
					if (!empty($_POST['amount'])){
						 $amount = $_POST['amount'];
					}
					if (!empty($_POST['duration'])){
						 $duration = $_POST['duration'];
					}

					$this->load->model("ApiAdditionalServiceModel","additionalServiceModel");
					$result = $this->additionalServiceModel->updateAdditionaServiceDetail($Id,$amount,$duration);
					if($result){
								$mesage = 'Successfully added';
								$totalPages = 1;
								$currentPages = 1;
								$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

					}else{
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"Unable to update");
					}
				}

				/* Method to removeAdditionalServiceDetail
					 Created By: Manzz Baria
				*/
				function removeAdditionalServiceDetail_post(){
					$data=json_decode(file_get_contents('php://input'));
					$this->verifyRequiredParams(array('Id'),POST_TYPE);

					/***** getting params *****/
					$additionalServiceDetailId = $_POST['Id'];

					$this->load->model("ApiAdditionalServiceModel","additionalServiceModel");
					$result = $this->additionalServiceModel->removeAdditionalServiceDetail($additionalServiceDetailId);
					if($result){
								$mesage = 'Successfully deleted';
								$totalPages = 1;
								$currentPages = 1;
								$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

					}else{
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"Unable to delete additional service detail");
					}
				}

				/* Method to addAdditionalServiceDetailsEntry
					 Created By: Manzz Baria
				*/
				function addAdditionalServiceDetailsEntry_post(){
					$data=json_decode(file_get_contents('php://input'));
					$this->verifyRequiredParams(array('additionalServiceId','carTypeId','serviceId','amount','duration'),POST_TYPE);

					/***** getting params *****/
					$additionalServiceId = $_POST['additionalServiceId'];
					$carTypeId = $_POST['carTypeId'];
					$serviceId = $_POST['serviceId'];
					$amount = $_POST['amount'];
					$duration = $_POST['duration'];

						$this->load->model("ApiAdditionalServiceModel","additionalServiceModel");
				//	$result = $this->additionalServiceModel->addAdditionalServiceDetail($additionalServiceId,$carTypeId,$serviceId,$amount,$duration);
					$result = $this->additionalServiceModel->addAdditionalServiceDetailsEntry($additionalServiceId,$carTypeId,$serviceId,$amount,$duration);
					if($result != null){
								$mesage = 'Successfully added';
								$totalPages = 1;
								$currentPages = 1;
								$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

					}else{
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"Already added");
					}
				}

				/* Method to getAdditionalServicesByServiceIds_get
					Created By: Manzz Baria
				*/
				function getAdditionalServicesByServiceIds_get(){
					$data=json_decode(file_get_contents('php://input'));
					$this->verifyRequiredParams(array('carTypeId','serviceIds'),GET_TYPE);
					$carTypeId = $this->get('carTypeId');
					$serviceIds = $this->get('serviceIds');
					$this->load->model("ApiAdditionalServiceModel","additionalServiceModel");
					$result = $this->additionalServiceModel->getAdditionalServicesByServiceIds($carTypeId,$serviceIds);
					if($result != null){
								$mesage = 'Found data';
								$totalPages = 1;
								$currentPages = 1;
								$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

					}else{
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"No additional service found");
					}
				}



}
?>
