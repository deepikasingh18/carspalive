<?php

defined('BASEPATH') OR exit('No direct script access allowed');
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class Category extends REST_Controller
{
	public function __construct()
	{
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		$method = $_SERVER['REQUEST_METHOD'];
		if($method == "OPTIONS") {
				die();
		}
		  parent::__construct();
      $this->load->library('session');
      $this->load->helper(array('form','url'));
      $this->load->library('form_validation');
			$this->load->library('mylibrary');
			 $this->load->database();
	}
	    /* Method to display default error message
	    		 Created By: Manzz Baria
	    	*/
	    	private function displayDefaultJsonWithoutData($status,$message){
	    		if($status == JSON_SUCCESS_STATUS){
	    			$this->response([
	    				'Status' => $status,
	    				'Message' => $message
	    			], REST_Controller::HTTP_OK);
	    		}else{
	    			$this->response([
	    				'Status' => $status,
	    				'Message' => $message
	    			], REST_Controller::HTTP_OK);
	    		}
	    	}

	    	/* Method to display default success with Result data and totals
	    		 Created By: Manzz Baria
	    	*/
	    	private function displayDefaultJsonWithData($status,$mesage,$result,$totalPages,$currentPages){
	    		if($status == JSON_SUCCESS_STATUS){
	    			$this->response([
	    				'Status' => $status,
	    				'Message' => $mesage,
	    				'TotalPage' => $totalPages,
	    				'CurrentPage' => $currentPages,
	    				'Data' => $result
	    			], REST_Controller::HTTP_OK);
	    		}else{
	    			$this->response([
	    				'Status' => $status,
	    				'Message' => $mesage,
	    				'TotalPage' => $totalPages,
	    				'CurrentPage' => $currentPages,
	    				'Data' => $result
	    			], REST_Controller::HTTP_OK);
	    		}
	    	}

	    /* Method to check parameters is null or empty
	        Created By: Manzz Baria
	    */
	    public function verifyRequiredParams($required_fields,$type){
	        $error = false;
	        $error_fields = "";
	        foreach ($required_fields as $field) {
	    			if($type == POST_TYPE){
	    				if ($this->post($field) == null) {
	    	      	$error = true;
	    	        $error_fields .= $field . ', ';
	    	      }
	    	    }else if($type == GET_TYPE){
	    				if ($this->get($field) == null) {
	    	      	$error = true;
	    	        $error_fields .= $field . ', ';
	    	      }
	    	    }else if($type == PUT_TYPE){
	    				if ($this->put($field) == null) {
	    	      	$error = true;
	    	        $error_fields .= $field . ', ';
	    	      }
	    	    }else{
	    				if ($this->delete($field) == null) {
	    	      	$error = true;
	    	        $error_fields .= $field . ', ';
	    	      }
	    			}
	        }
	        if ($error) {
	        	$message = 'Required field(s) ' . substr($error_fields, 0, -2) . ' is missing or empty';
	          $this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$message);
	        }
	      }

				/* Method to show Message
					Created By: Manzz Baria
				*/
				public function showMessage($status,$message){
					$this->response([
						'Status' => $status,
						'Message' => $message
					], REST_Controller::HTTP_OK);
				}

		/*
				Method to addCategory
				Created By: Manzz Baria
		*/
		function addCategory_post(){
					$data=json_decode(file_get_contents('php://input'));
					$this->verifyRequiredParams(array('name'),POST_TYPE);

					/***** getting params *****/
					$name = $_POST['name'];
					$language = LANGUAGE_ENGLISH;
					if (!empty($_POST['language'])){
						 $language = $_POST['language'];
					}
					$nameAR = '';
					if (!empty($_POST['nameAR'])){
						 $nameAR = $_POST['nameAR'];
					}
					$fileUrl = "";
					if(isset($_FILES['file']['name'])){
						 $file=$_FILES['file']['name'];

						 $image_path='./uploads/files/';
						 $thumb_path='./uploads/files/thumb/';
						 $data = $this->uploadFileContent();
						 $filename=$data['upload_data']['file_name'];
						 $videofile=$_FILES["file"]["tmp_name"];

						 // Images create thumbnail and upload
						 $imagefile = $filename;
						 $imagefile = $this->generateImageThumbContent($imagefile,$data);
						 $fileUrl = $filename;
					}else{
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"icon is required");
					}
					
					if(isset($_FILES['imageUrl']['name'])){
						 $file=$_FILES['imageUrl']['name'];

						 $image_path='./uploads/files/';
						 $thumb_path='./uploads/files/thumb/';
						 $data = $this->uploadFileContentImage();
						 $filename=$data['upload_data']['file_name'];
						 $videofile=$_FILES["imageUrl"]["tmp_name"];

						 // Images create thumbnail and upload
						 $imageUrlfile = $filename;
						 $imageUrlfile = $this->generateImageThumbContent($imageUrlfile,$data);
						 $imageUrlName = $filename;
					}else{
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"imageUrl is required");
					}

					$this->load->model("ApiCategoryModel","categoryModel");
					$result = $this->categoryModel->addCategory($name,$nameAR,$fileUrl,$imageUrlName,$language);
					if($result != null){
								$mesage = 'Category successfully added';
								if ($language == LANGUAGE_ARABIC) {
									$mesage = 'تم إضافة الفئة بنجاح';
								}
								$totalPages = 1;
								$currentPages = 1;
								$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

					}else{
						$mesage = 'Unable to add category';
						if ($language == LANGUAGE_ARABIC) {
							$mesage = 'غير قادر على إضافة الفئة';
						}
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
					}
			}

				/* Method to Upload content
					Created By: Nishit Patel
				*/
				public function uploadFileContent(){
					$time = time();
					$new_name = 'carspa_'.$time;
					$config['upload_path'] = './uploads/files/';
					$config['allowed_types']        = '*';
					$config['max_size']             = 0;
					$config['max_width']            = 9000;
					$config['max_height']           = 8000;
					$config['file_name'] = $new_name;

					$this->load->library('upload', $config);
					$this->upload->initialize($config);
					if(!$this->upload->do_upload('file')){
						$error = array('error' => $this->upload->display_errors());
						$msg=$error['error'];
						$status=FALSE;
						$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$msg);
					}else{
						$data = array('upload_data' => $this->upload->data());
						$filename=$data['upload_data']['file_name'];
					}
					$data = $this->upload->data();
					$data=array('upload_data' => $this->upload->data());
					return $data;
				}
				
					/* Method to Upload content
					Created By: Nishit Patel
				*/
				public function uploadFileContentImage(){
					$time = time();
					$new_name = 'carspa_'.$time;
					$config['upload_path'] = './uploads/files/';
					$config['allowed_types']        = '*';
					$config['max_size']             = 0;
					$config['max_width']            = 9000;
					$config['max_height']           = 8000;
					$config['file_name'] = $new_name;

					$this->load->library('upload', $config);
					$this->upload->initialize($config);
					if(!$this->upload->do_upload('imageUrl')){
						$error = array('error' => $this->upload->display_errors());
						$msg=$error['error'];
						$status=FALSE;
						$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$msg);
					}else{
						$data = array('upload_data' => $this->upload->data());
						$filename=$data['upload_data']['file_name'];
					}
					$data = $this->upload->data();
					$data=array('upload_data' => $this->upload->data());
					return $data;
				}
				public function generateImageThumbContent($imagefile,$data){
					$config['image_library'] = 'gd2';
					$config['source_image'] =  $data['upload_data']['full_path'];
					$config['new_image'] =  $data['upload_data']['full_path'].'/thumb/'.$imagefile;
					$config['maintain_ratio'] = FALSE;
					$config['overwrite'] = TRUE;
					$config['allowed_types'] = '*';
					$config['max_size'] = 0;
					$config['width'] = 120;
					$config['height'] = 100;
					$this->load->library('image_lib', $config);
					if($this->image_lib->resize()){}
					return $imagefile;
				}

			/* Method to updateCategory
					Created By: Manzz Baria
			*/
			function updateCategory_post(){
					$data=json_decode(file_get_contents('php://input'));
					$this->verifyRequiredParams(array('name','categoryId'),POST_TYPE);

					/***** getting params *****/
					$name = $_POST['name'];
					$categoryId = $_POST['categoryId'];
					$language = LANGUAGE_ENGLISH;
					if (!empty($_POST['language'])){
						 $language = $_POST['language'];
					}
					$nameAR = '';
					if (!empty($_POST['nameAR'])){
						 $nameAR = $_POST['nameAR'];
					}
					$fileUrl = "";
					$imageUrlName = "";
					if(isset($_FILES['file']['name'])){
						 $file=$_FILES['file']['name'];

						 $image_path='./uploads/files/';
						 $thumb_path='./uploads/files/thumb/';
						 $data = $this->uploadFileContent();
						 $filename=$data['upload_data']['file_name'];
						 $videofile=$_FILES["file"]["tmp_name"];

						 // Images create thumbnail and upload
						 $imagefile = $filename;
						 $imagefile = $this->generateImageThumbContent($imagefile,$data);
						 $fileUrl = $filename;
					}
					if(isset($_FILES['imageUrl']['name'])){
						 $file=$_FILES['imageUrl']['name'];

						 $image_path='./uploads/files/';
						 $thumb_path='./uploads/files/thumb/';
						 $data = $this->uploadFileContentImage();
						 $filename=$data['upload_data']['file_name'];
						 $videofile=$_FILES["imageUrl"]["tmp_name"];

						 // Images create thumbnail and upload
						 $imageUrlfile = $filename;
						 $imageUrlfile = $this->generateImageThumbContent($imageUrlfile,$data);
						 $imageUrlName = $filename;
					}

					$this->load->model("ApiCategoryModel","categoryModel");
					$result = $this->categoryModel->updateCategory($name,$nameAR,$categoryId,$fileUrl,$imageUrlName,$language);
					if($result != null){
								$mesage = 'Category successfully updated';
								if ($language == LANGUAGE_ARABIC) {
									$mesage = 'تم تحديث الفئة بنجاح ';
								}
								$totalPages = 1;
								$currentPages = 1;
								$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

					}else{
						$mesage = 'Unable to update category';
						if ($language == LANGUAGE_ARABIC) {
							$mesage = 'غير قادر على تحديث الفئة';
						}
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
					}
			}

			/* Method to deleteCategory
					Created By: Manzz Baria
			*/
			function deleteCategory_post(){
					$data=json_decode(file_get_contents('php://input'));
					$this->verifyRequiredParams(array('categoryId'),POST_TYPE);

					/***** getting params *****/
					$categoryId = $_POST['categoryId'];
					$language = LANGUAGE_ENGLISH;
					if (!empty($_POST['language'])){
						 $language = $_POST['language'];
					}
					$this->load->model("ApiCategoryModel","categoryModel");
					$result = $this->categoryModel->deleteCategoryId($categoryId);
					if($result){
								$mesage = 'Category successfully deleted';
								if ($language == LANGUAGE_ARABIC) {
									$mesage = 'تم حذف الفئة بنجاح  ';
								}
								$totalPages = 1;
								$currentPages = 1;
								$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

					}else{
						$mesage = 'Unable to delete category';
						if ($language == LANGUAGE_ARABIC) {
							$mesage = 'غير قادر على حذف الفئة';
						}
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
					}
			}

			/*
					Method to getCategories
					Created By: Manzz Baria
			*/

			function getCategories_get(){
					$data=json_decode(file_get_contents('php://input'));
					$this->load->model("ApiCategoryModel","categoryModel");
					$language = $this->get('language');
					if (empty($language)){
						$language = LANGUAGE_ENGLISH;
					}
					$result = $this->categoryModel->getCategories($language);
					if($result != null){
								$mesage = 'Found data';
								$totalPages = 1;
								$currentPages = 1;
								$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

					}else{
						$mesage = 'No category found';
						if ($language == LANGUAGE_ARABIC) {
							$mesage = 'لم يتم العثور على الفئة';
						}
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
					}
			}
		
		function searchcategory_get()
		{

			$data=json_decode(file_get_contents('php://input'));
			//$this->verifyRequiredParams(array('name'),GET_TYPE);
		
			$name = $this->get('name');
		   $language = LANGUAGE_ENGLISH;
		   $this->load->model("ApiCategoryModel","categoryModel");
		   
		   if($name=="")
		   {
		   
		   		$result = $this->categoryModel->getCategories($language);
		   		$mesage = 'Found data';
						$totalPages = 1;
						$currentPages = 1;
						$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);
		   }
		   else
		   {
		 	 
			$result = $this->categoryModel->searchGetCategoryServices($name);
					if($result != null)
					{
								$mesage = 'Found data';
								$totalPages = 1;
								$currentPages = 1;
								$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

					}else{
						$mesage = 'No Category found';
						$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
					}
		 	}
		
	}



}
?>
