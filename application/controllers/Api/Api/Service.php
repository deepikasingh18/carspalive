<?php

defined('BASEPATH') OR exit('No direct script access allowed');
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class Service extends REST_Controller
{
	public function __construct()
	{
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		$method = $_SERVER['REQUEST_METHOD'];
		if($method == "OPTIONS") {
				die();
		}
		  parent::__construct();
      $this->load->library('session');
      $this->load->helper(array('form','url'));
      $this->load->library('form_validation');
			$this->load->library('mylibrary');
			 $this->load->database();
	}
	    /* Method to display default error message
	    		 Created By: Manzz Baria
	    	*/
	    	private function displayDefaultJsonWithoutData($status,$message){
	    		if($status == JSON_SUCCESS_STATUS){
	    			$this->response([
	    				'Status' => $status,
	    				'Message' => $message
	    			], REST_Controller::HTTP_OK);
	    		}else{
	    			$this->response([
	    				'Status' => $status,
	    				'Message' => $message
	    			], REST_Controller::HTTP_OK);
	    		}
	    	}

	    	/* Method to display default success with Result data and totals
	    		 Created By: Manzz Baria
	    	*/
	    	private function displayDefaultJsonWithData($status,$mesage,$result,$totalPages,$currentPages){
	    		if($status == JSON_SUCCESS_STATUS){
	    			$this->response([
	    				'Status' => $status,
	    				'Message' => $mesage,
	    				'TotalPage' => $totalPages,
	    				'CurrentPage' => $currentPages,
	    				'Data' => $result
	    			], REST_Controller::HTTP_OK);
	    		}else{
	    			$this->response([
	    				'Status' => $status,
	    				'Message' => $mesage,
	    				'TotalPage' => $totalPages,
	    				'CurrentPage' => $currentPages,
	    				'Data' => $result
	    			], REST_Controller::HTTP_OK);
	    		}
	    	}

	    /* Method to check parameters is null or empty
	        Created By: Manzz Baria
	    */
	    public function verifyRequiredParams($required_fields,$type){
	        $error = false;
	        $error_fields = "";
	        foreach ($required_fields as $field) {
	    			if($type == POST_TYPE){
	    				if ($this->post($field) == null) {
	    	      	$error = true;
	    	        $error_fields .= $field . ', ';
	    	      }
	    	    }else if($type == GET_TYPE){
	    				if ($this->get($field) == null) {
	    	      	$error = true;
	    	        $error_fields .= $field . ', ';
	    	      }
	    	    }else if($type == PUT_TYPE){
	    				if ($this->put($field) == null) {
	    	      	$error = true;
	    	        $error_fields .= $field . ', ';
	    	      }
	    	    }else{
	    				if ($this->delete($field) == null) {
	    	      	$error = true;
	    	        $error_fields .= $field . ', ';
	    	      }
	    			}
	        }
	        if ($error) {
	        	$message = 'Required field(s) ' . substr($error_fields, 0, -2) . ' is missing or empty';
	          $this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$message);
	        }
	      }

				/* Method to show Message
					Created By: Manzz Baria
				*/
				public function showMessage($status,$message){
					$this->response([
						'Status' => $status,
						'Message' => $message
					], REST_Controller::HTTP_OK);
				}

				/* Method to Upload content
					Created By: Nishit Patel
				*/
				public function uploadFileContent(){
					$time = time();
					$new_name = 'carspa_'.$time;
					$config['upload_path'] = './uploads/files/';
					$config['allowed_types']        = '*';
					$config['max_size']             = 0;
					$config['max_width']            = 9000;
					$config['max_height']           = 8000;
					$config['file_name'] = $new_name;

					$this->load->library('upload', $config);
					$this->upload->initialize($config);
					if(!$this->upload->do_upload('file')){
						$error = array('error' => $this->upload->display_errors());
						$msg=$error['error'];
						$status=FALSE;
						$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$msg);
					}else{
						$data = array('upload_data' => $this->upload->data());
						$filename=$data['upload_data']['file_name'];
					}
					$data = $this->upload->data();
					$data=array('upload_data' => $this->upload->data());
					return $data;
				}
				public function generateImageThumbContent($imagefile,$data){
					$config['image_library'] = 'gd2';
					$config['source_image'] =  $data['upload_data']['full_path'];
					$config['new_image'] =  $data['upload_data']['full_path'].'/thumb/'.$imagefile;
					$config['maintain_ratio'] = FALSE;
					$config['overwrite'] = TRUE;
					$config['allowed_types'] = '*';
					$config['max_size'] = 0;
					$config['width'] = 120;
					$config['height'] = 100;
					$this->load->library('image_lib', $config);
					if($this->image_lib->resize()){}
					return $imagefile;
				}



	/***************************    NEW APIS    ***************************/

				/* Method to addService
					Created By: Manzz Baria
				*/
				function addService_post(){
					$data=json_decode(file_get_contents('php://input'));
					$this->verifyRequiredParams(array('name','description','categoryId','details'),POST_TYPE);

					/***** getting params *****/
					$name = $_POST['name'];
					$description = $_POST['description'];
					$categoryId = $_POST['categoryId'];
					$details = $_POST['details'];
					$language = LANGUAGE_ENGLISH;
					if (!empty($_POST['language'])){
						 $language = $_POST['language'];
					}
					$nameAR = '';
					if (!empty($_POST['nameAR'])){
						 $nameAR = $_POST['nameAR'];
					}
					$descriptionAR = '';
					if (!empty($_POST['descriptionAR'])){
						 $descriptionAR = $_POST['descriptionAR'];
					}


					$fileUrl = "";
					if(isset($_FILES['file']['name'])){
						 $file=$_FILES['file']['name'];

						 $image_path='./uploads/files/';
						 $thumb_path='./uploads/files/thumb/';
						 $data = $this->uploadFileContent();
						 $filename=$data['upload_data']['file_name'];
						 $videofile=$_FILES["file"]["tmp_name"];

						 // Images create thumbnail and upload
						 $imagefile = $filename;
						 $imagefile = $this->generateImageThumbContent($imagefile,$data);
						 $fileUrl = $filename;
					}else{
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"icon is required");
					}

					$this->load->model("ApiServiceModel","serviceModel");
					$result = $this->serviceModel->addService($name,$nameAR,$description,$descriptionAR,$categoryId,$fileUrl,$details,$language);
					if($result != null){
								$mesage = 'Successfully Added';
								if ($language == LANGUAGE_ARABIC) {
									$mesage = 'تم إضافة خدمة بنجاح ';
								}
								$totalPages = 1;
								$currentPages = 1;
								$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

					}else{
						$mesage = 'Service already added with same name';
						if ($language == LANGUAGE_ARABIC) {
							$mesage = 'وسبق وأن أضفت الخدمة مع نفس الاسم ';
						}
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
					}
				}

				/* Method to updateServiceMaster
						Created By: Manzz Baria
				*/
				function updateServiceMaster_post(){
						$data=json_decode(file_get_contents('php://input'));
						$this->verifyRequiredParams(array('name','serviceId','categoryId'),POST_TYPE);

						/***** getting params *****/
						$name = $_POST['name'];
						$serviceId  = $_POST['serviceId'];
						$categoryId  = $_POST['categoryId'];
						$language = LANGUAGE_ENGLISH;
						if (!empty($_POST['language'])){
							 $language = $_POST['language'];
						}
						$description = "";
						if (!empty($_POST['description'])){
							 $description = $_POST['description'];
						}
						$descriptionAR = "";
						if (!empty($_POST['descriptionAR'])){
							 $descriptionAR = $_POST['descriptionAR'];
						}
						$nameAR = '';
						if (!empty($_POST['nameAR'])){
							 $nameAR = $_POST['nameAR'];
						}


						$fileUrl = "";
						if(isset($_FILES['file']['name'])){
							 $file=$_FILES['file']['name'];

							 $image_path='./uploads/files/';
							 $thumb_path='./uploads/files/thumb/';
							 $data = $this->uploadFileContent();
							 $filename=$data['upload_data']['file_name'];
							 $videofile=$_FILES["file"]["tmp_name"];

							 // Images create thumbnail and upload
							 $imagefile = $filename;
							 $imagefile = $this->generateImageThumbContent($imagefile,$data);
							 $fileUrl = $filename;
						}

					  $this->load->model("ApiServiceModel","serviceModel");
				$result = $this->serviceModel->updateServiceMaster($serviceId,$name,$nameAR,$description,$descriptionAR,$categoryId,$fileUrl,$language);
						if($result != null){
									$mesage = 'Service Successfully updated';
									if ($language == LANGUAGE_ARABIC) {
										$mesage = 'تم تحديث الخدمة بنجاح ';
									}
									$totalPages = 1;
									$currentPages = 1;
									$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

						}else{
							$mesage = 'Unable to update service';
							if ($language == LANGUAGE_ARABIC) {
								$mesage = ' غير قادر على تحديث الخدمة ';
							}
								$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
						}
				}


			/* Method to updateServiceDetail
				Created By: Manzz Baria
			*/
			function updateServiceDetail_post(){
						$data=json_decode(file_get_contents('php://input'));

						$this->verifyRequiredParams(array('serviceDetailId','carTypeId','amount','duration'),POST_TYPE);

						/***** getting params *****/
						$serviceDetailId  = $_POST['serviceDetailId'];
						$carTypeId  = $_POST['carTypeId'];
						$amount  = $_POST['amount'];
						$duration = $_POST['duration'];
						$variableAmount = 0;
						if (!empty($_POST['variableAmount'])){
							 $variableAmount = $_POST['variableAmount'];
						}
						$language = LANGUAGE_ENGLISH;
						if (!empty($_POST['language'])){
							 $language = $_POST['language'];
						}

						$this->load->model("ApiServiceModel","serviceModel");
						$result = $this->serviceModel->updateServiceDetail($serviceDetailId,$carTypeId,$amount,$duration,$variableAmount,$language);
						if($result){
									$mesage = 'Successfully updated';
									if ($language == LANGUAGE_ARABIC) {
										$mesage = 'تم تحديث الخدمة بنجاح ';
									}
									$totalPages = 1;
									$currentPages = 1;
									$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

						}else{
							$mesage = 'Unable to update service';
							if ($language == LANGUAGE_ARABIC) {
								$mesage = ' غير قادر على تحديث الخدمة ';
							}
								$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
						}
			}


			/*
				Method to deleteService
				Created By: Manzz Baria
			*/
			function deleteService_post(){
				$data=json_decode(file_get_contents('php://input'));
				$this->verifyRequiredParams(array('serviceId'),POST_TYPE);

				/***** getting params *****/
				$serviceId = $_POST['serviceId'];
				$language = LANGUAGE_ENGLISH;
				if (!empty($_POST['language'])){
					 $language = $_POST['language'];
				}

				$this->load->model("ApiServiceModel","serviceModel");
				$result = $this->serviceModel->deleteServiceById($serviceId);
				if($result){
							$mesage = 'Service successfully deleted';
							if ($language == LANGUAGE_ARABIC) {
								$mesage = 'حذف الخدمة بنجاح ';
							}
							$totalPages = 1;
							$currentPages = 1;
							$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

				}else{
					$mesage = 'Unable to delete service';
					if ($language == LANGUAGE_ARABIC) {
						$mesage = 'غير قادر على حذف الخدمة';
					}
						$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
				}
			}

			/*
				Method to getAllServices
				 Created By: Manzz Baria
			*/
			function getAllServices_get(){
				$data=json_decode(file_get_contents('php://input'));
				$this->load->model("ApiServiceModel","serviceModel");
				$language = $this->get('language');
				if (empty($language)){
					$language = LANGUAGE_ENGLISH;
				}
				$result = $this->serviceModel->getAllServices($language);
			//	$this->load->model("ApiCategoryModel","categoryModel");
			//	$result = $this->categoryModel->getCategoryServices();
				if($result != null){
							$mesage = 'Found data';
							$totalPages = 1;
							$currentPages = 1;
							$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

				}else{
				$mesage = 'No service found';
				if ($language == LANGUAGE_ARABIC) {
					$mesage = ' لم يتم العثور على الخدمة ';
				}
						$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
				}
			}

			/*
				Method to getAllServicesByCategory
				 Created By: Manzz Baria
			*/
			function getAllServicesByCategory_get(){
				$data=json_decode(file_get_contents('php://input'));
			//	$this->load->model("ApiServiceModel","serviceModel");
			//	$result = $this->serviceModel->getAllServices();
			$language = $this->get('language');
			if (empty($language)){
				$language = LANGUAGE_ENGLISH;
			}
				$this->load->model("ApiCategoryModel","categoryModel");
				$result = $this->categoryModel->getCategoryServices($language);
				if($result != null){
							$mesage = 'Found data';
							$totalPages = 1;
							$currentPages = 1;
							$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

				}else{
					$mesage = 'No service found';
					if ($language == LANGUAGE_ARABIC) {
						$mesage = ' لم يتم العثور على الخدمة ';
					}
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
				}
			}


			/* Method to getServicesByCarTypeAndCategory
				Created By: Manzz Baria
			*/
			function getServicesByCarTypeAndCategory_get(){
				$data=json_decode(file_get_contents('php://input'));
				$this->verifyRequiredParams(array('categoryId','carTypeId'),GET_TYPE);
				$categoryId = $this->get('categoryId');
				$carTypeId = $this->get('carTypeId');
				$language = $this->get('language');
				
				
				
				if (empty($language)){
					$language = LANGUAGE_ENGLISH;
				}
				
				$this->load->model("ApiServiceModel","serviceModel");
				$result = $this->serviceModel->getServicesByCarTypeAndCategory($categoryId,$carTypeId,$language);
				if($result != null){
							$mesage = 'Found data';
							$totalPages = 1;
							$currentPages = 1;
							$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

				}else{
					$mesage = 'No service found';
					if ($language == LANGUAGE_ARABIC) {
						$mesage = ' لم يتم العثور على الخدمة ';
					}
					$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
				}
			}
			
			function getServicesByCarTypeAndCategories_get(){
				$data=json_decode(file_get_contents('php://input'));
				$this->verifyRequiredParams(array('categoryId','carTypeId','latitude','longitude'),GET_TYPE);
				$categoryId = $this->get('categoryId');
				$carTypeId = $this->get('carTypeId');
				$language = $this->get('language');
				$latitude = $this->get('latitude');
				$longitude = $this->get('longitude');
				$type="";
				$userId="";
				if (empty($language)){
					$language = LANGUAGE_ENGLISH;
				}
						$apiName="getServicesByCarTypeAndCategoriesApi";
						$ip =  $_SERVER['REMOTE_ADDR'];
						$requestBody=array("categoryId"=>$categoryId,"carTypeId"=>$carTypeId,"language"=>$language,"latitude"=>$latitude,"longitude"=>$longitude);

				$this->load->model("ApiServiceModel","serviceModel");
				$this->load->model("ApiUserModel","userModel");

				$result = $this->serviceModel->getServicesByCarTypeAndCategories($categoryId,$carTypeId,$language,$latitude,$longitude);

				

				if($result != null){
							$mesage = 'Found data';
							$totalPages = 1;
							$currentPages = 1;
							$returnresult=array("Status"=>JSON_SUCCESS_STATUS,"mesage"=>$mesage,"result"=>$result,"totalPages"=>$totalPages,"currentPages"=>$currentPages);
							$response=$this->userModel->callapilog($userId,$type,$apiName,$ip,$requestBody,$returnresult);
							$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

				}else{
					$mesage = 'We are Sorry for not being able to serve you now; there is no service provider nearby your area.';
					if ($language == LANGUAGE_ARABIC) {
						$mesage = 'نأسف لعدم تمكننا من خدمتك الان, لا يوجد مزود خدمة متاح بمنطقتك ';
					}
					$returnresult=array("Status"=>JSON_ERROR_STATUS,"mesage"=>$mesage,"result"=>$result);
					$response=$this->userModel->callapilog($userId,$type,$apiName,$ip,$requestBody,$returnresult);
					$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
				}
			}

			/* Method to removeServiceForCarTypeById
				 Created By: Manzz Baria
			*/
			function removeServiceForCarTypeById_post(){
				$data=json_decode(file_get_contents('php://input'));
				$this->verifyRequiredParams(array('Id'),POST_TYPE);

				/***** getting params *****/
				$serviceDetailsId = $_POST['Id'];
				$language = LANGUAGE_ENGLISH;
				if (!empty($_POST['language'])){
					 $language = $_POST['language'];
				}

				$this->load->model("ApiServiceModel","serviceModel");
				$result = $this->serviceModel->removeServiceForCarTypeById($serviceDetailsId,$language);
				if($result){
							$mesage = 'Service successfully deleted';
							if ($language == LANGUAGE_ARABIC) {
								$mesage = 'حذف الخدمة بنجاح ';
							}
							$totalPages = 1;
							$currentPages = 1;
							$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

				}else{
					$mesage = 'Unable to delete service for cartype';
					if ($language == LANGUAGE_ARABIC) {
						$mesage = 'غير قادر على حذف خدمة ';
					}
						$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
				}
			}

			/* Method to addCarTypeToService
				 Created By: Manzz Baria
			*/
			function addCarTypeToService_post(){
				$data=json_decode(file_get_contents('php://input'));
				$this->verifyRequiredParams(array('serviceId','carTypeId','amount','duration','variableAmount'),POST_TYPE);

				/***** getting params *****/
				$serviceId = $_POST['serviceId'];
				$carTypeId = $_POST['carTypeId'];
				$amount = $_POST['amount'];
				$duration = $_POST['duration'];
				$variableAmount = $_POST['variableAmount'];
				$language = LANGUAGE_ENGLISH;
				if (!empty($_POST['language'])){
					 $language = $_POST['language'];
				}

				$this->load->model("ApiServiceModel","serviceModel");
				$result = $this->serviceModel->saveServiceDetail($serviceId,$carTypeId,$amount,$duration,$variableAmount,$language);
				if($result != null){
							$mesage = 'Service successfully added';
							if ($language == LANGUAGE_ARABIC) {
								$mesage = 'تم إضافة الخدمة بنجاح';
							}
							$totalPages = 1;
							$currentPages = 1;
							$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

				}else{
				$mesage = 'Service already added with same name';
				if ($language == LANGUAGE_ARABIC) {
					$mesage = 'وسبق وأن أضفت الخدمة مع نفس الاسم ';
				}
						$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
				}
			}
	
			function searchService_get()
			{

				$data=json_decode(file_get_contents('php://input'));
				//$this->verifyRequiredParams(array('name'),GET_TYPE);
			
				$name = $this->get('name');
			   $language = LANGUAGE_ENGLISH;
			  $this->load->model("ApiCategoryModel","categoryModel");
			   
			   if($name=="")
			   {
			   
			   		$result = $this->categoryModel->getCategoryServices($language);
			   		$mesage = 'Found data';
							$totalPages = 1;
							$currentPages = 1;
							$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);
			   }
			   else
			   {
			 	 
					$result = $this->categoryModel->searchService($name);
						if($result != null)
						{
									$mesage = 'Found data';
									$totalPages = 1;
									$currentPages = 1;
									$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

						}else{
							$mesage = 'No Service found';
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
						}
			 	}
			 }


}
?>
