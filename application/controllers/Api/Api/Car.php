<?php

defined('BASEPATH') OR exit('No direct script access allowed');
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class Car extends REST_Controller
{
	public function __construct()
	{
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		$method = $_SERVER['REQUEST_METHOD'];
		if($method == "OPTIONS") {
				die();
		}
		  parent::__construct();
      $this->load->library('session');
      $this->load->helper(array('form','url'));
      $this->load->library('form_validation');
			$this->load->library('mylibrary');
			 $this->load->database();
	}
	    /* Method to display default error message
	    		 Created By: Manzz Baria
	    	*/
	    	private function displayDefaultJsonWithoutData($status,$message){
	    		if($status == JSON_SUCCESS_STATUS){
	    			$this->response([
	    				'Status' => $status,
	    				'Message' => $message
	    			], REST_Controller::HTTP_OK);
	    		}else{
	    			$this->response([
	    				'Status' => $status,
	    				'Message' => $message
	    			], REST_Controller::HTTP_OK);
	    		}
	    	}

	    	/* Method to display default success with Result data and totals
	    		 Created By: Manzz Baria
	    	*/
	    	private function displayDefaultJsonWithData($status,$mesage,$result,$totalPages,$currentPages){
	    		if($status == JSON_SUCCESS_STATUS){
	    			$this->response([
	    				'Status' => $status,
	    				'Message' => $mesage,
	    				'TotalPage' => $totalPages,
	    				'CurrentPage' => $currentPages,
	    				'Data' => $result
	    			], REST_Controller::HTTP_OK);
	    		}else{
	    			$this->response([
	    				'Status' => $status,
	    				'Message' => $mesage,
	    				'TotalPage' => $totalPages,
	    				'CurrentPage' => $currentPages,
	    				'Data' => $result
	    			], REST_Controller::HTTP_OK);
	    		}
	    	}

	    /* Method to check parameters is null or empty
	        Created By: Manzz Baria
	    */
	    public function verifyRequiredParams($required_fields,$type){
	        $error = false;
	        $error_fields = "";
	        foreach ($required_fields as $field) {
	    			if($type == POST_TYPE){
	    				if ($this->post($field) == null) {
	    	      	$error = true;
	    	        $error_fields .= $field . ', ';
	    	      }
	    	    }else if($type == GET_TYPE){
	    				if ($this->get($field) == null) {
	    	      	$error = true;
	    	        $error_fields .= $field . ', ';
	    	      }
	    	    }else if($type == PUT_TYPE){
	    				if ($this->put($field) == null) {
	    	      	$error = true;
	    	        $error_fields .= $field . ', ';
	    	      }
	    	    }else{
	    				if ($this->delete($field) == null) {
	    	      	$error = true;
	    	        $error_fields .= $field . ', ';
	    	      }
	    			}
	        }
	        if ($error) {
	        	$message = 'Required field(s) ' . substr($error_fields, 0, -2) . ' is missing or empty';
	          $this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$message);
	        }
	      }

				/* Method to show Message
					Created By: Manzz Baria
				*/
				public function showMessage($status,$message){
					$this->response([
						'Status' => $status,
						'Message' => $message
					], REST_Controller::HTTP_OK);
				}

				/* Method to add car type
					Created By: Manzz Baria
				*/
				function addCarType_post(){
					$data=json_decode(file_get_contents('php://input'));
					$this->verifyRequiredParams(array('name'),POST_TYPE);

					/***** getting params *****/
					$name = $_POST['name'];
					$language = LANGUAGE_ENGLISH;
					if (!empty($_POST['language'])){
						 $language = $_POST['language'];
					}
					$nameAR = '';
					if (!empty($_POST['nameAR'])){
						 $nameAR = $_POST['nameAR'];
					}

					$fileUrl = "";
					if(isset($_FILES['file']['name'])){
						 $file=$_FILES['file']['name'];

						 $image_path='./uploads/files/';
						 $thumb_path='./uploads/files/thumb/';
						 $data = $this->uploadFileContent();
						 $filename=$data['upload_data']['file_name'];
						 $videofile=$_FILES["file"]["tmp_name"];

						 // Images create thumbnail and upload
						 $imagefile = $filename;
						 $imagefile = $this->generateImageThumbContent($imagefile,$data);
						 $fileUrl = $filename;
					}else{
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"icon is required");
					}

					$this->load->model("ApiCarModel","carModel");
					$result = $this->carModel->addCarType($name,$nameAR,$fileUrl,$language);
					if($result != null){
								$mesage = 'Vehicle Type successfully added';
								if ($language == LANGUAGE_ARABIC) {
									$mesage = 'تمت إضافة نوع مركبة جديدة';
								}
								$totalPages = 1;
								$currentPages = 1;
								$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

					}else{
						$mesage = 'Unable to add vehicle type';
						if ($language == LANGUAGE_ARABIC) {
							$mesage = 'غير قادر على إضافة نوع مركبة جديدة';
						}
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
					}
				}

				/* Method to Upload content
					Created By: Nishit Patel
				*/
				public function uploadFileContent(){
					$time = time();
					$new_name = 'carspa_'.$time;
					$config['upload_path'] = './uploads/files/';
					$config['allowed_types']        = '*';
					$config['max_size']             = 0;
					$config['max_width']            = 9000;
					$config['max_height']           = 8000;
					$config['file_name'] = $new_name;

					$this->load->library('upload', $config);
					$this->upload->initialize($config);
					if(!$this->upload->do_upload('file')){
						$error = array('error' => $this->upload->display_errors());
						$msg=$error['error'];
						$status=FALSE;
						$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$msg);
					}else{
						$data = array('upload_data' => $this->upload->data());
						$filename=$data['upload_data']['file_name'];
					}
					$data = $this->upload->data();
					$data=array('upload_data' => $this->upload->data());
					return $data;
				}
				public function generateImageThumbContent($imagefile,$data){
					$config['image_library'] = 'gd2';
					$config['source_image'] =  $data['upload_data']['full_path'];
					$config['new_image'] =  $data['upload_data']['full_path'].'/thumb/'.$imagefile;
					$config['maintain_ratio'] = FALSE;
					$config['overwrite'] = TRUE;
					$config['allowed_types'] = '*';
					$config['max_size'] = 0;
					$config['width'] = 120;
					$config['height'] = 100;
					$this->load->library('image_lib', $config);
					if($this->image_lib->resize()){}
					return $imagefile;
				}

				/* Method to add car type
					Created By: Manzz Baria
				*/
				function updateCarType_post(){
					$data=json_decode(file_get_contents('php://input'));
					$this->verifyRequiredParams(array('name','carTypeId'),POST_TYPE);

					/***** getting params *****/
					$name = $_POST['name'];
					$carTypeId = $_POST['carTypeId'];
					$language = LANGUAGE_ENGLISH;
					if (!empty($_POST['language'])){
						 $language = $_POST['language'];
					}
					$nameAR = '';
					if (!empty($_POST['nameAR'])){
						 $nameAR = $_POST['nameAR'];
					}

					$fileUrl = "";
					if(isset($_FILES['file']['name'])){
						 $file=$_FILES['file']['name'];

						 $image_path='./uploads/files/';
						 $thumb_path='./uploads/files/thumb/';
						 $data = $this->uploadFileContent();
						 $filename=$data['upload_data']['file_name'];
						 $videofile=$_FILES["file"]["tmp_name"];

						 // Images create thumbnail and upload
						 $imagefile = $filename;
						 $imagefile = $this->generateImageThumbContent($imagefile,$data);
						 $fileUrl = $filename;
					}

					$this->load->model("ApiCarModel","carModel");
					$result = $this->carModel->updateCarType($name,$nameAR,$carTypeId,$fileUrl,$language);
					if($result != null){
								$mesage = 'Vehicle Type successfully updated';
								if ($language == LANGUAGE_ARABIC) {
									$mesage = 'لقد تم تحديث نوع المركبة بنجاح';
								}
								$totalPages = 1;
								$currentPages = 1;
								$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

					}else{
						$mesage = 'Unable to update vehicle type';
						if ($language == LANGUAGE_ARABIC) {
							$mesage = 'غير قادر على تحديث نوع المركبة';
						}
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
					}
				}

				/* Method to add car type
					Created By: Manzz Baria
				*/
				function deleteCarType_post(){
					$data=json_decode(file_get_contents('php://input'));
					$this->verifyRequiredParams(array('carTypeId'),POST_TYPE);

					/***** getting params *****/
					$carTypeId = $_POST['carTypeId'];
					$language = LANGUAGE_ENGLISH;
					if (!empty($_POST['language'])){
						 $language = $_POST['language'];
					}
					$this->load->model("ApiCarModel","carModel");
					$result = $this->carModel->deleteCarTypeId($carTypeId);
					if($result){
								$mesage = 'Vehicle Type successfully deleted';
								if ($language == LANGUAGE_ARABIC) {
									$mesage = 'لقد تم حذف نوع المركبة بنجاح';
								}
								$totalPages = 1;
								$currentPages = 1;
								$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

					}else{
						$mesage = 'Unable to delete vehicle type';
						if ($language == LANGUAGE_ARABIC) {
							$mesage = 'غير قادر على حذف نوع المركبة';
						}
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
					}
				}

				/* Method to add car type
					Created By: Manzz Baria
				*/
				function getCarTypes_get(){
					$data=json_decode(file_get_contents('php://input'));
					$this->load->model("ApiCarModel","carModel");
					$language = $this->get('language');
					if (empty($language)){
						$language = LANGUAGE_ENGLISH;
					}
					$result = $this->carModel->getCarTypes($language);
					if($result != null){
								$mesage = 'Found data';
								$totalPages = 1;
								$currentPages = 1;
								$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

					}else{
						$mesage = 'No vehicle type found';
						if ($language == LANGUAGE_ARABIC) {
							$mesage = 'لم نتمكن من العثور على أي نوع مركبة ';
						}
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
					}
				}


				/* Method to add car
					Created By: Manzz Baria
				*/
		function addCar_post(){
					$data=json_decode(file_get_contents('php://input'));
					$this->verifyRequiredParams(array('userId','brand','model'),POST_TYPE);

					/***** getting params *****/
					$userId = $_POST['userId'];
					$brand = $_POST['brand'];
					$model = $_POST['model'];
					$plateNumber = "";
					if (!empty($_POST['plateNumber'])){
							$plateNumber = $_POST['plateNumber'];
					}
					$language = LANGUAGE_ENGLISH;
					if (!empty($_POST['language'])){
						 $language = $_POST['language'];
					}

					$this->load->model("ApiCarModel","carModel");
					$result = $this->carModel->addCar($userId,$brand,$model,$plateNumber);
					if($result != null){
								$mesage = 'Vehicle successfully added';
								if ($language == LANGUAGE_ARABIC) {
									$mesage = 'لقد تمت إضافة نوع المركبة بنجاح';
								}
								$totalPages = 1;
								$currentPages = 1;
								$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

					}else{
						$mesage = 'Vehicle already added in your account with same plate number and car type ';
						if ($language == LANGUAGE_ARABIC) {
							$mesage = 'هناك مركبة بحسابك تحمل ذات المواصفات';
						}
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
					}
			}
			/* Method to updateCar
				Created By: Manzz Baria
			*/
	function updateCar_post(){
				$data=json_decode(file_get_contents('php://input'));
				$this->verifyRequiredParams(array('carId','userId'),POST_TYPE);

				/***** getting params *****/
				$carId = $_POST['carId'];
				$userId = $_POST['userId'];

				$brand = "";
				$model = "";
				$plateNumber ="";
				$language = LANGUAGE_ENGLISH;
				if (!empty($_POST['language'])){
					 $language = $_POST['language'];
				}

				if (!empty($_POST['brand'])){
						$brand = $_POST['brand'];
			 	}
				if (!empty($_POST['model'])){
						$model = $_POST['model'];
			 	}
				if (!empty($_POST['plateNumber'])){
						$plateNumber = $_POST['plateNumber'];
			 	}

				$this->load->model("ApiCarModel","carModel");
				$result = $this->carModel->updateCar($carId,$userId,$brand,$model,$plateNumber);
				if($result != null){
							$mesage = 'Vehicle details successfully updated';
							if ($language == LANGUAGE_ARABIC) {
								$mesage = 'لقد تم تحديث مواصفات المركبة بنجاح ';
							}
							$totalPages = 1;
							$currentPages = 1;
							$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

				}else{
					$mesage = 'Unable to update car detail';
					if ($language == LANGUAGE_ARABIC) {
						$mesage = 'غير قادر على تحديث مواصفات المركبة ';
					}
						$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
				}
		}

		/* Method to add car type
			Created By: Manzz Baria
		*/
		function deleteCar_post(){
			$data=json_decode(file_get_contents('php://input'));
			$this->verifyRequiredParams(array('carId','userId'),POST_TYPE);

			/***** getting params *****/
			$carId = $_POST['carId'];
			$userId = $_POST['userId'];
			$language = LANGUAGE_ENGLISH;
			if (!empty($_POST['language'])){
				 $language = $_POST['language'];
			}

			$this->load->model("ApiCarModel","carModel");
			$result = $this->carModel->deleteCarId($userId,$carId);
			if($result){
						$mesage = 'Vehicle successfully deleted';

						if ($language == LANGUAGE_ARABIC) {
							$mesage = 'لقد تم حذف المركبة بنجاح';
						}
						$totalPages = 1;
						$currentPages = 1;
						$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

			}else{
				$mesage = 'Access denied';
				if ($language == LANGUAGE_ARABIC) {
					$mesage = 'تم الرفض  ';
				}
					$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
			}
		}
		/* Method to getCars
			Created By: Manzz Baria
		*/
		function getCars_get(){
			$data=json_decode(file_get_contents('php://input'));
			$this->verifyRequiredParams(array('userId'),GET_TYPE);
			$language = $this->get('language');
			if (empty($language)){
				$language = LANGUAGE_ENGLISH;
			}
			$userId = (int)$this->get('userId');
		  $this->load->model("ApiCarModel","carModel");
			$result = $this->carModel->getCars($userId);
			if($result != null){
						$mesage = 'Found data';
						$totalPages = 1;
						$currentPages = 1;
						$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

			}else{
				$mesage = 'No vehicle found';
				if ($language == LANGUAGE_ARABIC) {
					$mesage = 'لم يتم العثور على المركبة';
				}
					$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
			}
		}





}
?>
