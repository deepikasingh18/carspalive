<?php

defined('BASEPATH') OR exit('No direct script access allowed');
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class Address extends REST_Controller
{
	public function __construct()
	{
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		$method = $_SERVER['REQUEST_METHOD'];
		if($method == "OPTIONS") {
				die();
		}

		  parent::__construct();
      $this->load->library('session');
      $this->load->helper(array('form','url'));
      $this->load->library('form_validation');
			$this->load->library('mylibrary');
			 $this->load->database();
	}
	    /* Method to display default error message
	    		 Created By: Manzz Baria
	    	*/
	    	private function displayDefaultJsonWithoutData($status,$message){
	    		if($status == JSON_SUCCESS_STATUS){
	    			$this->response([
	    				'Status' => $status,
	    				'Message' => $message
	    			], REST_Controller::HTTP_OK);
	    		}else{
	    			$this->response([
	    				'Status' => $status,
	    				'Message' => $message
	    			], REST_Controller::HTTP_OK);
	    		}
	    	}

	    	/* Method to display default success with Result data and totals
	    		 Created By: Manzz Baria
	    	*/
	    	private function displayDefaultJsonWithData($status,$mesage,$result,$totalPages,$currentPages){
	    		if($status == JSON_SUCCESS_STATUS){
	    			$this->response([
	    				'Status' => $status,
	    				'Message' => $mesage,
	    				'TotalPage' => $totalPages,
	    				'CurrentPage' => $currentPages,
	    				'Data' => $result
	    			], REST_Controller::HTTP_OK);
	    		}else{
	    			$this->response([
	    				'Status' => $status,
	    				'Message' => $mesage,
	    				'TotalPage' => $totalPages,
	    				'CurrentPage' => $currentPages,
	    				'Data' => $result
	    			], REST_Controller::HTTP_OK);
	    		}
	    	}

	    /* Method to check parameters is null or empty
	        Created By: Manzz Baria
	    */
	    public function verifyRequiredParams($required_fields,$type){
	        $error = false;
	        $error_fields = "";
	        foreach ($required_fields as $field) {
	    			if($type == POST_TYPE){
	    				if ($this->post($field) == null) {
	    	      	$error = true;
	    	        $error_fields .= $field . ', ';
	    	      }
	    	    }else if($type == GET_TYPE){
	    				if ($this->get($field) == null) {
	    	      	$error = true;
	    	        $error_fields .= $field . ', ';
	    	      }
	    	    }else if($type == PUT_TYPE){
	    				if ($this->put($field) == null) {
	    	      	$error = true;
	    	        $error_fields .= $field . ', ';
	    	      }
	    	    }else{
	    				if ($this->delete($field) == null) {
	    	      	$error = true;
	    	        $error_fields .= $field . ', ';
	    	      }
	    			}
	        }
	        if ($error) {
	        	$message = 'Required field(s) ' . substr($error_fields, 0, -2) . ' is missing or empty';
	          $this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$message);
	        }
	      }

				/* Method to show Message
					Created By: Manzz Baria
				*/
				public function showMessage($status,$message){
					$this->response([
						'Status' => $status,
						'Message' => $message
					], REST_Controller::HTTP_OK);
				}

				/* Method to add car type
					Created By: Manzz Baria
				*/
				function addAddress_post(){
					$data=json_decode(file_get_contents('php://input'));
					$this->verifyRequiredParams(array('userId','address','addresstitle','latitude','longitude'),POST_TYPE);

					/***** getting params *****/
					$userId = $_POST['userId'];
					$address = $_POST['address'];
					$addresstitle = $_POST['addresstitle'];


					 $varr = preg_replace("/%u([0-9a-f]{3,4})/i","&#x\\1;",urldecode($addresstitle)); 
				     $addresstitle = html_entity_decode($varr,null,'UTF-8');

					$latitude = $_POST['latitude'];
					$longitude = $_POST['longitude'];
					$language = LANGUAGE_ENGLISH;
					$type="";


					if (!empty($_POST['language'])){
						 $language = $_POST['language'];
					}
					$this->load->model("ApiUserModel","userModel");
						$apiName="addAddressApi";
						$ip =  $_SERVER['REMOTE_ADDR'];
						$requestBody=array("address"=>$address,"addresstitle"=>$addresstitle,"language"=>$language,"latitude"=>$latitude,"longitude"=>$longitude);

					$this->load->model("ApiAddressModel","addressModel");
					$result = $this->addressModel->addAddress($userId,$address,$addresstitle,$latitude,$longitude);
					$response=$this->userModel->callapilog($userId,$type,$apiName,$ip,$requestBody,$result);
					if($result != null){
								$mesage = 'Address successfully Added';
								if ($language == LANGUAGE_ARABIC) {
									$mesage = 'العنوان اضيف بنجاح ';
								}
								$totalPages = 1;
								$currentPages = 1;
								$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

					}else{
						$mesage = 'Address already added';
						if ($language == LANGUAGE_ARABIC) {
							$mesage = 'لقد تمت إضافة العنوان من قبل ';
						}
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
					}
				}

				/* Method to updateAddress
					Created By: Manzz Baria
				*/
				function updateAddress_post(){
					$data=json_decode(file_get_contents('php://input'));
					$this->verifyRequiredParams(array('addressId','userId','address','addresstitle','latitude','longitude'),POST_TYPE);

					/***** getting params *****/
					$addressId = $_POST['addressId'];
					$userId = $_POST['userId'];
					$address = $_POST['address'];
					$addresstitle = $_POST['addresstitle'];
					$latitude = $_POST['latitude'];
					$longitude = $_POST['longitude'];
					$language = LANGUAGE_ENGLISH;
					if (!empty($_POST['language'])){
						 $language = $_POST['language'];
					}

					$this->load->model("ApiAddressModel","addressModel");
					$result = $this->addressModel->updateAddress($addressId,$userId,$address,$addresstitle,$latitude,$longitude);
					if($result != null){
								$mesage = 'Address successfully updated';
								if ($language == LANGUAGE_ARABIC) {
								$mesage = ' العنوان عدل بنجاح ';
								}
								$totalPages = 1;
								$currentPages = 1;
								$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

					}else{
						$mesage = 'Address already added';
						if ($language == LANGUAGE_ARABIC) {
							$mesage = 'لقد تمت إضافة العنوان من قبل ';
						}
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
					}
				}

				/* Method to deleteAddress
					Created By: Manzz Baria
				*/
				function deleteAddress_post(){
					$data=json_decode(file_get_contents('php://input'));
					$this->verifyRequiredParams(array('addressId'),POST_TYPE);

					/***** getting params *****/
					$addressId = $_POST['addressId'];
					$language = LANGUAGE_ENGLISH;
					if (!empty($_POST['language'])){
						 $language = $_POST['language'];
					}
				  $this->load->model("ApiAddressModel","addressModel");
					$result = $this->addressModel->deleteAddressId($addressId);
					if($result){
								$mesage = 'Address successfully deleted';
								if ($language == LANGUAGE_ARABIC) {
								$mesage = 'لقد تم حذف العنوان';
								}
								$totalPages = 1;
								$currentPages = 1;
								$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

					}else{
						$mesage = 'Unable to delete address';
						if ($language == LANGUAGE_ARABIC) {
						$mesage = 'غير قادر على حذف العنوان';
						}
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
					}
				}

				/* Method to add car type
					Created By: Manzz Baria
				*/
				function getAddresses_get(){
					$data=json_decode(file_get_contents('php://input'));
					$this->verifyRequiredParams(array('userId'),GET_TYPE);
					$userId = (int)$this->get('userId');
					$language = $this->get('language');
					if (empty($language)){
						$language = LANGUAGE_ENGLISH;
					}
					$this->load->model("ApiAddressModel","addressModel");
					$result = $this->addressModel->getAddresses($userId);
					if($result != null){
								$mesage = 'Found data';
								$totalPages = 1;
								$currentPages = 1;
								$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

					}else{
						$mesage = 'No address found';
						if ($language == LANGUAGE_ARABIC) {
						$mesage = 'لم يتم العثور على العنوان';
						}
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
					}
				}

				/* Method to add car type
					Created By: Manzz Baria
				*/
				function sendEmail_get(){
					$data=json_decode(file_get_contents('php://input'));
					$this->verifyRequiredParams(array('email'),GET_TYPE);
					$email = $this->get('email');
					$this->load->model("Utility","utility");
					$result = $this->utility->sentTestEmail($email);
					if($result){
								$mesage = 'Found data';
								$totalPages = 1;
								$currentPages = 1;
								$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

					}else{
						$mesage = 'No address found';
						if ($language == LANGUAGE_ARABIC) {
						$mesage = 'لم يتم العثور على عنوان ';
						}
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$result.$mesage);
					}
				}



}
?>
