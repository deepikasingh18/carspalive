<?php

defined('BASEPATH') OR exit('No direct script access allowed');
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class Email extends REST_Controller
{

	public function __construct()
	{
	header('Access-Control-Allow-Origin: *');
	header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
	header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
	$method = $_SERVER['REQUEST_METHOD'];
	if($method == "OPTIONS") {
	die();
	}

	  parent::__construct();
      $this->load->library('session');
      $this->load->helper(array('form','url'));
      $this->load->library('form_validation');
	$this->load->library('mylibrary');
	 $this->load->database();

	}
	    /* Method to display default error message
	    	 Created By: Manzz Baria
	    	*/
	    	private function displayDefaultJsonWithoutData($status,$message){
	    	if($status == JSON_SUCCESS_STATUS){
	    	$this->response([
	    	'Status' => $status,
	    	'Message' => $message
	    	], REST_Controller::HTTP_OK);
	    	}else{
	    	$this->response([
	    	'Status' => $status,
	    	'Message' => $message
	    	], REST_Controller::HTTP_OK);
	    	}
	    	}

	    	/* Method to display default success with Result data and totals
	    	 Created By: Manzz Baria
	    	*/
	    	private function displayDefaultJsonWithData($status,$mesage,$result,$totalPages,$currentPages){
	    	if($status == JSON_SUCCESS_STATUS){
	    	$this->response([
	    	'Status' => $status,
	    	'Message' => $mesage,
	    	'TotalPage' => $totalPages,
	    	'CurrentPage' => $currentPages,
	    	'Data' => $result
	    	], REST_Controller::HTTP_OK);
	    	}else{
	    	$this->response([
	    	'Status' => $status,
	    	'Message' => $mesage,
	    	'TotalPage' => $totalPages,
	    	'CurrentPage' => $currentPages,
	    	'Data' => $result
	    	], REST_Controller::HTTP_OK);
	    	}
	    	}

	    /* Method to check parameters is null or empty
	        Created By: Manzz Baria
	    */
	    public function verifyRequiredParams($required_fields,$type){
	        $error = false;
	        $error_fields = "";
	        foreach ($required_fields as $field) {
	    	if($type == POST_TYPE){
	    	if ($this->post($field) == null) {
	    	      	$error = true;
	    	        $error_fields .= $field . ', ';
	    	      }
	    	    }else if($type == GET_TYPE){
	    	if ($this->get($field) == null) {
	    	      	$error = true;
	    	        $error_fields .= $field . ', ';
	    	      }
	    	    }else if($type == PUT_TYPE){
	    	if ($this->put($field) == null) {
	    	      	$error = true;
	    	        $error_fields .= $field . ', ';
	    	      }
	    	    }else{
	    	if ($this->delete($field) == null) {
	    	      	$error = true;
	    	        $error_fields .= $field . ', ';
	    	      }
	    	}
	        }
	        if ($error) {
	        	$message = 'Required field(s) ' . substr($error_fields, 0, -2) . ' is missing or empty';
	          $this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$message);
	        }
	      }

	/* Method to show Message
	Created By: Manzz Baria
	*/
	public function showMessage($status,$message){
	$this->response([
	'Status' => $status,
	'Message' => $message
	], REST_Controller::HTTP_OK);
	}
	
	
	function appointmentDeley_post(){
	$data=json_decode(file_get_contents('php://input'));
	$this->verifyRequiredParams(array('appointmentId'),POST_TYPE);
	$appointmentId = $_POST['appointmentId'];
                                         
                                                $language = LANGUAGE_ENGLISH;
	              if (!empty($_POST['language']))
                                                       {
	            $language = $_POST['language'];
	               }
	$this->load->model("ApiUserModel","uModel");
	$this->load->model("ApiAdminModel","aModel");
	$this->load->model("ApiAppoinmnetModel","appointModel");
	$this->load->model("ApiRequestModel","reqModel");
	$this->load->model("ApiServiceModel","sModel");
	$this->load->model("ApiBillModel","bModel");
        $this->load->model("ApiContactModel","ContactModel");

	$this->load->model("ApiSMSModel","smsModel");
	$this->load->model("ApiCarModel","cModel");
	$this->load->model("ApiAddressModel","addrModel");
	$this->load->model("ApiCategoryModel","catgryModel");
	$this->load->model("Utility","util");
	$this->load->model("ApiVanOperatorModel","vanOperatorModel");

	$userId = $this->appointModel->getUserIdOfAppointment($appointmentId);
	$userName = $this->uModel->getFullName($userId);
	$email = $this->uModel->getEmail($userId);
        $countrycode = $this->uModel->getCountryCode($userId);
	$userMobile = $this->uModel->getFullMobileNumber($userId);
	$requestId = $this->appointModel->getRequestIdOfAppointment($appointmentId);
	$suffixId = $this->appointModel->getSuffixIdfAppointment($appointmentId);
	            $suffix = $this->util->getSuffix($requestId,$suffixId);
	$appointmentTime1 = $this->appointModel->getTimeOfAppointment($appointmentId);
	$operatorDatetime1 = $this->appointModel->getTimeOfAppointmentOperator($appointmentId);
	$carTypeObj = $this->cModel->getCarTypeByAppointmentId($appointmentId);
	$addressObj = $this->addrModel->getAddressByAppointmentId($appointmentId);

	$carTypeName = $carTypeObj['name'];
	$carTypeId = $carTypeObj['Id'];
	$addressRequest = $addressObj['address'];
	$categoryName = $this->catgryModel->getCategoryNameOfAppointment($appointmentId);
	$serviceObject = $this->sModel->getServicesByAppointmentId($appointmentId,$carTypeId);

	$servicesDetails = "";
	$servicesMSG = "";
	
	 $appointmentTime=date("d-m-Y H:i",strtotime($appointmentTime1));
	 
	 $operatorDatetime=date("d-m-Y H:i",strtotime($operatorDatetime1));

	//$logoUrl = EMAIL_HEADER_LOGO;
	$logoUrl = LOGO_URL;

	foreach ($serviceObject as $service) {
	 $servicesDetails = $servicesDetails.'<br> # '.$service['name'].' - '.$service['amount'].' '.$service['currency'];
	 $servicesMSG = $servicesMSG.''.$service['name'].'-'.$service['amount'].' '.$service['currency'];
	
	}

	$totalAmount  =$this->bModel->getAmountdOfAppointment($appointmentId);
	$this->load->model("ApiCurrencyModel","currencyModel");
	$currency  =$this->currencyModel->getAmountCurrencyOfAppointment($appointmentId,$language);

	$validationToken = $userId;
	$accessToken = $this->util->generateRandomString(16);
	$this->uModel->saveAccessToken($accessToken,$userId);

	//$contacts = $this->aModel->getCustomerServiceSupport();
        $name='CarSpa';
$contacts = $this->ContactModel->getContactcountrycode($countrycode);
	$title = $name;
	$phoneNumber = "+".$contacts['countryCode'].$contacts['mobileNumber'];
	$contactemail = $contacts['email'];
	$address = $contacts['address'];


	$vanId = $this->appointModel->getAppointmentVanId($appointmentId);
	$operators = $this->vanOperatorModel->getVanOperatorsByVanId($vanId);
	$operatorMessage = "";
	$operatorDetailsMessage = "";
	if ($operators != null) {
	foreach ($operators as $operator) {
	$operatorMessage = $operatorMessage.''.$operator['fullName'].',';
	$operatorDetailsMessage = $operatorDetailsMessage.'<tr><td>Operator name : '.$operator['fullName'].'</td></tr>';
	$operatorDetailsMessage = $operatorDetailsMessage.'<tr><td>Operator email : '.$operator['email'].'</td></tr>';
	$operatorDetailsMessage = $operatorDetailsMessage.'<tr><td>Operator phone number : +'.$operator['countryCode'].$operator['mobileNumber'].'</td></tr><br>';
	}
	}


	$bodyAdmin=' 

	<div style="background-image:  url(http://carspaco.com/carspaDev/assets/images/login-header-image.png);width:100%;background-repeat: no-repeat; background-size: 100%;"> 
	<img src="http://carspaco.com/carspaDev/assets/images/login-header-image.png" style="visibility: hidden;height: auto;width:100%" /></div>
	
	 <div style="max-width:100%"  align="center">
	 <div style="solid;max-width:60%;"
	 align="center">
	 <br>
	 <table style="width:100%" border="0" height="80px;"
	style="margin:0px;padding:0px">
	<tr>
	<td width="50%" align="left">
	<p style="Margin-top: 0;color: #565656;font-family: Georgia,serif;font-size:
	16px;line-height: 25px;Margin-bottom: 25px;">Hi '.$title.', </p>
	</td></tr><tr><td width="50%" align="left"><p style="Margin-top: 0;color: #565656;
	font-family: Georgia,serif;font-size: 16px;line-height: 25px;Margin-bottom: 25px;
	color:#000000">This mail is to inform you that request '.$suffix.' is
	 going delay. The time of appointment is '.$operatorDatetime.' . </p></td></tr>
	 </table></div><div style="max-width:100%"  align="center">
	 <div style="solid;max-width:60%;" align="center">
	 <table style="width:100%"  border="0" cellpadding="0" cellspacing="0">
	 <tr style="border-bottom:1px solid">
	   <td width="50%" style="font-weight:600;"><u>User Detail :- </u></td></tr><tr>
	  <td><br></td></tr><tr><td width="50%">Name  : '.$userName.'
	 <br>Mobile No : '.$userMobile.'</td></tr></table>
	 	<br><br>
	 <table style="width:100%"
	 border="0" cellpadding="0" cellspacing="0"><tr style="border-bottom:1px solid">
	 <td width="50%" style="font-weight:600;"><u>Request Detail :- </u></td></tr><tr>
	 <td><br></td></tr><tr><td width="50%">Request  : '.$suffix.'
	 <br>Address : '.$addressRequest.'<br>Cartype : '.$carTypeName.'<br>Category : '.$categoryName.'<br>Services :
	 '.$servicesDetails.'</td></tr></table>
	 <table style="width:100%" border="0" cellpadding="0"
	 cellspacing="0"><tr style="font-weight:600;"><td width="35%"><u>Operator Detail :- </u>
	 </td></tr><tr style="font-weight:600;"><td width="35%"></td></tr>'.$operatorDetailsMessage.'</table><br><br><br>
	 <div><table style="width:100%" border="0" cellpadding="0"
	cellspacing="0"><tr style="font-weight:600;"><td width="35%"><u>Bill Detail :- </u>
	</td></tr><tr style="font-weight:600;"><td width="35%"><br></td></tr><tr>
	<td>Total Amount : '.$totalAmount.' '.$currency.'</td></tr></table></div>
	</table>
	<hr style="width:100%;border:1px dashed;border-color:#4CAF50;"><table
	style="width:100%" border="0" cellpadding="0" cellspacing="0"><tr><td width="50%"
	align="left"><p style="Margin-top: 0;color: #565656;font-family: Georgia,serif;
	font-size: 16px;line-height: 25px;Margin-bottom: 25px;color:#8CC540">
	'.$title.' <br><span style="color:#000000;font-size:12px">
	'.$phoneNumber.' <br><span style="color:#000000;font-size:12px">
	'.$contactemail.'<br>'.$address.'</span></p></td>
	<td width="50%" align="right" style="font-size: 24px;line-height: 25px;
	Margin-bottom: 25px;color:#8CC540">Thank You</td></tr></table>
	<div style="border:1px solid;background-color:#4CAF50;border-color:#4CAF50;
	height:20px"></div></div></div>';

	$this->load->library('email');


	$subject = "Request  ".$suffix. " is delay";
	// $contactemail = "bhadolakrishna@gmail.com";
	$this->sendgripEmail($contactemail,$subject,$bodyAdmin);
	return true;



	}
	
	
	

	function fixAppointmentConfirmation_post(){
	$data=json_decode(file_get_contents('php://input'));
	$this->verifyRequiredParams(array('appointmentId'),POST_TYPE);
	$appointmentId = $_POST['appointmentId'];
                                         
                                                $language = LANGUAGE_ENGLISH;
	              if (!empty($_POST['language']))
                                                       {
	            $language = $_POST['language'];
	               }
	$this->load->model("ApiUserModel","uModel");
	$this->load->model("ApiAdminModel","aModel");
	$this->load->model("ApiAppoinmnetModel","appointModel");
	$this->load->model("ApiRequestModel","reqModel");
	$this->load->model("ApiServiceModel","sModel");
	$this->load->model("ApiBillModel","bModel");


	$this->load->model("ApiSMSModel","smsModel");
	$this->load->model("ApiCarModel","cModel");
	$this->load->model("ApiAddressModel","addrModel");
	$this->load->model("ApiCategoryModel","catgryModel");
	$this->load->model("Utility","util");
	$this->load->model("ApiVanOperatorModel","vanOperatorModel");
        $this->load->model("ApiContactModel","ContactModel");

	$userId = $this->appointModel->getUserIdOfAppointment($appointmentId);
	$userName = $this->uModel->getFullName($userId);
	$email = $this->uModel->getEmail($userId);
	$userMobile = $this->uModel->getFullMobileNumber($userId);
        $countrycode = $this->uModel->getCountryCode($userId);
	$requestId = $this->appointModel->getRequestIdOfAppointment($appointmentId);
	$suffixId = $this->appointModel->getSuffixIdfAppointment($appointmentId);
	            $suffix = $this->util->getSuffix($requestId,$suffixId);
	$appointmentTime1 = $this->appointModel->getTimeOfAppointment($appointmentId);
	$operatorDatetime1 = $this->appointModel->getTimeOfAppointmentOperator($appointmentId);
	$carTypeObj = $this->cModel->getCarTypeByAppointmentId($appointmentId);
	$addressObj = $this->addrModel->getAddressByAppointmentId($appointmentId);

	$carTypeName = $carTypeObj['name'];
	$carTypeNameAr = $carTypeObj['nameAR'];
	$carTypeId = $carTypeObj['Id'];
	$addressRequest = $addressObj['address'];
	$categoryName = $this->catgryModel->getCategoryNameOfAppointment($appointmentId);
	$categoryNameAr = $this->catgryModel->getCategoryNameArOfAppointment($appointmentId);
	$serviceObject = $this->sModel->getServicesByAppointmentId($appointmentId,$carTypeId);

	$servicesDetails = "";
	$servicesMSG = "";
	$servicesMSGAr = "";
	$servicesDetailsAr="";
	
	 $appointmentTime=date("d-m-Y H:i",strtotime($appointmentTime1));
	 
	 $operatorDatetime=date("d-m-Y H:i",strtotime($operatorDatetime1));

	//$logoUrl = EMAIL_HEADER_LOGO;
	$logoUrl = LOGO_URL;

	foreach ($serviceObject as $service) {
	 $servicesDetails = $servicesDetails.'<br> # '.$service['name'].' - '.$service['amount'].' '.$service['currency'];
	 $servicesDetailsAr = $servicesDetailsAr.'<br> # '.$service['nameAR'].' - '.$service['amount'].' '.$service['currencyAR'];
	 $servicesMSG = $servicesMSG.''.$service['name'].'-'.$service['amount'].' '.$service['currency'];
	 $servicesMSGAr = $servicesMSGAr.''.$service['nameAR'].'-'.$service['amount'].' '.$service['currencyAR'];
	/*	$additionalServices = "";
	if ($service['additionalService'] != null) {
	 $servicesAdditional = $service['additionalService'];
	foreach ($servicesAdditional as $additional) {
	$additionalServices = $additionalServices.'<br> -- '.$additional['name'].' - '.$additional['amount'].' SR';
	}
	}
	$servicesDetails = $servicesDetails.''.$additionalServices;
	*/
	}

	$totalAmount  =$this->bModel->getAmountdOfAppointment($appointmentId);
	$this->load->model("ApiCurrencyModel","currencyModel");
	$currency=$this->currencyModel->getAmountCurrencyOfAppointment($appointmentId,$language);
	$currencyAR=$this->currencyModel->getAmountCurrencyAROfAppointment($appointmentId);

	$validationToken = $userId;
	$accessToken = $this->util->generateRandomString(16);
	$this->uModel->saveAccessToken($accessToken,$userId);

	//$contacts = $this->aModel->getCustomerServiceSupport();
         $contacts = $this->ContactModel->getContactcountrycode($countrycode);        
         $name='CarSpa';
	$title = $name;
	$phoneNumber = "+".$contacts['countryCode'].$contacts['mobileNumber'];
	$contactemail = $contacts['email'];
	$address = $contacts['address'];


	$vanId = $this->appointModel->getAppointmentVanId($appointmentId);
	$operators = $this->vanOperatorModel->getVanOperatorsByVanId($vanId);
	$operatorMessage = "";
	$operatorDetailsMessage = "";
	$provideroperatorDetailsMessage = "";
	if ($operators != null) {
	foreach ($operators as $operator) {
	$operatorMessage = $operatorMessage.''.$operator['fullName'].',';
	$operatorDetailsMessage = $operatorDetailsMessage.'<tr><td>Operator name : '.$operator['fullName'].'</td></tr>';
	$operatorDetailsMessage = $operatorDetailsMessage.'<tr><td>Operator email : '.$operator['email'].'</td></tr>';
	$operatorDetailsMessage = $operatorDetailsMessage.'<tr><td>Operator phone number : +'.$operator['countryCode'].$operator['mobileNumber'].'</td></tr><br>';
	
	$provideroperatorDetailsMessage = $provideroperatorDetailsMessage.'<tr><td>الإسم : '.$operator['fullName'].'</td></tr>';
	$provideroperatorDetailsMessage = $provideroperatorDetailsMessage.'<tr><td>البريد الالكتروني : '.$operator['email'].'</td></tr>';
	$provideroperatorDetailsMessage = $provideroperatorDetailsMessage.'<tr><td>رقم الجوال : +'.$operator['countryCode'].$operator['mobileNumber'].'</td></tr><br>';
	}
	}

	
	$bodyUser='
	 <div   style="background-image:  url(http://carspaco.com/carspaDev/assets/images/login-header-image.png);width:100%;background-repeat: no-repeat; background-size: 100%;"> 
	 <img src="http://carspaco.com/carspaDev/assets/images/login-header-image.png" style="visibility: hidden;height: auto;width:100%" /></div>

	 
	 	<div style="max-width:100%"  align="center"><div style="solid;max-width:60%;"
	 align="center"><br><table style="width:100%" border="0" height="80px;"
	style="margin:0px;padding:0px"><tr><td width="50%" align="left">
	<p style="Margin-top: 0;color: #565656;font-family: Georgia,serif;font-size:
	16px;line-height: 25px;Margin-bottom: 25px;">Hi '.$userName.', </p>
	</td></tr><tr><td width="50%" align="left"><p style="Margin-top: 0;color: #565656;
	font-family: Georgia,serif;font-size: 16px;line-height: 25px;Margin-bottom: 25px;
	color:#000000">This mail is to inform you that your request is
	 approved. We have fixed your appointment on '.$appointmentTime.' Expect a 15 minutes delay due to unexpected traffic. </p></td></tr>
	 </table></div><div style="max-width:100%"  align="center">
	 <div style="solid;max-width:60%;" align="center"><table style="width:100%"
	 border="0" cellpadding="0" cellspacing="0"><tr style="border-bottom:1px solid">
	 <td width="50%" style="font-weight:600;"><u>Request Detail :- </u></td></tr><tr>
	 <td><br></td></tr><tr><td width="50%">Request  : '.$suffix.'
	 <br>Address : '.$addressRequest.'<br>Cartype : '.$carTypeName.'<br>Category : '.$categoryName.'<br><strong>Services :</strong>
	 '.$servicesDetails.'</td></tr></table>
	 <br><br><table style="width:100%" border="0" cellpadding="0"
	cellspacing="0"><tr style="font-weight:600;"><td width="35%"><u>Bill Detail :- </u>
	</td></tr><tr style="font-weight:600;"><td width="35%"><br></td></tr><tr>
	<td>Total Amount : '.$totalAmount.' '.$currency.'</td></tr></table>
	
	 <div style="max-width:100%"  align="right" dir="rtl"><div style="solid;max-width:60%;"
	 align="right"><br><table style="width:100%" border="0" height="80px;"
	style="margin:0px;padding:0px"><tr><td width="50%" align="right">
	<p style="Margin-top: 0;color: #565656;font-family: Georgia,serif;font-size:
	16px;line-height: 25px;Margin-bottom: 25px;"> مرحبا '.$userName.' ,</p>
	</td></tr><tr><td width="50%" align="right"><p style="Margin-top: 0;color: #565656;
	font-family: Georgia,serif;font-size: 16px;line-height: 25px;Margin-bottom: 25px;
	color:#000000" dir="rtl">لقد تمت الموافقة على طلبك: الموعد المحدد هو '.$appointmentTime.' : قد يكون هناك بعض التاخير نتيجة زحمة المرور</td></tr>
	 </table></div><div style="max-width:100%"  align="right">
	 <div style="solid;max-width:60%;" align="right"><table style="width:100%"
	 border="0" cellpadding="0" cellspacing="0"><tr style="border-bottom:1px solid">
	 <td width="50%" style="font-weight:600;"><u>الطلب تفاصيل   :- </u></td></tr><tr>
	 <td><br></td></tr><tr><td width="50%">رقم الطلب  : '.$suffix.'
	 <br>العنوان : '.$addressRequest.'<br>نوع المركبة : '.$carTypeNameAr.'<br>الفئة : '.$categoryNameAr.'<br><strong>الخدمات :</strong>
	 '.$servicesDetailsAr.'</td></tr></table>
	 <br><br><table style="width:100%" border="0" cellpadding="0"
	cellspacing="0"><tr style="font-weight:600;"><td width="35%"><u>تفاصيل الفاتورة :- </u>
	</td></tr><tr style="font-weight:600;"><td width="35%"><br></td></tr><tr>
	<td>المبلغ الإجمالي : '.$totalAmount.' '.$currencyAR.'</td></tr></table></div></div></div>
	
	<hr style="width:100%;border:1px dashed;border-color:#4CAF50;"><table
	style="width:100%" border="0" cellpadding="0" cellspacing="0"><tr><td width="50%"
	align="left"><p style="Margin-top: 0;color: #565656;font-family: Georgia,serif;
	font-size: 16px;line-height: 25px;Margin-bottom: 25px;color:#8CC540">
	'.$title.' <br><span style="color:#000000;font-size:12px">
	'.$phoneNumber.' <br><span style="color:#000000;font-size:12px">
	'.$contactemail.'<br>'.$address.'</span></p></td>
	<td width="50%" align="right" style="font-size: 24px;line-height: 25px;
	Margin-bottom: 25px;color:#8CC540">Thank You</td></tr></table>
	<div style="border:1px solid;background-color:#4CAF50;border-color:#4CAF50;
	height:20px"></div></div></div>';

	$bodyAdmin='<div   style="background-image:  url(http://carspaco.com/carspaDev/assets/images/login-header-image.png);width:100%;background-repeat: no-repeat; background-size: 100%;"> <img src="http://carspaco.com/carspaDev/assets/images/login-header-image.png" style="visibility: hidden;height: auto;width:100%" /></div>
	
	 <div style="max-width:100%"  align="center">
	 <div style="solid;max-width:60%;"
	 align="center">
	 <br>
	 <table style="width:100%" border="0" height="80px;"
	style="margin:0px;padding:0px">
	<tr>
	<td width="50%" align="left">
	<p style="Margin-top: 0;color: #565656;font-family: Georgia,serif;font-size:
	16px;line-height: 25px;Margin-bottom: 25px;">Hi '.$title.', </p>
	</td></tr><tr><td width="50%" align="left"><p style="Margin-top: 0;color: #565656;
	font-family: Georgia,serif;font-size: 16px;line-height: 25px;Margin-bottom: 25px;
	color:#000000">This mail is to inform you that one new request is
	 approved. The time of appointment is '.$operatorDatetime.' . </p></td></tr>
	 </table></div><div style="max-width:100%"  align="center">
	 <div style="solid;max-width:60%;" align="center">
	 <table style="width:100%"  border="0" cellpadding="0" cellspacing="0">
	 <tr style="border-bottom:1px solid">
	   <td width="50%" style="font-weight:600;"><u>User Detail :- </u></td></tr><tr>
	  <td><br></td></tr><tr><td width="50%">Name  : '.$userName.'
	 <br>Mobile No : '.$userMobile.'</td></tr></table>
	 	<br><br>
	 <table style="width:100%"
	 border="0" cellpadding="0" cellspacing="0"><tr style="border-bottom:1px solid">
	 <td width="50%" style="font-weight:600;"><u>Request Detail :- </u></td></tr><tr>
	 <td><br></td></tr><tr><td width="50%">Request  : '.$suffix.'
	 <br>Address : '.$addressRequest.'<br>Cartype : '.$carTypeName.'<br>Category : '.$categoryName.'<br>Services :
	 '.$servicesDetails.'</td></tr></table>
	 <table style="width:100%" border="0" cellpadding="0"
	 cellspacing="0"><tr style="font-weight:600;"><td width="35%"><u>Operator Detail :- </u>
	 </td></tr><tr style="font-weight:600;"><td width="35%"></td></tr>'.$operatorDetailsMessage.'</table><br><br><br>
	 <div><table style="width:100%" border="0" cellpadding="0"
	cellspacing="0"><tr style="font-weight:600;"><td width="35%"><u>Bill Detail :- </u>
	</td></tr><tr style="font-weight:600;"><td width="35%"><br></td></tr><tr>
	<td>Total Amount : '.$totalAmount.' '.$currency.'</td></tr></table></div>
	</table>
	<hr style="width:100%;border:1px dashed;border-color:#4CAF50;"><table
	style="width:100%" border="0" cellpadding="0" cellspacing="0"><tr><td width="50%"
	align="left"><p style="Margin-top: 0;color: #565656;font-family: Georgia,serif;
	font-size: 16px;line-height: 25px;Margin-bottom: 25px;color:#8CC540">
	'.$title.' <br><span style="color:#000000;font-size:12px">
	'.$phoneNumber.' <br><span style="color:#000000;font-size:12px">
	'.$contactemail.'<br>'.$address.'</span></p></td>
	<td width="50%" align="right" style="font-size: 24px;line-height: 25px;
	Margin-bottom: 25px;color:#8CC540">Thank You</td></tr></table>
	<div style="border:1px solid;background-color:#4CAF50;border-color:#4CAF50;
	height:20px"></div></div></div>';

	$vanOwner = $this->appointModel->getRequestVanOwnerInfo($appointmentId);
	if($vanOwner != null && $vanOwner['type'] == 2){
	$ownerName = $vanOwner['name'];
	$ownerEmail = $vanOwner['email'];
	$ownerEmail = $vanOwner['email'];
	$providerContactEmail = $vanOwner['contactEmail'];
	$providerContactName = $vanOwner['contactName'];
	$providerContactNumber = $vanOwner['countryCode']."".$vanOwner['mobileNumber'];

	if ($providerContactName == null) {
	 $providerContactName = $ownerName;
	 $providerContactEmail = $ownerEmail;
	}


	if ($providerContactName != null) {
	  $msg = "New appointment on ".$operatorDatetime .". Request Id: ".$suffix.",CustomerName: ".$userName.",Number: ".$userMobile.",Service: ".$servicesMSG." Operator: ".$operatorMessage." Total amount: ".$totalAmount.' '.$currency;
	$this->smsModel->sendSMSByMobileNumber($msg,$providerContactNumber);
	}
	

	$bodyProvider='<div   style="background-image:  url(http://carspaco.com/carspaDev/assets/images/login-header-image.png);width:100%;background-repeat: no-repeat; background-size: 100%;"> <img src="http://carspaco.com/carspaDev/assets/images/login-header-image.png" style="visibility: hidden;height: auto;width:100%" /></div>
	
	 <div style="max-width:100%"  align="center">
	 <div style="solid;max-width:60%;"
	 align="center">
	 <br>
	 <table style="width:100%" border="0" height="80px;"
	style="margin:0px;padding:0px">
	<tr>
	<td width="50%" align="left">
	<p style="Margin-top: 0;color: #565656;font-family: Georgia,serif;font-size:
	16px;line-height: 25px;Margin-bottom: 25px;">Hi '.$providerContactName.', </p>
	</td></tr><tr><td width="50%" align="left"><p style="Margin-top: 0;color: #565656;
	font-family: Georgia,serif;font-size: 16px;line-height: 25px;Margin-bottom: 25px;
	color:#000000">This mail is to inform you that one new request is
	 approved. The time of appointment is '.$operatorDatetime.' . </p></td></tr>
	 </table></div><div style="max-width:100%"  align="center">
	 <div style="solid;max-width:60%;" align="center">
	 <table style="width:100%"  border="0" cellpadding="0" cellspacing="0">
	 <tr style="border-bottom:1px solid">
	   <td width="50%" style="font-weight:600;"><u>User Detail :- </u></td></tr><tr>
	  <td><br></td></tr><tr><td width="50%">Name  : '.$userName.'
	 <br>Mobile No : '.$userMobile.'</td></tr></table>
	 	<br><br>
	 <table style="width:100%"
	 border="0" cellpadding="0" cellspacing="0"><tr style="border-bottom:1px solid">
	 <td width="50%" style="font-weight:600;"><u>Request Detail :- </u></td></tr><tr>
	 <td><br></td></tr><tr><td width="50%">Request  : '.$suffix.'
	 <br>Address : '.$addressRequest.'<br>Cartype : '.$carTypeName.'<br>Category : '.$categoryName.'<br>Services :
	 '.$servicesDetails.'</td></tr></table>
	 <table style="width:100%" border="0" cellpadding="0"
	 cellspacing="0"><tr style="font-weight:600;"><td width="35%"><u>Operator Detail :- </u>
	 </td></tr><tr style="font-weight:600;"><td width="35%"></td></tr>'.$operatorDetailsMessage.'</table><br><br><br>
	 <div><table style="width:100%" border="0" cellpadding="0"
	cellspacing="0"><tr style="font-weight:600;"><td width="35%"><u>Bill Detail :- </u>
	</td></tr><tr style="font-weight:600;"><td width="35%"><br></td></tr><tr>
	<td>Total Amount : '.$totalAmount.' '.$currencyAR.'</td></tr></table></div>
	</table>
	
	<div style="max-width:100%"  align="right" dir="rtl"> 
	 <div style="solid;max-width:60%;"
	 align="right">
	 <br>
	 <table style="width:100%" border="0" height="80px;"
	style="margin:0px;padding:0px">
	<tr>
	<td width="50%" align="right">
	<p style="Margin-top: 0;color: #565656;font-family: Georgia,serif;font-size:
	16px;line-height: 25px;Margin-bottom: 25px;">مرحبا '.$providerContactName.', </p>
	</td></tr><tr><td width="50%" align="right"><p style="Margin-top: 0;color: #565656;
	font-family: Georgia,serif;font-size: 16px;line-height: 25px;Margin-bottom: 25px;
	color:#000000">لقد تمت الموافقة على طلبك: الموعد المحدد هو '.$operatorDatetime.' . </p></td></tr>
	 </table></div><div style="max-width:100%"  align="right">
	 <div style="solid;max-width:60%;" align="right">
	 <table style="width:100%"  border="0" cellpadding="0" cellspacing="0">
	 <tr style="border-bottom:1px solid">
	   <td width="50%" style="font-weight:600;"><u>تفاصيل العميل :- </u></td></tr><tr>
	  <td><br></td></tr><tr><td width="50%">الإسم  : '.$userName.'
	 <br>رقم الجوال : '.$userMobile.'</td></tr></table>
	 	<br><br>
	 <table style="width:100%"
	 border="0" cellpadding="0" cellspacing="0"><tr style="border-bottom:1px solid">
	 <td width="50%" style="font-weight:600;"><u>تفاصيل الطلب :- </u></td></tr><tr>
	 <td><br></td></tr><tr><td width="50%">رقم الطلب  : '.$suffix.'
	 <br>العنوان : '.$addressRequest.'<br>نوع المركبة : '.$carTypeNameAr.'<br>الفئة : '.$categoryNameAr.'<br>الخدمات :
	 '.$servicesDetailsAr.'</td></tr></table>
	 <table style="width:100%" border="0" cellpadding="0"
	 cellspacing="0"><tr style="font-weight:600;"><td width="35%"><u>معلومات مشغل الخدمة :- </u>
	 </td></tr><tr style="font-weight:600;"><td width="35%"></td></tr>'.$provideroperatorDetailsMessage.'</table><br><br><br>
	 <div><table style="width:100%" border="0" cellpadding="0"
	cellspacing="0"><tr style="font-weight:600;"><td width="35%"><u>تفاصيل الفاتورة :- </u>
	</td></tr><tr style="font-weight:600;"><td width="35%"><br></td></tr><tr>
	<td>المبلغ الإجمالي : '.$totalAmount.' '.$currencyAR.'</td></tr></table></div>
	</table></div></div></div>
	
	<hr style="width:100%;border:1px dashed;border-color:#4CAF50;"><table
	style="width:100%" border="0" cellpadding="0" cellspacing="0"><tr><td width="50%"
	align="left"><p style="Margin-top: 0;color: #565656;font-family: Georgia,serif;
	font-size: 16px;line-height: 25px;Margin-bottom: 25px;color:#8CC540">
	'.$title.' <br><span style="color:#000000;font-size:12px">
	'.$phoneNumber.' <br><span style="color:#000000;font-size:12px">
	'.$contactemail.'<br>'.$address.'</span></p></td>
	<td width="50%" align="right" style="font-size: 24px;line-height: 25px;
	Margin-bottom: 25px;color:#8CC540">Thank You</td></tr></table>
	<div style="border:1px solid;background-color:#4CAF50;border-color:#4CAF50;
	height:20px"></div></div></div>';


	
	$this->load->library('email');
	$subject = "New Request  ".$suffix. " on ". $operatorDatetime;
	
	//$providerContactEmail = "bariamanoj@gmail.com";
	$this->sendgripEmail($providerContactEmail,$subject,$bodyProvider);
	}
	$this->load->library('email');
	
	$subject = "New Request  ".$suffix. " on ". $appointmentTime;
	//$email = "bariamanoj@gmail.com";
	$this->sendgripEmail($email,$subject,$bodyUser);


	$subject = "New Request  ".$suffix. " on ". $operatorDatetime;
	$this->sendgripEmail($contactemail,$subject,$bodyAdmin);
	return true;

	}

	function rejectRequestEmail_post(){

	$data=json_decode(file_get_contents('php://input'));
	$this->verifyRequiredParams(array('appointmentId','reason'),POST_TYPE);
	$appointmentId = $_POST['appointmentId'];
	$reason = $_POST['reason'];

	$this->load->model("ApiUserModel","userModel");
	$this->load->model("ApiAdminModel","adminModel");
	$this->load->model("ApiRequestModel","requestModel");
	$this->load->model("ApiAppoinmnetModel","appoinmnetModel");
	$this->load->model("Utility","utility");
        $this->load->model("ApiContactModel","ContactModel");

	$userId = $this->appoinmnetModel->getUserIdOfAppointment($appointmentId);
	$userName = $this->userModel->getFullName($userId);
	$email = $this->userModel->getEmail($userId);
        $countrycode = $this->userModel->getCountryCode($userId);
	$requestId = $this->appoinmnetModel->getRequestIdOfAppointment($appointmentId);
	$suffixId = $this->appoinmnetModel->getSuffixIdfAppointment($appointmentId);
	$suffix = $this->utility->getSuffix($requestId,$suffixId);

	//$contacts = $this->adminModel->getCustomerServiceSupport();
       $contacts = $this->ContactModel->getContactcountrycode($countrycode);
        $name='CarSpa';
	$title = $name;
	$phoneNumber = "+".$contacts['countryCode'].$contacts['mobileNumber'];
	$contactemail = $contacts['email'];
	$address = $contacts['address'];

	$body='<div   style="background-image:  url(http://carspaco.com/carspaDev/assets/images/login-header-image.png);width:100%;background-repeat: no-repeat; background-size: 100%;"> <img src="http://carspaco.com/carspaDev/assets/images/login-header-image.png" style="visibility: hidden;height: auto;width:100%" /></div>
	<div style="max-width:100%"  align="center"><div style="solid;max-width:60%;" align="center">	 
	<br><table align="center" width="100%"><tr><td bgcolor="#FFFFFF"><div align="center"><table border="0" align="center" style="padding:15px">
	<tr><td width="50%" align="left"><p style="Margin-top: 0;color: #565656;
	font-family: Georgia,serif;font-size: 16px;line-height: 25px;Margin-bottom: 25px;
	color:#000000">Dear '.$userName.',</p><p style="font-size:17px;">
	We are sorry to inform you that your Request '.$suffix.' for car cleaning has been rejected for <strong>'.$reason.'</strong>.<br />
	 We are sorry for inconvenience.</p>
	 
	 <div style="max-width:100%"  align="right" dir="rtl"><div style="solid;max-width:60%;" align="right">	 
	<br><table align="right" width="100%"><tr><td bgcolor="#FFFFFF"><div align="right"><table border="0" align="right" style="padding:15px">
	<tr><td width="50%" align="right"><p style="Margin-top: 0;color: #565656;
	font-family: Georgia,serif;font-size: 16px;line-height: 25px;Margin-bottom: 25px;
	color:#000000">عزيزي '.$userName.',</p><p style="font-size:17px;">
	ناسف لإلغاء طلبكم '.$suffix.' للسبب التالي :  <strong> '.$reason.' </strong>.<br />
	 ناسف لاي ازعاج قد تسببنا به.</p></td></tr></table></div></td></tr></table></div></div>
	 
	 <hr style="width:100%;border:1px dashed;border-color:#4CAF50;"><table
	style="width:100%" border="0" cellpadding="0" cellspacing="0"><tr><td width="50%"
	align="left"><p style="Margin-top: 0;color: #565656;font-family: Georgia,serif;
	font-size: 16px;line-height: 25px;Margin-bottom: 25px;color:#8CC540">
	'.$title.' <br><span style="color:#000000;font-size:12px">
	'.$phoneNumber.' <br><span style="color:#000000;font-size:12px">
	'.$contactemail.'<br>'.$address.'</span></p></td>
	<td width="50%" align="right" style="font-size: 24px;line-height: 25px;
	Margin-bottom: 25px;color:#8CC540">Thank You</td></tr></table>
	<div style="border:1px solid;background-color:#4CAF50;border-color:#4CAF50;height:20px"></div></div></div>';

	//  ->from('support@carspaco.com','CarSpa')
	$this->load->library('email');

	$subject = 'Your request '.$suffix.' has been rejected' ;
	$this->sendgripEmail($email,$subject,$body);

	$bodyAdmin='<div   style="background-image:  url(http://carspaco.com/carspaDev/assets/images/login-header-image.png);width:100%;background-repeat: no-repeat; background-size: 100%;"> 
	<img src="http://carspaco.com/carspaDev/assets/images/login-header-image.png" style="visibility: hidden;height: auto;width:100%" /></div>
	<div style="max-width:100%"  align="center"><div style="solid;max-width:60%;" align="center">	 
	<br><table align="center" width="100%"><tr><td bgcolor="#FFFFFF"><div align="center"><table border="0" align="center" style="padding:15px">
	<tr><td width="50%" align="left"><p style="Margin-top: 0;color: #565656;
	font-family: Georgia,serif;font-size: 16px;line-height: 25px;Margin-bottom: 25px;
	color:#000000">Dear Admin,</p><p style="font-size:17px;">
	We are sorry to inform you that Request '.$suffix.' for car cleaning has been rejected for <strong>'.$reason.'</strong>.<br />
	 We are sorry for inconvenience.</p><hr style="width:100%;border:1px dashed;border-color:#4CAF50;"><table
	style="width:100%" border="0" cellpadding="0" cellspacing="0"><tr><td width="50%"
	align="left"><p style="Margin-top: 0;color: #565656;font-family: Georgia,serif;
	font-size: 16px;line-height: 25px;Margin-bottom: 25px;color:#8CC540">
	'.$title.' <br><span style="color:#000000;font-size:12px">
	'.$phoneNumber.' <br><span style="color:#000000;font-size:12px">
	'.$contactemail.'<br>'.$address.'</span></p></td>
	<td width="50%" align="right" style="font-size: 24px;line-height: 25px;
	Margin-bottom: 25px;color:#8CC540">Thank You</td></tr></table>
	<div style="border:1px solid;background-color:#4CAF50;border-color:#4CAF50;height:20px"></div></div></div>';

	//  ->from('support@carspaco.com','CarSpa')
	$this->load->library('email');

	$subject = 'Your request '.$suffix.' has been rejected' ;
	$this->sendgripEmail($contactemail,$subject,$body);


	

	$vanOwner = $this->appoinmnetModel->getRequestVanOwnerInfo($appointmentId);
	if($vanOwner != null && $vanOwner['type'] == 2){
	$ownerName = $vanOwner['name'];
	$ownerEmail = $vanOwner['email'];
	$vanName = $vanOwner['vanName'];
	$providerContactEmail = $vanOwner['contactEmail'];
	$providerContactName = $vanOwner['contactName'];
	if ($providerContactName == null) {
	 $providerContactName = $ownerName;
	 $providerContactEmail = $ownerEmail;
	}

	
	$body='<div   style="background-image:  url(http://carspaco.com/carspaDev/assets/images/login-header-image.png);width:100%;background-repeat: no-repeat; background-size: 100%;"> <img src="http://carspaco.com/carspaDev/assets/images/login-header-image.png" style="visibility: hidden;height: auto;width:100%" /></div>
	<div style="max-width:100%"  align="center"><div style="solid;max-width:60%;" align="center">	 
	<br><table align="center" width="100%"><tr><td bgcolor="#FFFFFF"><div align="center"><table border="0" align="center" style="padding:15px">
	<tr><td width="50%" align="left"><p style="Margin-top: 0;color: #565656;
	font-family: Georgia,serif;font-size: 16px;line-height: 25px;Margin-bottom: 25px;
	color:#000000">Dear '.$providerContactName.',</p><p style="font-size:17px;">
	We are sorry to inform you that Request '.$suffix.' for car cleaning has been rejected for <strong>'.$reason.'</strong>.<br />
	 We are sorry for inconvenience.</p>
	 
	 <div style="max-width:100%"  align="right" dir="rtl"><div style="solid;max-width:60%;" align="right">	 
	<br><table align="right" width="100%"><tr><td bgcolor="#FFFFFF"><div align="right"><table border="0" align="right" style="padding:15px">
	<tr><td width="50%" align="right"><p style="Margin-top: 0;color: #565656;
	font-family: Georgia,serif;font-size: 16px;line-height: 25px;Margin-bottom: 25px;
	color:#000000">عزيزي '.$userName.',</p><p style="font-size:17px;">
	ناسف لإلغاء طلبكم '.$suffix.' للسبب التالي :  <strong> '.$reason.' </strong>.<br/>
	 ناسف لاي ازعاج قد تسببنا به.</p></td></tr></table></div></td></tr></table></div></div>
	 
	 <hr style="width:100%;border:1px dashed;border-color:#4CAF50;"><table
	style="width:100%" border="0" cellpadding="0" cellspacing="0"><tr><td width="50%"
	align="left"><p style="Margin-top: 0;color: #565656;font-family: Georgia,serif;
	font-size: 16px;line-height: 25px;Margin-bottom: 25px;color:#8CC540">
	'.$title.' <br><span style="color:#000000;font-size:12px">
	'.$phoneNumber.' <br><span style="color:#000000;font-size:12px">
	'.$contactemail.'<br>'.$address.'</span></p></td>
	<td width="50%" align="right" style="font-size: 24px;line-height: 25px;
	Margin-bottom: 25px;color:#8CC540">Thank You</td></tr></table>
	<div style="border:1px solid;background-color:#4CAF50;border-color:#4CAF50;height:20px"></div></div></div>';
	$this->load->library('email');
	
	$subject = 'Request '.$suffix.' has been rejected' ;
	$this->sendgripEmail($providerContactEmail,$subject,$body);


	}
	return true;

	}
	
	
	function sendEmailForChangeRequestVan_post(){

	$data=json_decode(file_get_contents('php://input'));
	$this->verifyRequiredParams(array('appointmentId','oldVanId','newVanId'),POST_TYPE);
	$appointmentId = $_POST['appointmentId'];
	$oldVanId = $_POST['oldVanId'];
	$newVanId = $_POST['newVanId'];

	    $language = LANGUAGE_ENGLISH;
	              if (!empty($_POST['language']))
                                                       {
	            $language = $_POST['language'];
	               }

	$this->load->model("ApiServiceModel","sModel");
	$this->load->model("ApiBillModel","bModel");


	$this->load->model("ApiCarModel","cModel");
	$this->load->model("ApiAddressModel","addrModel");
	$this->load->model("ApiCategoryModel","catgryModel");

	$this->load->model("ApiAdminModel","adminModel");
	$this->load->model("ApiAppoinmnetModel","appoinmnetModel");
	$this->load->model("ApiVanOperatorModel","vanOperatorModel");
	 
	$this->load->model("Utility","utility");
        $this->load->model("ApiContactModel","ContactModel");
	
	$requestId = $this->appoinmnetModel->getRequestIdOfAppointment($appointmentId);
	$suffixId = $this->appoinmnetModel->getSuffixIdfAppointment($appointmentId);
	$suffix = $this->utility->getSuffix($requestId,$suffixId);
	$oldVanName= $this->vanOperatorModel->getVanNamebyId($oldVanId);
	$newVanName= $this->vanOperatorModel->getVanNamebyId($newVanId);
	
	//$contacts = $this->adminModel->getCustomerServiceSupport();
         $name='CarSpa';
        $contacts = $this->ContactModel->getContactcountrycode(966);
	$title = $name;
	$phoneNumber = "+".$contacts['countryCode'].$contacts['phoneNumber'];
	$contactemail = $contacts['email'];
	$address = $contacts['address'];



	$appointmentTime1 = $this->appoinmnetModel->getTimeOfAppointment($appointmentId);
	$operatorDatetime1 = $this->appoinmnetModel->getTimeOfAppointmentOperator($appointmentId);
	$carTypeObj = $this->cModel->getCarTypeByAppointmentId($appointmentId);
	$addressObj = $this->addrModel->getAddressByAppointmentId($appointmentId);

	$carTypeName = $carTypeObj['name'];
	$carTypeNameAr = $carTypeObj['nameAR'];
	$carTypeId = $carTypeObj['Id'];
	$addressRequest = $addressObj['address'];
	$categoryName = $this->catgryModel->getCategoryNameOfAppointment($appointmentId);
	$categoryNameAr = $this->catgryModel->getCategoryNameArOfAppointment($appointmentId);
	$serviceObject = $this->sModel->getServicesByAppointmentId($appointmentId,$carTypeId);

	$servicesDetails = "";
	$servicesMSG = "";
	$servicesMSGAr = "";
	$servicesDetailsAr="";
	
	 $appointmentTime=date("d-m-Y H:i",strtotime($appointmentTime1));
	 
	 $operatorDatetime=date("d-m-Y H:i",strtotime($operatorDatetime1));

	 foreach ($serviceObject as $service) {
	 $servicesDetails = $servicesDetails.'<br> # '.$service['name'].' - '.$service['amount'].' '.$service['currency'];
	 $servicesDetailsAr = $servicesDetailsAr.'<br> # '.$service['nameAR'].' - '.$service['amount'].' '.$service['currencyAR'];
	 	 $servicesMSG = $servicesMSG.''.$service['name'].'-'.$service['amount'].' '.$service['currency'];
	 	 $servicesMSGAr = $servicesMSGAr.''.$service['nameAR'].'-'.$service['amount'].' '.$service['currencyAR'];
	}

	$totalAmount  =$this->bModel->getAmountdOfAppointment($appointmentId);
	$this->load->model("ApiCurrencyModel","currencyModel");
	$currency  =$this->currencyModel->getAmountCurrencyOfAppointment($appointmentId,$language);
	$currencyAR=$this->currencyModel->getAmountCurrencyAROfAppointment($appointmentId);

	//$contacts = $this->adminModel->getCustomerServiceSupport();
        $contacts = $this->ContactModel->getContactcountrycode(966);	
        $name='CarSpa';
        $title = $name;
	$phoneNumber = "+".$contacts['countryCode'].$contacts['mobileNumber'];
	$contactemail = $contacts['email'];
	$address = $contacts['address'];


	   $oldoperators = $this->vanOperatorModel->getVanOperatorsByVanId($oldVanId);
	
	$OldoperatorDetailsMessage = "";
	if ($oldoperators != null) {
	foreach ($oldoperators as $operator) {
	
	$OldoperatorDetailsMessage = $OldoperatorDetailsMessage.'<tr><td>Operator name : '.$operator['fullName'].'</td></tr>';
	$OldoperatorDetailsMessage = $OldoperatorDetailsMessage.'<tr><td>Operator email : '.$operator['email'].'</td></tr>';
	$OldoperatorDetailsMessage = $OldoperatorDetailsMessage.'<tr><td>Operator phone number : +'.$operator['countryCode'].$operator['mobileNumber'].'</td></tr><br>';
	}
	}

	    $newoperators = $this->vanOperatorModel->getVanOperatorsByVanId($newVanId);
	
	$newoperatorDetailsMessage = "";
	if ($newoperators != null) {
	foreach ($newoperators as $operator) {
	
	$newoperatorDetailsMessage = $newoperatorDetailsMessage.'<tr><td>Operator name : '.$operator['fullName'].'</td></tr>';
	$newoperatorDetailsMessage = $newoperatorDetailsMessage.'<tr><td>Operator email : '.$operator['email'].'</td></tr>';
	$newoperatorDetailsMessage = $newoperatorDetailsMessage.'<tr><td>Operator phone number : +'.$operator['countryCode'].$operator['mobileNumber'].'</td></tr><br>';
	}
	}	



	$bodyAdmin='<div   style="background-image:  url(http://carspaco.com/carspaDev/assets/images/login-header-image.png);width:100%;background-repeat: no-repeat; background-size: 100%;"> <img src="http://carspaco.com/carspaDev/assets/images/login-header-image.png" style="visibility: hidden;height: auto;width:100%" /></div>

	 <div style="max-width:100%"  align="center"><div style="solid;max-width:60%;"
	 align="center"><br><table style="width:100%" border="0" height="80px;"
	style="margin:0px;padding:0px"><tr><td width="50%" align="left">
	<p style="Margin-top: 0;color: #565656;font-family: Georgia,serif;font-size:
	16px;line-height: 25px;Margin-bottom: 25px;">Dear Admin </p>
	</td></tr><tr><td width="50%" align="left"><p style="Margin-top: 0;color: #565656;
	font-family: Georgia,serif;font-size: 16px;line-height: 25px;Margin-bottom: 25px;
	color:#000000">We have changed the van which is going to serve the request '.$suffix.' </p></td></tr>
	 </table></div><div style="max-width:100%"  align="center">
	 <div style="solid;max-width:60%;" align="center"><table style="width:100%"
	 border="0" cellpadding="0" cellspacing="0"><tr style="border-bottom:1px solid">
	 <td width="50%" style="font-weight:600;"><u>Old Van Detail :- </u></td></tr><tr>
	 <td><br></td></tr><tr><td width="50%">Name  : '.$oldVanName.'</td></tr></table><br>
	 <table style="width:100%" border="0" cellpadding="0"
	 cellspacing="0"><tr style="font-weight:600;"><td width="35%">Operator Detail :- 
	 </td></tr><tr style="font-weight:600;"><td width="35%"></td></tr>'.$OldoperatorDetailsMessage.'</table>
	<br><br> <table style="width:100%"
	 border="0" cellpadding="0" cellspacing="0"><tr style="border-bottom:1px solid">
	 <td width="50%" style="font-weight:600;"><u>New Van Detail :- </u></td></tr><tr>
	 <td><br></td></tr><tr><td width="50%">Name  : '.$newVanName.'</td></tr></table>
	 <br><table style="width:100%" border="0" cellpadding="0"
	 cellspacing="0"><tr style="font-weight:600;"><td width="35%">Operator Detail :-
	 </td></tr><tr style="font-weight:600;"><td width="35%"></td></tr>'.$newoperatorDetailsMessage.'</table>
	<hr style="width:100%;border:1px dashed;border-color:#4CAF50;"><table
	style="width:100%" border="0" cellpadding="0" cellspacing="0"><tr><td width="50%"
	align="left"><p style="Margin-top: 0;color: #565656;font-family: Georgia,serif;
	font-size: 16px;line-height: 25px;Margin-bottom: 25px;color:#8CC540">
	'.$title.' <br><span style="color:#000000;font-size:12px">
	'.$phoneNumber.' <br><span style="color:#000000;font-size:12px">
	'.$contactemail.'<br>'.$address.'</span></p></td>
	<td width="50%" align="right" style="font-size: 24px;line-height: 25px;
	Margin-bottom: 25px;color:#8CC540">Thank You</td></tr></table>
	<div style="border:1px solid;background-color:#4CAF50;border-color:#4CAF50;height:20px"></div></div></div>';

	//  ->from('support@carspaco.com','CarSpa')
	$this->load->library('email');
	//$result = $this->email
	//	->from('noreply@carspaco.com',''.$title)
	//	->to(''.$email)
	//	->subject('Your request '.$suffix.' has been rejected')
	//	->message($body)
	//	->send();
	$subject = 'Request '.$suffix.' has changed the van' ;
	$this->sendgripEmail($contactemail,$subject,$bodyAdmin);


	

	$oldvanOwner = $this->vanOperatorModel->getVanOwnerInfoByVanId($oldVanId);
	print_r($oldvanOwner);
	$newvanOwner = $this->vanOperatorModel->getVanOwnerInfoByVanId($newVanId);
	print_r($newvanOwner);

	if($oldvanOwner !=null && $newvanOwner !=null && ($oldvanOwner['ownerId']!= $newvanOwner['ownerId']))
	{

	echo "not match";
	if($oldvanOwner != null && $oldvanOwner['type'] == 2){
	echo "inside oldvanowner";
	$ownerName = $oldvanOwner['name'];
	$ownerEmail = $oldvanOwner['email'];
	$vanName = $oldvanOwner['vanName'];
	$providerContactEmail = $oldvanOwner['contactEmail'];
	$providerContactName = $oldvanOwner['contactName'];
	if ($providerContactName == null) {
	 $providerContactName = $ownerName;
	 $providerContactEmail = $ownerEmail;
	}

	$body='<div   style="background-image:  url(http://carspaco.com/carspaDev/assets/images/login-header-image.png);width:100%;background-repeat: no-repeat; background-size: 100%;"> <img src="http://carspaco.com/carspaDev/assets/images/login-header-image.png" style="visibility: hidden;height: auto;width:100%" /></div>
	<div style="max-width:100%"  align="center"><div style="solid;max-width:60%;" align="center">	
	<br><table align="center" width="100%"><tr>
	<td bgcolor="#FFFFFF"><div align="center"><table border="0" align="center" style="padding:15px">
	<tr><td width="50%" align="left"><p style="Margin-top: 0;color: #565656;
	font-family: Georgia,serif;font-size: 16px;line-height: 25px;Margin-bottom: 25px;
	color:#000000">Hello '.$providerContactName.',</p><p style="font-size:17px;">
	We are sorry to inform you that Request '.$suffix.' which assigned to your van '.$vanName.' for car cleaning has been removed .<br />
	 We are sorry for inconvenience.</p>
	 
	 
	 <div style="max-width:100%"  align="right" dir="rtl"><div style="solid;max-width:60%;" align="right">	
	<br><table align="right" width="100%"><tr>
	<td bgcolor="#FFFFFF"><div align="right"><table border="0" align="right" style="padding:15px">
	<tr><td width="50%" align="right"><p style="Margin-top: 0;color: #565656;
	font-family: Georgia,serif;font-size: 16px;line-height: 25px;Margin-bottom: 25px;
	color:#000000">مرحبا '.$providerContactName.',</p><p style="font-size:17px;">
	لقد تم إلغاء طلب رقم $suffix و المكلف بخدمتك هو '.$vanName.' .<br />
	 ناسف لاي ازعاج قد تسببنا به .</p></td></tr></table></div></td></tr></table></div></div>
	 
	 <hr style="width:100%;border:1px dashed;border-color:#4CAF50;"><table
	style="width:100%" border="0" cellpadding="0" cellspacing="0"><tr><td width="50%"
	align="left"><p style="Margin-top: 0;color: #565656;font-family: Georgia,serif;
	font-size: 16px;line-height: 25px;Margin-bottom: 25px;color:#8CC540">
	'.$title.' <br><span style="color:#000000;font-size:12px">
	'.$phoneNumber.' <br><span style="color:#000000;font-size:12px">
	'.$contactemail.'<br>'.$address.'</span></p></td>
	<td width="50%" align="right" style="font-size: 24px;line-height: 25px;
	Margin-bottom: 25px;color:#8CC540">Thank You</td></tr></table>
	<div style="border:1px solid;background-color:#4CAF50;border-color:#4CAF50;height:20px"></div></div></div>';
	$this->load->library('email');
	//$result = $this->email
	//	->from('noreply@carspaco.com',''.$title)
	//	->to(''.$providerContactEmail)
	//	->subject('Request '.$suffix.' has been rejected')
	//	->message($body)
	//	->send();
	$subject = 'Request '.$suffix.' has been removed' ;
	$this->sendgripEmail($providerContactEmail,$subject,$body);


	}


	if($newvanOwner != null && $newvanOwner['type'] == 2){
	$ownerName = $newvanOwner['name'];
	$ownerEmail = $newvanOwner['email'];
	$vanName = $newvanOwner['vanName'];
	$providerContactEmail = $newvanOwner['contactEmail'];
	$providerContactName = $newvanOwner['contactName'];
	if ($providerContactName == null) {
	 $providerContactName = $ownerName;
	 $providerContactEmail = $ownerEmail;
	}

	$body='<div   style="background-image:  url(http://carspaco.com/carspaDev/assets/images/login-header-image.png);width:100%;background-repeat: no-repeat; background-size: 100%;"> <img src="http://carspaco.com/carspaDev/assets/images/login-header-image.png" style="visibility: hidden;height: auto;width:100%" /></div>
	 <div style="max-width:100%"  align="center"><div style="solid;max-width:60%;"
	 align="center"><br><table style="width:100%" border="0" height="80px;"
	style="margin:0px;padding:0px"><tr><td width="50%" align="left">
	<p style="Margin-top: 0;color: #565656;font-family: Georgia,serif;font-size:
	16px;line-height: 25px;Margin-bottom: 25px;">Hello '.$providerContactName.', </p>
	</td></tr><tr><td width="50%" align="left"><p style="Margin-top: 0;color: #565656;
	font-family: Georgia,serif;font-size: 16px;line-height: 25px;Margin-bottom: 25px;
	color:#000000">This mail is to inform you that  new request was transfered to you on '.$operatorDatetime.' . </p></td></tr>
	 </table></div><div style="max-width:100%"  align="center">
	 <div style="solid;max-width:60%;" align="center"><table style="width:100%"
	 border="0" cellpadding="0" cellspacing="0"><tr style="border-bottom:1px solid">
	 <td width="50%" style="font-weight:600;"><u>Request Detail :- </u></td></tr><tr>
	 <td><br></td></tr><tr><td width="50%">Request  : '.$suffix.'
	 <br>Address : '.$addressRequest.'<br>Cartype : '.$carTypeName.'<br>Category : '.$categoryName.'<br>Services :
	 '.$servicesDetails.'</td></tr></table>
	 <br><br><table style="width:100%" border="0" cellpadding="0"
	cellspacing="0"><tr style="font-weight:600;"><td width="35%"><u>Bill Detail :- </u>
	</td></tr><tr style="font-weight:600;"><td width="35%"><br></td></tr><tr>
	<td>Total Amount : '.$totalAmount.' '.$currency.'</td></tr></table>
	</table>
	
	<div style="max-width:100%"  align="right" dir="rtl"><div style="solid;max-width:60%;"
	 align="right"><br><table style="width:100%" border="0" height="80px;"
	style="margin:0px;padding:0px"><tr><td width="50%" align="right">
	<p style="Margin-top: 0;color: #565656;font-family: Georgia,serif;font-size:
	16px;line-height: 25px;Margin-bottom: 25px;">مرحبا '.$providerContactName.', </p>
	</td></tr><tr><td width="50%" align="right"><p style="Margin-top: 0;color: #565656;
	font-family: Georgia,serif;font-size: 16px;line-height: 25px;Margin-bottom: 25px;
	color:#000000">هنحيطكم علما ان الطلب   '.$suffix .'   قد حول لكم على '.$operatorDatetime.' . </p></td></tr>
	 </table></div><div style="max-width:100%"  align="right">
	 <div style="solid;max-width:60%;" align="right"><table style="width:100%"
	 border="0" cellpadding="0" cellspacing="0"><tr style="border-bottom:1px solid">
	 <td width="50%" style="font-weight:600;"><u>تفاصيل الطلب :- </u></td></tr><tr>
	 <td><br></td></tr><tr><td width="50%">رقم الطلب  : '.$suffix.'
	 <br>العنوان : '.$addressRequest.'<br>نوع المركبة : '.$carTypeNameAr.' <br>الفئة : '.$categoryNameAr.'<br>الخدمات :
	 '.$servicesDetailsAr.'</td></tr></table>
	 <br><br><table style="width:100%" border="0" cellpadding="0"
	cellspacing="0"><tr style="font-weight:600;"><td width="35%"><u>تفاصيل الفاتورة :- </u>
	</td></tr><tr style="font-weight:600;"><td width="35%"><br></td></tr><tr>
	<td>المبلغ الإجمالي : '.$totalAmount.' '.$currencyAR.'</td></tr></table>
	</table></div></div></div>
	
	<hr style="width:100%;border:1px dashed;border-color:#4CAF50;"><table
	style="width:100%" border="0" cellpadding="0" cellspacing="0"><tr><td width="50%"
	align="left"><p style="Margin-top: 0;color: #565656;font-family: Georgia,serif;
	font-size: 16px;line-height: 25px;Margin-bottom: 25px;color:#8CC540">
	'.$title.' <br><span style="color:#000000;font-size:12px">
	'.$phoneNumber.' <br><span style="color:#000000;font-size:12px">
	'.$contactemail.'<br>'.$address.'</span></p></td>
	<td width="50%" align="right" style="font-size: 24px;line-height: 25px;
	Margin-bottom: 25px;color:#8CC540">Thank You</td></tr></table>
	<div style="border:1px solid;background-color:#4CAF50;border-color:#4CAF50;
	height:20px"></div></div></div>';
	$this->load->library('email');
	
	$subject = 'New job request '.$suffix.' transfered' ;
	$this->sendgripEmail($providerContactEmail,$subject,$body);


	}

	}
	return true;

	}
	
	
	

	function operatorSendEmail_post(){
	$data=json_decode(file_get_contents('php://input'));
	$this->verifyRequiredParams(array('appointmentId'),POST_TYPE);
	$appointmentId = $_POST['appointmentId'];
	$this->load->model("ApiUserModel","userModel");
	$this->load->model("ApiAdminModel","adminModel");
	$this->load->model("ApiRequestModel","requestModel");
	$this->load->model("ApiAppoinmnetModel","appoinmnetModel");
	$this->load->model("ApiVanOperatorModel","vanOperatorModel");
	$this->load->model("Utility","utility");
        $this->load->model("ApiContactModel","ContactModel");

	$userId = $this->appoinmnetModel->getUserIdOfAppointment($appointmentId);
	$userName = $this->userModel->getFullName($userId);
	$email = $this->userModel->getEmail($userId);
        $countrycode = $this->userModel->getCountryCode($userId);

	$requestId = $this->appoinmnetModel->getRequestIdOfAppointment($appointmentId);
	$suffixId = $this->appoinmnetModel->getSuffixIdfAppointment($appointmentId);
	$suffix = $this->utility->getSuffix($requestId,$suffixId);

	$vanId = $this->appoinmnetModel->getAppointmentVanId($appointmentId);
	$operators = $this->vanOperatorModel->getVanOperatorsByVanId($vanId);
	$operatorMessage = "";
	$operatorMessagearabic="";
	if ($operators != null) {
	foreach ($operators as $operator) {
	$operatorMessage = $operatorMessage.'<tr><td>Operator name : '.$operator['fullName'].'</td></tr>';
	$operatorMessage = $operatorMessage.'<tr><td>Operator email : '.$operator['email'].'</td></tr>';
	$operatorMessage = $operatorMessage.'<tr><td>Operator phone number : +'.$operator['countryCode'].$operator['mobileNumber'].'</td></tr><br>';
	
	$operatorMessagearabic = $operatorMessagearabic.'<tr><td>الإسم : '.$operator['fullName'].'</td></tr>';
	$operatorMessagearabic = $operatorMessagearabic.'<tr><td>البريد الالكتروني : '.$operator['email'].'</td></tr>';
	$operatorMessagearabic = $operatorMessagearabic.'<tr><td>رقم الجوال : +'.$operator['countryCode'].$operator['mobileNumber'].'</td></tr><br>';
	}

	}
	//$contacts = $this->adminModel->getCustomerServiceSupport();
        $name='CarSpa';
        $contacts = $this->ContactModel->getContactcountrycode($countrycode);
	$title = $name;
	$phoneNumber = "+".$contacts['countryCode'].$contacts['mobileNumber'];
	$contactemail = $contacts['email'];
	$address = $contacts['address'];
$logoUrl = LOGO_URL;
	 $body='
	 <div   style="background-image:  url(http://carspaco.com/carspaDev/assets/images/login-header-image.png);width:100%;background-repeat: no-repeat; background-size: 100%;"> <img src="http://carspaco.com/carspaDev/assets/images/login-header-image.png" style="visibility: hidden;height: auto;width:100%" />
	</div>
	<div style="max-width:100%"  align="center"><div style="solid;max-width:60%;" align="center">
	<br>
	<table align="center" width="100%"><tr><td bgcolor="#FFFFFF">
	 <div align="center"><table border="0" align="center" style="padding:15px"><tr><td width="50%" align="left"><p style="Margin-top: 0;color: #565656;
	font-family: Georgia,serif;font-size: 16px;line-height: 25px;Margin-bottom: 25px;
	color:#000000">Hi, '.$userName.'</p><p style="font-size:17px;">
	This mail is to inform you that We have sent our Operator for request
	<strong>'.$suffix.'.</strong><br>Please be ready with your car as our opeartor will arrive within short time. Expect a 15 minutes delay due to unexpected traffic.
	</p><table style="width:100%" border="0" cellpadding="0"
	 cellspacing="0"><tr style="font-weight:600;"><td width="35%"><u>Operator Detail :- </u>
	 </td></tr><tr style="font-weight:600;"><td width="35%"><br></td></tr>'.$operatorMessage.'</table>
	 
	 <div style="max-width:100%"  align="right" dir="rtl"><div style="solid;max-width:60%;" align="right">
	<br>
	<table align="right" width="100%"><tr><td bgcolor="#FFFFFF">
	 <div align="right"><table border="0" align="right" style="padding:15px"><tr><td width="50%" align="right"><p style="Margin-top: 0;color: #565656;
	font-family: Georgia,serif;font-size: 16px;line-height: 25px;Margin-bottom: 25px;
	color:#000000">مرحبا, '.$userName.'</p><p style="font-size:17px;">
	نحيطكم علما ان مشغل الخدمة بالطريق إليكم لخدمتكم لطلب 
	<strong>'.$suffix.'.</strong><br>نرجو تجهيز مركبتكم حيث مزود الخدمة بالطريق اليكم: نتوقع بعض التاخير بسبب زحمة السير .
	</p><table style="width:100%" border="0" cellpadding="0"
	 cellspacing="0"><tr style="font-weight:600;"><td width="35%"><u>معلومات مشغل الخدمة :- </u>
	 </td></tr><tr style="font-weight:600;"><td width="35%"><br></td></tr>'.$operatorMessagearabic.'</table></td></tr></table></div></div></div></td></tr></table>
	 
	 <hr style="width:100%;border:1px dashed;border-color:#4CAF50;"><table
	style="width:100%" border="0" cellpadding="0" cellspacing="0"><tr><td width="50%"
	align="left"><p style="Margin-top: 0;color: #565656;font-family: Georgia,serif;
	font-size: 16px;line-height: 25px;Margin-bottom: 25px;color:#8CC540">
	'.$title.' <br><span style="color:#000000;font-size:12px">
	'.$phoneNumber.' <br><span style="color:#000000;font-size:12px">
	'.$contactemail.'<br>'.$address.'</span></p></td>
	<td width="50%" align="right" style="font-size: 24px;line-height: 25px;
	Margin-bottom: 25px;color:#8CC540">Thank You</td></tr></table>
	<div style="border:1px solid;background-color:#4CAF50;border-color:#4CAF50;height:20px"></div></div></div>';

	$this->load->library('email');
	
	$subject = 'Operator is on the way for request '.$suffix ;
	$this->sendgripEmail($email ,$subject,$body);
	return true;

	}

	  function invoice_of_job($appointmentId,$language){
	$this->load->model("ApiUserModel","userModel");
	$this->load->model("ApiAdminModel","adminModel");
	$this->load->model("ApiAppoinmnetModel","appoinmnetModel");
	$this->load->model("ApiRequestModel","requestModel");
	$this->load->model("ApiServiceModel","serviceModel");
	$this->load->model("ApiBillModel","billModel");

	$this->load->model("ApiCarModel","cModel");
	$this->load->model("ApiAddressModel","addrModel");
	$this->load->model("ApiCategoryModel","catgryModel");
        $this->load->model("ApiContactModel","ContactModel");
	$this->load->model("Utility","util");

	$userId = $this->appoinmnetModel->getUserIdOfAppointment($appointmentId);
	$userName = $this->userModel->getFullName($userId);
	$email = $this->userModel->getEmail($userId);
        $countrycode = $this->userModel->getCountryCode($userId);
	$requestId = $this->appoinmnetModel->getRequestIdOfAppointment($appointmentId);
	$suffixId = $this->appoinmnetModel->getSuffixIdfAppointment($appointmentId);
	$suffix = $this->util->getSuffix($requestId,$suffixId);

	$appointmentTime = $this->appoinmnetModel->getTimeOfAppointment($appointmentId);
	$carTypeObj = $this->cModel->getCarTypeByAppointmentId($appointmentId);
	$addressObj = $this->addrModel->getAddressByAppointmentId($appointmentId);
	$carTypeName = $carTypeObj['name'];
	$carTypeNameAr = $carTypeObj['nameAR'];
	$carTypeId = $carTypeObj['Id'];
	$addressRequest = $addressObj['address'];
	$categoryName = $this->catgryModel->getCategoryNameOfAppointment($appointmentId);
	$categoryNameAr = $this->catgryModel->getCategoryNameArOfAppointment($appointmentId);
	$serviceObject = $this->serviceModel->getServicesByAppointmentId($appointmentId,$carTypeId);

	$servicesDetails = "";
	$servicesDetailsAr="";
	$counter = 0;
	foreach ($serviceObject as $service) {
	    $counter = $counter + 1;
	$servicesDetails = $servicesDetails.'<br> '.$counter.') '.$service['name'].' - '.$service['amount'].' '.$service['currency'];
	$servicesDetailsAr = $servicesDetailsAr.'<br> # '.$service['nameAR'].' - '.$service['amount'].' '.$service['currencyAR'];
	}
	$totalAmount  =$this->billModel->getAmountdOfAppointment($appointmentId);
	$this->load->model("ApiCurrencyModel","currencyModel");
	$currency  =$this->currencyModel->getAmountCurrencyOfAppointment($appointmentId,$language);
	$currencyAR=$this->currencyModel->getAmountCurrencyAROfAppointment($appointmentId);
	$validationToken = $userId;
	$accessToken = $this->util->generateRandomString(16);
	$this->userModel->saveAccessToken($accessToken,$userId);

         $name='CarSpa';
	//$contacts = $this->adminModel->getCustomerServiceSupport();
        $contacts = $this->ContactModel->getContactcountrycode($countrycode);
	$title = $name;
	$phoneNumber = "+".$contacts['countryCode'].$contacts['mobileNumber'];
	$contactemail = $contacts['email'];
	$address = $contacts['address'];
	$logoUrl = LOGO_URL;

	$datetime = $this->util->getCurrentDate('Y/m/d h:i:s');

	$bodyUser='<div   style="background-image:  url(http://carspaco.com/carspaDev/assets/images/login-header-image.png);width:100%;background-repeat: no-repeat; background-size: 100%;"> <img src="http://carspaco.com/carspaDev/assets/images/login-header-image.png" style="visibility: hidden;height: auto;width:100%" /></div>
	 <div style="max-width:100%"  align="center"><div style="solid;max-width:60%;"
	 align="center"><br><table style="width:100%" border="0" height="80px;"
	style="margin:0px;padding:0px"><tr><td width="50%" align="left">
	<p style="Margin-top: 0;color: #565656;font-family: Georgia,serif;font-size:
	16px;line-height: 25px;Margin-bottom: 25px;">Hi '.$userName.', </p>
	</td></tr> </table><table style="width:100%;"
	 border="0" cellpadding="0" cellspacing="0"><tr style="border-bottom:1px solid">
	 <td width="50%" style="font-weight:600;"><u>Request :- '.$suffix.' </u></td></tr><tr>
	 <td><br></td></tr><tr><td width="50%">Datetime : '.$datetime.'<br>Address : '.$addressRequest.'<br>
	 Cartype : '.$carTypeName.'<br>Category : '.$categoryName.'<br><br><strong>Services :</strong>
	 '.$servicesDetails.'</td></tr><br><tr>
	 <td><strong>Net Amount : '.$totalAmount.' '.$currency.'</strong></td></tr></table>
	
	<div style="max-width:100%"  align="right" dir="rtl"><div style="solid;max-width:60%;"
	 align="right"><br><table style="width:100%" border="0" height="80px;"
	style="margin:0px;padding:0px"><tr><td width="50%" align="right">
	<p style="Margin-top: 0;color: #565656;font-family: Georgia,serif;font-size:
	16px;line-height: 25px;Margin-bottom: 25px;">مرحبا '.$userName.', </p>
	</td></tr> </table><table style="width:100%;"
	 border="0" cellpadding="0" cellspacing="0"><tr style="border-bottom:1px solid">
	 <td width="50%" style="font-weight:600;"><u>رقم الطلب :- '.$suffix.' </u></td></tr><tr>
	 <td><br></td></tr><tr><td width="50%">التاريخ والوقت : '.$datetime.'<br>العنوان : '.$addressRequest.'<br>
	 نوع المركبة : '.$carTypeNameAr.'<br>الفئة : '.$categoryNameAr.'<br><br><strong>الخدمات :</strong>
	 '.$servicesDetailsAr.'</td></tr><br><tr>
	 <td><strong>المبلغ الإجمالي : '.$totalAmount.' '.$currencyAR.'</strong></td></tr></table></div></div>
	
	<hr style="width:100%;border:1px dashed;border-color:#4CAF50;"><table
	style="width:100%" border="0" cellpadding="0" cellspacing="0"><tr><td width="50%"
	align="left"><p style="Margin-top: 0;color: #565656;font-family: Georgia,serif;
	font-size: 16px;line-height: 25px;Margin-bottom: 25px;color:#8CC540">
	'.$title.' <br><span style="color:#000000;font-size:12px">
	'.$phoneNumber.' <br><span style="color:#000000;font-size:12px">
	'.$contactemail.'<br>'.$address.'</span></p></td>
	<td width="50%" align="right" style="font-size: 24px;line-height: 25px;
	Margin-bottom: 25px;color:#8CC540">Thank You</td></tr></table>
	<div style="border:1px solid;background-color:#4CAF50;border-color:#4CAF50;
	height:20px"></div></div></div>';

	$this->load->library('email');
	
	
	$subject = "Invoice of Request ".$suffix;
	//$email = "bariamanoj@gmail.com";
	$this->sendgripEmail($email,$subject,$bodyUser);
	
	return true;
	}



	function completedRequest_post(){

	$data=json_decode(file_get_contents('php://input'));
	$this->verifyRequiredParams(array('appointmentId'),POST_TYPE);
	$appointmentId = $_POST['appointmentId'];

                                                        $language = LANGUAGE_ENGLISH;
	                    if (!empty($_POST['language'])){
	                $language = $_POST['language'];
	                    }

	$this->load->model("ApiUserModel","userModel");
	$this->load->model("ApiAdminModel","adminModel");
	$this->load->model("ApiAppoinmnetModel","appoinmnetModel");
	$this->load->model("ApiRequestModel","requestModel");
	$this->load->model("ApiServiceModel","serviceModel");
	$this->load->model("ApiBillModel","billModel");
        $this->load->model("ApiContactModel","ContactModel");

	$this->load->model("ApiCarModel","cModel");
	$this->load->model("ApiAddressModel","addrModel");
	$this->load->model("ApiCategoryModel","catgryModel");
	$this->load->model("Utility","util");
     
	$userId = $this->appoinmnetModel->getUserIdOfAppointment($appointmentId);
	$userName = $this->userModel->getFullName($userId);
        $email = $this->userModel->getEmail($userId);
        $countrycode = $this->userModel->getCountryCode($userId);
        //echo $countrycode;
	$requestId = $this->appoinmnetModel->getRequestIdOfAppointment($appointmentId);
	$suffixId = $this->appoinmnetModel->getSuffixIdfAppointment($appointmentId);
	$suffix = $this->util->getSuffix($requestId,$suffixId);

	$appointmentTime = $this->appoinmnetModel->getTimeOfAppointment($appointmentId);
	$carTypeObj = $this->cModel->getCarTypeByAppointmentId($appointmentId);
	$addressObj = $this->addrModel->getAddressByAppointmentId($appointmentId);
	$carTypeName = $carTypeObj['name'];
	$carTypeNameAr = $carTypeObj['nameAR'];
	$carTypeId = $carTypeObj['Id'];
	$addressRequest = $addressObj['address'];
	$categoryName = $this->catgryModel->getCategoryNameOfAppointment($appointmentId);
	$categoryNameAr = $this->catgryModel->getCategoryNameArOfAppointment($appointmentId);
	$serviceObject = $this->serviceModel->getServicesByAppointmentId($appointmentId,$carTypeId);

	$servicesDetails = "";
	$servicesDetailsAr="";
	foreach ($serviceObject as $service) {
	//$servicesDetails = $evaluationMessage.'<tr><td>-</td><td>'.$evaluation['evaluation'].'</td></tr>';
	$servicesDetails = $servicesDetails.'<br> # '.$service['name'].' - '.$service['amount'].' '.$service['currency'];
	$servicesDetailsAr = $servicesDetailsAr.'<br> # '.$service['nameAR'].' - '.$service['amount'].' '.$service['currencyAR'];
	}

	$totalAmount=$this->billModel->getAmountdOfAppointment($appointmentId);
	$this->load->model("ApiCurrencyModel","currencyModel");
	$currency  =$this->currencyModel->getAmountCurrencyOfAppointment($appointmentId,$language);
	$currencyAR=$this->currencyModel->getAmountCurrencyAROfAppointment($appointmentId);
	$validationToken = $userId;
	$accessToken = $this->util->generateRandomString(16);
	$this->userModel->saveAccessToken($accessToken,$userId);


        $name='CarSpa';
	//$contacts = $this->adminModel->getCustomerServiceSupport();
        $contacts = $this->ContactModel->getContactcountrycode($countrycode);
       // print_r($contacts);
	$title = $name;
	$phoneNumber = "+".$contacts['countryCode'].$contacts['mobileNumber'];
	$contactemail = $contacts['email'];
	$address = $contacts['address'];

                $logoUrl = LOGO_URL;
	$bodyUser='<div   style="background-image:  url(http://carspaco.com/carspaDev/assets/images/login-header-image.png);width:100%;background-repeat: no-repeat; background-size: 100%;"> <img src="http://carspaco.com/carspaDev/assets/images/login-header-image.png" style="visibility: hidden;height: auto;width:100%" /></div>
	 <div style="max-width:100%"  align="center"><div style="solid;max-width:60%;"
	 align="center"><br><table style="width:100%" border="0" height="80px;"
	style="margin:0px;padding:0px"><tr><td width="50%" align="left">
	<p style="Margin-top: 0;color: #565656;font-family: Georgia,serif;font-size:
	16px;line-height: 25px;Margin-bottom: 25px;">Hi '.$userName.', </p>
	</td></tr><tr><td width="50%" align="left"><p style="Margin-top: 0;color: #565656;
	font-family: Georgia,serif;font-size: 16px;line-height: 25px;Margin-bottom: 25px;
	color:#000000">This mail is to inform you that your request '.$suffix.' has been completed. </p></td></tr>
	 </table></div><div style="max-width:100%"  align="center">
	 <div style="solid;max-width:60%;" align="center"><table style="width:100%"
	 border="0" cellpadding="0" cellspacing="0"><tr style="border-bottom:1px solid">
	 <td width="50%" style="font-weight:600;"><u>Request Detail :- </u></td></tr><tr>
	 <td><br></td></tr><tr><td width="50%">Request : '.$suffix.'
	 <br>Address : '.$addressRequest.'<br>Cartype : '.$carTypeName.'<br>Category : '.$categoryName.'<br><strong>Services :</strong>
	 '.$servicesDetails.'</td></tr></table>
	 <br><br><table style="width:100%" border="0" cellpadding="0"
	cellspacing="0"><tr style="font-weight:600;"><td width="35%"><u>Bill Detail :- </u>
	</td></tr><tr style="font-weight:600;"><td width="35%"><br></td></tr><tr>
	<td>Total Amount : '.$totalAmount.' '.$currency.'</td></tr></table>
	
	<div style="max-width:100%"  align="right" dir="rtl"><div style="solid;max-width:60%;"
	 align="right"><br><table style="width:100%" border="0" height="80px;"
	style="margin:0px;padding:0px"><tr><td width="50%" align="right">
	<p style="Margin-top: 0;color: #565656;font-family: Georgia,serif;font-size:
	16px;line-height: 25px;Margin-bottom: 25px;">مرحبا '.$userName.' , </p>
	</td></tr><tr><td width="50%" align="right"><p style="Margin-top: 0;color: #565656;
	font-family: Georgia,serif;font-size: 16px;line-height: 25px;Margin-bottom: 25px;
	color:#000000">لقد تم الإنتهاء من تنفيذ طلبكم رقم '.$suffix.' </p></td></tr>
	 </table></div><div style="max-width:100%"  align="right">
	 <div style="solid;max-width:60%;" align="right"><table style="width:100%"
	 border="0" cellpadding="0" cellspacing="0"><tr style="border-bottom:1px solid">
	 <td width="50%" style="font-weight:600;"><u>تفاصيل الطلب :- </u></td></tr><tr>
	 <td><br></td></tr><tr><td width="50%">رقم الطلب : '.$suffix.'
	 <br>العنوان : '.$addressRequest.' <br>نوع المركبة : '.$carTypeNameAr.' <br>الفئة : '.$categoryNameAr.' <br><strong>الخدمات :</strong>
	 '.$servicesDetailsAr.' </td></tr></table>
	 <br><br><table style="width:100%" border="0" cellpadding="0"
	cellspacing="0"><tr style="font-weight:600;"><td width="35%"><u>تفاصيل الفاتورة :- </u>
	</td></tr><tr style="font-weight:600;"><td width="35%"><br></td></tr><tr>
	<td>المبلغ الإجمالي : '.$totalAmount .' '.$currencyAR.' </td></tr></table></div></div></div>
	
	<hr style="width:100%;border:1px dashed;border-color:#4CAF50;"><table
	style="width:100%" border="0" cellpadding="0" cellspacing="0"><tr><td width="50%"
	align="left"><p style="Margin-top: 0;color: #565656;font-family: Georgia,serif;
	font-size: 16px;line-height: 25px;Margin-bottom: 25px;color:#8CC540">
	'.$title.' <br><span style="color:#000000;font-size:12px">
	'.$phoneNumber.' <br><span style="color:#000000;font-size:12px">
	'.$contactemail.'<br>'.$address.'</span></p></td>
	<td width="50%" align="right" style="font-size: 24px;line-height: 25px;
	Margin-bottom: 25px;color:#8CC540">Thank You</td></tr></table>
	<div style="border:1px solid;background-color:#4CAF50;border-color:#4CAF50;
	height:20px"></div></div></div>';

	$bodyAdmin='<div   style="background-image:  url(http://carspaco.com/carspaDev/assets/images/login-header-image.png);width:100%;background-repeat: no-repeat; background-size: 100%;"> <img src="http://carspaco.com/carspaDev/assets/images/login-header-image.png" style="visibility: hidden;height: auto;width:100%" /></div>
	 <div style="max-width:100%"  align="center"><div style="solid;max-width:60%;"
	 align="center"><br><table style="width:100%" border="0" height="80px;"
	style="margin:0px;padding:0px"><tr><td width="50%" align="left">
	<p style="Margin-top: 0;color: #565656;font-family: Georgia,serif;font-size:
	16px;line-height: 25px;Margin-bottom: 25px;"> Dear Admin, </p>
	</td></tr><tr><td width="50%" align="left"><p style="Margin-top: 0;color: #565656;
	font-family: Georgia,serif;font-size: 16px;line-height: 25px;Margin-bottom: 25px;
	color:#000000">This mail is to inform you that request '.$suffix.' has been completed. </p></td></tr>
	 </table></div><div style="max-width:100%"  align="center">
	 <div style="solid;max-width:60%;" align="center"><table style="width:100%"
	 border="0" cellpadding="0" cellspacing="0"><tr style="border-bottom:1px solid">
	 <td width="50%" style="font-weight:600;"><u>Request Detail :- </u></td></tr><tr>
	 <td><br></td></tr><tr><td width="50%">Request : '.$suffix.'
	 <br>Address : '.$addressRequest.'<br>Cartype : '.$carTypeName.'<br>Category : '.$categoryName.'<br>Services :
	 '.$servicesDetails.'</td></tr></table>
	 <br><br><table style="width:100%" border="0" cellpadding="0"
	cellspacing="0"><tr style="font-weight:600;"><td width="35%"><u>Bill Detail :- </u>
	</td></tr><tr style="font-weight:600;"><td width="35%"><br></td></tr><tr>
	<td>Total Amount : '.$totalAmount.' '.$currency.'</td></tr></table>
	<hr style="width:100%;border:1px dashed;border-color:#4CAF50;"><table
	style="width:100%" border="0" cellpadding="0" cellspacing="0"><tr><td width="50%"
	align="left"><p style="Margin-top: 0;color: #565656;font-family: Georgia,serif;
	font-size: 16px;line-height: 25px;Margin-bottom: 25px;color:#8CC540">
	'.$title.' <br><span style="color:#000000;font-size:12px">
	'.$phoneNumber.' <br><span style="color:#000000;font-size:12px">
	'.$contactemail.'<br>'.$address.'</span></p></td>
	<td width="50%" align="right" style="font-size: 24px;line-height: 25px;
	Margin-bottom: 25px;color:#8CC540">Thank You</td></tr></table>
	<div style="border:1px solid;background-color:#4CAF50;border-color:#4CAF50;
	height:20px"></div></div></div>';

	//  ->from('support@carspaco.com','CarSpa')

	$this->load->library('email');
	
	$subject = "Request ".$suffix ." has been completed";
	$this->sendgripEmail($email,$subject,$bodyUser);
	
	$subject = "Request ".$suffix ." has been completed";
	$this->sendgripEmail($contactemail,$subject,$bodyAdmin);
	

	$vanOwner = $this->appoinmnetModel->getRequestVanOwnerInfo($appointmentId);
	if($vanOwner != null && $vanOwner['type'] == 2){
	$ownerName = $vanOwner['name'];
	$ownerEmail = $vanOwner['email'];
	$providerContactEmail = $vanOwner['contactEmail'];
	$providerContactName = $vanOwner['contactName'];
	if ($providerContactName == null) {
	 $providerContactName = $ownerName;
	 $providerContactEmail = $ownerEmail;
	}

	
	$providerContactEmail='<div   style="background-image:  url(http://carspaco.com/carspaDev/assets/images/login-header-image.png);width:100%;background-repeat: no-repeat; background-size: 100%;"> <img src="http://carspaco.com/carspaDev/assets/images/login-header-image.png" style="visibility: hidden;height: auto;width:100%" /></div>
	 <div style="max-width:100%"  align="center"><div style="solid;max-width:60%;"
	 align="center"><br><table style="width:100%" border="0" height="80px;"
	style="margin:0px;padding:0px"><tr><td width="50%" align="left">
	<p style="Margin-top: 0;color: #565656;font-family: Georgia,serif;font-size:
	16px;line-height: 25px;Margin-bottom: 25px;"> Dear '.$providerContactName.', </p>
	</td></tr><tr><td width="50%" align="left"><p style="Margin-top: 0;color: #565656;
	font-family: Georgia,serif;font-size: 16px;line-height: 25px;Margin-bottom: 25px;
	color:#000000">This mail is to inform you that request '.$suffix.' has been completed. </p></td></tr>
	 </table></div><div style="max-width:100%"  align="center">
	 <div style="solid;max-width:60%;" align="center"><table style="width:100%"
	 border="0" cellpadding="0" cellspacing="0"><tr style="border-bottom:1px solid">
	 <td width="50%" style="font-weight:600;"><u>Request Detail :- </u></td></tr><tr>
	 <td><br></td></tr><tr><td width="50%">Request : '.$suffix.'
	 <br>Address : '.$addressRequest.'<br>Cartype : '.$carTypeName.'<br>Category : '.$categoryName.'<br>Services :
	 '.$servicesDetails.'</td></tr></table>
	 <br><br><table style="width:100%" border="0" cellpadding="0"
	cellspacing="0"><tr style="font-weight:600;"><td width="35%"><u>Bill Detail :- </u>
	</td></tr><tr style="font-weight:600;"><td width="35%"><br></td></tr><tr>
	<td>Total Amount : '.$totalAmount.' '.$currency.'</td></tr></table>
	
	<div style="max-width:100%"  align="right" dir="rtl"><div style="solid;max-width:60%;"
	 align="right"><br><table style="width:100%" border="0" height="80px;"
	style="margin:0px;padding:0px"><tr><td width="50%" align="right">
	<p style="Margin-top: 0;color: #565656;font-family: Georgia,serif;font-size:
	16px;line-height: 25px;Margin-bottom: 25px;">مرحبا '.$userName.' , </p>
	</td></tr><tr><td width="50%" align="right"><p style="Margin-top: 0;color: #565656;
	font-family: Georgia,serif;font-size: 16px;line-height: 25px;Margin-bottom: 25px;
	color:#000000">لقد تم الإنتهاء من تنفيذ طلبكم رقم '.$suffix.' </p></td></tr>
	 </table></div><div style="max-width:100%"  align="right">
	 <div style="solid;max-width:60%;" align="right"><table style="width:100%"
	 border="0" cellpadding="0" cellspacing="0"><tr style="border-bottom:1px solid">
	 <td width="50%" style="font-weight:600;"><u>تفاصيل الطلب :- </u></td></tr><tr>
	 <td><br></td></tr><tr><td width="50%">رقم الطلب : '.$suffix.'
	 <br>العنوان : '.$addressRequest.' <br>نوع المركبة : '.$carTypeNameAr.' <br>الفئة : '.$categoryNameAr.' <br><strong>الخدمات :</strong>
	 '.$servicesDetailsAr.' </td></tr></table>
	 <br><br><table style="width:100%" border="0" cellpadding="0"
	cellspacing="0"><tr style="font-weight:600;"><td width="35%"><u>تفاصيل الفاتورة :- </u>
	</td></tr><tr style="font-weight:600;"><td width="35%"><br></td></tr><tr>
	<td>المبلغ الإجمالي : '.$totalAmount .' '.$currencyAR.' </td></tr></table></div></div></div>
	
	
	<hr style="width:100%;border:1px dashed;border-color:#4CAF50;"><table
	style="width:100%" border="0" cellpadding="0" cellspacing="0"><tr><td width="50%"
	align="left"><p style="Margin-top: 0;color: #565656;font-family: Georgia,serif;
	font-size: 16px;line-height: 25px;Margin-bottom: 25px;color:#8CC540">
	'.$title.' <br><span style="color:#000000;font-size:12px">
	'.$phoneNumber.' <br><span style="color:#000000;font-size:12px">
	'.$contactemail.'<br>'.$address.'</span></p></td>
	<td width="50%" align="right" style="font-size: 24px;line-height: 25px;
	Margin-bottom: 25px;color:#8CC540">Thank You</td></tr></table>
	<div style="border:1px solid;background-color:#4CAF50;border-color:#4CAF50;
	height:20px"></div></div></div>';


	
	
	$subject ="Request ".$suffix ." has been completed";
	$this->sendgripEmail($providerContactEmail,$subject,$bodyProvider);


	}

	$this->invoice_of_job($appointmentId,$language);
	return true;

	}

	function sendResetPasswordLink_post(){

	      $data=json_decode(file_get_contents('php://input'));
	    $this->verifyRequiredParams(array('email'),POST_TYPE);
	    $email = $_POST['email'];

	      $this->load->model("ApiUserModel","userModel");
	      $this->load->model("ApiAdminModel","adminModel");
	      $this->load->model("Utility","utility");
              $this->load->model("ApiContactModel","ContactModel");

	      $userId = $this->userModel->getUserIdFromEmail($email);
	      $userName = $this->userModel->getFullName($userId);
             $countrycode = $this->userModel->getCountryCode($userId);
	      $validationToken = $userId;

	      $accessToken = $this->utility->generateRandomString(16);
	      $this->userModel->saveAccessToken($accessToken,$userId);

	     // $contacts = $this->adminModel->getCustomerServiceSupport();
              $contacts = $this->ContactModel->getContactcountrycode($countrycode);
               $name='CarSpa';
	      $title = $name;
	      $phoneNumber = "+".$contacts['countryCode'].$contacts['mobileNumber'];
	      $contactemail = $contacts['email'];
	      $address = $contacts['address'];


	      $body='<div   style="background-image:  url(http://carspaco.com/carspaDev/assets/images/login-header-image.png);width:100%;background-repeat: no-repeat; background-size: 100%;"> <img src="http://carspaco.com/carspaDev/assets/images/login-header-image.png" style="visibility: hidden;height: auto;width:100%" /></div><br>
	       <div align="center"><table border="0" align="center" style="padding:15px"><tr><td width="50%" align="left"><p style="Margin-top: 0;color: #565656;
	font-family: Georgia,serif;font-size: 16px;line-height: 25px;Margin-bottom: 25px;
	color:#000000">Dear '.$userName.', </p><p style="font-size:17px;">
	We got a request to reset your CarSpa password
	</p>
	
	<div align="right" dir="rlt"> <table border="0" align="right" style="padding:15px"><tr><td width="100%" align="right"><p style="Margin-top: 0;color: #565656;
	font-family: Georgia,serif;font-size: 16px;line-height: 25px;Margin-bottom: 25px;
	color:#000000">عزيزي '.$userName.', </p><p style="font-size:17px;">
	لقد وصلنا طلب لتغيير كلمة المرور
	</p><td></tr></table></div>

	<table width="100%" border="0" cellpadding="0" cellspacing="0" >
	      <tr align="center"><td><p style="Margin-bottom: 35px;">
	      <a href="'.base_url().'resetpassword.html?validationToken='.$validationToken.'&accessToken='.$accessToken.'"
	      style="text-decoration:none;border:solid 1px;color:#4CAF50;padding:10px;font-weight:600">Confirm</a>
	      </p></td></tr></table>
	
	
	
	 <hr style="width:100%;border:1px dashed;border-color:#4CAF50;"><table
	style="width:100%" border="0" cellpadding="0" cellspacing="0"><tr><td width="50%"
	align="left"><p style="Margin-top: 0;color: #565656;font-family: Georgia,serif;
	font-size: 16px;line-height: 25px;Margin-bottom: 25px;color:#8CC540">
	'.$title.' <br><span style="color:#000000;font-size:12px">
	'.$phoneNumber.' <br><span style="color:#000000;font-size:12px">
	'.$contactemail.'<br>'.$address.'</span></p></td>
	<td width="50%" align="right" style="font-size: 24px;line-height: 25px;
	Margin-bottom: 25px;color:#8CC540">Thank You</td></tr></table>
	<div style="border:1px solid;background-color:#4CAF50;border-color:#4CAF50;height:20px"></div></div></div>';

	        //  ->from('support@carspaco.com','CarSpa')
	          $this->load->library('email');
	        
	
	$subject ="Reset password link";
	$this->sendgripEmail($email,$subject,$body);	
	          return true;

	    }

function updateAppointmentConfirmation_post(){
	$data=json_decode(file_get_contents('php://input'));
	$this->verifyRequiredParams(array('appointmentId'),POST_TYPE);
	$appointmentId = $_POST['appointmentId'];

                                $language = LANGUAGE_ENGLISH;
	if (!empty($_POST['language'])){
	 $language = $_POST['language'];
	}
	$this->load->model("ApiUserModel","uModel");
	$this->load->model("ApiAdminModel","aModel");
	$this->load->model("ApiAppoinmnetModel","appointModel");
	$this->load->model("ApiRequestModel","reqModel");
	$this->load->model("ApiServiceModel","sModel");
	$this->load->model("ApiBillModel","bModel");


	$this->load->model("ApiCarModel","cModel");
	$this->load->model("ApiAddressModel","addrModel");
	$this->load->model("ApiCategoryModel","catgryModel");
	$this->load->model("Utility","util");
        $this->load->model("ApiContactModel","ContactModel");

	$userId = $this->appointModel->getUserIdOfAppointment($appointmentId);
	$userName = $this->uModel->getFullName($userId);
        $countrycode = $this->uModel->getCountryCode($userId);
	$email = $this->uModel->getEmail($userId);

	$requestId = $this->appointModel->getRequestIdOfAppointment($appointmentId);
	$suffixId = $this->appointModel->getSuffixIdfAppointment($appointmentId);
	      $suffix = $this->util->getSuffix($requestId,$suffixId);
	$appointmentTime = $this->appointModel->getTimeOfAppointment($appointmentId);
	$operatorDatetime = $this->appointModel->getTimeOfAppointmentOperator($appointmentId);
	$carTypeObj = $this->cModel->getCarTypeByAppointmentId($appointmentId);
	$addressObj = $this->addrModel->getAddressByAppointmentId($appointmentId);

	$carTypeName = $carTypeObj['name'];
	$carTypeNameAr = $carTypeObj['nameAR'];
	$carTypeId = $carTypeObj['Id'];
	$addressRequest = $addressObj['address'];
	$categoryName = $this->catgryModel->getCategoryNameOfAppointment($appointmentId);
	$categoryNameAr = $this->catgryModel->getCategoryNameArOfAppointment($appointmentId);
	$serviceObject = $this->sModel->getServicesByAppointmentId($appointmentId,$carTypeId);

	$servicesDetails = "";
	$servicesDetailsAr="";
	foreach ($serviceObject as $service) {
	 $servicesDetails = $servicesDetails.'<br> # '.$service['name'].' - '.$service['amount'].' '.$service['currency'];
	 $servicesDetailsAr = $servicesDetailsAr.'<br> # '.$service['nameAR'].' - '.$service['amount'].' '.$service['currencyAR'];
	/*	$additionalServices = "";
	if ($service['additionalService'] != null) {
	 $servicesAdditional = $service['additionalService'];
	foreach ($servicesAdditional as $additional) {
	$additionalServices = $additionalServices.'<br> -- '.$additional['name'].' - '.$additional['amount'].' SR';
	}
	}
	$servicesDetails = $servicesDetails.''.$additionalServices;
	*/
	}

	$totalAmount  =$this->bModel->getAmountdOfAppointment($appointmentId);
	$this->load->model("ApiCurrencyModel","currencyModel");
	$currency  =$this->currencyModel->getAmountCurrencyOfAppointment($appointmentId,$language);
	$currencyAR=$this->currencyModel->getAmountCurrencyAROfAppointment($appointmentId);

	$validationToken = $userId;
	$accessToken = $this->util->generateRandomString(16);
	$this->uModel->saveAccessToken($accessToken,$userId);

	//$contacts = $this->aModel->getCustomerServiceSupport();
	
        $name='CarSpa';
         $contacts = $this->ContactModel->getContactcountrycode($countrycode);
        $title = $name;
	$phoneNumber = "+".$contacts['countryCode'].$contacts['mobileNumber'];
	$contactemail = $contacts['email'];
	$address = $contacts['address'];

	$logoUrl = LOGO_URL;
	$bodyUser='<div   style="background-image:  url(http://carspaco.com/carspaDev/assets/images/login-header-image.png);width:100%;background-repeat: no-repeat; background-size: 100%;"> <img src="http://carspaco.com/carspaDev/assets/images/login-header-image.png" style="visibility: hidden;height: auto;width:100%" /></div>
	 <div style="max-width:100%"  align="center"><div style="solid;max-width:60%;"
	 align="center"><br><table style="width:100%" border="0" height="80px;"
	style="margin:0px;padding:0px"><tr><td width="50%" align="left">
	<p style="Margin-top: 0;color: #565656;font-family: Georgia,serif;font-size:
	16px;line-height: 25px;Margin-bottom: 25px;">Hi '.$userName.', </p>
	</td></tr><tr><td width="50%" align="left"><p style="Margin-top: 0;color: #565656;
	font-family: Georgia,serif;font-size: 16px;line-height: 25px;Margin-bottom: 25px;
	color:#000000">This mail is to inform you that your request '.$suffix.' is
	 upated. Now your new appointment time is '.$appointmentTime.' Expect a 15 minutes delay due to unexpected traffic. </p></td></tr>
	 </table></div><div style="max-width:100%"  align="center">
	 <div style="solid;max-width:60%;" align="center"><table style="width:100%"
	 border="0" cellpadding="0" cellspacing="0"><tr style="border-bottom:1px solid">
	 <td width="50%" style="font-weight:600;"><u>Request Detail :- </u></td></tr><tr>
	 <td><br></td></tr><tr><td width="50%">Request  : '.$suffix.'
	 <br><strong>Address : </strong>'.$addressRequest.'<br>Cartype : '.$carTypeName.'<br>Category : '.$categoryName.'<br><strong>Services :</strong>
	 '.$servicesDetails.'</td></tr></table>
	 <br><br><table style="width:100%" border="0" cellpadding="0"
	cellspacing="0"><tr style="font-weight:600;"><td width="35%"><u>Bill Detail :- </u>
	</td></tr><tr style="font-weight:600;"><td width="35%"><br></td></tr><tr>
	<td>Total Amount : '.$totalAmount.' '.$currency.'</td></tr></table>
	
	<div style="max-width:100%" align="right" dir="rtl"><div style="solid;max-width:60%;"
	 align="right"><br><table style="width:100%" border="0" height="80px;"
	style="margin:0px;padding:0px"><tr><td width="50%" align="right">
	<p style="Margin-top: 0;color: #565656;font-family: Georgia,serif;font-size:
	16px;line-height: 25px;Margin-bottom: 25px;">مرحبا '.$userName.' , </p>
	</td></tr><tr><td width="50%" align="right"><p style="Margin-top: 0;color: #565656;
	font-family: Georgia,serif;font-size: 16px;line-height: 25px;Margin-bottom: 25px;
	color:#000000">لقد تم تحديث طلبك '.$suffix.' : الموعد المحدد هو '.$appointmentTime.' : قد يكون هناك بعض التاخير نتيجة زحمة المرور. </p></td></tr>
	 </table></div><div style="max-width:100%"  align="right">
	 <div style="solid;max-width:60%;" align="right"><table style="width:100%"
	 border="0" cellpadding="0" cellspacing="0"><tr style="border-bottom:1px solid">
	 <td width="50%" style="font-weight:600;"><u>تفاصيل الطلب :- </u></td></tr><tr>
	 <td><br></td></tr><tr><td width="50%">رقم الطلب  : '.$suffix .'
	 <br><strong>العنوان : </strong>'.$addressRequest.' <br>نوع المركبة : '.$carTypeNameAr.' <br>الفئة : '.$categoryNameAr.' <br><strong>الخدمات :</strong>
	 '.$servicesDetailsAr.' </td></tr></table>
	 <br><br><table style="width:100%" border="0" cellpadding="0"
	cellspacing="0"><tr style="font-weight:600;"><td width="35%"><u>تفاصيل الفاتورة :- </u>
	</td></tr><tr style="font-weight:600;"><td width="35%"><br></td></tr><tr>
	<td>المبلغ الإجمالي : '.$totalAmount.'  '.$currencyAR.' </td></tr></table></div></div></div>


	<hr style="width:100%;border:1px dashed;border-color:#4CAF50;"><table
	style="width:100%" border="0" cellpadding="0" cellspacing="0"><tr><td width="50%"
	align="left"><p style="Margin-top: 0;color: #565656;font-family: Georgia,serif;
	font-size: 16px;line-height: 25px;Margin-bottom: 25px;color:#8CC540">
	'.$title.' <br><span style="color:#000000;font-size:12px">
	'.$phoneNumber.' <br><span style="color:#000000;font-size:12px">
	'.$contactemail.'<br>'.$address.'</span></p></td>
	<td width="50%" align="right" style="font-size: 24px;line-height: 25px;
	Margin-bottom: 25px;color:#8CC540">Thank You</td></tr></table>
	<div style="border:1px solid;background-color:#4CAF50;border-color:#4CAF50;
	height:20px"></div></div></div>';

	$bodyAdmin='<div   style="background-image:  url(http://carspaco.com/carspaDev/assets/images/login-header-image.png);width:100%;background-repeat: no-repeat; background-size: 100%;"> <img src="http://carspaco.com/carspaDev/assets/images/login-header-image.png" style="visibility: hidden;height: auto;width:100%" /></div>
	 <div style="max-width:100%"  align="center"><div style="solid;max-width:60%;"
	 align="center"><br><table style="width:100%" border="0" height="80px;"
	style="margin:0px;padding:0px"><tr><td width="50%" align="left">
	<p style="Margin-top: 0;color: #565656;font-family: Georgia,serif;font-size:
	16px;line-height: 25px;Margin-bottom: 25px;">Hi '.$title.', </p>
	</td></tr><tr><td width="50%" align="left"><p style="Margin-top: 0;color: #565656;
	font-family: Georgia,serif;font-size: 16px;line-height: 25px;Margin-bottom: 25px;
	color:#000000">This mail is to inform you that request '.$suffix.' for car cleaning is
	 upated. The new time of appointment is '.$operatorDatetime.' . </p></td></tr>
	 </table></div><div style="max-width:100%"  align="center">
	 <div style="solid;max-width:60%;" align="center"><table style="width:100%"
	 border="0" cellpadding="0" cellspacing="0"><tr style="border-bottom:1px solid">
	 <td width="50%" style="font-weight:600;"><u>Request Detail :- </u></td></tr><tr>
	 <td><br></td></tr><tr><td width="50%">Request  : '.$suffix.'
	 <br><strong>Address : </strong>'.$addressRequest.'<br>Cartype : '.$carTypeName.'<br>Category : '.$categoryName.'<br>Services :
	 '.$servicesDetails.'</td></tr></table>
	 <br><br><table style="width:100%" border="0" cellpadding="0"
	cellspacing="0"><tr style="font-weight:600;"><td width="35%"><u>Bill Detail :- </u>
	</td></tr><tr style="font-weight:600;"><td width="35%"><br></td></tr><tr>
	<td>Total Amount : '.$totalAmount.' '.$currency.'</td></tr></table>
	<hr style="width:100%;border:1px dashed;border-color:#4CAF50;"><table
	style="width:100%" border="0" cellpadding="0" cellspacing="0"><tr><td width="50%"
	align="left"><p style="Margin-top: 0;color: #565656;font-family: Georgia,serif;
	font-size: 16px;line-height: 25px;Margin-bottom: 25px;color:#8CC540">
	'.$title.' <br><span style="color:#000000;font-size:12px">
	'.$phoneNumber.' <br><span style="color:#000000;font-size:12px">
	'.$contactemail.'<br>'.$address.'</span></p></td>
	<td width="50%" align="right" style="font-size: 24px;line-height: 25px;
	Margin-bottom: 25px;color:#8CC540">Thank You</td></tr></table>
	<div style="border:1px solid;background-color:#4CAF50;border-color:#4CAF50;
	height:20px"></div></div></div>';

	$vanOwner = $this->appointModel->getRequestVanOwnerInfo($appointmentId);
	if($vanOwner != null && $vanOwner['type'] == 2){
	$ownerName = $vanOwner['name'];
	$ownerEmail = $vanOwner['email'];
	$providerContactEmail = $vanOwner['contactEmail'];
	$providerContactName = $vanOwner['contactName'];
	if ($providerContactName == null) {
	 $providerContactName = $ownerName;
	 $providerContactEmail = $ownerEmail;
	}


	// $bodyProvider='<div   style="background-image:  url(http://carspaco.com/carspaDev/assets/images/login-header-image.png);width:100%;background-repeat: no-repeat; background-size: 100%;"> <img src="http://carspaco.com/carspaDev/assets/images/login-header-image.png" style="visibility: hidden;height: auto;width:100%" /></div>
	//  <div style="max-width:100%"  align="center"><div style="solid;max-width:60%;"
	//  align="center"><br><table style="width:100%" border="0" height="80px;"
	// 	style="margin:0px;padding:0px"><tr><td width="50%" align="left">
	// 	<p style="Margin-top: 0;color: #565656;font-family: Georgia,serif;font-size:
	// 	16px;line-height: 25px;Margin-bottom: 25px;">Hello '.$providerContactName.', </p>
	// 	</td></tr><tr><td width="50%" align="left"><p style="Margin-top: 0;color: #565656;
	// 	font-family: Georgia,serif;font-size: 16px;line-height: 25px;Margin-bottom: 25px;
	// 	color:#000000">This mail is to inform you that request '.$suffix.' for car cleaning is
	// 	 upated. The new time of appointment is '.$operatorDatetime.' . </p></td></tr>
	// 	 </table></div><div style="max-width:100%"  align="center">
	// 	 <div style="solid;max-width:60%;" align="center"><table style="width:100%"
	// 	 border="0" cellpadding="0" cellspacing="0"><tr style="border-bottom:1px solid">
	// 	 <td width="50%" style="font-weight:600;"><u>Request Detail :- </u></td></tr><tr>
	// 	 <td><br></td></tr><tr><td width="50%">Request  : '.$suffix.'
	// 	 <br><strong>Address : </strong>'.$addressRequest.'<br>Cartype : '.$carTypeName.'<br>Category : '.$categoryName.'<br>Services :
	// 	 '.$servicesDetails.'</td></tr></table>
	// 	 <br><br><table style="width:100%" border="0" cellpadding="0"
	// 	cellspacing="0"><tr style="font-weight:600;"><td width="35%"><u>Bill Detail :- </u>
	// 	</td></tr><tr style="font-weight:600;"><td width="35%"><br></td></tr><tr>
	// 	<td>Total Amount : '.$totalAmount.' '.$currency.'</td></tr></table>
	// 	<hr style="width:100%;border:1px dashed;border-color:#4CAF50;"><table
	// 	style="width:100%" border="0" cellpadding="0" cellspacing="0"><tr><td width="50%"
	// 	align="left"><p style="Margin-top: 0;color: #565656;font-family: Georgia,serif;
	// 	font-size: 16px;line-height: 25px;Margin-bottom: 25px;color:#8CC540">
	// 	'.$title.' <br><span style="color:#000000;font-size:12px">
	// 	'.$phoneNumber.' <br><span style="color:#000000;font-size:12px">
	// 	'.$contactemail.'<br>'.$address.'</span></p></td>
	// 	<td width="50%" align="right" style="font-size: 24px;line-height: 25px;
	// 	Margin-bottom: 25px;color:#8CC540">Thank You</td></tr></table>
	// 	<div style="border:1px solid;background-color:#4CAF50;border-color:#4CAF50;
	// 	height:20px"></div></div></div>';

	$bodyProvider='<div   style="background-image:  url(http://carspaco.com/carspaDev/assets/images/login-header-image.png);width:100%;background-repeat: no-repeat; background-size: 100%;"> <img src="http://carspaco.com/carspaDev/assets/images/login-header-image.png" style="visibility: hidden;height: auto;width:100%" /></div>
	 <div style="max-width:100%"  align="center"><div style="solid;max-width:60%;"
	 align="center"><br><table style="width:100%" border="0" height="80px;"
	style="margin:0px;padding:0px"><tr><td width="50%" align="left">
	<p style="Margin-top: 0;color: #565656;font-family: Georgia,serif;font-size:
	16px;line-height: 25px;Margin-bottom: 25px;">Hi '.$providerContactName.', </p>
	</td></tr><tr><td width="50%" align="left"><p style="Margin-top: 0;color: #565656;
	font-family: Georgia,serif;font-size: 16px;line-height: 25px;Margin-bottom: 25px;
	color:#000000">This mail is to inform you that request '.$suffix.' for car cleaning is
	 upated. The new time of appointment is '.$operatorDatetime.' . </p></td></tr>
	 </table></div><div style="max-width:100%"  align="center">
	 <div style="solid;max-width:60%;" align="center"><table style="width:100%"
	 border="0" cellpadding="0" cellspacing="0"><tr style="border-bottom:1px solid">
	 <td width="50%" style="font-weight:600;"><u>Request Detail :- </u></td></tr><tr>
	 <td><br></td></tr><tr><td width="50%">Request  : '.$suffix.'
	 <br><strong>Address : </strong>'.$addressRequest.'<br>Cartype : '.$carTypeName.'<br>Category : '.$categoryName.'<br>Services :
	 '.$servicesDetails.'</td></tr></table>
	 <br><br><table style="width:100%" border="0" cellpadding="0"
	cellspacing="0"><tr style="font-weight:600;"><td width="35%"><u>Bill Detail :- </u>
	</td></tr><tr style="font-weight:600;"><td width="35%"><br></td></tr><tr>
	<td>Total Amount : '.$totalAmount.' '.$currency.'</td></tr></table>
	

	<div style="max-width:100%" align="right" dir="rtl"><div style="solid;max-width:60%;"
	 align="right"><br><table style="width:100%" border="0" height="80px;"
	style="margin:0px;padding:0px"><tr><td width="50%" align="right">
	<p style="Margin-top: 0;color: #565656;font-family: Georgia,serif;font-size:
	16px;line-height: 25px;Margin-bottom: 25px;">مرحبا '.$userName.' </p>
	</td></tr><tr><td width="50%" align="right"><p style="Margin-top: 0;color: #565656;
	font-family: Georgia,serif;font-size: 16px;line-height: 25px;Margin-bottom: 25px;
	color:#000000">لقد تم تحديث طلبك '.$suffix.' : الموعد المحدد هو '.$appointmentTime.': قد يكون هناك بعض التاخير نتيجة زحمة المرور</p></td></tr>
	 </table></div><div style="max-width:100%"  align="right">
	 <div style="solid;max-width:60%;" align="right"><table style="width:100%"
	 border="0" cellpadding="0" cellspacing="0"><tr style="border-bottom:1px solid">
	 <td width="50%" style="font-weight:600;"><u>تفاصيل الطلب :- </u></td></tr><tr>
	 <td><br></td></tr><tr><td width="50%">رقم الطلب  : '.$suffix .'
	 <br><strong>العنوان : </strong>'.$addressRequest.' <br>نوع المركبة : '.$carTypeNameAr.' <br>الفئة : '.$categoryNameAr.' <br><strong>الخدمات :</strong>
	 '.$servicesDetailsAr .'</td></tr></table>
	 <br><br><table style="width:100%" border="0" cellpadding="0"
	cellspacing="0"><tr style="font-weight:600;"><td width="35%"><u>تفاصيل الفاتورة :- </u>
	</td></tr><tr style="font-weight:600;"><td width="35%"><br></td></tr><tr>
	<td>المبلغ الإجمالي : '.$totalAmount.'  '.$currencyAR.' </td></tr></table></div></div></div>

	<hr style="width:100%;border:1px dashed;border-color:#4CAF50;"><table
	style="width:100%" border="0" cellpadding="0" cellspacing="0"><tr><td width="50%"
	align="left"><p style="Margin-top: 0;color: #565656;font-family: Georgia,serif;
	font-size: 16px;line-height: 25px;Margin-bottom: 25px;color:#8CC540">
	'.$title.' <br><span style="color:#000000;font-size:12px">
	'.$phoneNumber.' <br><span style="color:#000000;font-size:12px">
	'.$contactemail.'<br>'.$address.'</span></p></td>
	<td width="50%" align="right" style="font-size: 24px;line-height: 25px;
	Margin-bottom: 25px;color:#8CC540">Thank You</td></tr></table>
	<div style="border:1px solid;background-color:#4CAF50;border-color:#4CAF50;
	height:20px"></div></div></div>';
	//$resultProvider = $this->email
	//	->from('noreply@carspaco.com',''.$title)
	//	->to(''.$providerContactEmail)
	//	->subject("Request ".$suffix." is updated")
	//	->message($bodyProvider)
	//	->send();
	$this->load->library('email');
	$subject ="Request ".$suffix." is updated";
	$this->sendgripEmail($providerContactEmail,$subject,$bodyProvider);	

	}
	$this->load->library('email');
	//$result = $this->email
	//	->from('noreply@carspaco.com',''.$title)
	//	->to(''.$email)
	//	->subject("Request ".$suffix." is updated")
	//	->message($bodyUser)
	//	->send();
	
	$subject ="Request ".$suffix." is updated";
	$this->sendgripEmail($email,$subject,$bodyUser);


	//$resultAdmin = $this->email
	//	->from('noreply@carspaco.com',''.$title)
	//	->to(''.$contactemail)
	//	  ->subject("Request ".$suffix." is updated")
	//	->message($bodyAdmin)
	//	->send();
	$subject ="Request ".$suffix." is updated";
	$this->sendgripEmail($contactemail,$subject,$bodyAdmin);
	return true;

	}

	
	
	function newRequestForPartner_post()
                                        {
	$data=json_decode(file_get_contents('php://input'));
	$this->verifyRequiredParams(array('partnerId'),POST_TYPE);
	$partnerId = $_POST['partnerId'];
	
	$language = LANGUAGE_ENGLISH;
	if (!empty($_POST['language'])){
	 $language = $_POST['language'];
	}
	$this->load->model("ApiPartnerModel","partnerModel");
	$this->load->model("ApiAdminModel","adminModel");
	
	$this->load->model("Utility","utility");

	$operators = $this->partnerModel->getOurPartnerById($partnerId);
        
	$operatorMessage = "";
	$partnerName="";
	$partnerEmail="";
	if ($operators != null) {
	foreach ($operators as $operator) {
	$partnerName=$operator['fullName'];
	$partnerEmail=$operator['email'];
	$operatorMessage = $operatorMessage.'<tr><td>Full Name : '.$operator['fullName'].'</td></tr>';
	$operatorMessage = $operatorMessage.'<tr><td>Email address  : '.$operator['email'].'</td></tr>';
	$operatorMessage = $operatorMessage.'<tr><td>Age   : '.$operator['age'].'</td></tr>';
	$operatorMessage = $operatorMessage.'<tr><td>Nationality   : '.$operator['nationality'].'</td></tr>';
	$operatorMessage = $operatorMessage.'<tr><td>Iqama/National Id/Passport  : '.$operator['nationalId'].'</td></tr>';
	$operatorMessage = $operatorMessage.'<tr><td>Mobile Number : '.$operator['countryCode'].$operator['mobileNo'].'</td></tr>';
	$operatorMessage = $operatorMessage.'<tr><td>Car brand/Year   : '.$operator['year'].'</td></tr>';
	/*$operatorMessage = $operatorMessage.'<tr><td>Covering Country : '.$operator['coveringCountry'].'</td></tr>';*/
	$operatorMessage = $operatorMessage.'<tr><td>Covering City : '.$operator['coveringCity'].'</td></tr>';
	$categories=$operator['providedcategory'];
	
	if ($categories != null) {
	$operatorMessage = $operatorMessage.'<tr><td>Provider Categories : </td></tr>';
	foreach ($categories as $category) {
	$operatorMessage = $operatorMessage.'<tr><td>Category Name : '.$category['name'].'</td></tr>';
	}
	
	}
	$operatorMessage = $operatorMessage.'<tr><td>Proposed new suggestion : '.$operator['suggestion'].'</td></tr>';
	$operatorMessage = $operatorMessage.'<tr><td>UploadedDocuments   : <a href='.$operator['uploaddocumentone'].'><img src="http://carspaco.com/carspaDev/assets/images/document.png" width="10%" height="10%"></a> <a href='.$operator['uploaddocumenttwo'].'><img src="http://carspaco.com/carspaDev/assets/images/document.png" width="10%" height="10%"></a> <a href='.$operator['uploaddocumentthree'].'><img src="http://carspaco.com/carspaDev/assets/images/document.png" width="10%" height="10%"></a></td></tr>';
	}

	}
	//$contacts = $this->adminModel->getCustomerServiceSupport();
        $this->load->model("ApiContactModel","ContactModel");
        $contacts = $this->ContactModel->getContactcountrycode(966);

        $name='CarSpa';
	$title = $name;
	$phoneNumber = "+".$contacts['countryCode'].$contacts['mobileNumber'];
	$contactemail = $contacts['email'];
	$address = $contacts['address'];
$logoUrl = LOGO_URL;
	 $body='<div   style="background-image:  url(http://carspaco.com/carspaDev/assets/images/login-header-image.png);width:100%;background-repeat: no-repeat; background-size: 100%;"> <img src="http://carspaco.com/carspaDev/assets/images/login-header-image.png" style="visibility: hidden;height: auto;width:100%" /></div>


	 <div style="max-width:100%"  align="center"><div style="solid;max-width:60%;" align="center">	
	<br><table align="center" width="100%"><tr><td bgcolor="#FFFFFF">
	 <div align="center"><table border="0" align="center" style="padding:15px"><tr><td width="50%" align="left"><p style="Margin-top: 0;color: #565656;
	font-family: Georgia,serif;font-size: 16px;line-height: 25px;Margin-bottom: 25px;
	color:#000000">Hello CarSpa, </p><p style="font-size:17px;">
	We have new request for join our partner , below are detail of partner
	</p><table style="width:100%" border="0" cellpadding="0"
	 cellspacing="0"><tr style="font-weight:600;"><td width="35%"><u>Partner Detail :- </u>
	 </td></tr><tr style="font-weight:600;"><td width="35%"><br></td></tr>'.$operatorMessage.'</table>
	 <hr style="width:100%;border:1px dashed;border-color:#4CAF50;"><table
	style="width:100%" border="0" cellpadding="0" cellspacing="0"><tr><td width="50%"
	align="left"><p style="Margin-top: 0;color: #565656;font-family: Georgia,serif;
	font-size: 16px;line-height: 25px;Margin-bottom: 25px;color:#8CC540">
	'.$title.' <br><span style="color:#000000;font-size:12px">
	'.$phoneNumber.' <br><span style="color:#000000;font-size:12px">
	'.$contactemail.'<br>'.$address.'</span></p></td>
	<td width="50%" align="right" style="font-size: 24px;line-height: 25px;
	Margin-bottom: 25px;color:#8CC540">Thank You</td></tr></table>
	<div style="border:1px solid;background-color:#4CAF50;border-color:#4CAF50;height:20px"></div></div></div>';
	 
	 $bodyuser="";
	 $subjectuser = 'Join CarSpa Team Request' ;
	 
	
	  $bodyuser='<div style="background-image:url(http://carspaco.com/carspaDev/assets/images/login-header-image.png);width:100%;background-repeat: no-repeat; background-size: 100%;"> <img src="http://carspaco.com/carspaDev/assets/images/login-header-image.png" style="visibility: hidden;height: auto;width:100%" /></div>

	 <div style="max-width:100%"  align="center"><div style="solid;max-width:60%;" align="center">
	<br><table align="center" width="100%"><tr><td bgcolor="#FFFFFF">
	 <div align="center"><table border="0" align="center" style="padding:15px"><tr><td width="50%" align="left"><p style="Margin-top: 0;color: #565656;
	font-family: Georgia,serif;font-size: 16px;line-height: 25px;Margin-bottom: 25px;
	color:#000000">Hello '.$partnerName.' , </p><p style="font-size:17px;">
	Thank you for your Interest to join our Team; Your request is under process and we will come back to you shortly.
	</p> 

	<div style="max-width:100%"  align="right" dir="rtl"><div style="solid;max-width:60%;" align="right">
	<br><table align="right" width="100%"><tr><td bgcolor="#FFFFFF">
	 <div align="right"><table border="0" align="right" style="padding:15px"><tr><td width="50%" align="right"><p style="Margin-top: 0;color: #565656;
	font-family: Georgia,serif;font-size: 16px;line-height: 25px;Margin-bottom: 25px;
	color:#000000" dir="rtl">مرحبا '.$partnerName.'  </p><p style="font-size:17px;" dir="rtl">
	شكرا لرغبتك للانضمام لفريق عملنا: سوف ندرس طلبك و نتواصل معك قريبا .
	</p></td></tr></table></td></tr></table></div></div>


	<hr style="width:100%;border:1px dashed;border-color:#4CAF50;"><table
	style="width:100%" border="0" cellpadding="0" cellspacing="0"><tr><td width="50%"
	align="left"><p style="Margin-top: 0;color: #565656;font-family: Georgia,serif;
	font-size: 16px;line-height: 25px;Margin-bottom: 25px;color:#8CC540">
	'.$title.' <br><span style="color:#000000;font-size:12px">
	'.$phoneNumber.' <br><span style="color:#000000;font-size:12px">
	'.$contactemail.'<br>'.$address.'</span></p></td>
	<td width="50%" align="right" style="font-size: 24px;line-height: 25px;
	Margin-bottom: 25px;color:#8CC540">Thank You</td></tr></table>
	<div style="border:1px solid;background-color:#4CAF50;border-color:#4CAF50;height:20px"></div></div></div>';

	
	
	$this->load->library('email');
	//$result = $this->email->from('noreply@carspaco.com',''.$title)->to(''.$email)->subject('Operator is on the way for request 
                                                                        //                                                                        '.$suffix)->message($body)->send();
	$subject = 'New Request for our partner ' ;
	$email="info@carspaco.com";
	//$email="singhdeepikad2009@gmail.com"
	$emailuser=$partnerEmail;
	//echo "$emailuser".$emailuser;
	$this->sendgripEmail($email,$subject,$body);
	$this->sendgripEmail($emailuser,$subjectuser,$bodyuser);
	return true;

	}


	function noCategoryforServiceMail_post()
        {
	$data=json_decode(file_get_contents('php://input'));
	$this->verifyRequiredParams(array('categoryId','addressId','userId'),POST_TYPE);
	
	$categoryId = $_POST['categoryId'];
	$addressId = $_POST['addressId'];
	$userId = $_POST['userId'];
	
	
	$this->load->model("ApiCategoryModel","CategoryModel");
	$this->load->model("ApiAddressModel","AddressModel");
	$this->load->model("ApiUserModel","UserModel");
	 $this->load->model("ApiContactModel","ContactModel");
	$this->load->model("Utility","utility");

	//$carObj = $this->CarModel->getCarTypeById($carId);
	$categoryObj = $this->CategoryModel->getCategoryById($categoryId);
	$addressObj = $this->AddressModel->getAddressById($addressId);
	$userObj = $this->UserModel->getUserDetail($userId);
	$country=$this->ContactModel->getcountry($userObj['countryCode']);
        
	
	//$contacts = $this->adminModel->getCustomerServiceSupport();
       
        $contacts = $this->ContactModel->getContactcountrycode(966);

        $name='CarSpa';
	$title = $name;
	$phoneNumber = "+".$contacts['countryCode'].$contacts['mobileNumber'];
	$contactemail = $contacts['email'];
	$address = $contacts['address'];
$logoUrl = LOGO_URL;

$subject = 'No service available' ;

	 $body='<div   style="background-image:  url(http://carspaco.com/carspaDev/assets/images/login-header-image.png);width:100%;background-repeat: no-repeat; background-size: 100%;"> <img src="http://carspaco.com/carspaDev/assets/images/login-header-image.png" style="visibility: hidden;height: auto;width:100%" /></div>


	 <div style="max-width:100%"  align="center"><div style="solid;max-width:60%;" align="center">	
	<br><table align="center" width="100%"><tr><td bgcolor="#FFFFFF">
	 <div align="center"><table border="0" align="center" style="padding:15px"><tr><td width="50%" align="left"><p style="Margin-top: 0;color: #565656;
	font-family: Georgia,serif;font-size: 16px;line-height: 25px;Margin-bottom: 25px;
	color:#000000">Hello CarSpa, </p><p style="font-size:17px;">
	 There is no particular Service availale for category "<b>'.$categoryObj['name'].'</b>" for below User
	</p><table style="width:100%" border="0" cellpadding="0"
	 cellspacing="0"><tr style="font-weight:600;"><td width="35%"><u>User Detail :- </u>
	 </td></tr><tr style="font-weight:600;"><td width="35%"><br></td></tr><tr><td>Customer Name : '.$userObj['fullName'].'</td></tr><tr><td>Mobile Number : +'.$userObj['countryCode'].''.$userObj['mobileNumber'].'</td></tr><tr><td>Country: '.$country['name'].'</td></tr><tr><td>Address: '.$addressObj['address'].'</td></tr></table>
	 <hr style="width:100%;border:1px dashed;border-color:#4CAF50;"><table
	style="width:100%" border="0" cellpadding="0" cellspacing="0"><tr><td width="50%"
	align="left"><p style="Margin-top: 0;color: #565656;font-family: Georgia,serif;
	font-size: 16px;line-height: 25px;Margin-bottom: 25px;color:#8CC540">
	'.$title.' <br><span style="color:#000000;font-size:12px">
	'.$phoneNumber.' <br><span style="color:#000000;font-size:12px">
	'.$contactemail.'<br>'.$address.'</span></p></td>
	<td width="50%" align="right" style="font-size: 24px;line-height: 25px;
	Margin-bottom: 25px;color:#8CC540">Thank You</td></tr></table>
	<div style="border:1px solid;background-color:#4CAF50;border-color:#4CAF50;height:20px"></div></div></div>';
	 
	 

	
	
	$this->load->library('email');
	
	
	
	$email=$contactemail;
	//$email="bhadolakrishna@gmail.com";
	//echo "$emailuser".$emailuser;
	$this->sendgripEmail($email,$subject,$body);
	//$this->sendgripEmail($emailuser,$subjectuser,$bodyuser);
	$this->displayDefaultJsonWithoutData(1,"Successfully sent");
	//return true;

	}






/***************************     Unused functions       *************************/



function sendFeedbackEmailToAdmin_post(){

	$data=json_decode(file_get_contents('php://input'));
	$this->verifyRequiredParams(array('userId'),POST_TYPE);
	$userId = $_POST['userId'];

	$this->load->model("ApiUserModel","userModel");
	$this->load->model("ApiAdminModel","adminModel");
	$this->load->model("ApiFeedbackModel","feedbackModel");
	$this->load->model("Utility","utility");
        $this->load->model("ApiContactModel","ContactModel");

	$userName = $this->userModel->getFullName($userId);
	$email = $this->userModel->getEmail($userId);
	$userNumber = $this->userModel->getMobileNumber($userId);
        $countrycode = $this->userModel->getCountryCode($userId);
	$evaluations = $this->feedbackModel->getUserFeedbacks($userId);
	//  $evaluations = $this->serviceModel->getServices();
	$evaluationMessage = "";
	foreach ($evaluations as $evaluation) {
	$evaluationMessage = $evaluationMessage.'<tr><td>-</td><td>'.$evaluation['feedback'].'</td></tr>';
	}
	//$contacts = $this->adminModel->getCustomerServiceSupport();
        $contacts = $this->ContactModel->getContactcountrycode($countrycode);
        $name='CarSpa';
	$title = $name;
	$phoneNumber = "+".$contacts['countryCode'].$contacts['mobileNumber'];
	$contactemail = $contacts['email'];
	$address = $contacts['address'];

$logoUrl = LOGO_URL;
	$body='<div   style="background-image:  url(http://carspaco.com/carspaDev/assets/images/login-header-image.png);width:100%;background-repeat: no-repeat; background-size: 100%;"> <img src="http://carspaco.com/carspaDev/assets/images/login-header-image.png" style="visibility: hidden;height: auto;width:100%" /></div>
	 <div align="center"><table border="0" align="center" style="padding:15px"><tr><td width="50%" align="left"><p style="Margin-top: 0;color: #565656;
	font-family: Georgia,serif;font-size: 16px;line-height: 25px;Margin-bottom: 25px;
	color:#000000">Dear  Admin, </p><p style="font-size:17px;">
	I am '.$userName.' and below is feedback
	</p><table width="100%" border="0" cellpadding="0" cellspacing="0">'.$evaluationMessage.'</table>
	 <hr style="width:100%;border:1px dashed;border-color:#4CAF50;"><table
	style="width:100%" border="0" cellpadding="0" cellspacing="0"><tr><td width="50%"
	align="left"><p style="Margin-top: 0;color: #565656;font-family: Georgia,serif;
	font-size: 16px;line-height: 25px;Margin-bottom: 25px;color:#8CC540">
	'.$userName.' <br><span style="color:#000000;font-size:12px">
	'.$userNumber.' <br><span style="color:#000000;font-size:12px">
	'.$email.'</span></p></td>
	<td width="50%" align="right" style="font-size: 24px;line-height: 25px;
	Margin-bottom: 25px;color:#8CC540">Thank You</td></tr></table>
	<div style="border:1px solid;background-color:#4CAF50;border-color:#4CAF50;height:20px"></div></div></div>';

	//  ->from('support@carspaco.com','CarSpa')
	$this->load->library('email');
	//$result = $this->email
	//	->from('noreply@carspaco.com',''.$title)
	//	->to(''.$contactemail)
	//	->subject($userName.' feedback')
	//	->message($body)
	//	->send();
	$subject = $userName.' feedback';
	$this->sendgripEmail($contactemail,$subject,$body);
	return true;

	}


	public function sendgripEmail($email,$subject,$body){
	$this->config->load('sendgrid', TRUE);
	$email_config = $this->config->item('sendgrid');
	$this->email->initialize($email_config);
	
	$this->email->to($email);
	$this->email->from("noreply@carspaco.com","CarSpa");
	$this->email->subject($subject);
	$this->email->message($body);
	$this->email->send();
	}



	function testEmailDemo_post(){
	$data=json_decode(file_get_contents('php://input'));
	$body = "test email manoj sendgrid test api";
	
	$this->load->library('email');
	
	$email = "bariamanoj@gmail.com";
	
	$subject ='sub hr';
	$body = "test fun";
	$this->sendgripEmail($email,$subject,$body);
	
	
echo $body;
	// loading the config
$this->config->load('sendgrid', TRUE);
$email_config = $this->config->item('sendgrid');
$this->email->initialize($email_config);

$this->email->to($email);
//$this->email->set_category("category_in_sendgrid");
$this->email->from("noreply@carspaco.com");
$this->email->subject("Subject");

$message = "Email here..testapi live server";
$this->email->message($message);
$this->email->send();
	}

}
?>