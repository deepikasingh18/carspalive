<?php

defined('BASEPATH') OR exit('No direct script access allowed');
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class Feedback extends REST_Controller
{
	public function __construct()
	{
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		$method = $_SERVER['REQUEST_METHOD'];
		if($method == "OPTIONS") {
				die();
		}

		  parent::__construct();
      $this->load->library('session');
      $this->load->helper(array('form','url'));
      $this->load->library('form_validation');
			$this->load->library('mylibrary');
	}
	    /* Method to display default error message
	    		 Created By: Manzz Baria
	    	*/
	    	private function displayDefaultJsonWithoutData($status,$message){
	    		if($status == JSON_SUCCESS_STATUS){
	    			$this->response([
	    				'Status' => $status,
	    				'Message' => $message
	    			], REST_Controller::HTTP_OK);
	    		}else{
	    			$this->response([
	    				'Status' => $status,
	    				'Message' => $message
	    			], REST_Controller::HTTP_OK);
	    		}
	    	}

	    	/* Method to display default success with Result data and totals
	    		 Created By: Manzz Baria
	    	*/
	    	private function displayDefaultJsonWithData($status,$mesage,$result,$totalPages,$currentPages){
	    		if($status == JSON_SUCCESS_STATUS){
	    			$this->response([
	    				'Status' => $status,
	    				'Message' => $mesage,
	    				'TotalPage' => $totalPages,
	    				'CurrentPage' => $currentPages,
	    				'Data' => $result
	    			], REST_Controller::HTTP_OK);
	    		}else{
	    			$this->response([
	    				'Status' => $status,
	    				'Message' => $mesage,
	    				'TotalPage' => $totalPages,
	    				'CurrentPage' => $currentPages,
	    				'Data' => $result
	    			], REST_Controller::HTTP_OK);
	    		}
	    	}

	    /* Method to check parameters is null or empty
	        Created By: Manzz Baria
	    */
	    public function verifyRequiredParams($required_fields,$type){
	        $error = false;
	        $error_fields = "";
	        foreach ($required_fields as $field) {
	    			if($type == POST_TYPE){
	    				if ($this->post($field) == null) {
	    	      	$error = true;
	    	        $error_fields .= $field . ', ';
	    	      }
	    	    }else if($type == GET_TYPE){
	    				if ($this->get($field) == null) {
	    	      	$error = true;
	    	        $error_fields .= $field . ', ';
	    	      }
	    	    }else if($type == PUT_TYPE){
	    				if ($this->put($field) == null) {
	    	      	$error = true;
	    	        $error_fields .= $field . ', ';
	    	      }
	    	    }else{
	    				if ($this->delete($field) == null) {
	    	      	$error = true;
	    	        $error_fields .= $field . ', ';
	    	      }
	    			}
	        }
	        if ($error) {
	        	$message = 'Required field(s) ' . substr($error_fields, 0, -2) . ' is missing or empty';
	          $this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$message);
	        }
	      }

				/* Method to show Message
					Created By: Manzz Baria
				*/
				public function showMessage($status,$message){
					$this->response([
						'Status' => $status,
						'Message' => $message
					], REST_Controller::HTTP_OK);
				}

				/* Method to addFeedback
					Created By: Manzz Baria
				*/
				function addFeedback_post(){
					$data=json_decode(file_get_contents('php://input'));
					$this->verifyRequiredParams(array('userId','feedback','name','email','phoneNumber'),POST_TYPE);

					/***** getting params *****/
					$userId = $_POST['userId'];
					$feedback = $_POST['feedback'];
					$name = $_POST['name'];
					$email = $_POST['email'];
					$phoneNumber = $_POST['phoneNumber'];

					$this->load->model("ApiFeedbackModel","feedbackModel");
					$result = $this->feedbackModel->addFeedback($feedback,$userId,$name,$email,$phoneNumber);
					if($result != null){
								$mesage = 'Thank you for your feedback';
								$totalPages = 1;
								$currentPages = 1;
								$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

					}else{
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"Unable to send your feedback");
					}
				}
				/* Method to getFeedbacks
					Created By: Manzz Baria
				*/
				function getFeedbacks_get(){
					$data=json_decode(file_get_contents('php://input'));
					$pageIndex = (int)$this->get('pageIndex');
					if (empty($pageIndex)){
						$pageIndex = 0;
					}
					$this->load->model("ApiFeedbackModel","feedbackModel");
					$result = $this->feedbackModel->getFeedbacks($pageIndex);
					$totalPage = $this->feedbackModel->getTotalPagesOfFeedbacks();
					$totalFeedbacks = $this->feedbackModel->getTotalFeedBack();
					$totalUnreadFeedbacks = $this->feedbackModel->getTotalUnreadFeedBack();
					if($result != null){
								$mesage = 'Found data';
								$totalPages = $totalPage;
								$currentPages = $pageIndex + 1;
								$this->response([
			    				'Status' => JSON_SUCCESS_STATUS,
			    				'Message' => $mesage,
			    				'TotalPage' => $totalPages,
			    				'CurrentPage' => $currentPages,
									'TotalFeedback' => $totalFeedbacks,
									'UnreadFeedback' => $totalUnreadFeedbacks,
			    				'Data' => $result
			    			], REST_Controller::HTTP_OK);

					}else{
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"No data found");
					}
				}

			/* Method to getFeedbackDetail
				Created By: Manzz Baria
			*/
			function getFeedbackDetail_get(){
				$data=json_decode(file_get_contents('php://input'));
				$this->verifyRequiredParams(array('feedbackId'),GET_TYPE);
				$feedbackId = (int)$this->get('feedbackId');

				$this->load->model("ApiFeedbackModel","feedbackModel");
				$result = $this->feedbackModel->getFeedbackDetail($feedbackId);
				if($result != null){
							$mesage = 'Data found';
							$totalPages = 1;
							$currentPages =  1;
							$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

				}else{
						$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"No data found");
				}
			}


}
?>
