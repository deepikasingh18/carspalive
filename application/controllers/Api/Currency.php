<?php

defined('BASEPATH') OR exit('No direct script access allowed');
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class Currency extends REST_Controller
{
	public function __construct()
	{
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		$method = $_SERVER['REQUEST_METHOD'];
		if($method == "OPTIONS") {
				die();
		}
		  parent::__construct();
      $this->load->library('session');
      $this->load->helper(array('form','url'));
      $this->load->library('form_validation');
			$this->load->library('mylibrary');
			 $this->load->database();
	}
	    /* Method to display default error message
	    		 Created By: Manzz Baria
	    	*/
	    	private function displayDefaultJsonWithoutData($status,$message){
	    		if($status == JSON_SUCCESS_STATUS){
	    			$this->response([
	    				'Status' => $status,
	    				'Message' => $message
	    			], REST_Controller::HTTP_OK);
	    		}else{
	    			$this->response([
	    				'Status' => $status,
	    				'Message' => $message
	    			], REST_Controller::HTTP_OK);
	    		}
	    	}

	    	/* Method to display default success with Result data and totals
	    		 Created By: Manzz Baria
	    	*/
	    	private function displayDefaultJsonWithData($status,$mesage,$result,$totalPages,$currentPages){
	    		if($status == JSON_SUCCESS_STATUS){
	    			$this->response([
	    				'Status' => $status,
	    				'Message' => $mesage,
	    				'TotalPage' => $totalPages,
	    				'CurrentPage' => $currentPages,
	    				'Data' => $result
	    			], REST_Controller::HTTP_OK);
	    		}else{
	    			$this->response([
	    				'Status' => $status,
	    				'Message' => $mesage,
	    				'TotalPage' => $totalPages,
	    				'CurrentPage' => $currentPages,
	    				'Data' => $result
	    			], REST_Controller::HTTP_OK);
	    		}
	    	}

	    /* Method to check parameters is null or empty
	        Created By: Manzz Baria
	    */
	    public function verifyRequiredParams($required_fields,$type){
	        $error = false;
	        $error_fields = "";
	        foreach ($required_fields as $field) {
	    			if($type == POST_TYPE){
	    				if ($this->post($field) == null) {
	    	      	$error = true;
	    	        $error_fields .= $field . ', ';
	    	      }
	    	    }else if($type == GET_TYPE){
	    				if ($this->get($field) == null) {
	    	      	$error = true;
	    	        $error_fields .= $field . ', ';
	    	      }
	    	    }else if($type == PUT_TYPE){
	    				if ($this->put($field) == null) {
	    	      	$error = true;
	    	        $error_fields .= $field . ', ';
	    	      }
	    	    }else{
	    				if ($this->delete($field) == null) {
	    	      	$error = true;
	    	        $error_fields .= $field . ', ';
	    	      }
	    			}
	        }
	        if ($error) {
	        	$message = 'Required field(s) ' . substr($error_fields, 0, -2) . ' is missing or empty';
	          $this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$message);
	        }
	      }

				/* Method to show Message
					Created By: Manzz Baria
				*/
				public function showMessage($status,$message){
					$this->response([
						'Status' => $status,
						'Message' => $message
					], REST_Controller::HTTP_OK);
				}

		/*
				Method to listCountry
				Created By: Deepika Merai
		*/
		function listCountry_get()
		{
			$data=json_decode(file_get_contents('php://input'));
					$this->load->model("ApiCurrencyModel","currencyModel");
					
					$countryId=$this->get('countryId');
				
					$result = $this->currencyModel->getCountry($countryId);
					if($result != null)
					{
								$mesage = 'Found data';
								$totalPages = 1;
								$currentPages = 1;
								$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);
					}
					else
					{
						$mesage = 'No Country found';
						$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
					}	
		}
		
		/*
				Method to addContact
				Created By: Deepika Merai
		*/
		
		function addCurrency_post()
		{
					$data=json_decode(file_get_contents('php://input'));
					$this->verifyRequiredParams(array('country','currency','exchangerate'),POST_TYPE);

					/***** getting params *****/
					$country = $_POST['country'];
					$currency = $_POST['currency'];
					$exchangerate = $_POST['exchangerate'];
					
					$language = LANGUAGE_ENGLISH;
					if (!empty($_POST['language'])){
						 $language = $_POST['language'];
					}
					
					$currencyAR = '';
					if (!empty($_POST['currencyAR'])){
						 $currencyAR = $_POST['currencyAR'];
					}
					
					$this->load->model("ApiCurrencyModel","currencyModel");
					$result = $this->currencyModel->addContact($country,$currency,$currencyAR,$exchangerate,$language);
					
					if($result != null){
								$mesage = 'Currency successfully added';
								$totalPages = 1;
								$currentPages = 1;
								$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

					}
					else
					{
						$mesage = 'Unable to add Currency';
						$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
					}
			}
	
			/*
					Method to updateCurrency
					Created By: Deepika Merai
			*/
				function updateCurrency_post()
				{
					$data=json_decode(file_get_contents('php://input'));
					$this->verifyRequiredParams(array('currrencyId','countryId','currency','exchangerate'),POST_TYPE);

					/***** getting params *****/
					$currrencyId = $_POST['currrencyId'];
					$countryId = $_POST['countryId'];
					$curerncy = $_POST['currency'];
					$exchangerate = $_POST['exchangerate'];
					
					$language = LANGUAGE_ENGLISH;
					if (!empty($_POST['language'])){
						 $language = $_POST['language'];
					}
					$currencyAR = '';
					if (!empty($_POST['currencyAR'])){
						 $currencyAR = $_POST['currencyAR'];
					}
					
					$this->load->model("ApiCurrencyModel","currencyModel");
					$result = $this->currencyModel->updateCurrency($currrencyId,$countryId,$curerncy,$currencyAR,$exchangerate,$language);
					if($result != null)
					{
								$mesage = 'Currency Successfully updated';
								$totalPages = 1;
								$currentPages = 1;
								$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

					}
					else
					{
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"Unable to update Currency Detail");
					}
				}
			
			/*
					Method to deleteCurrency
					Created By: Deepika Merai
			*/
			
			function deleteCurrency_post()
			{
					$data=json_decode(file_get_contents('php://input'));
					$this->verifyRequiredParams(array('currencyId'),POST_TYPE);

					/***** getting params *****/
					$currencyId = $_POST['currencyId'];
				
					$this->load->model("ApiCurrencyModel","currencyModel");
					$result = $this->currencyModel->deleteCountryId($currencyId);
					
					if($result)
					{
								$mesage = 'Currency successfully deleted';
								$totalPages = 1;
								$currentPages = 1;
								$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

					}
					else
					{
						$mesage = 'Unable to delete Currency';
						$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
					}
			}

			/*
					Method to getAllCurrency
					Created By: Manzz Baria
			*/

			function getAllCurrency_get(){
					$data=json_decode(file_get_contents('php://input'));
					
					$language = $this->get('language');
					if (empty($language)){
						$language = LANGUAGE_ENGLISH;
					}
					
					$this->load->model("ApiCurrencyModel","currencyModel");
					
					$result = $this->currencyModel->getAllCurrency($language);
					if($result != null){
								$mesage = 'Found data';
								$totalPages = 1;
								$currentPages = 1;
								$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

					}else{
						$mesage = 'No Currency found';
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
					}
			}
			
		/*
			Method to getCurrencyFromCountryCode
			Created By: Manzz Baria
		*/
		function getCurrencyFromCountryCode_get()
		{
			$data=json_decode(file_get_contents('php://input'));
					$this->load->model("ApiCurrencyModel","currencyModel");
					
					$countryCode=$this->get('countryCode');
					if (empty($countryCode)){
						$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"countryCode required");
					}
					
					$result = $this->currencyModel->getCurrencyFromCountryCode($countryCode);
					if($result != null)
					{
								$mesage = 'Found data';
								$totalPages = 1;
								$currentPages = 1;
								$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);
					}
					else
					{
						$mesage = 'No currency found';
						
						$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
					}	
		}
	/*
			Method to getExchangeRateFromCountryCode
			Created By: Manzz Baria
		*/
		function getExchangeRateFromCountryCode_get()
		{
			$data=json_decode(file_get_contents('php://input'));
					$this->load->model("ApiCurrencyModel","currencyModel");
					
					$countryCode=$this->get('countryCode');
					if (empty($countryCode)){
						$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"countryCode required");
					}
				
					$result = $this->currencyModel->getExchangeRateFromCountryCode($countryCode);
					if($result != null)
					{
								$mesage = 'Found data';
								$totalPages = 1;
								$currentPages = 1;
								$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);
					}
					else
					{
						$mesage = 'No exchange rate found';
						$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
					}	
		}



	/*
			Method to getCurrencyAndExchangeRateFromCountryCode
			Created By: Manzz Baria
		*/
		function getCurrencyAndExchangeRateFromCountryCode_get()
		{
			$data=json_decode(file_get_contents('php://input'));
					$this->load->model("ApiCurrencyModel","currencyModel");
					
					$countryCode=$this->get('countryCode');
					if (empty($countryCode)){
						$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"countryCode required");
					}
					
					$result = $this->currencyModel->getCurrencyAndExchangeRateFromCountryCode($countryCode);
					
				
						$currencyId = $this->currencyModel->getCurrencyIdFromCountryCode($countryCode);
						if($currencyId == null)
					{
						$result = $this->currencyModel->getCurrencyAndExchangeRateFromCountryCode("sa");
					}
					if($result != null)
					{
								$mesage = 'Found data';
								$totalPages = 1;
								$currentPages = 1;
								$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);
					}
					else
					{
						$mesage = 'No currency found';
						if ($language == LANGUAGE_ARABIC) {
							$mesage = 'لم يتم العثور على فئة';
						}
						$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
					}	
		}


}
?>
