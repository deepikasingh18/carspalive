<?php

defined('BASEPATH') OR exit('No direct script access allowed');
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class Contact extends REST_Controller
{
	public function __construct()
	{
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		$method = $_SERVER['REQUEST_METHOD'];
		if($method == "OPTIONS") {
				die();
		}
		  parent::__construct();
      $this->load->library('session');
      $this->load->helper(array('form','url'));
      $this->load->library('form_validation');
			$this->load->library('mylibrary');
			$this->load->database();
	}
	    /* Method to display default error message
	    		 Created By: Manzz Baria
	    	*/
	    	private function displayDefaultJsonWithoutData($status,$message){
	    		if($status == JSON_SUCCESS_STATUS){
	    			$this->response([
	    				'Status' => $status,
	    				'Message' => $message
	    			], REST_Controller::HTTP_OK);
	    		}else{
	    			$this->response([
	    				'Status' => $status,
	    				'Message' => $message
	    			], REST_Controller::HTTP_OK);
	    		}
	    	}

	    	/* Method to display default success with Result data and totals
	    		 Created By: Manzz Baria
	    	*/
	    	private function displayDefaultJsonWithData($status,$mesage,$result,$totalPages,$currentPages){
	    		if($status == JSON_SUCCESS_STATUS){
	    			$this->response([
	    				'Status' => $status,
	    				'Message' => $mesage,
	    				'TotalPage' => $totalPages,
	    				'CurrentPage' => $currentPages,
	    				'Data' => $result
	    			], REST_Controller::HTTP_OK);
	    		}else{
	    			$this->response([
	    				'Status' => $status,
	    				'Message' => $mesage,
	    				'TotalPage' => $totalPages,
	    				'CurrentPage' => $currentPages,
	    				'Data' => $result
	    			], REST_Controller::HTTP_OK);
	    		}
	    	}

	    /* Method to check parameters is null or empty
	        Created By: Manzz Baria
	    */
	    public function verifyRequiredParams($required_fields,$type){
	        $error = false;
	        $error_fields = "";
	        foreach ($required_fields as $field) {
	    			if($type == POST_TYPE){
	    				if ($this->post($field) == null) {
	    	      	$error = true;
	    	        $error_fields .= $field . ', ';
	    	      }
	    	    }else if($type == GET_TYPE){
	    				if ($this->get($field) == null) {
	    	      	$error = true;
	    	        $error_fields .= $field . ', ';
	    	      }
	    	    }else if($type == PUT_TYPE){
	    				if ($this->put($field) == null) {
	    	      	$error = true;
	    	        $error_fields .= $field . ', ';
	    	      }
	    	    }else{
	    				if ($this->delete($field) == null) {
	    	      	$error = true;
	    	        $error_fields .= $field . ', ';
	    	      }
	    			}
	        }
	        if ($error) {
	        	$message = 'Required field(s) ' . substr($error_fields, 0, -2) . ' is missing or empty';
	          $this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$message);
	        }
	      }

				/* Method to show Message
					Created By: Manzz Baria
				*/
				public function showMessage($status,$message){
					$this->response([
						'Status' => $status,
						'Message' => $message
					], REST_Controller::HTTP_OK);
				}

				


	/***************************    NEW APIS    ***************************/

				/* Method to addcontact
					Created By: Henal bhandari
				*/
				function addContact_post(){
					$data=json_decode(file_get_contents('php://input'));
					$this->verifyRequiredParams(array('countryId','address','countryCode','mobileNumber','email'),POST_TYPE);

					/***** getting params *****/
					$countryId = $_POST['countryId'];
					$address = $_POST['address'];
					$countryCode = $_POST['countryCode'];
					$mobileNumber = $_POST['mobileNumber'];
				    $email = $_POST['email'];


					 $this->load->model("ApiContactModel","contactModel");
					$result = $this->contactModel->addContact($countryId,$address,$countryCode,$mobileNumber,$email);
					if($result != null){
								$mesage = 'Successfully Added';
								$totalPages = 1;
								$currentPages = 1;
								$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

					}else{
						$mesage = 'Contact already added';
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
					}
				}

				/* Method to updateContact
						Created By: Henal bhandari
				*/
				function updateContact_post(){
						$data=json_decode(file_get_contents('php://input'));
					$this->verifyRequiredParams(array('contactId','countryId','address','countryCode','mobileNumber','email'),POST_TYPE);

					/***** getting params *****/
					$contactId = $_POST['contactId'];
					$countryId = $_POST['countryId'];
					$address = $_POST['address'];
					$countryCode = $_POST['countryCode'];
					$mobileNumber = $_POST['mobileNumber'];
				    $email = $_POST['email'];


					 $this->load->model("ApiContactModel","contactModel");
					$result = $this->contactModel->updateContact($contactId,$countryId,$address,$countryCode,$mobileNumber,$email);
					if($result != null){
								$mesage = 'Successfully updated';
								$totalPages = 1;
								$currentPages = 1;
								$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

					}else{
						$mesage = 'Unable to update contact';
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
					}
				}


			/*
				Method to deletecontact
				Created By: Henal bhandari
			*/
			function deletecontact_post(){
				$data=json_decode(file_get_contents('php://input'));
				$this->verifyRequiredParams(array('contactId'),POST_TYPE);

				/***** getting params *****/
				$contactId = $_POST['contactId'];
				
				$this->load->model("ApiContactModel","contactModel");
				$result = $this->contactModel->deleteContactById($contactId);
				if($result){
							$mesage = 'Contact successfully deleted';
							$totalPages = 1;
							$currentPages = 1;
							$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

				}else{
					$mesage = 'Unable to delete contact';
					$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
				}
			}

			/*
				Method to getAllContacts
				 Created By: Henal bhandari
			*/
			function getAllContacts_get(){
				$data=json_decode(file_get_contents('php://input'));
				$this->load->model("ApiContactModel","contactModel");
				$language = $this->get('language');
					if (empty($language)){
						$language = LANGUAGE_ENGLISH;
					}
				$result = $this->contactModel->getAllContactsbycountry($language);
				if($result != null){
							$mesage = 'Found data';
							$totalPages = 1;
							$currentPages = 1;
							$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

				}else{
				$mesage = 'No contact found';
				$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
				}
			}

                        function getAllCountry_get(){

					$data=json_decode(file_get_contents('php://input'));
					$this->load->model("ApiContactModel","contactModel");
					$language = $this->get('language');
					if (empty($language)){
						$language = LANGUAGE_ENGLISH;
					}
					$result = $this->contactModel->getAllCountry($language);
					if($result != null){
								$mesage = 'Found data';
								$totalPages = 1;
								$currentPages = 1;
								$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

					}else{
						$mesage = 'No country found';
						if ($language == LANGUAGE_ARABIC) {
							$mesage = 'لم يتم العثور على فئة';
						}
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
					}
			}




}
?>
