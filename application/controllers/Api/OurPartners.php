<?php

defined('BASEPATH') OR exit('No direct script access allowed');
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class OurPartners extends REST_Controller
{
	public function __construct()
	{
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		$method = $_SERVER['REQUEST_METHOD'];
		if($method == "OPTIONS") {
				die();
		}
		  parent::__construct();
      $this->load->library('session');
      $this->load->helper(array('form','url'));
      $this->load->library('form_validation');
			$this->load->library('mylibrary');
			 $this->load->database();
	}
	    /* Method to display default error message
	    		 Created By: Manzz Baria
	    	*/
	    	private function displayDefaultJsonWithoutData($status,$message){
	    		if($status == JSON_SUCCESS_STATUS){
	    			$this->response([
	    				'Status' => $status,
	    				'Message' => $message
	    			], REST_Controller::HTTP_OK);
	    		}else{
	    			$this->response([
	    				'Status' => $status,
	    				'Message' => $message
	    			], REST_Controller::HTTP_OK);
	    		}
	    	}

				/* Method to show Message
					Created By: Manzz Baria
				*/
				public function showMessage($status,$message){
					$this->response([
						'Status' => $status,
						'Message' => $message
					], REST_Controller::HTTP_OK);
				}

	    	/* Method to display default success with Result data and totals
	    		 Created By: Manzz Baria
	    	*/
	    	private function displayDefaultJsonWithData($status,$mesage,$result,$totalPages,$currentPages){
	    		if($status == JSON_SUCCESS_STATUS){
	    			$this->response([
	    				'Status' => $status,
	    				'Message' => $mesage,
	    				'TotalPage' => $totalPages,
	    				'CurrentPage' => $currentPages,
	    				'Data' => $result
	    			], REST_Controller::HTTP_OK);
	    		}else{
	    			$this->response([
	    				'Status' => $status,
	    				'Message' => $mesage,
	    				'TotalPage' => $totalPages,
	    				'CurrentPage' => $currentPages,
	    				'Data' => $result
	    			], REST_Controller::HTTP_OK);
	    		}
	    	}

	    /* Method to check parameters is null or empty
	        Created By: Manzz Baria
	    */
	    public function verifyRequiredParams($required_fields,$type){
	        $error = false;
	        $error_fields = "";
	        foreach ($required_fields as $field) {
	    			if($type == POST_TYPE){
	    				if ($this->post($field) == null) {
	    	      	$error = true;
	    	        $error_fields .= $field . ', ';
	    	      }
	    	    }else if($type == GET_TYPE){
	    				if ($this->get($field) == null) {
	    	      	$error = true;
	    	        $error_fields .= $field . ', ';
	    	      }
	    	    }else if($type == PUT_TYPE){
	    				if ($this->put($field) == null) {
	    	      	$error = true;
	    	        $error_fields .= $field . ', ';
	    	      }
	    	    }else{
	    				if ($this->delete($field) == null) {
	    	      	$error = true;
	    	        $error_fields .= $field . ', ';
	    	      }
	    			}
	        }
	        if ($error) {
	        	$message = 'Required field(s) ' . substr($error_fields, 0, -2) . ' is missing or empty';
	          $this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$message);
	        }
	      }

				

				/* Method to customer registration
					Created By: Manzz Baria
				*/
				function addOurPartners_post(){
					$data=json_decode(file_get_contents('php://input'));
					$this->verifyRequiredParams(array('userId','fullName','age','nationality','nationalId','countryCode','mobileNo','year','coveringCity','providedcategoryId','joinDate'),POST_TYPE);

					/***** getting params *****/
					//$todayDate = date("Y-m-d H:i:s");   

					$userId = $_POST['userId'];
					$fullName = $_POST['fullName'];

					$varr = preg_replace("/%u([0-9a-f]{3,4})/i","&#x\\1;",urldecode($fullName)); 
				    $fullName = html_entity_decode($varr,null,'UTF-8');

					$age = $_POST['age'];
					$nationality = $_POST['nationality'];

					$varr = preg_replace("/%u([0-9a-f]{3,4})/i","&#x\\1;",urldecode($nationality)); 
				    $nationality = html_entity_decode($varr,null,'UTF-8');

					$nationalId = $_POST['nationalId'];
					$countryCode = $_POST['countryCode']; 
					$mobileNo = $_POST['mobileNo'];
					$year = $_POST['year'];
					$joinDate = $_POST['joinDate'];
					//$coveringCountry = $_POST['coveringCountry'];
					$coveringCity = $_POST['coveringCity'];

					$varr = preg_replace("/%u([0-9a-f]{3,4})/i","&#x\\1;",urldecode($coveringCity)); 
				    $coveringCity = html_entity_decode($varr,null,'UTF-8');

					$providedcategoryId = $_POST['providedcategoryId'];

					$language = LANGUAGE_ENGLISH;
					if (!empty($_POST['language'])){
						 $language = $_POST['language'];
					}
					$email="";
                                        if(!empty($_POST['email']))
                                        {
                                            $email = $_POST['email'];
                                        }

					$suggestion = "";
					if (!empty($_POST['suggestion'])){
						 $suggestion = $_POST['suggestion'];


						 $varr = preg_replace("/%u([0-9a-f]{3,4})/i","&#x\\1;",urldecode($suggestion)); 
				    	 $suggestion = html_entity_decode($varr,null,'UTF-8');

					}
                                            
                                         $coveringCountry="";
                                         if (!empty($_POST['coveringCountry'])){
						 $coveringCountry = $_POST['coveringCountry'];
					}

					$Document = "";
					$Document1 = "";
					$Document2 = "";


					if(isset($_FILES['documentone']['name'])){
						 $file=$_FILES['documentone']['name'];

						 $image_path='./uploads/documentone/';
						// $thumb_path='./uploads/files/thumb/';
						 $data = $this->uploadFileContent();
						 $filename=$data['upload_data']['file_name'];
						 $videofile=$_FILES["documentone"]["tmp_name"];

						 // Images create thumbnail and upload
						// $imagefile = $filename;
						// $imagefile = $this->generateImageThumbContent($imagefile,$data);
						 $Document = $filename;
					}
					else{
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"Document One is required");
					}

					if(isset($_FILES['documenttwo']['name'])){
						 $file=$_FILES['documenttwo']['name'];

						 $image_path='./uploads/documenttwo/';
						// $thumb_path='./uploads/files/thumb/';
						 $data = $this->uploadFileContentDocument1();
						 $filename=$data['upload_data']['file_name'];
						 $videofile=$_FILES["documenttwo"]["tmp_name"];

						 // Images create thumbnail and upload
						// $imageUrlfile = $filename;
						// $imageUrlfile = $this->generateImageThumbContent($imageUrlfile,$data);
						 $Document1 = $filename;
					}
					else
					{
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"Document Two is required");
					}


					if(isset($_FILES['documentthree']['name'])){
						 $file=$_FILES['documenttwo']['name'];

						 $image_path='./uploads/documentthree/';
						// $thumb_path='./uploads/files/thumb/';
						 $data = $this->uploadFileContentDocument2();
						 $filename=$data['upload_data']['file_name'];
						 $videofile=$_FILES["documenttwo"]["tmp_name"];

						 // Images create thumbnail and upload
						// $imageUrlfile = $filename;
						// $imageUrlfile = $this->generateImageThumbContent($imageUrlfile,$data);
						 $Document2 = $filename;
					}
					else{
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"Document Three is required");
					}
					
					$this->load->model("ApiPartnerModel","PartnerModel");
					
				
						$result = $this->PartnerModel->addOurPartners($userId,$fullName,$email,$age,$nationality,$nationalId,$countryCode,$mobileNo,$year,$joinDate,$coveringCountry,$coveringCity,$providedcategoryId,$suggestion,$Document,$Document1,$Document2,$language);
						
						if($result){
									$mesage = 'Successfully added';
									if ($language == LANGUAGE_ARABIC) {
										$mesage = 'شكراً لاهتمامك للانضمام لفريق عملنا؛لقد استلمنا طلبك و سوف نتصل بك قريباً';
									}
									$totalPages = 1;
									$currentPages = 1;
									$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

						}else{
							$mesage = 'unable to add partner detail';
							if ($language == LANGUAGE_ARABIC) {
								$mesage = 'البريد الإلكتروني أو رقم الجوال مسجل بالفعل ';
							}
								$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
						
					}

				}
				
				public function uploadFileContent(){
					$time = time();
					$new_name = 'carspaDocument_'.$time;
					$config['upload_path'] = './uploads/documentone/';
					$config['allowed_types']        = '*';
					$config['max_size']             = 0;
					//$config['max_width']            = 9000;
					//$config['max_height']           = 8000;
					$config['file_name'] = $new_name;

					$this->load->library('upload', $config);
					$this->upload->initialize($config);
					if(!$this->upload->do_upload('documentone')){
						$error = array('error' => $this->upload->display_errors());
						$msg=$error['error'];
						$status=FALSE;
						$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$msg);
					}else{
						$data = array('upload_data' => $this->upload->data());
						$filename=$data['upload_data']['file_name'];
					}
					$data = $this->upload->data();
					$data=array('upload_data' => $this->upload->data());
					return $data;
				}

				public function uploadFileContentDocument1(){
					$time = time();
					$new_name = 'carspaDocument_'.$time;
					$config['upload_path'] = './uploads/documenttwo/';
					$config['allowed_types']        = '*';
					$config['max_size']             = 0;
					//$config['max_width']            = 9000;
					//$config['max_height']           = 8000;
					$config['file_name'] = $new_name;

					$this->load->library('upload', $config);
					$this->upload->initialize($config);
					if(!$this->upload->do_upload('documenttwo')){
						$error = array('error' => $this->upload->display_errors());
						$msg=$error['error'];
						$status=FALSE;
						$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$msg);
					}else{
						$data = array('upload_data' => $this->upload->data());
						$filename=$data['upload_data']['file_name'];
					}
					$data = $this->upload->data();
					$data=array('upload_data' => $this->upload->data());
					return $data;
				}

				public function uploadFileContentDocument2(){
					$time = time();
					$new_name = 'carspaDocument_'.$time;
					$config['upload_path'] = './uploads/documentthree/';
					$config['allowed_types']        = '*';
					$config['max_size']             = 0;
					//$config['max_width']            = 9000;
					//$config['max_height']           = 8000;
					$config['file_name'] = $new_name;

					$this->load->library('upload', $config);
					$this->upload->initialize($config);
					if(!$this->upload->do_upload('documenttwo')){
						$error = array('error' => $this->upload->display_errors());
						$msg=$error['error'];
						$status=FALSE;
						$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$msg);
					}else{
						$data = array('upload_data' => $this->upload->data());
						$filename=$data['upload_data']['file_name'];
					}
					$data = $this->upload->data();
					$data=array('upload_data' => $this->upload->data());
					return $data;
				}
				


				function getOurPartners_get(){
					$data=json_decode(file_get_contents('php://input'));

					/***** getting params *****/

					$language = $this->get('language');
					if (empty($language)){
						$language = LANGUAGE_ENGLISH;
					}
					
                                       $fromdate = $this->get('fromdate');
					if (empty($fromdate)){
						$fromdate = 0;
					}
					
					$todate = $this->get('todate');
					if (empty($todate)){
						$todate = 0;
					}
					
					$categoryId = $this->get('categoryId');
					if (empty($categoryId)){
						$categoryId = -1;
					}
					
					$country = $this->get('country');
					if (empty($country)){
						$country = -1;
					}
					
					$this->load->model("ApiPartnerModel","PartnerModel");
					
				
						$result = $this->PartnerModel->getOurPartners($language,$fromdate,$todate,$categoryId,$country);
						if($result!=null){
									$mesage = 'Data Found';
									$totalPages = 1;
									$currentPages = 1;
									$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

						}
						else
						{
							$mesage = 'No Data Found';
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
						
					}

				}
				
				function searchOurPartner_get()
				{
					$data=json_decode(file_get_contents('php://input'));

					/***** getting params *****/
					
					$language = $this->get('language');
					if (empty($language)){
						$language = LANGUAGE_ENGLISH;
					}

                    $fromdate = $this->get('fromdate');
					if (empty($fromdate)){
						$fromdate = 0;
					}
					
					$todate = $this->get('todate');
					if (empty($todate)){
						$todate = 0;
					}
					
					$categoryId = $this->get('categoryId');
					if (empty($categoryId)){
						$categoryId = -1;
					}
					
					$country = $this->get('country');
					if (empty($country)){
						$country = -1;
					}
					$name = $this->get('name');	

						$this->load->model("ApiPartnerModel","PartnerModel");

					if($name=="")
			  		 {

							$result = $this->PartnerModel->getOurPartners($language,$fromdate,$todate,$categoryId,$country);
							$mesage = 'Data Found';
							$totalPages = 1;
							$currentPages = 1;
							$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);
						
					  }
					  else
					  {
					  		$result = $this->PartnerModel->searchOurPartners($language,$fromdate,$todate,$categoryId,$country,$name);
							if($result != null)
							{
										$mesage = 'Found data';
										$totalPages = 1;
										$currentPages = 1;
										$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

							}
							else
							{
								$mesage = 'No Partners found';
								$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
							}
					  }
				}
				
				
				function searchpotentialSupplier_get()
				{

					$data=json_decode(file_get_contents('php://input'));
					$this->load->model("ApiVanModel","vanModel");
					$name = $this->get('name');
					
					
					if (empty($suppplierId)){
						$suppplierId = 0;
					}
				
							  if($name=="")
							   {
							   		$result = $this->vanModel->getOwnerList($suppplierId);
									
												$mesage = 'Data found';
												$totalPages = 1;
												$currentPages = 1;
												$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);
							   }
							   else
							   {
							 	 		$result = $this->vanModel->searchSuppliers($name);
										if($result != null)
										{
													$mesage = 'Found data';
													$totalPages = 1;
													$currentPages = 1;
													$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

										}
										else
										{
											$mesage = 'No Supplier found';
											$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
										}
							   }
				}
				
				function getOurPartnerById_get(){
					$data=json_decode(file_get_contents('php://input'));
					$this->verifyRequiredParams(array('partnerId'),GET_TYPE);
					/***** getting params *****/

					$partnerId = $this->get('partnerId');
					$this->load->model("ApiPartnerModel","PartnerModel");
					
				
						$result = $this->PartnerModel->getOurPartnerById($partnerId);
						if($result!=null){
									$mesage = 'Data Found';
									$totalPages = 1;
									$currentPages = 1;
									$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

						}
						else
						{
							$mesage = 'No Data Found';
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
						
					}

				}
				
				function deleteOurpartner_post(){
					$data=json_decode(file_get_contents('php://input'));
					$this->verifyRequiredParams(array('partnerId'),POST_TYPE);

					/***** getting params *****/
					$partnerId = $_POST['partnerId'];

					$this->load->model("ApiPartnerModel","PartnerModel");
					$result = $this->PartnerModel->deleteOurPartnerById($partnerId);
					if($result){
								$mesage = 'Partner successfully deleted';
								$totalPages = 1;
								$currentPages = 1;
								$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

					}else{
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"Unable to delete partner");
					}
				}
		


}
?>
