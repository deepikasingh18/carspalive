<?php

defined('BASEPATH') OR exit('No direct script access allowed');
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class User extends REST_Controller
{
	public function __construct()
	{
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		$method = $_SERVER['REQUEST_METHOD'];
		if($method == "OPTIONS") {
				die();
		}
		  parent::__construct();
      $this->load->library('session');
      $this->load->helper(array('form','url'));
      $this->load->library('form_validation');
			$this->load->library('mylibrary');
			 $this->load->database();
	}
	    /* Method to display default error message
	    		 Created By: Manzz Baria
	    	*/
	    	private function displayDefaultJsonWithoutData($status,$message){
	    		if($status == JSON_SUCCESS_STATUS){
	    			$this->response([
	    				'Status' => $status,
	    				'Message' => $message
	    			], REST_Controller::HTTP_OK);
	    		}else{
	    			$this->response([
	    				'Status' => $status,
	    				'Message' => $message
	    			], REST_Controller::HTTP_OK);
	    		}
	    	}

				/* Method to show Message
					Created By: Manzz Baria
				*/
				public function showMessage($status,$message){
					$this->response([
						'Status' => $status,
						'Message' => $message
					], REST_Controller::HTTP_OK);
				}

	    	/* Method to display default success with Result data and totals
	    		 Created By: Manzz Baria
	    	*/
	    	private function displayDefaultJsonWithData($status,$mesage,$result,$totalPages,$currentPages){
	    		if($status == JSON_SUCCESS_STATUS){
	    			$this->response([
	    				'Status' => $status,
	    				'Message' => $mesage,
	    				'TotalPage' => $totalPages,
	    				'CurrentPage' => $currentPages,
	    				'Data' => $result
	    			], REST_Controller::HTTP_OK);
	    		}else{
	    			$this->response([
	    				'Status' => $status,
	    				'Message' => $mesage,
	    				'TotalPage' => $totalPages,
	    				'CurrentPage' => $currentPages,
	    				'Data' => $result
	    			], REST_Controller::HTTP_OK);
	    		}
	    	}

	    /* Method to check parameters is null or empty
	        Created By: Manzz Baria
	    */
	    public function verifyRequiredParams($required_fields,$type){
	        $error = false;
	        $error_fields = "";
	        foreach ($required_fields as $field) {
	    			if($type == POST_TYPE){
	    				if ($this->post($field) == null) {
	    	      	$error = true;
	    	        $error_fields .= $field . ', ';
	    	      }
	    	    }else if($type == GET_TYPE){
	    				if ($this->get($field) == null) {
	    	      	$error = true;
	    	        $error_fields .= $field . ', ';
	    	      }
	    	    }else if($type == PUT_TYPE){
	    				if ($this->put($field) == null) {
	    	      	$error = true;
	    	        $error_fields .= $field . ', ';
	    	      }
	    	    }else{
	    				if ($this->delete($field) == null) {
	    	      	$error = true;
	    	        $error_fields .= $field . ', ';
	    	      }
	    			}
	        }
	        if ($error) {
	        	$message = 'Required field(s) ' . substr($error_fields, 0, -2) . ' is missing or empty';
	          $this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$message);
	        }
	      }

				/* Method to Upload content
					Created By: Nishit Patel
				*/
				public function uploadFileContent(){
					$time = time();
					$new_name = 'carspa_'.$time;
					$config['upload_path'] = './uploads/files/';
					$config['allowed_types']        = '*';
					$config['max_size']             = 0;
					$config['max_width']            = 9000;
					$config['max_height']           = 8000;
					$config['file_name'] = $new_name;

					$this->load->library('upload', $config);
					$this->upload->initialize($config);
					if(!$this->upload->do_upload('file')){
						$error = array('error' => $this->upload->display_errors());
						$msg=$error['error'];
						$status=FALSE;
						$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$msg);
					}else{
						$data = array('upload_data' => $this->upload->data());
						$filename=$data['upload_data']['file_name'];
					}
					$data = $this->upload->data();
					$data=array('upload_data' => $this->upload->data());
					return $data;
				}
				public function generateImageThumbContent($imagefile,$data){
					$config['image_library'] = 'gd2';
					$config['source_image'] =  $data['upload_data']['full_path'];
					$config['new_image'] =  $data['upload_data']['full_path'].'/thumb/'.$imagefile;
					$config['maintain_ratio'] = FALSE;
					$config['overwrite'] = TRUE;
					$config['allowed_types'] = '*';
					$config['max_size'] = 0;
					$config['width'] = 120;
					$config['height'] = 100;
					$this->load->library('image_lib', $config);
					if($this->image_lib->resize()){}
					return $imagefile;
				}

				/* Method to customer registration
					Created By: Manzz Baria
				*/
				function registration_post(){
					$data=json_decode(file_get_contents('php://input'));
					$this->verifyRequiredParams(array('fullName','email','countryCode','mobileNumber','password','userType'),POST_TYPE);

					/***** getting params *****/
					$userType = 0;
					$fullName = $_POST['fullName'];
				//	$fullName = utf8_decode(urldecode($fullName));

					$varr = preg_replace("/%u([0-9a-f]{3,4})/i","&#x\\1;",urldecode($fullName)); 
				   $fullName = html_entity_decode($varr,null,'UTF-8');

					$email = $_POST['email'];
					$mobileNumber = $_POST['mobileNumber'];
					$countryCode = $_POST['countryCode'];
					$password = $_POST['password'];
					$userType = $_POST['userType'];
				//	$cityId = $_POST['cityId'];
					$cityId = 1;
					$userId=0;

					$language = LANGUAGE_ENGLISH;
					if (!empty($_POST['language'])){
						 $language = $_POST['language'];
					}

					$fileUrl = "";
					if(isset($_FILES['file']['name'])){
						 $file=$_FILES['file']['name'];

						 $image_path='./uploads/files/';
						 $thumb_path='./uploads/files/thumb/';
						 $data = $this->uploadFileContent();
						 $filename=$data['upload_data']['file_name'];
		 				 $videofile=$_FILES["file"]["tmp_name"];

						 // Images create thumbnail and upload
						 $imagefile = $filename;
						 $imagefile = $this->generateImageThumbContent($imagefile,$data);
						 $fileUrl = $filename;
					}

					$ip =  $_SERVER['REMOTE_ADDR'];
					 $requestBody=array("fullName"=>$fullName,"email"=>$email,"countryCode"=>$countryCode,"mobileNumber"=>$mobileNumber,"password"=>$password,"fileUrl"=>$fileUrl);  
                         
                                                $apiName="registrationAPI";

					$this->load->model("ApiUserModel","userModel");
					$this->load->model("ApiCompanyModel","companyModel");
					if ($userType == USER_TYPE_COMPANY) {
						$this->companyModel->addCompany($fullName,$email,$countryCode,$mobileNumber,$password,$fileUrl);
						$result = true;
						$mesage = 'Companies will be delt outside the application and our customer service will contact you for further discussion.';
						$totalPages = 1;
						$currentPages = 1;
						$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

					}else{
						$result = $this->userModel->registration($fullName,$email,$cityId,$countryCode,$mobileNumber,$password,$fileUrl);
						
						if($result){
									$mesage = 'Successfully registered,we have sent activation code via an sms to your registered mobile number';
									if ($language == LANGUAGE_ARABIC) {
										$mesage = ' سجلت بنجاح، لقد قمنا بإرسال رمز التفعيل عن طريق الرسائل القصيرة إلى رقم الجوال المسجل لدينا ';
									}
									$totalPages = 1;
									$currentPages = 1;
									$returnresult=array("Status"=>JSON_SUCCESS_STATUS,"mesage"=>$mesage,"result"=>$result,"totalPages"=>$totalPages,"currentPages"=>$currentPages);  
									$response=$this->userModel->callapilog($userId,$userType,$apiName,$ip,$requestBody,$returnresult);

									$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

						}else{
							$mesage = 'email or mobile number already registered';
							if ($language == LANGUAGE_ARABIC) {
								$mesage = 'البريد الإلكتروني أو رقم الجوال مسجل بالفعل ';
							}
							$returnresult=array("Status"=>JSON_ERROR_STATUS,"mesage"=>$mesage,"result"=>$result);  
							$response=$this->userModel->callapilog($userId,$userType,$apiName,$ip,$requestBody,$returnresult);
								$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
						}
					}

				}


				// Function to get the client IP address
					function get_client_ip() {
					    $ipaddress = '';
					    if (isset($_SERVER['HTTP_CLIENT_IP']))
					        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
					    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
					        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
					    else if(isset($_SERVER['HTTP_X_FORWARDED']))
					        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
					    else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
					        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
					    else if(isset($_SERVER['HTTP_FORWARDED']))
					        $ipaddress = $_SERVER['HTTP_FORWARDED'];
					    else if(isset($_SERVER['REMOTE_ADDR']))
					        $ipaddress = $_SERVER['REMOTE_ADDR'];
					    else
					        $ipaddress = 'UNKNOWN';
					    return $ipaddress;
					}

				/* Method to customer social login
					Created By: Manzz Baria
				*/
				function socialLogin_post(){
					$data=json_decode(file_get_contents('php://input'));
					$this->verifyRequiredParams(array('fullName','socialId','socialType','deviceType','deviceToken'),POST_TYPE);
					/***** getting params *****/
					$fullName = $_POST['fullName'];
					$socialId = $_POST['socialId'];
					$socialType = $_POST['socialType'];
					$deviceType = $_POST['deviceType'];
					$deviceToken = $_POST['deviceToken'];
					$logindatetime= date("Y-m-d h:i:sa");
					$requestBody=null;
					$type=1;
				//	$latitude = $_POST['latitude'];
				//	$longitude = $_POST['longitude'];

					$email="";
					$mobileNumber = "";
					if (!empty($_POST['email'])){
							$email = $_POST['email'];
				  }
					if (!empty($_POST['mobileNumber'])){
							$mobileNumber = $_POST['mobileNumber'];
				  }
					$latitude = 0;
					$longitude = 0;
					if (!empty($_POST['latitude'])){
							$latitude = $_POST['latitude'];
					}
					if (!empty($_POST['longitude'])){
							$longitude = $_POST['longitude'];
					}
					if ($latitude <= 0 || $longitude <= 0) {
						$latitude = VAN_DEFAULT_LATITUDE;
						$longitude = VAN_DEFAULT_LONGITUDE;
					}
					$language = LANGUAGE_ENGLISH;
					if (!empty($_POST['language'])){
						 $language = $_POST['language'];
					}
					$requestBody=array("fullName"=>$fullName,"socialId"=>$socialId,"socialType"=>$socialType,"deviceType"=>$deviceType,"deviceToken"=>$deviceToken); 
					 $ip =  $_SERVER['REMOTE_ADDR'];
					 $apiName="socialLoginApi";

					$this->load->model("ApiUserModel","userModel");
					$this->load->model("ApiDeviceModel","deviceModel");
					$isAlready = $this->userModel->isSocilAlreadyRegister($socialId,$socialType);
					if(!$isAlready){
						$this->userModel->socialRegistration($fullName,$email,$mobileNumber,$socialId,$socialType,$deviceType,$deviceToken);
					}
					$userId = $this->userModel->getUserIdFromSocialId($socialId);
					$this->userModel->updateUserLocation($userId,$latitude,$longitude,$logindatetime);
					$this->deviceModel->addDeviceToken($userId,$deviceToken,$deviceType);
					$this->userModel->updateUserSatus($userId,USER_STATUS_ONLINE);
					$result = $this->userModel->getSocialUserById($socialId);
					$this->userModel->updateLanguage($deviceToken,$language);
					$response=$this->userModel->callapilog($userId,$type,$apiName,$ip,$requestBody,$result);
					if($result != null){
								$mesage = 'Successfully login';
								$totalPages = 1;
								$currentPages = 1;
								$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

					}else{
						$mesage = 'unable to login';
						if ($language == LANGUAGE_ARABIC) {
							$mesage = 'غير قادر على تسجيل الدخول ';
						}
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
					}
				}

				/* Method to customer login
					Created By: Manzz Baria
				*/
				function customerLogin_post(){
					$data=json_decode(file_get_contents('php://input'));
					$this->verifyRequiredParams(array('email','password','deviceType','deviceToken'),POST_TYPE);
					/***** getting params *****/
					$email = $_POST['email'];
					$password = $_POST['password'];
					$deviceType = $_POST['deviceType'];
					$deviceToken = $_POST['deviceToken'];
					$requestBody=null;
					$result=null;
					$latitude = 0;
					$longitude = 0;
					$type=1;
					$mobileNumber="";
					$logindatetime= date("Y-m-d h:i:sa");
                          
                         $requestBody=array("email"=>$email,"mobileNumber"=>$mobileNumber,"password"=>$password,"deviceType"=>$deviceType,"deviceToken"=>$deviceToken);  
                         	$ip =  $_SERVER['REMOTE_ADDR'];
                                                $apiName="customerLoginAPI";

					if (strpos($email, '@') === FALSE) {
						  $email=substr($email,3);
					   }
                                          
					if (!empty($_POST['latitude'])){
							$latitude = $_POST['latitude'];
					}
					if (!empty($_POST['longitude'])){
							$longitude = $_POST['longitude'];
					}
					if ($latitude <= 0 || $longitude <= 0) {
						$latitude = VAN_DEFAULT_LATITUDE;
						$longitude = VAN_DEFAULT_LONGITUDE;
					}
					$language = LANGUAGE_ENGLISH;
					if (!empty($_POST['language'])){
						 $language = $_POST['language'];
					}
					$this->load->model("Utility","util");
					//$this->util->dumpTester('customerLogin',$latitude,$longitude);

					$this->load->model("ApiUserModel","userModel");
					$this->load->model("ApiDeviceModel","deviceModel");
					$isAvailable = $this->userModel->isAccountAvailable($email);
					
					if ($isAvailable) {
							$isActive = $this->userModel->isAccountActive($email);
							if ($isActive) {
										$userId = $this->userModel->getUserIdFromEmail($email);
										$this->util->dumpTester('customerLogin',$latitude,$longitude,$userId);
										$this->deviceModel->addDeviceToken($userId,$deviceToken,$deviceType);
										$this->userModel->updateUserSatus($userId,USER_STATUS_ONLINE);
										$this->userModel->updateUserLocation($userId,$latitude,$longitude,$logindatetime);
										$this->userModel->updateLanguage($deviceToken,$language);
										$result = $this->userModel->loginUser($email,$password);
										$response=$this->userModel->callapilog($userId,$type,$apiName,$ip,$requestBody,$result);
										//echo $response;
										if($result != null){
										
													$mesage = 'Successfully login';
													$totalPages = 1;
													$currentPages = 1;
													$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);
										
										}else{
											$mesage = 'unable to login';
											if ($language == LANGUAGE_ARABIC) {
												$mesage = 'غير قادر على تسجيل الدخول ';
											}
												$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
										}
							}else{
								$mesage = 'Your account is not activated yet, to active your account check your registered email account';
								if ($language == LANGUAGE_ARABIC) {
									$mesage = 'لم يتم تنشيط الحساب الخاص بك حتى الآن، لتنشيط حسابك تحقق من حساب بريدك الإلكتروني';
								}
								$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
							}
					}else{
						$mesage = 'Invalid email or password';
						if ($language == LANGUAGE_ARABIC) {
							$mesage = 'البريد الإلكتروني أو كلمة السر خاطئة ';
						}
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
					}

				}
				
				/* Method to customer login
					Created By: Manzz Baria
				*/
				function signout_get(){
					$data=json_decode(file_get_contents('php://input'));
					$this->verifyRequiredParams(array('userId'),GET_TYPE);
					/***** getting params *****/
					$userId= $_GET['userId'];
					
					
					$this->load->model("ApiDeviceModel","deviceModel");
					$this->load->model("ApiUserModel","userModel");
					$this->userModel->updateUserSatus($userId,USER_STATUS_OFFLINE);
					$result = $this->deviceModel->deleteDeviceToken($userId);
					if($result){
					   $mesage = 'Successfully singout';
					   $totalPages = 1;
					   $currentPages = 1;
					   $this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

					}else{
					  $mesage = 'unable to signout';
	  				  $this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
					}
							
				}		

				/* Method to changePassword
					Created By: Manzz Baria
				*/
				function changePassword_post(){
					$data=json_decode(file_get_contents('php://input'));
					$this->verifyRequiredParams(array('userId','oldPassword','newPassword'),POST_TYPE);

					/***** getting params *****/
					$userId = $_POST['userId'];
					$oldPassword = $_POST['oldPassword'];
					$newPassword = $_POST['newPassword'];
					$language = LANGUAGE_ENGLISH;
					if (!empty($_POST['language'])){
						 $language = $_POST['language'];
					}
					$this->load->model("ApiUserModel","userModel");
					$result = $this->userModel->changePassword($userId,$oldPassword,$newPassword);
					if($result){
								$mesage = 'Successfully changed password';
								if ($language == LANGUAGE_ARABIC) {
									$mesage = 'تم تغيير كلمة السر بنجاح ';
								}
								$totalPages = 1;
								$currentPages = 1;
								$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

					}else{
						$mesage = 'Password mismatch';
						if ($language == LANGUAGE_ARABIC) {
							$mesage = 'عدم تطابق كلمه المرور ';
						}
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
					}
				}

				/* Method to customer registration
					Created By: Manzz Baria
				*/
				function updateProfile_post(){
					$data=json_decode(file_get_contents('php://input'));
					$this->verifyRequiredParams(array('userId'),POST_TYPE);

					/***** getting params *****/
					$fullName = "";;
					$email = "";
					$mobileNumber = "";
					$countryCode = "";
					$type="";

						
          				
          			$this->load->model("ApiUserModel","userModel");

					$userId = $_POST['userId'];
					$language = LANGUAGE_ENGLISH;
					if (!empty($_POST['language'])){
						 $language = $_POST['language'];
					}

					if (!empty($_POST['fullName'])){
						 $fullName = $_POST['fullName'];
					}
					if (!empty($_POST['email'])){
						 $email = $_POST['email'];
						$isEmailOwn = $this->userModel->isUserEmailIsOwn($email,$userId);
						if (!$isEmailOwn) {
							$isEmailRegistered = $this->userModel->isUserEmailAlreadyRegister($email);
 						 if($isEmailRegistered){
							 $mesage = 'Email already registered';
							if ($language == LANGUAGE_ARABIC) {
								$mesage = 'البريد الإلكتروني مسجل مسبقا ';
							}
								$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
 						 }
						}
					}
					if (!empty($_POST['mobileNumber'])){
						 	$mobileNumber = $_POST['mobileNumber'];
							 if (empty($_POST['countryCode'])){
		 							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"countryCode required");
									return;
							 }
						 $countryCode = $_POST['countryCode'];
						 $isOwnNumber = $this->userModel->isUserMobileNumberIsOwn($mobileNumber,$userId);
						 if (!$isOwnNumber) {
							 $isMobileNumberRegistered = $this->userModel->isUserMobileNumberAlreadyRegister($mobileNumber);
  						 if($isMobileNumberRegistered){
								 $mesage = 'Mobile number already registered';
		 						if ($language == LANGUAGE_ARABIC) {
		 							$mesage = ' تم تسجيل رقم الجوال سابقا ';
		 						}
  								$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
  						 }
						 }
					}

					$fileUrl = "";
					if(isset($_FILES['file']['name'])){
						 $file=$_FILES['file']['name'];

						 $image_path='./uploads/files/';
						 $thumb_path='./uploads/files/thumb/';
						 $data = $this->uploadFileContent();
						 $filename=$data['upload_data']['file_name'];
						 $videofile=$_FILES["file"]["tmp_name"];

						 // Images create thumbnail and upload
						 $imagefile = $filename;
						 $imagefile = $this->generateImageThumbContent($imagefile,$data);
						 $fileUrl = $filename;

					}

					 $requestBody=array("fullName"=>$fullName,"email"=>$email,"countryCode"=>$countryCode,"mobileNumber"=>$mobileNumber,"fileUrl"=>$fileUrl); 
                      $apiName="updateProfileApi";
                      $ip =  $_SERVER['REMOTE_ADDR'];

                     $varr = preg_replace("/%u([0-9a-f]{3,4})/i","&#x\\1;",urldecode($fullName)); 
				     $fullName = html_entity_decode($varr,null,'UTF-8');


					$result = $this->userModel->updateProfile($userId,$fullName,$email,$countryCode,$mobileNumber,$fileUrl);
					if($result){
										$mesage = 'Profile successfully updated';
										if ($language == LANGUAGE_ARABIC) {
				 							$mesage = ' تم تحديث الملف الشخصي بنجاح ';
				 						}
										$totalPages = 1;
										$currentPages = 1;
										$returnresult=array("Status"=>JSON_SUCCESS_STATUS,"mesage"=>$mesage,"result"=>$result,"totalPages"=>$totalPages,"currentPages"=>$currentPages);  
										$response=$this->userModel->callapilog($userId,$type,$apiName,$ip,$requestBody,$returnresult);
										$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

					}else{
						$mesage = 'Unable to update profile, please try again later';
						if ($language == LANGUAGE_ARABIC) {
							$mesage = ' غير قادر على تحديث الملف الشخصي، يرجى المحاولة مرة أخرى في وقت لاحق  ';
						}
						$returnresult=array("Status"=>JSON_ERROR_STATUS,"mesage"=>$mesage,"result"=>$result);  
						$response=$this->userModel->callapilog($userId,$type,$apiName,$ip,$requestBody,$returnresult);
									$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
					}

				}

		/* Method to getCustomers
			Created By: Manzz Baria
		*/
		function getCustomers_get(){
			$data=json_decode(file_get_contents('php://input'));
			$this->verifyRequiredParams(array('isActive','blocked'),GET_TYPE);
			$isActive = (int)$this->get('isActive');
			$blocked = (boolean)$this->get('blocked');
			$pageIndex = (int)$this->get('pageIndex');
			if (empty($pageIndex)){
				$pageIndex = 0;
			}
			$language = $this->get('language');
			if (empty($language)){
				$language = LANGUAGE_ENGLISH;
			}
			$this->load->model("ApiUserModel","userModel");
			$result = $this->userModel->getCustomers($pageIndex,$isActive,$blocked);
			$totalPage = $this->userModel->getTotalPagesOfCustomers($isActive,$blocked);
			if($result != null){
						$mesage = 'Data found';
						$totalPages = $totalPage;
						$currentPages = $pageIndex + 1;
						$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

			}else{
				$mesage = 'No customer found';
				if ($language == LANGUAGE_ARABIC) {
					$mesage = ' لم يتم العثور على العميل ';
				}
				$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);

			}
		}


		function getUserList_get()
		{
			$data=json_decode(file_get_contents('php://input'));
			$this->load->model("ApiUserModel","userModel");
			$result = $this->userModel->getuserlist();
			
			if($result != null){
						$mesage = 'Data found';
						$totalPages = 1;
						$currentPages = 1;
						$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

			}else{
				$mesage = 'No customer found';
				$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);

			}

		}
                 
               	function getCustomersbyCountry_get(){
			$data=json_decode(file_get_contents('php://input'));
			$this->verifyRequiredParams(array('isActive','blocked','type','countrycode'),GET_TYPE);
			$isActive = (int)$this->get('isActive');
			$blocked = (boolean)$this->get('blocked');
			$pageIndex = (int)$this->get('pageIndex');
			$type = (int)$this->get('type');
			$countrycode = (int)$this->get('countrycode');
			
			if (empty($pageIndex)){
				$pageIndex = 0;
			}
			$language = $this->get('language');
			if (empty($language)){
				$language = LANGUAGE_ENGLISH;
			}
			$this->load->model("ApiUserModel","userModel");
			$result = $this->userModel->getCustomersbyCountry($pageIndex,$isActive,$blocked,$type,$countrycode);
		//	$totalPage = $this->userModel->getTotalPagesOfCustomers($isActive,$blocked);
			if($result != null){
						$mesage = 'Data found';
						$totalPages = 1; //$totalPage;
						$currentPages = $pageIndex + 1;
						$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

			}else{
				$mesage = 'No User found';
				if ($language == LANGUAGE_ARABIC) {
					$mesage = ' لم يتم العثور على العملاء ';
				}
				$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);

			}
		}

		/* Method to verifyOTP
			Created By: Manzz Baria
		*/
		function verifyOTP_post(){
			$data=json_decode(file_get_contents('php://input'));
			$this->verifyRequiredParams(array('mobileNumber','OTPCode','deviceType','deviceToken'),POST_TYPE);
			/***** getting params *****/
			$mobileNumber = $_POST['mobileNumber'];
			$OTPCode = $_POST['OTPCode'];
			$deviceType = $_POST['deviceType'];
			$deviceToken = $_POST['deviceToken'];
			$language = LANGUAGE_ENGLISH;
			$logindatetime= date("Y-m-d h:i:sa");
			if (!empty($_POST['language'])){
				 $language = $_POST['language'];
			}
			$latitude = 0;
			$longitude = 0;
			if (!empty($_POST['latitude'])){
					$latitude = $_POST['latitude'];
			}
			if (!empty($_POST['longitude'])){
					$longitude = $_POST['longitude'];
			}
			if ($latitude <= 0 || $longitude <= 0) {
				$latitude = VAN_DEFAULT_LATITUDE;
				$longitude = VAN_DEFAULT_LONGITUDE;
			}

		  $this->load->model("ApiUserModel","userModel");
			$this->load->model("ApiDeviceModel","deviceModel");
			$isAvailable = $this->userModel->isAccountAvailable($mobileNumber);
			if ($isAvailable) {
								$userId = $this->userModel->getUserIdFromEmail($mobileNumber);
								$this->userModel->updateUserLocation($userId,$latitude,$longitude,$logindatetime);
								$this->deviceModel->addDeviceToken($userId,$deviceToken,$deviceType);
								$this->userModel->updateUserSatus($userId,USER_STATUS_ONLINE);
								$result = $this->userModel->verifyOTP($mobileNumber,$OTPCode);
								if($result != null){

											$mesage = 'Successfully';
											$totalPages = 1;
											$currentPages = 1;
											$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

								}else{
									$mesage = 'Invalid verification code';
									if ($language == LANGUAGE_ARABIC) {
										$mesage = 'رمز التحقق غير صالح ';
									}
										$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
								}
			}else{
				$mesage = ' Your account is not available';
				if ($language == LANGUAGE_ARABIC) {
					$mesage = 'حسابك غير متوفر ';
				}
					$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
			}

		}

		/*
		  Method to askAgainActivationCode
			Created By: Manzz Baria
		*/
		function askAgainActivationCode_post(){
			$data=json_decode(file_get_contents('php://input'));
			$this->verifyRequiredParams(array('mobileNumber'),POST_TYPE);
			/***** getting params *****/
			$mobileNumber = $_POST['mobileNumber'];
			$language = LANGUAGE_ENGLISH;
			if (!empty($_POST['language'])){
				 $language = $_POST['language'];
			}

			$this->load->model("ApiSMSModel","sMSModel");
			$result = $this->sMSModel->sendOTPSMSByMobileNumber($mobileNumber);
			if($result){
						$mesage = 'We have sent an sms with code to your phone number';
						if ($language == LANGUAGE_ARABIC) {
							$mesage = 'لقد قمنا بإرسال رسالة نصية قصيرة مع الرمز لرقم الهاتف الخاص بك ';
						}
						$totalPages = 1;
						$currentPages = 1;
						$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

			}else{
				$mesage = 'Unable to send a one time SMS message';
				if ($language == LANGUAGE_ARABIC) {
					$mesage = ' غير قادر على إرسال رسالة SMS لمرة واحدة ';
				}
					$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
			}


		}

		/*
			Method to forgetPassword
			Created By: Manzz Baria
		*/
		function forgetPassword_post(){
			$data=json_decode(file_get_contents('php://input'));
			$this->verifyRequiredParams(array('email'),POST_TYPE);
			/***** getting params *****/
			$email = $_POST['email'];
			$language = LANGUAGE_ENGLISH;
			if (!empty($_POST['language'])){
				 $language = $_POST['language'];
			}
			
			   if (strpos($email, '@') === FALSE) {
					$email=substr($email,3);
				}

			$this->load->model("ApiUserModel","userModel");
			$isRegi = $this->userModel->isUserEmailAlreadyRegister($email);
			if($isRegi){
			
				$emailAddress = $email;
				 if (strpos($email, '@') === FALSE) {
				 	$emailAddress = $this->userModel->getEmailFromMobileNumber($email);
				 }
				$result = $this->userModel->forgetPassword($emailAddress);
				if($result){
							$mesage = 'We have sent an email with reset password link to your registered email address';
							if ($language == LANGUAGE_ARABIC) {
								$mesage = ' لقد قمنا بإرسال رسالة بريد إلكتروني مع وصلة إعادة تعيين كلمة المرور إلى عنوان البريد الإلكتروني الخاص بك ';
							}
							$totalPages = 1;
							$currentPages = 1;
							$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

				}else{
					$mesage = 'Unable to send email';
					if ($language == LANGUAGE_ARABIC) {
						$mesage = ' غير قادر على إرسال البريد الإلكتروني ';
					}
						$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
				}
			}else{
				$mesage = 'Email address or mobile number is not registered';
				if ($language == LANGUAGE_ARABIC) {
					$mesage = 'البريد الاكتروني او رقم الجوال غير مسجل لدينا';
				}
					$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
			}
		}

		/* Method to reset password
		Created By: Manzz Baria
	*/
	function resetPassword_post(){
			$data=json_decode(file_get_contents('php://input'));
			$this->verifyRequiredParams(array('validationToken','accessToken','newPassword'),POST_TYPE);

			/***** getting params *****/
			$validationToken = $_POST['validationToken'];
			$accessToken = $_POST['accessToken'];
			$newPassword = $_POST['newPassword'];
			$this->load->model("ApiUserModel","userModel");
			$isSeted = $this->userModel->resetPassword($validationToken,$accessToken,$newPassword);
			if (!$isSeted) {
				$this->showMessage("0","Session expired");
			}else{
				$this->showMessage("1","Password successfully reset");
			}
}



		/* Method to testLogin
			Created By: Manzz Baria
		*/
		function testLogin_post(){
			$data=json_decode(file_get_contents('php://input'));
			$this->verifyRequiredParams(array('userId'),POST_TYPE);
			/***** getting params *****/
			$userId = $_POST['userId'];
		//	$latitude = $_POST['latitude'];
		//	$longitude = $_POST['longitude'];
      $this->load->model("ApiSMSModel","sMSModel");
			$this->load->model("ApiUserModel","userModel");
	/*		$isAvailable = $this->userModel->isAccountAvailable($email);
			if ($isAvailable) {
					$isActive = $this->userModel->isAccountActive($email);
					if ($isActive) {
					*/
								$result = $this->sMSModel->sendOTPSMS($userId);
								if($result){
											$mesage = 'Successfully login';
											$totalPages = 1;
											$currentPages = 1;
											$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

								}else{
										$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"unable to login");
								}
			/*		}else{
						$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"Your account is not activated yet, to active your account check your registered email account");
					}
			}else{
					$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"Invalid email or password");
			}
			*/

		}


		/* Method to getCustomers
			Created By: Manzz Baria
		*/
		function sendEmailTest_get(){
			$data=json_decode(file_get_contents('php://input'));

			$this->load->model("ApiEmailModel","emailModel");
			$result = $this->emailModel->fixAppointmentConfirmation(168);
			if($result != null){
						$mesage = 'Data found';
						$totalPages = 1;
						$currentPages =  1;
						$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

			}else{
					$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"No data found");
			}
		}

		/* Method to updateLanguage
			Created By: Deepika Merai
		*/
		function updateLanguage_post()
		{
			$data=json_decode(file_get_contents('php://input'));
			$this->verifyRequiredParams(array('deviceToken','languageCode'),POST_TYPE);

					/***** getting params *****/
					$deviceToken = $_POST['deviceToken'];
					$languagecode = $_POST['languageCode'];
					$userId="";
					$type="";
					$apiName="updateLanguageApi";
					$ip =  $_SERVER['REMOTE_ADDR'];
					$requestBody=array("deviceToken"=>$deviceToken,"languagecode"=>$languagecode);

					$this->load->model("ApiUserModel","userModel");
					$result = $this->userModel->updateLanguage($deviceToken,$languagecode);
					$response=$this->userModel->callapilog($userId,$type,$apiName,$ip,$requestBody,$result);

					if($result != null){
								$mesage = 'Successfully updated';
								$totalPages = 1;
								$currentPages = 1;
								$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

					}
					else
					{
					 	    $mesage = 'Unable to update Langugae';
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
					}
		}

			/* Method to updateLanguage
			Created By: Deepika Merai
		*/
		function callnotifi_get()
		{
					/***** getting params *****/
					$appointmentId = $this->get('appointmentId');
					$status = $this->get('status');
					$reason = $this->get('reason');
					$serviceId = $this->get('serviceId');

					$this->load->model("ApiNotificationModel","NotificationModel");
					$result = $this->NotificationModel->sendNotificationOperaterToUserForRequestAction($appointmentId,$status,$reason,$serviceId);
					if($result != null){
								$mesage = 'Successfully send';
								$totalPages = 1;
								$currentPages = 1;
								$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

					}
					else
					{
					 	    $mesage = 'Unable to send';
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
					}
		}
		function SearchCustomers_get()
		{
			$data=json_decode(file_get_contents('php://input'));
			
			$isActive = (int)$this->get('isActive');
			$blocked = (boolean)$this->get('blocked');
			$name = $this->get('name');

			if(empty($pageIndex))
			{
				$pageIndex = 0;
			}
			
			$this->load->model("ApiUserModel","userModel");

			if($name=="")
			{
					$result = $this->userModel->getCustomers($pageIndex,$isActive,$blocked);
					$totalPage = $this->userModel->getTotalPagesOfCustomers($isActive,$blocked);
					$mesage = 'Data found';
					$totalPages = $totalPage;
					$currentPages = $pageIndex + 1;
					$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);
			}
			else
			{
					$result = $this->userModel->searchUser($isActive,$blocked,$name);
					if($result != null)
					{
							$mesage = 'Found data';
							$totalPages = 1;
							$currentPages = 1;
							$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

					}
					else
					{
							$mesage = 'No customer found';
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
					}
			}
		}
		
		


		
}
?>
