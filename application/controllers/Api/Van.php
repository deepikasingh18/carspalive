<?php

defined('BASEPATH') OR exit('No direct script access allowed');
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class Van extends REST_Controller
{
	public function __construct()
	{
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		$method = $_SERVER['REQUEST_METHOD'];
		if($method == "OPTIONS") {
				die();
		}
		  parent::__construct();
      $this->load->library('session');
      $this->load->helper(array('form','url'));
      $this->load->library('form_validation');
			$this->load->library('mylibrary');
$this->load->database();
	}
	    /* Method to display default error message
	    		 Created By: Manzz Baria
	    	*/
	    	private function displayDefaultJsonWithoutData($status,$message){
	    		if($status == JSON_SUCCESS_STATUS){
	    			$this->response([
	    				'Status' => $status,
	    				'Message' => $message
	    			], REST_Controller::HTTP_OK);
	    		}else{
	    			$this->response([
	    				'Status' => $status,
	    				'Message' => $message
	    			], REST_Controller::HTTP_OK);
	    		}
	    	}

	    	/* Method to display default success with Result data and totals
	    		 Created By: Manzz Baria
	    	*/
	    	private function displayDefaultJsonWithData($status,$mesage,$result,$totalPages,$currentPages){
	    		if($status == JSON_SUCCESS_STATUS){
	    			$this->response([
	    				'Status' => $status,
	    				'Message' => $mesage,
	    				'TotalPage' => $totalPages,
	    				'CurrentPage' => $currentPages,
	    				'Data' => $result
	    			], REST_Controller::HTTP_OK);
	    		}else{
	    			$this->response([
	    				'Status' => $status,
	    				'Message' => $mesage,
	    				'TotalPage' => $totalPages,
	    				'CurrentPage' => $currentPages,
	    				'Data' => $result
	    			], REST_Controller::HTTP_OK);
	    		}
	    	}

	    /* Method to check parameters is null or empty
	        Created By: Manzz Baria
	    */
	    public function verifyRequiredParams($required_fields,$type){
	        $error = false;
	        $error_fields = "";
	        foreach ($required_fields as $field) {
	    			if($type == POST_TYPE){
	    				if ($this->post($field) == null) {
	    	      	$error = true;
	    	        $error_fields .= $field . ', ';
	    	      }
	    	    }else if($type == GET_TYPE){
	    				if ($this->get($field) == null) {
	    	      	$error = true;
	    	        $error_fields .= $field . ', ';
	    	      }
	    	    }else if($type == PUT_TYPE){
	    				if ($this->put($field) == null) {
	    	      	$error = true;
	    	        $error_fields .= $field . ', ';
	    	      }
	    	    }else{
	    				if ($this->delete($field) == null) {
	    	      	$error = true;
	    	        $error_fields .= $field . ', ';
	    	      }
	    			}
	        }
	        if ($error) {
	        	$message = 'Required field(s) ' . substr($error_fields, 0, -2) . ' is missing or empty';
	          $this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$message);
	        }
	      }

		/* Method to show Message
			Created By: Manzz Baria
		*/
		public function showMessage($status,$message){
			$this->response([
				'Status' => $status,
				'Message' => $message
			], REST_Controller::HTTP_OK);
		}

		/* Method to updateVan
			Created By: Manzz Baria
		*/
		function updateVan_post(){
			$data=json_decode(file_get_contents('php://input'));
			$this->verifyRequiredParams(array('vanId'),POST_TYPE);

			$vanName = "";
			$plate="";
			$carType="";
			$ownerId = "";
			$type = 0;
			$name = "";
			$email = "";
			$mobileNumber = "";
			$address = "";
			$countryCode= "";

	    $vanId = $_POST['vanId'];
			$language = 'en';
			if (!empty($_POST['language'])){
				 $language = $_POST['language'];
			}

			if (!empty($_POST['plateNo'])){
				 $plate = $_POST['plateNo'];
			}
			
			if (!empty($_POST['carType'])){
				 $carType = $_POST['carType'];
			}
			
			if (!empty($_POST['vanName'])){
				 $vanName = $_POST['vanName'];
			}

			if (!empty($_POST['type'])){
				 $type = $_POST['type'];
			}
			if (!empty($_POST['ownerId'])){
				 $ownerId = $_POST['ownerId'];
			}
		/*	if ($type == VAN_TYPE_THIRDPARTY) {

					if (!empty($_POST['ownerId'])){
						 $ownerId = $_POST['ownerId'];
					}
					if ($ownerId == 0) {
						$this->verifyRequiredParams(array('name','email','countryCode','mobileNumber','address'),POST_TYPE);
						if (!empty($_POST['name'])){
							 $name = $_POST['name'];
						}
						if (!empty($_POST['email'])){
							 $email = $_POST['email'];
						}
						if (!empty($_POST['countryCode'])){
							 $countryCode = $_POST['countryCode'];
						}
						if (!empty($_POST['mobileNumber'])){
							 $mobileNumber = $_POST['mobileNumber'];
						}
						if (!empty($_POST['address'])){
							 $address = $_POST['address'];
						}
					}
			}
			*/
			//
			$this->load->model("ApiVanModel","vanModel");

			if ($vanName != "") {
					$isAdded = $this->vanModel->isVanAlreadyAddedORItsOwnVan($vanId,$vanName);
					if ($isAdded) {
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"Van already added with same name");
					}
			}
				$result = $this->vanModel->updatedVan($vanId,$vanName,$plate,$carType,$type,$ownerId);
				if($result != null){
							$mesage = 'Van successfully updated';
							$totalPages = 1;
							$currentPages = 1;
							$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

				}else{
						$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"Unable to update van");
				}


    }

		/*
			Method to addProvider
			Created By: Manzz Baria
		*/
	function addProvider_post(){

			 $data=json_decode(file_get_contents('php://input'));
			 $this->verifyRequiredParams(array('name','email','city','country','countryCode','mobileNumber','address','salesPerson','maxDistance'),POST_TYPE);

			 $name = $_POST['name'];
			 $email = $_POST['email'];
			 $city = $_POST['city'];
			 $country = $_POST['country'];
			 $countryCode = $_POST['countryCode'];
			 $mobileNumber = $_POST['mobileNumber'];
			 $address = $_POST['address'];
			 $salesPerson = $_POST['salesPerson'];
			 $maxDistance = $_POST['maxDistance'];
			 $countryCodeTwo = "";
			 $mobileNumberTwo = "";
			 $password="";
			 $language = 'en';
			 if (!empty($_POST['language'])){
					$language = $_POST['language'];
			 }

			 if (!empty($_POST['countryCodeTwo'])){
					$countryCodeTwo = $_POST['countryCodeTwo'];
			 }
			 
			 if (!empty($_POST['password'])){
					$password = $_POST['password'];
			 }
			 if (!empty($_POST['mobileNumberTwo'])){
					$mobileNumberTwo = $_POST['mobileNumberTwo'];
			 }
				$fileUrl = "";
			if(isset($_FILES['file']['name'])){
				 $file=$_FILES['file']['name'];

				 $image_path='./uploads/files/';
				 $thumb_path='./uploads/files/thumb/';
				 $data = $this->uploadFileContent();
				 $filename=$data['upload_data']['file_name'];
				 $videofile=$_FILES["file"]["tmp_name"];

				 // Images create thumbnail and upload
				 $imagefile = $filename;
				 $imagefile = $this->generateImageThumbContent($imagefile,$data);
				 $fileUrl = $filename;
			}else{
					$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"icon is required");
			}
			$this->load->model("ApiVanModel","vanModel");
			$result = $this->vanModel->addOwner($name,$email,$password,$city,$country,$countryCode,$mobileNumber,$countryCodeTwo,$mobileNumberTwo,$salesPerson,$maxDistance,$address,$fileUrl);
			if($result != null){
						$mesage = 'Provider successfully added';
						$totalPages = 1;
						$currentPages = 1;
						$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

			}else{
					$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"Unable to add provider");
			}
 }


				/* Method to Upload content
					Created By: Nishit Patel
				*/
				public function uploadFileContent(){
					$time = time();
					$new_name = 'carspa_'.$time;
					$config['upload_path'] = './uploads/files/';
					$config['allowed_types']        = '*';
					$config['max_size']             = 0;
					$config['max_width']            = 9000;
					$config['max_height']           = 8000;
					$config['file_name'] = $new_name;

					$this->load->library('upload', $config);
					$this->upload->initialize($config);
					if(!$this->upload->do_upload('file')){
						$error = array('error' => $this->upload->display_errors());
						$msg=$error['error'];
						$status=FALSE;
						$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$msg);
					}else{
						$data = array('upload_data' => $this->upload->data());
						$filename=$data['upload_data']['file_name'];
					}
					$data = $this->upload->data();
					$data=array('upload_data' => $this->upload->data());
					return $data;
				}
				public function generateImageThumbContent($imagefile,$data){
					$config['image_library'] = 'gd2';
					$config['source_image'] =  $data['upload_data']['full_path'];
					$config['new_image'] =  $data['upload_data']['full_path'].'/thumb/'.$imagefile;
					$config['maintain_ratio'] = FALSE;
					$config['overwrite'] = TRUE;
					$config['allowed_types'] = '*';
					$config['max_size'] = 0;
					$config['width'] = 120;
					$config['height'] = 100;
					$this->load->library('image_lib', $config);
					if($this->image_lib->resize()){}
					return $imagefile;
				}

 /*
	 Method to updateProvider
	 Created By: Manzz Baria
 */
function updateProvider_post(){
		$data=json_decode(file_get_contents('php://input'));
		$this->verifyRequiredParams(array('ownerId'),POST_TYPE);

		$ownerId = $_POST['ownerId'];
		$name = "";
		$email = "";
		$city = "";
		$country = "";
		$countryCode= "";
		$mobileNumber = "";
		$address = "";
		$password="";
		$salesPerson ="";
		$maxDistance ="";
		$countryCodeTwo = "";
		$mobileNumberTwo = "";
		$language = 'en';
		if (!empty($_POST['language'])){
			 $language = $_POST['language'];
		}

		if (!empty($_POST['name'])){
			 $name = $_POST['name'];
		}
		if (!empty($_POST['email'])){
			 $email = $_POST['email'];
		}
		if (!empty($_POST['city'])){
			 $city = $_POST['city'];
		}
		if (!empty($_POST['country'])){
			 $country = $_POST['country'];
		}
		if (!empty($_POST['countryCode'])){
			 $countryCode = $_POST['countryCode'];
		}
		if (!empty($_POST['mobileNumber'])){
			 $mobileNumber = $_POST['mobileNumber'];
		}
		if (!empty($_POST['address'])){
			 $address = $_POST['address'];
		}
		if (!empty($_POST['salesPerson'])){
			 $salesPerson = $_POST['salesPerson'];
		}
		if (!empty($_POST['maxDistance'])){
			 $maxDistance = $_POST['maxDistance'];
		}
		if (!empty($_POST['countryCodeTwo'])){
			 $countryCodeTwo = $_POST['countryCodeTwo'];
		}
		if (!empty($_POST['mobileNumberTwo'])){
			 $mobileNumberTwo = $_POST['mobileNumberTwo'];
		}
		
		if (!empty($_POST['password'])){
			 $password = $_POST['password'];
		}
		$fileUrl = "";
		if(isset($_FILES['file']['name'])){
			 $file=$_FILES['file']['name'];

			 $image_path='./uploads/files/';
			 $thumb_path='./uploads/files/thumb/';
			 $data = $this->uploadFileContent();
			 $filename=$data['upload_data']['file_name'];
			 $videofile=$_FILES["file"]["tmp_name"];

			 // Images create thumbnail and upload
			 $imagefile = $filename;
			 $imagefile = $this->generateImageThumbContent($imagefile,$data);
			 $fileUrl = $filename;
		}
	 $this->load->model("ApiVanModel","vanModel");
	 $result = $this->vanModel->updatedProvider($ownerId,$name,$email,$password,$city,$country,$countryCode,$mobileNumber,$address,$salesPerson,$maxDistance,$countryCodeTwo,$mobileNumberTwo,$fileUrl);
	 if($result != null){
				 $mesage = 'Provider successfully updated';
				 $totalPages = 1;
				 $currentPages = 1;
				 $this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

	 }else{
			 $this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"Unable to update provider");
	 }
}


		/* Method to addVan
			Created By: Manzz Baria
		*/
		function addVan_post(){
			$data=json_decode(file_get_contents('php://input'));
			$this->verifyRequiredParams(array('vanName','plateNo','carType','type','ownerId','serviceIds','operatorIds'),POST_TYPE);

			/***** getting params *****/
			$vanName = $_POST['vanName'];
			$plateNo = $_POST['plateNo'];
			$carType = $_POST['carType'];
			$type = $_POST['type'];
			$serviceIds = $_POST['serviceIds'];
			$operatorIds = $_POST['operatorIds'];
			$ownerId = $_POST['ownerId'];
			$language = 'en';
			if (!empty($_POST['language'])){
				 $language = $_POST['language'];
			}

			$name = "";
			$email = "";
			$mobileNumber = "";
			$address = "";
			$countryCode= "";

			$this->load->model("ApiVanModel","vanModel");
			$result = $this->vanModel->addVan($vanName,$plateNo,$carType,$type,$serviceIds,$operatorIds,$ownerId);
			if($result != null){
						$mesage = 'Van successfully added';
						$totalPages = 1;
						$currentPages = 1;
						$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

			}else{
					$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"Van already added with same name");
			}
		}
		/* Method to getVanList
			Created By: Manzz Baria
		*/
			function getVanList_get(){
				$data=json_decode(file_get_contents('php://input'));
			
				$pageIndex = (int)$this->get('pageIndex');
				if (empty($pageIndex)){
					$pageIndex = 0;
				}
				$language = $this->get('language');
				if (empty($language)){
					$language = 'en';
				}
				$this->load->model("ApiVanModel","vanModel");
				$result = $this->vanModel->getVanList($pageIndex);
				$totalPage = $this->vanModel->getTotalPagesForVanList();
				if ($result != null) {
							$mesage = 'Data found';
							$totalPages = $totalPage;
							$currentPages = $pageIndex + 1;
							$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);
					}else{
					$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"No van found");
				//	return;
				}
			}
			
			
			function searchVan_get()
			{
						$data=json_decode(file_get_contents('php://input'));
					//	$this->verifyRequiredParams(array('electraId'),GET_TYPE);
						$name = $this->get('name');
						
						$this->load->model("ApiVanModel","vanModel");

						$pageIndex = (int)$this->get('pageIndex');
						
						if (empty($pageIndex))
						{	
							$pageIndex = 0;
						}
				
					   
					   if($name=="")
					   {
					   		$result = $this->vanModel->getVanList($pageIndex);
							$totalPage = $this->vanModel->getTotalPagesForVanList();
										$mesage = 'Data found';
										$totalPages = $totalPage;
										$currentPages = $pageIndex + 1;
										$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);
					   }
					   else
					   {
					 	 		$result = $this->vanModel->searchVan($name);
								if($result != null)
								{
											$mesage = 'Found data';
											$totalPages = 1;
											$currentPages = 1;
											$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

								}
								else
								{
									$mesage = 'No Van found';
									$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
								}
					 	}

				}


			function searchSuppliers_get()
			{

				$data=json_decode(file_get_contents('php://input'));
					$this->load->model("ApiVanModel","vanModel");
					$name = $this->get('name');
					
					
					if (empty($suppplierId)){
						$suppplierId = 0;
					}
				
							  if($name=="")
							   {
							   		$result = $this->vanModel->getOwnerList($suppplierId);
									
												$mesage = 'Data found';
												$totalPages = 1;
												$currentPages = 1;
												$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);
							   }
							   else
							   {
							 	 		$result = $this->vanModel->searchSuppliers($name);
										if($result != null)
										{
													$mesage = 'Found data';
													$totalPages = 1;
													$currentPages = 1;
													$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

										}
										else
										{
											$mesage = 'No Supplier found';
											$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
										}
							   }

				}
			
				/* Method to getVanList
			Created By: Manzz Baria
		*/
			function getVanListByServices_get(){
				$data=json_decode(file_get_contents('php://input'));
				$this->verifyRequiredParams(array('serviceIds'),GET_TYPE);
				$serviceIds = $this->get('serviceIds');
				$language = $this->get('language');
				if (empty($language)){
					$language = 'en';
				}
				$this->load->model("ApiVanModel","vanModel");
				$result = $this->vanModel->getVanListByServices($serviceIds);
				if ($result != null) {
							$mesage = 'Data found';
							$totalPages = 1;
							$currentPages = 1;
							$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);
					}else{
					$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"No van found");
				//	return;
				}
			}

			function getOutStandingCost_get()
			{
				$data=json_decode(file_get_contents('php://input'));
				$this->verifyRequiredParams(array('userId'),GET_TYPE);
				$userId = $this->get('userId');
			
				$this->load->model("ApiRequestModel","RequestModel");
				$result = $this->RequestModel->getgetOutStandingCostByUserId($userId);
				if ($result != null) {
							$mesage = 'Data found';
							$totalPages = 1;
							$currentPages = 1;
							$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);
					}else{
					$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"No Cost Available");
				//	return;
				}
			}

				function getOutStandingAllOperators_get()
			{
				$data=json_decode(file_get_contents('php://input'));
				
			
				$this->load->model("ApiRequestModel","RequestModel");
				$result = $this->RequestModel->getOutStandingAmountForAllOperator();
				if ($result != null) {
							$mesage = 'Data found';
							$totalPages = 1;
							$currentPages = 1;
							$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);
					}else{
					$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"No Cost Available");
				//	return;
				}
			}


			function getOutStandingCostBysupplier_get()
			{
				$data=json_decode(file_get_contents('php://input'));
				$this->verifyRequiredParams(array('supplierId'),GET_TYPE);
				$supplierId = $this->get('supplierId');
			
				$this->load->model("ApiRequestModel","RequestModel");
				$result = $this->RequestModel->getOutStandingCostBySupplierId($supplierId);
				if ($result != null) {
							$mesage = 'Data found';
							$totalPages = 1;
							$currentPages = 1;
							$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);
					}else{
					$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"No Cost Available");
				//	return;
				}
			}


					/***** getting params *****/
				
			function insertCostMargin_POST()
			{
				$data=json_decode(file_get_contents('php://input'));
				$this->verifyRequiredParams(array('servicedetailId','marginDetail'),POST_TYPE);
					$marginDetail = $_POST['marginDetail'];
					$servicedetailId = $_POST['servicedetailId'];
					
			
				$this->load->model("ApiVanModel","vanModel");
				$result = $this->vanModel->insertcostmargin($servicedetailId,$marginDetail);

				if ($result!=null) {
							$mesage = 'Data Inserted Successfully';
							$totalPages = 1;
							$currentPages = 1;
							$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);
					}else{
					$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"Not able to insert");
				//	return;
				}
			}

			function insertOperatorReceivedCost_post()
			{
				    $operatorId = "";
				    $supplierId = "";
					$operatorCost = "";
					$operatorDescription = "";
					$operatorPaymentRecivedDate="";
					//$language = 'en';
	
					$request = file_get_contents('php://input');
					$input = json_decode($request);
					
					if($input != null)
					{
							$request = null;
	
							if(!property_exists($input, 'supplierId') || $input->supplierId == null){
								$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"supplierId is required");
							}
							if(!property_exists($input, 'Operatordataarray') || $input->Operatordataarray == null ){
								$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"Operatordataarray is required");
							}
							$supplierId = $input->supplierId;
							$Operatordataarrays = $input->Operatordataarray;
	
							$this->load->model("ApiVanModel","vanModel");
							if ($Operatordataarrays != null) {
								// Check validation
									foreach($Operatordataarrays as $operatordataarray)
									{
	
											if(!property_exists($operatordataarray, 'operatorId') || $operatordataarray->operatorId == null)
											{
												$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"operatorId is required");
											}
											else
											{
												$operatorId = $operatordataarray->operatorId;
											}
											if(!property_exists($operatordataarray, 'operatorCost') || $operatordataarray->operatorCost == null)
											{
												$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"operatorCost is required");
											}
											else
											{
												$operatorCost = $operatordataarray->operatorCost;
											}

											if(!property_exists($operatordataarray, 'operatorDescription') || $operatordataarray->operatorDescription == null){
												$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"operatorDescription is required");
											}
											else
											{
												$operatorDescription = $operatordataarray->operatorDescription;
											}
											if(!property_exists($operatordataarray, 'operatorPaymentRecivedDate') || $operatordataarray->operatorPaymentRecivedDate == null){
													$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"operatorPaymentRecivedDate is required");
											}
											else
											{
													$operatorPaymentRecivedDate = $operatordataarray->operatorPaymentRecivedDate;
													
											}
									}
								}
						}
						else
						{
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"operator details is required");
						}	

						//print_r($Operatordataarrays);
								$result = $this->vanModel->insertOperatorReceivedCost($supplierId,$Operatordataarrays);
								if($result){
											$mesage = 'Successfully Added';
											$totalPages = 1;
											$currentPages = 1;
											$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

								}else{
										$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"Unable to add");
								}

			}

			function insertReceivedAmount_POST()
			{
				$data=json_decode(file_get_contents('php://input'));
				$this->verifyRequiredParams(array('supplierId','amount'),POST_TYPE);
					$supplierId = $_POST['supplierId'];
					$amount = $_POST['amount'];
					
			
				$this->load->model("ApiVanModel","vanModel");
				$result = $this->vanModel->insertReceivedAmount($supplierId,$amount);

				if ($result!=null) {
							$mesage = 'Data Inserted Successfully';
							$totalPages = 1;
							$currentPages = 1;
							$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);
					}else{
					$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"Not able to insert");
				//	return;
				}
			}
			
					/* Method to getVanListBySupplierId
			Created By: Manzz Baria
		*/
			function getVanListBySupplierIdForServices_get(){
				$data=json_decode(file_get_contents('php://input'));
				$this->verifyRequiredParams(array('supplierId','serviceIds'),GET_TYPE);
				
				$language = $this->get('language');
				if (empty($language)){
					$language = 'en';
				}
				
				$supplierId = $this->get('supplierId');
				$serviceIds = $this->get('serviceIds');
				
				$this->load->model("ApiVanModel","vanModel");
				$result = $this->vanModel->getVanListBySupplierIdForServices($supplierId,$serviceIds);
				
				if ($result != null) {
							$mesage = 'Data found';
							$totalPages = 1;
							$currentPages = 1;
							$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);
					}else{
					$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"No van found");
				//	return;
				}
			}



			function getSupplierByServiceId_get(){
				$data=json_decode(file_get_contents('php://input'));
				$this->verifyRequiredParams(array('serviceId'),GET_TYPE);

				$serviceId = $this->get('serviceId');
				
				$servicedetailId = $this->get('servicedetailId');
			//	echo $servicedetailId;
				if (empty($servicedetailId)){
					$servicedetailId = 0 ;
				}

				$this->load->model("ApiVanModel","vanModel");
				$result = $this->vanModel->getSupplierByServiceId($serviceId,$servicedetailId);
				
				if ($result != null) {
							$mesage = 'Data found';
							$totalPages = 1;
							$currentPages = 1;
							$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);
					}else{
					$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"No Supplier found");
				//	return;
				}
			}


			/* Method to getOwnerList
				Created By: Manzz Baria
			*/
				function getOwnerList_get(){
					$data=json_decode(file_get_contents('php://input'));
					$this->load->model("ApiVanModel","vanModel");
					$language = $this->get('language');
					$suppplierId = $this->get('suppplierId');
					
					if (empty($suppplierId)){
						$suppplierId = 0;
					}
					
					if (empty($language)){
						$language = 'en';
					}
					$result = $this->vanModel->getOwnerList($suppplierId);
					if ($result != null) {
								$mesage = 'Data found';
								$totalPages = 1;
								$currentPages = 1;
								$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);
						}else{
						$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"No provider found");
					//	return;
					}
				}

				function getOperatorBySupplier_get(){
					$data=json_decode(file_get_contents('php://input'));
					$this->verifyRequiredParams(array('suppplierId'),GET_TYPE);
					$this->load->model("ApiVanModel","vanModel");
				
					$suppplierId = $this->get('suppplierId');
					
					if (empty($suppplierId)){
						$suppplierId = 0;
					}
					
					if (empty($language)){
						$language = 'en';
					}
					$result = $this->vanModel->getOperatorBySupplier($suppplierId);
					if ($result != null) {
								$mesage = 'Data found';
								$totalPages = 1;
								$currentPages = 1;
								$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);
						}else{
						$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"No Operator found");
					//	return;
					}
				}
				/* Method to blockVanProvider
					Created By: Manzz Baria
				*/
				function blockVanProvider_post(){
					$data=json_decode(file_get_contents('php://input'));
					$this->verifyRequiredParams(array('ownerId'),POST_TYPE);

					/***** getting params *****/
					$ownerId = $_POST['ownerId'];

					$this->load->model("ApiVanModel","vanModel");
					$result = $this->vanModel->blockVanProvider($ownerId);
					if($result){
								$mesage = 'Provider successfully deleted';
								$totalPages = 1;
								$currentPages = 1;
								$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

					}else{
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"Unable to delete provider");
					}
				}

			/* Method to blockVan
				Created By: Manzz Baria
			*/
			function blockVan_post(){
				$data=json_decode(file_get_contents('php://input'));
				$this->verifyRequiredParams(array('vanId'),POST_TYPE);

				/***** getting params *****/
				$vanId = $_POST['vanId'];

				$this->load->model("ApiVanModel","vanModel");
				$result = $this->vanModel->blockVan($vanId);
				if($result){
							$mesage = 'Van successfully deleted';
							$totalPages = 1;
							$currentPages = 1;
							$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

				}else{
						$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"Unable to delete the van");
				}
			}

			/* Method to removeServiceFromVanById
				Created By: Manzz Baria
			*/
			function removeServiceFromVanById_post(){
				$data=json_decode(file_get_contents('php://input'));
				$this->verifyRequiredParams(array('serviceId','vanId'),POST_TYPE);

				/***** getting params *****/
				$serviceId = $_POST['serviceId'];
				$vanId = $_POST['vanId'];

				$this->load->model("ApiVanModel","vanModel");
				$result = $this->vanModel->removeServiceFromVanById($serviceId,$vanId);
				if($result){
							$mesage = 'Service successfully removed from the van';
							$totalPages = 1;
							$currentPages = 1;
							$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

				}else{
						$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"Unable to remove service from van");
				}
			}

			/* Method to removeOperatorFromVanById
				Created By: Manzz Baria
			*/
			function removeOperatorFromVanById_post(){
				$data=json_decode(file_get_contents('php://input'));
				$this->verifyRequiredParams(array('vanOperatorId'),POST_TYPE);

				/***** getting params *****/
				$vanOperatorId = $_POST['vanOperatorId'];

				$this->load->model("ApiVanModel","vanModel");
				$result = $this->vanModel->removeOperatorFromVanById($vanOperatorId);
				if($result){
							$mesage = 'Operator successfully removed from van';
							$totalPages = 1;
							$currentPages = 1;
							$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

				}else{
						$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"Unable to remove operator from the van");
				}
			}

			/* Method to addServiceToVan
				Created By: Manzz Baria
			*/
			function addServiceToVan_post(){
				$data=json_decode(file_get_contents('php://input'));
				$this->verifyRequiredParams(array('vanId','serviceIds'),POST_TYPE);

				/***** getting params *****/
				$vanId = $_POST['vanId'];
				$serviceIds = $_POST['serviceIds'];

				$this->load->model("ApiVanModel","vanModel");
				$result = $this->vanModel->addServicesToVan($vanId,$serviceIds);
				if($result){
							$mesage = 'Service successfully added to the van';
							$totalPages = 1;
							$currentPages = 1;
							$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

				}else{
						$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"Unable to add service to the van");
				}
			}

			/* Method to addOperatorToVan
				Created By: Manzz Baria
			*/
			function addOperatorToVan_post(){
				$data=json_decode(file_get_contents('php://input'));
				$this->verifyRequiredParams(array('vanId','operatorId'),POST_TYPE);

				/***** getting params *****/
				$vanId = $_POST['vanId'];
				$operatorIds = $_POST['operatorId'];

				$this->load->model("ApiVanModel","vanModel");
				$result = $this->vanModel->addOperatorsToVan($vanId,$operatorIds);
				if($result){
							$mesage = 'Operator successfully added to the van';
							$totalPages = 1;
							$currentPages = 1;
							$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

				}else{
						$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"Unable to add operator to the van");
				}
			}


			/* Method to getRemainVanServices
				Created By: Manzz Baria
			*/
				function getRemainVanServices_get(){
					$data=json_decode(file_get_contents('php://input'));
					$this->verifyRequiredParams(array('vanId'),GET_TYPE);
					$vanId = (int)$this->get('vanId');
					$language = $this->get('language');
					if (empty($language)){
						$language = 'en';
					}

					$this->load->model("ApiServiceModel","serviceModel");
					$result = $this->serviceModel->getRemainServicesByVanId($vanId);
					if ($result != null) {
								$mesage = 'Data found';
								$totalPages = 1;
								$currentPages =  1;
								$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);
						}else{
						$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"No service found");
					//	return;
					}
				}

				/* Method to getRemainVanServices
					Created By: Manzz Baria
				*/
					function getRemainVanOperators_get(){
						$data=json_decode(file_get_contents('php://input'));
						$this->verifyRequiredParams(array('vanId'),GET_TYPE);
						$vanId = (int)$this->get('vanId');
						$language = $this->get('language');
						if (empty($language)){
							$language = 'en';
						}

						$this->load->model("ApiVanOperatorModel","vanOperatorModel");
						$result = $this->vanOperatorModel->getRemainVanOperators($vanId);
						if ($result != null) {
									$mesage = 'Data found';
									$totalPages = 1;
									$currentPages =  1;
									$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);
							}else{
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"No operator found");
						//	return;
						}
					}

					/* Method to getNearByVan
						Created By: Manzz Baria
					*/
					function getNearByVan_post(){
						$data=json_decode(file_get_contents('php://input'));
						$this->verifyRequiredParams(array('latitude','longitude'),POST_TYPE);


						/***** getting params *****/
						$latitude = $_POST['latitude'];
						$longitude = $_POST['longitude'];
						$language = LANGUAGE_ENGLISH;
						$userId="";
						$userType="";


						if (!empty($_POST['language'])){
							 $language = $_POST['language'];
						}

						$apiName="getNearByVanApi";
						$ip =  $_SERVER['REMOTE_ADDR'];
						$requestBody=array("latitude"=>$latitude,"longitude"=>$longitude,"language"=>$language);

						$this->load->model("ApiVanModel","vanModel");
						$this->load->model("ApiUserModel","userModel");
						$result = $this->vanModel->getNearByVanList($latitude,$longitude);

						$response=$this->userModel->callapilog($userId,$userType,$apiName,$ip,$requestBody,$result);

						if($result != null){
									$mesage = 'Data found';
									$totalPages = 1;
									$currentPages = 1;
									$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

						}else{
							$mesage = 'No van found';
							if ($language == LANGUAGE_ARABIC) {
								$mesage = ' لم يجد شاحنة ';
							}
								$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
						}
					}

			/*
				Method to addProviderContact
				Created By: Manzz Baria
			*/
		 function addProviderContact_post(){

				 $data=json_decode(file_get_contents('php://input'));
				 $this->verifyRequiredParams(array('vanOwnerId','fullName','email','title','countryCode','mobileNumber','isPrimary'),POST_TYPE);

				 $vanOwnerId = $_POST['vanOwnerId'];
				 $fullName = $_POST['fullName'];
				 $email = $_POST['email'];
				 $title = $_POST['title'];
				 $countryCode = $_POST['countryCode'];
				 $mobileNumber = $_POST['mobileNumber'];
				 $isPrimary = $_POST['isPrimary'];
				 $countryCodeTwo = "";
				 $mobileNumberTwo = "";
				 $language = 'en';
				 if (!empty($_POST['language'])){
						$language = $_POST['language'];
				 }

				 if (!empty($_POST['countryCodeTwo'])){
						$countryCodeTwo = $_POST['countryCodeTwo'];
				 }
				 if (!empty($_POST['mobileNumberTwo'])){
						$mobileNumberTwo = $_POST['mobileNumberTwo'];
				 }

				$this->load->model("ApiVanModel","vanModel");
				$result = $this->vanModel->addProviderContact($vanOwnerId,$fullName,$email,$title,$countryCode,$mobileNumber,$countryCodeTwo,$mobileNumberTwo,$isPrimary);
				if($result != null){
							$mesage = 'Contact person successfully added';
							$totalPages = 1;
							$currentPages = 1;
							$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

				}else{
						$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"Unable to add contact person");
				}
	 }

	 /* Method to getProviderContacts
		 Created By: Manzz Baria
	 */
		 function getProviderContacts_get(){
			 $data=json_decode(file_get_contents('php://input'));
			 $this->verifyRequiredParams(array('vanOwnerId'),GET_TYPE);
			 $vanOwnerId = (int)$this->get('vanOwnerId');
			 $language = $this->get('language');
			 if (empty($language)){
			 	$language = 'en';
			 }
			 $this->load->model("ApiVanModel","vanModel");
			 $result = $this->vanModel->getProviderContactsByProviderId($vanOwnerId);
			 if ($result != null) {
						 $mesage = 'Data found';
						 $totalPages = 1;
						 $currentPages =  1;
						 $this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);
				 }else{
				 $this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"No provider contact person found");
			 //	return;
			 }
		 }

		 /*
		 	Method to updateProviderContacts
		 	Created By: Manzz Baria
		 */
		 function updateProviderContacts_post(){
		 	 $data=json_decode(file_get_contents('php://input'));
		 	 $this->verifyRequiredParams(array('providerContactId'),POST_TYPE);

		 	 $providerContactId = $_POST['providerContactId'];
		 	 $fullName = "";
		 	 $email = "";
		 	 $title = "";
		 	 $countryCode= "";
		 	 $mobileNumber = "";
		 	 $countryCodeTwo = "";
		 	 $mobileNumberTwo = "";
			 $isPrimary = "";
			 $language = 'en';
			 if (!empty($_POST['language'])){
					$language = $_POST['language'];
			 }

		 	 if (!empty($_POST['fullName'])){
		 			$fullName = $_POST['fullName'];
		 	 }
		 	 if (!empty($_POST['email'])){
		 			$email = $_POST['email'];
		 	 }
		 	 if (!empty($_POST['title'])){
		 			$title = $_POST['title'];
		 	 }
		 	 if (!empty($_POST['country'])){
		 			$country = $_POST['country'];
		 	 }
		 	 if (!empty($_POST['countryCode'])){
		 			$countryCode = $_POST['countryCode'];
		 	 }
		 	 if (!empty($_POST['mobileNumber'])){
		 			$mobileNumber = $_POST['mobileNumber'];
		 	 }

		 	 if (!empty($_POST['countryCodeTwo'])){
		 			$countryCodeTwo = $_POST['countryCodeTwo'];
		 	 }
		 	 if (!empty($_POST['mobileNumberTwo'])){
		 			$mobileNumberTwo = $_POST['mobileNumberTwo'];
		 	 }
			 if (isset($_POST['isPrimary'])){
			 	   $isPrimary = $_POST['isPrimary'];
			 }

		 	$this->load->model("ApiVanModel","vanModel");
		 	$result = $this->vanModel->updateProviderContacts($providerContactId,$fullName,$email,$title,$countryCode,$mobileNumber,$countryCodeTwo,$mobileNumberTwo,$isPrimary);
		 	if($result != null){
		 				$mesage = 'Contact person successfully updated';
		 				$totalPages = 1;
		 				$currentPages = 1;
		 				$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

		 	}else{
		 			$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"Unable to update contact person");
		 	}
		 }

		 /* Method to deleteProviderContact
	 		Created By: Manzz Baria
	 	*/
	 	function deleteProviderContact_post(){
	 		$data=json_decode(file_get_contents('php://input'));
	 		$this->verifyRequiredParams(array('providerContactId'),POST_TYPE);

	 		/***** getting params *****/
	 		$providerContactId = $_POST['providerContactId'];

	 		$this->load->model("ApiVanModel","vanModel");
	 		$result = $this->vanModel->deleteProviderContact($providerContactId);
	 		if($result){
	 					$mesage = 'Provider contact person successfully deleted';
	 					$totalPages = 1;
	 					$currentPages = 1;
	 					$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

	 		}else{
	 				$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"Unable to delete provider contact person");
	 		}
	 	}


/**************   REPORT   **************/

/* Method to getSupplierReport
	Created By: Manzz Baria
*/
	function getSupplierReport_get(){
		$data=json_decode(file_get_contents('php://input'));
		$this->load->model("ApiVanModel","vanModel");
		$supplierId = $this->get('supplierId');
		if (empty($language)){
			$supplierId = '-1';
		}
		$categotyId = $this->get('categotyId');
		if (empty($categotyId)){
			$categotyId = '-1';
		}
		$serviceId = $this->get('serviceId');
		if (empty($serviceId)){
			$serviceId = '-1';
		}
		$operatorStatus = $this->get('operatorStatus');
		if (empty($operatorStatus)){
			$operatorStatus = '-1';
		}
		$result = $this->vanModel->getSupplierReport($supplierId,$categotyId,$serviceId,$operatorStatus);
		if ($result != null) {
					$mesage = 'Data found';
					$totalPages = 1;
					$currentPages = 1;
					$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);
			}else{
			$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"No report found");
		//	return;
		}
	}


                       function supplierLogin_post(){
					$data=json_decode(file_get_contents('php://input'));
					$this->verifyRequiredParams(array('email','password'),POST_TYPE);

					/***** getting params *****/
					$email = $_POST['email'];
					$password = $_POST['password'];

					$language = LANGUAGE_ENGLISH;
					if (!empty($_POST['language'])){
						 $language = $_POST['language'];
					}

					$this->load->model("ApiVanModel","vanModel");
					$result = $this->vanModel->supplierLogin($email,$password);
					if($result != null){
								$mesage = 'Successfully login';
								$totalPages = 1;
								$currentPages = 1;
								$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);
					}
					else
					{
						$mesage = 'Wrong email or pasword' ;
						if ($language == LANGUAGE_ARABIC) {
								$mesage = 'البريد الإلكتروني أو كلمة المرور غير صحيحين ';
						}
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
					}
				}



}
?>
