<?php

defined('BASEPATH') OR exit('No direct script access allowed');
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class Shift extends REST_Controller
{
	public function __construct()
	{
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		$method = $_SERVER['REQUEST_METHOD'];
		if($method == "OPTIONS") {
				die();
		}

		  parent::__construct();
      $this->load->library('session');
      $this->load->helper(array('form','url'));
      $this->load->library('form_validation');
			$this->load->library('mylibrary');
			 $this->load->database();
	}
	    /* Method to display default error message
	    		 Created By: Manzz Baria
	    	*/
	    	private function displayDefaultJsonWithoutData($status,$message){
	    		if($status == JSON_SUCCESS_STATUS){
	    			$this->response([
	    				'Status' => $status,
	    				'Message' => $message
	    			], REST_Controller::HTTP_OK);
	    		}else{
	    			$this->response([
	    				'Status' => $status,
	    				'Message' => $message
	    			], REST_Controller::HTTP_OK);
	    		}
	    	}

	    	/* Method to display default success with Result data and totals
	    		 Created By: Manzz Baria
	    	*/
	    	private function displayDefaultJsonWithData($status,$mesage,$result,$totalPages,$currentPages){
	    		if($status == JSON_SUCCESS_STATUS){
	    			$this->response([
	    				'Status' => $status,
	    				'Message' => $mesage,
	    				'TotalPage' => $totalPages,
	    				'CurrentPage' => $currentPages,
	    				'Data' => $result
	    			], REST_Controller::HTTP_OK);
	    		}else{
	    			$this->response([
	    				'Status' => $status,
	    				'Message' => $mesage,
	    				'TotalPage' => $totalPages,
	    				'CurrentPage' => $currentPages,
	    				'Data' => $result
	    			], REST_Controller::HTTP_OK);
	    		}
	    	}

	    /* Method to check parameters is null or empty
	        Created By: Manzz Baria
	    */
	    public function verifyRequiredParams($required_fields,$type){
	        $error = false;
	        $error_fields = "";
	        foreach ($required_fields as $field) {
	    			if($type == POST_TYPE){
	    				if ($this->post($field) == null) {
	    	      	$error = true;
	    	        $error_fields .= $field . ', ';
	    	      }
	    	    }else if($type == GET_TYPE){
	    				if ($this->get($field) == null) {
	    	      	$error = true;
	    	        $error_fields .= $field . ', ';
	    	      }
	    	    }else if($type == PUT_TYPE){
	    				if ($this->put($field) == null) {
	    	      	$error = true;
	    	        $error_fields .= $field . ', ';
	    	      }
	    	    }else{
	    				if ($this->delete($field) == null) {
	    	      	$error = true;
	    	        $error_fields .= $field . ', ';
	    	      }
	    			}
	        }
	        if ($error) {
	        	$message = 'Required field(s) ' . substr($error_fields, 0, -2) . ' is missing or empty';
	          $this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$message);
	        }
	      }

				/* Method to show Message
					Created By: Manzz Baria
				*/
				public function showMessage($status,$message){
					$this->response([
						'Status' => $status,
						'Message' => $message
					], REST_Controller::HTTP_OK);
				}

				/*
					Method to addShiftDate
					Created By: Manzz Baria
				*/
				function addShiftDate_post(){
					$data=json_decode(file_get_contents('php://input'));
					$this->verifyRequiredParams(array('title','startDate','endDate','ownerIds'),POST_TYPE);

					/***** getting params *****/
					$title = $_POST['title'];
					$startDate = $_POST['startDate'];
					$endDate = $_POST['endDate'];
					$ownerIds = $_POST['ownerIds'];
					$language = LANGUAGE_ENGLISH;
					if (!empty($_POST['language'])){
						 $language = $_POST['language'];
					}

					$days = "";
					$dates = "";

					if (!empty($_POST['days'])){
							$days = $_POST['days'];
					}

					if (!empty($_POST['dates'])){
							$dates = $_POST['dates'];
					}

					$this->load->model("ApiShiftModel","shiftModel");
					$result = $this->shiftModel->addShiftDate($title,$startDate,$endDate,$ownerIds,$days,$dates);
					if($result != null){
								$mesage = 'Shift date successfully added';
								if ($language == LANGUAGE_ARABIC) {
									$mesage ='أضيفت ساعات العمل مواعيد بنجاح ';
								}
								$totalPages = 1;
								$currentPages = 1;
								$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

					}else{
						$mesage = 'Unable to add shift date';
						if ($language == LANGUAGE_ARABIC) {
							$mesage = 'غير قادر على إضافة تاريخ العمل ';
						}
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
					}

				}

				/*
					Method to add Owner on shift
					Created By: Manzz Baria
				*/
				function addOwnerOnShift_post(){
					$data=json_decode(file_get_contents('php://input'));
					$this->verifyRequiredParams(array('shiftId','ownerIds'),POST_TYPE);

					/***** getting params *****/
					$shiftId = $_POST['shiftId'];
					$ownerIds = $_POST['ownerIds'];
					$language = LANGUAGE_ENGLISH;
					if (!empty($_POST['language'])){
						 $language = $_POST['language'];
					}

					$this->load->model("ApiShiftModel","shiftModel");
					$result = $this->shiftModel->updateOwnerShift($shiftId,$ownerIds);
					if($result != null){
								$mesage = 'Provider successfully added to shift';
								if ($language == LANGUAGE_ARABIC) {
									$mesage = 'تمت إضافة مزود الخدمة بنجاح لساعات العمل';
								}
								$totalPages = 1;
								$currentPages = 1;
								$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

					}else{
						$mesage = 'Unable to add provider to shift';
						if ($language == LANGUAGE_ARABIC) {
							$mesage ='غير قادر على إضافة مزودالخدمة لساعات العمل';
						}
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
					}
				}

				/*
					Method to removeProvidersFromShift
					Created By: Manzz Baria
				*/
				function removeProvidersFromShift_post(){
					$data=json_decode(file_get_contents('php://input'));
					$this->verifyRequiredParams(array('shiftId','ownerIds'),POST_TYPE);

					/***** getting params *****/
					$shiftId = $_POST['shiftId'];
					$ownerIds = $_POST['ownerIds'];
					$language = LANGUAGE_ENGLISH;
					if (!empty($_POST['language'])){
						 $language = $_POST['language'];
					}


					$this->load->model("ApiShiftModel","shiftModel");
					$result = $this->shiftModel->removeProviderFromShift($shiftId,$ownerIds);
					if($result != null){
								$mesage = 'Provider successfully removed from shift';
								if ($language == LANGUAGE_ARABIC) {
									$mesage = 'تمت إزالة مزود الخدمة بنجاح من ساعات العمل';
								}
								$totalPages = 1;
								$currentPages = 1;
								$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

					}else{
						$mesage = 'Unable to remove provider from shift';
						if ($language == LANGUAGE_ARABIC) {
							$mesage = 'غير قادر على إزالة مزود الخدمة من ساعات العمل ';
						}
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
					}
				}

				/*
					Method to addShiftDate
					Created By: Manzz Baria
				*/
				function addShiftTime_post(){
					$data=json_decode(file_get_contents('php://input'));
					$this->verifyRequiredParams(array('shiftId','startTime','endTime'),POST_TYPE);

					/***** getting params *****/
					$shiftId = $_POST['shiftId'];
					$startDate = $_POST['startTime'];
					$endDate = $_POST['endTime'];
					$language = LANGUAGE_ENGLISH;
					if (!empty($_POST['language'])){
						 $language = $_POST['language'];
					}

					$this->load->model("ApiShiftModel","shiftModel");
					$result = $this->shiftModel->addShiftTime($shiftId,$startDate,$endDate);
					if($result != null){
								$mesage = 'Shift time successfully added';
								if ($language == LANGUAGE_ARABIC) {
									$mesage = ' تمت إضافة ساعات العمل بنجاح';
								}
								$totalPages = 1;
								$currentPages = 1;
								$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

					}else{
						$mesage = 'Unable to add shift time';
						if ($language == LANGUAGE_ARABIC) {
							$mesage = 'غير قادر على إضافة ساعات العمل ';
						}
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
					}
				}

				/* Method to getShiftTimes
					Created By: Manzz Baria
				*/
				function getShiftTimes_get(){
					$data=json_decode(file_get_contents('php://input'));
					$this->verifyRequiredParams(array('shiftId'),GET_TYPE);
					$shiftId = (int)$this->get('shiftId');
					$this->load->model("ApiShiftModel","shiftModel");
					$language = $this->get('language');
					if (empty($language)){
						$language = LANGUAGE_ENGLISH;
					}
					$result = $this->shiftModel->getShiftTimes($shiftId);
					if($result != null){
								$mesage = 'Data found';
								$totalPages = 1;
								$currentPages = 1;
								$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

					}else{
						$mesage = 'No shift time found';
						if ($language == LANGUAGE_ARABIC) {
							$mesage = 'لم يتم العثور على ساعات العمل ';
						}
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
					}
				}


				/* Method to getShifts
					Created By: Manzz Baria
				*/
				function getShifts_get(){
					$data=json_decode(file_get_contents('php://input'));
				  $this->load->model("ApiShiftModel","shiftModel");
					$language = $this->get('language');
					if (empty($language)){
						$language = LANGUAGE_ENGLISH;
					}
					$result = $this->shiftModel->getShifts();
					if($result != null){
								$mesage = 'Data found';
								$totalPages = 1;
								$currentPages = 1;
								$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

					}else{
						$mesage = 'No shift found';
						if ($language == LANGUAGE_ARABIC) {
							$mesage = ' لم يتم العثور على ساعات العمل';
						}
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
					}
				}

				/* Method to blockShiftTime
					Created By: Manzz Baria
				*/
				function blockShiftTime_get(){
					$data=json_decode(file_get_contents('php://input'));
					$this->verifyRequiredParams(array('shiftTimeId'),GET_TYPE);
					$shiftTimeId = (int)$this->get('shiftTimeId');
					$this->load->model("ApiShiftModel","shiftModel");
					$language = $this->get('language');
					if (empty($language)){
						$language = LANGUAGE_ENGLISH;
					}
					$result = $this->shiftModel->blockedShiftTime($shiftTimeId);
					if($result){
								$mesage = 'Shift time successfully removed';
								if ($language == LANGUAGE_ARABIC) {
									$mesage = 'تمت إزالة ساعات العمل بنجاح';
								}
								$totalPages = 1;
								$currentPages = 1;
								$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

					}else{
						$mesage = 'Unable to remove shift time';
						if ($language == LANGUAGE_ARABIC) {
							$mesage = ' غير قادر على إزالة ساعات العمل ';
						}
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
					}
				}

				/* Method to removeAllWeeklyHolidays
					Created By: Manzz Baria
				*/
				function removeAllHolidays_get(){
					$data=json_decode(file_get_contents('php://input'));
					$this->verifyRequiredParams(array('shiftId','type'),GET_TYPE);
					$shiftId = (int)$this->get('shiftId');
					$type = (int)$this->get('type');
					$language = $this->get('language');
					if (empty($language)){
						$language = LANGUAGE_ENGLISH;
					}
					$this->load->model("ApiHolidayModel","holModel");
					$result = $this->holModel->deleteHolidays($shiftId,$type);
					if($result){
								$mesage = 'Shift Successfully deleted';
								if ($language == LANGUAGE_ARABIC) {
									$mesage = 'تم حذف ساعات العمل بنجاح';
								}
								$totalPages = 1;
								$currentPages = 1;
								$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

					}else{
						$mesage = 'Unable to delete shift';
						if ($language == LANGUAGE_ARABIC) {
							$mesage = 'غير قادر على حذف ساعات العمل';
						}
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
					}
				}

				/* Method to blockShiftDate
					Created By: Manzz Baria
				*/
				function blockShiftDate_get(){
					$data=json_decode(file_get_contents('php://input'));
					$this->verifyRequiredParams(array('shiftId'),GET_TYPE);
					$shiftId = (int)$this->get('shiftId');
					$this->load->model("ApiShiftModel","shiftModel");
					$language = $this->get('language');
					if (empty($language)){
						$language = LANGUAGE_ENGLISH;
					}
					$result = $this->shiftModel->blockedShiftDate($shiftId);
					if($result){
						$mesage = 'Shift Successfully deleted';
						if ($language == LANGUAGE_ARABIC) {
							$mesage = 'ساعات العمل حذف بنجاح ';
						}
								$totalPages = 1;
								$currentPages = 1;
								$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

					}else{
						$mesage = 'Unable to delete shift';
						if ($language == LANGUAGE_ARABIC) {
							$mesage = 'غير قادر على حذف ساعات العمل';
						}
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,$mesage);
					}
				}

				/*
					Method to updateShiftDate
					Created By: Manzz Baria
				*/
				function updateShiftDate_post(){
					$data=json_decode(file_get_contents('php://input'));
					$this->verifyRequiredParams(array('shiftId'),POST_TYPE);

					/***** getting params *****/
					$shiftId = $_POST['shiftId'];
					$startDate = "";
					$endDate = "";
					$title = "";
					if (!empty($_POST['startDate'])){
						$startDate = $_POST['startDate'];
					}
					if (!empty($_POST['endDate'])){
						$endDate = $_POST['endDate'];
					}

					if (!empty($_POST['title'])){
							$title = $_POST['title'];
					}
					$language = 'en';
					if (!empty($_POST['language'])){
						 $language = $_POST['language'];
					}

					$days = "";
					$dates = "";
					$ownerIds = "";

					if (!empty($_POST['ownerIds'])){
							$ownerIds = $_POST['ownerIds'];
					}

					if (!empty($_POST['days'])){
							$days = $_POST['days'];
					}

					if (!empty($_POST['dates'])){
							$dates = $_POST['dates'];
					}


					$this->load->model("ApiShiftModel","shiftModel");
					$result = $this->shiftModel->updateShiftDate($shiftId,$title,$startDate,$endDate,$days,$dates,$ownerIds);
					if($result != null){
								$mesage = 'Shift successfully updated';
								if ($language == LANGUAGE_ARABIC) {
							             $mesage = 'تم تحديث ساعات العمل بنجاح';
						                 }
								$totalPages = 1;
								$currentPages = 1;
								$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

					}else{
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"Unable to update shift");
                                                        if ($language == LANGUAGE_ARABIC) {
							   $this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"غير قادر على تحديث ساعات العمل");
                                                         }
					}
				}

				/*
					Method to updateShiftTime
					Created By: Manzz Baria
				*/
				function updateShiftTime_post(){
					$data=json_decode(file_get_contents('php://input'));
					$this->verifyRequiredParams(array('shiftTimeId'),POST_TYPE);

					/***** getting params *****/
					$shiftTimeId = $_POST['shiftTimeId'];
					$startTime = "";
					$endTime = "";
					if (!empty($_POST['startTime'])){
						$startTime = $_POST['startTime'];
					}
					if (!empty($_POST['endTime'])){
						$endTime = $_POST['endTime'];
					}
					$language = 'en';
					if (!empty($_POST['language'])){
						 $language = $_POST['language'];
					}

					$this->load->model("ApiShiftModel","shiftModel");
					$result = $this->shiftModel->updateShiftTime($shiftTimeId,$startTime,$endTime);
					if($result != null){
								$mesage = 'Shift time successfully updated';
								if ($language == LANGUAGE_ARABIC) {
							$mesage = 'تم تحديث ساعات العمل بنجاح';
						}
								$totalPages = 1;
								$currentPages = 1;
								$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

					}else{
                                                        if ($language == LANGUAGE_ARABIC) {
							 $this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"غير قادر على تحديث ساعات العمل");
						         }
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"Unable to update shift time");
                                                        
					}

				}

				/* Method to getUnassignedShiftProviders
					Created By: Manzz Baria
				*/
				function getUnassignedShiftProviders_get(){
					$data=json_decode(file_get_contents('php://input'));
					//$this->verifyRequiredParams(array('shiftId'),GET_TYPE);
					$this->load->model("ApiShiftModel","shiftModel");
					$language = $this->get('language');
					if (empty($language)){
						$language = 'en';
					}
					$result = $this->shiftModel->getProvidersOfshift(0);
					if($result != null){
								$mesage = 'Data found';
								$totalPages = 1;
								$currentPages = 1;
								$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

					}else{
                                                         if ($language == LANGUAGE_ARABIC) {
							             $this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"لا يوجد أي مزود للخدمة");
						                 }
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"No provider found");
                                                    
					}

				}

				/* Method to getProviders
					Created By: Manzz Baria
				*/
				function getProviders_get(){
					$data=json_decode(file_get_contents('php://input'));
					//$this->verifyRequiredParams(array('shiftId'),GET_TYPE);
					$this->load->model("ApiShiftModel","shiftModel");
					$language = $this->get('language');
					if (empty($language)){
						$language = 'en';
					}
					$result = $this->shiftModel->getProviders();
				//	$test = urldecode("Test+English+space");
					if($result != null){
								$mesage = 'Data found';
								$totalPages = 1;
								$currentPages = 1;
								$this->displayDefaultJsonWithData(JSON_SUCCESS_STATUS,$mesage,$result,$totalPages,$currentPages);

					}else{
                                                       if ($language == LANGUAGE_ARABIC) {
							             $this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"لا يوجد أي مزود للخدمة");
						                 }
							$this->displayDefaultJsonWithoutData(JSON_ERROR_STATUS,"No provider found");
                                                        
					}

				}

}
?>
