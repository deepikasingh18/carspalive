<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admindashboard extends CI_Controller {

	public function __construct()
	{
		  parent::__construct();
          $this->load->library('session');
          $this->load->helper(array('form','url'));
	}
	public function index()
	{
			$sesscheck=$this->session->userdata('data');	
			if($sesscheck['loginuser']==1)
			{
				$this->load->view('header');
				$this->load->view('Admindashboardview');
				$this->load->view('footer');
			}
			else
			{
				redirect('Login');
			}
	}
}
