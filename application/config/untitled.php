<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Outlet extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->helper(array('form','url'));
		$this->load->library('form_validation');
		$this->load->model('outlets');
	}

	public function index()
	{
		$sesscheck=$this->session->userdata('data');	
		if($sesscheck['loginuser']==1)
		{
			$this->load->view('header');
			$sesscheck=$this->session->userdata('data');
			$rs=$sesscheck['role'];
			$this->load->model('loginmodel');
			$menus=$this->loginmodel->getComponents($rs);
			$data1['result']=$menus;
			$this->load->view('sidemenubar',$data1);
			$this->load->view('outlet management');
			$this->load->view('footer');
		}
		else
		{
			redirect('login');
		} 
	}	

	public function activate($id)
	{
		$sql="SELECT * from outlets	 where ID=".$id;
		$query=$this->db->query($sql);
		$result=$query->result();

		foreach($result as $row)
		{
			$status=$row->Active;

			if($status == -1)
			{	
				$state= 1;
				echo "Deactivate";
			}
			else
			{
				$state = -1;
				echo "Activate";
			}
			$sql="UPDATE outlets set Active='".$state."' where ID=".$id;
			$query=$this->db->query($sql);
		}

		
	}
	
	public function deactivate($id)
	{
		$sql="SELECT * from outlets	 where ID=".$id;
		$query=$this->db->query($sql);
		$result=$query->result();

		foreach($result as $row)
		{
			$status=$row->Active;

			if($status == -1)
			{	
				$state = 1;
			}
			$sql="UPDATE outlets set Active='".$state."' where ID=".$id;
			$query=$this->db->query($sql);
		}

	}
	
	function ajax_state($id) 
	{
		$arrStates = $this->outlets->loadstatefromcountry($id);
		print_r($arrStates);
		
		foreach ($arrStates as $states) 
		{
			$arrstates[$states->ID] = $states->Name_en;
		}
		print form_dropdown('state1',$arrstates); 	
	}
	
	function ajax_district($select) 
	{
		$arrDistrict = $this->outlets->loaddistrictfromstates($select);

		foreach ($arrDistrict as $district) 
		{
			$arrdistrict[$district->ID] = $district->Name_en;
		}
		print form_dropdown('district1',$arrdistrict);
	}
	
	function ajax_area($select) 
	{
		$arrAreas = $this->outlets->loadareafromdistrict($select);

		foreach ($arrAreas as $area) 
		{
			$arrarea[$area->ID] = $area->Name_en;
		}
		print form_dropdown('area1',$arrarea);
	}
	
	function ajax_channel($select) 
	{
		$arrChannel = $this->outlets->loadcategoryfromchannel($select);

		foreach ($arrChannel as $channel) 
		{
			$arrchannel[$channel->ID] = $channel->Name_en;
		}
		print form_dropdown('channel',$arrchannel);
	}
	
	function ajax_subchannel($select) 
	{
		$arrsubChannel = $this->outlets->loadsubchannelfromchannel($select);

		foreach ($arrsubChannel as $subchannel) 
		{
			$arrsubchannel[$subchannel->ID] = $subchannel->Name_en;
		}
		print form_dropdown('subchannel',$arrsubchannel);
	}
	
	function ajax_addchannel() 
	{

		if (isset($_POST) && isset($_POST['category'])) 
		{
			$cat = $_POST['category'];
			$arrCategory = $this->outlets->loadcategoryfromchannel($cat);
			$arrstates1 = null;
			if($arrCategory) 
			{ 
				$arrstates1.= "<option value='none' selected='selected'>------------Select Channel------------</option>"; 
				foreach ($arrCategory as $cat) 
				{
					$arrstates1 .= "<option value='".$cat->ID."'>".$cat->Name_en."</option>";
				}
				echo $arrstates1;
			}
			else
			{
					
				$arrstates1.= "<option selected='selected'>------------Select Channel---------</option>"; 
				echo $arrstates1;
			}
		} 
		else 
		{
			echo "no data selected";
		}
	}
	
	function ajax_addsubchannel() 
	{

		if (isset($_POST) && isset($_POST['channel1'])) 
		{
			$cat1 = $_POST['channel1'];
			$arrsubchannel = $this->outlets->loadsubchannelfromchannel($cat1);
			$arrstates2 = null;
			if($arrsubchannel) 
			{ 
				$arrstates2 .= "<option value='none' selected='selected'>------------Select SubChannel-------</option>"; 
				foreach ($arrsubchannel as $chan) 
				{
					$arrstates2 .= "<option value='".$chan->ID."'>".$chan->Name_en."</option>";
				}
				echo $arrstates2;
			}
			else
			{
					
				$arrstates1.= "<option selected='selected'>------------Select SubChannel-------</option>"; 
				echo $arrstates1;
			}
		} 
		else 
		{
			echo "no data selected";
		}
	}
	/*public function contactVerify($contact, $otherField) 
		{
			  return ($contact != '' || $this->input->post($otherField) != '');
		}
		
		public function streetVerify($contact, $otherField) 
		{
			  return ($contact != '' || $this->input->post($otherField) != '');
		}*/
		function select_validate($abcd)
		{
			// 'none' is the first option that is default "-------Choose City-------"
			if($abcd=="none")
			{
				$this->form_validation->set_message('select_validate', 'Please Select Your Area.');
				return false;
			}
			else
			{
			// User picked something.
				return true;
			}
		}
		
		function select_validate1($abcd)
		{
			// 'none' is the first option that is default "-------Choose City-------"
			if($abcd=="none")
			{
				$this->form_validation->set_message('select_validate1', 'Please Select Your Category.');
				return false;
			}
			else
			{
			// User picked something.
				return true;
			}
		}
		function select_validate2($abcd)
		{
			// 'none' is the first option that is default "-------Choose City-------"
			if($abcd=="none")
			{
				$this->form_validation->set_message('select_validate2', 'Please Select Your Channel.');
				return false;
			}
			else
			{
			// User picked something.
				return true;
			}
		}
		function select_validate3($abcd)
		{
			// 'none' is the first option that is default "-------Choose City-------"
			if($abcd=="none")
			{
				$this->form_validation->set_message('select_validate3', 'Please Select Your SubChannel.');
				return false;
			}
			else
			{
			// User picked something.
				return true;
			}
		}
		
		function select_validate4($abcd)
		{
			// 'none' is the first option that is default "-------Choose City-------"
			if($abcd=="none")
			{
				$this->form_validation->set_message('select_validate4', 'Please Select Your Country.');
				return false;
			}
			else
			{
			// User picked something.
				return true;
			}
		}
		function select_validate5($abcd)
		{
			// 'none' is the first option that is default "-------Choose City-------"
			if($abcd=="none")
			{
				$this->form_validation->set_message('select_validate5', 'Please Select Your Governoment.');
				return false;
			}
			else
			{
			// User picked something.
				return true;
			}
		}
		function select_validate6($abcd)
		{
			// 'none' is the first option that is default "-------Choose City-------"
			if($abcd=="none")
			{
				$this->form_validation->set_message('select_validate6', 'Please Select Your District.');
				return false;
			}
			else
			{
			// User picked something.
				return true;
			}
		}
	public function insertoutlet()
	{
		$nameen=$this->input->post('nameen');
		$namear=$this->input->post('namear');
		$category=$this->input->post('category');
		$channel1=$this->input->post('channel1');
		$subchannel1=$this->input->post('subchannel1');
		$country=$this->input->post('country');
		$state=$this->input->post('state');
		$district=$this->input->post('district');
		$area=$this->input->post('area');
		$street=$this->input->post('street');
		$streetar=$this->input->post('streetar');
		$mobile=$this->input->post('mobile');
		$phoneno=$this->input->post('phoneno');
			//$email=$this->input->post('email');
		$date=date('Y-d-m');

		//$this->form_validation->set_message('contactVerify', 'Either English or Arabic field is required');
		//$this->form_validation->set_message('streetVerify', 'Either Street English or Arabic field is required');
		//$this->form_validation->set_rules('nameen','English','trim|callback_contactVerify[namear]');
		//$this->form_validation->set_rules('namear','Arabic','trim|callback_contactVerify[nameen]');
		$this->form_validation->set_rules('category','Category','required|callback_select_validate1');
		$this->form_validation->set_rules('channel1','channel','callback_select_validate2');
		$this->form_validation->set_rules('subchannel1','subchannel','callback_select_validate3');
		$this->form_validation->set_rules('country','Country','callback_select_validate4');
		$this->form_validation->set_rules('state','State','callback_select_validate5');
		$this->form_validation->set_rules('district','District','callback_select_validate6');
		$this->form_validation->set_rules('area','Area','required|callback_select_validate');
			//$this->form_validation->set_rules('area','area','required');
			//$this->form_validation->set_rules('area','area','required');
		//$this->form_validation->set_rules('street','street','trim|callback_streetVerify[streetar]');
		//$this->form_validation->set_rules('streetar','streetar','trim|callback_streetVerify[street]');
		$this->form_validation->set_rules('mobile','mobile number','required|is_unique[outlets.Mobile]|regex_match[/^[0-9]+$/]');
		$this->form_validation->set_rules('phoneno','phone number','regex_match[/^[0-9]+$/]');
			//$this->form_validation->set_rules('email','email','required|valid_email');


		if($this->form_validation->run()==FALSE)
		{	
			$this->output->set_status_header('500');
			$this->output->set_content_type('application/json');
			echo json_encode(array('status' => validation_errors()));
		}
		else
		{
			$this->outlets->insertOutlet($nameen,$namear,$category,$channel1,$subchannel1,$area,$street,$streetar,$mobile,$phoneno,$date);
			if($this->db->affected_rows()>0)
			{
				header("Content-Type: application/json; charset=utf-8", true);
				echo json_encode(array('status' => 1));
				$id=$this->db->insert_id();
				$this->getOffersForOutlets($id);
			}
			else
			{
				header("Content-Type: application/json; charset=utf-8", true);
				echo json_encode(array('status' => 0));
			}
		}
	}
	
	public function editoutlets()
	{
		//echo $id;
		$this->outlets->editoutletsrecord($this->input->post());
	} 

	public function viewOutlets()
	{
		//echo $id;
		$this->outlets->viewOutletsRecord($this->input->post());
	} 
	
	public function updateoutlets($id)
	{  
		$nameen=$this->input->post('nameen');
		$namear=$this->input->post('namear');
		$category=$this->input->post('category');
		$channel1=$this->input->post('channel1');
		$subchannel1=$this->input->post('subchannel1');
		$country=$this->input->post('country');
		$state=$this->input->post('state');
		$district=$this->input->post('district');
		$area=$this->input->post('area');
		$street=$this->input->post('street');
		$streetar=$this->input->post('streetar');
		$mobile=$this->input->post('mobile');
		$phoneno=$this->input->post('phoneno');
		
		$this->form_validation->set_rules('category','Category','required|callback_select_validate1');
		$this->form_validation->set_rules('channel1','channel','callback_select_validate2');
		$this->form_validation->set_rules('subchannel1','subchannel','callback_select_validate3');
		$this->form_validation->set_rules('country','Country','callback_select_validate4');
		$this->form_validation->set_rules('state','State','callback_select_validate5');
		$this->form_validation->set_rules('district','District','callback_select_validate6');
		$this->form_validation->set_rules('area','Area','required|callback_select_validate');
		
		if($this->form_validation->run()==FALSE)
		{	
			$this->output->set_status_header('500');
			$this->output->set_content_type('application/json');
			echo json_encode(array('status' => validation_errors()));
		}
		else
		{
			$sql="UPDATE outlets set  Name_en='".$nameen."', Name_ar='".$namear."', BusinessCategoryId='".$category."',ChannelID='".$channel1."',SubChannelID='".$subchannel1."',AreaID='".$area."',Street_en='".$street."',Street_ar='".$streetar."',Mobile='".$mobile."',PhoneNumber='".$phoneno."' where ID=".$id;
			$query=$this->db->query($sql);
			$this->getOffersForOutlets($id);
			echo "true";
		}
	}

	public function deleteoutlets($id)
	{
		$this->db->where('ID',$id);
		$this->db->delete('outlets');
	}

	public function getOffersForOutlets($outletId){
		$this->getOutletsFromOfferAreasCategorys($outletId);
	}

	public function getOutletsFromOfferAreasCategorys($outletId){
		$this->db->select('outlets.ID,outlets.Name_en,outlets.Name_ar,outlets.BusinessCategoryId,outlets.ChannelId,outlets.SubChannelId,offersareas.IDOffer as offID');
		$this->db->from('offersareas');
		$this->db->join('outlets','outlets.AreaID = offersareas.AreaId','inner');
		$this->db->join('offersoutlets','offersareas.IDOffer = offersoutlets.IDOffer','inner');
		$this->db->where('outlets.ID',$outletId);
		$queryoutlet = $this->db->get();
		//print_r($queryoutlet);
		return $this->displayOffersForOutlets($queryoutlet->result(),$outletId);
		//$listOut = $this->displayOffersForOutlets($queryoutlet->result(),$outletId);
		//echo json_encode($listOut);
	}

	public function displayOffersForOutlets($result,$outletId){
		$resultlist = null;
		if($result != null){
			$this->deleteOfferForOutlets($outletId);
			foreach($result as $row){
				$OfferId = (int)$row->offID;
				$categoryId = (int)$row->BusinessCategoryId;
				$channelId = (int)$row->ChannelId;
				$subChannelId = (int)$row->SubChannelId;

				$resultCategory = $this->getCategoryTranscation($OfferId);
				$ara = null;
				if ($resultCategory != null) {
					foreach ($resultCategory as $cRow) {
						$rCategoryId = $cRow->CategoryId;
						$rChannelId = $cRow->ChannelId;
						$rSubChannelId = $cRow->SubChannelId;

						if ($categoryId == $rCategoryId) {
							if ($rChannelId != null) {
								if ($rChannelId == $channelId) {
									if ($rSubChannelId != null) {
										if ($rSubChannelId == $subChannelId) {
											$ara = array('OfferId'=>$row->offID,'OutletId'=>$outletId);
										}
									}else{
										$ara = array('OfferId'=>$row->ID,'OutletId'=>$outletId);
									}
								}
							}else{
								$ara = array('OfferId'=>$row->ID,'OutletId'=>$outletId);
							}
						}
					}
				}
				if ($ara != null) {
					$this->insertOfferOutlets($OfferId,$outletId);	
					//$resultlist[] = $ara;
				}
			}	
		}
		return $resultlist;
	}

	public function insertOfferOutlets($offerId,$outletId){
		$dataArray = array("IDOffer"=>$offerId,"IDOutlet"=>$outletId);	
		$this->db->insert('offersoutlets',$dataArray);
		//$this->getOutletsDeviceId($outletId);
		return true;
	}

	public function deleteOfferForOutlets($outletId){
		$this->db->where('IDOutlet', $outletId);
		$this->db->delete('offersoutlets');
		return true;
	}

	public function getCategoryTranscation($offerId){
		$this->db->select('offercategorytransaction.ID,offercategorytransaction.OfferId,offercategorytransaction.CategoryId,offerchanneltransaction.ChannelId,offersubchanneltransaction.SubChannelId');
		$this->db->from('offercategorytransaction');
		$this->db->join('offerchanneltransaction','offercategorytransaction.ID = offerchanneltransaction.OfferCategoryTransId','left');
		$this->db->join('offersubchanneltransaction','offerchanneltransaction.ID = offersubchanneltransaction.OfferChannelTransId','left');
		$this->db->where('offercategorytransaction.OfferId',$offerId);

		$queryoutlet = $this->db->get();
		$result = $queryoutlet->result();
		return $result;
	}
}
?>