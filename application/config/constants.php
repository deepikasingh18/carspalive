<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code


define('LANGUAGE_EN','en');
define('LANGUAGE_AR','ar');

define('JSON_ERROR_STATUS',0);
define('JSON_SUCCESS_STATUS',1);

define('POST_TYPE','POST');
define('PUT_TYPE','PUT');
define('GET_TYPE','GET');
define('DELETE_TYPE','DELETE');


define('ANDROID_NOTIFICATION',1);
define('IOS_NOTIFICATION',2);

define('USER_STATUS_ONLINE',1);
define('USER_STATUS_OFFLINE',2);
define('USER_STATUS_BUSY',3);
define('USER_STATUS_OUT',4);
define('USER_STATUS_BLOCK',5);
define('USER_STATUS_DISCONNECT',6);

define('USER_TYPE_CUSTOMER',1);
define('USER_TYPE_OPERATOR',2);
define('USER_TYPE_ADMIN',3);
define('USER_TYPE_COMPANY',4);

define('REQUEST_STATUS_PENDING',1);
define('REQUEST_STATUS_IN_PROGRESS',2);
define('REQUEST_STATUS_FINISH',3);
define('REQUEST_STATUS_REJECTED',4);
define('REQUEST_STATUS_ON_THE_WAY',5);
define('REQUEST_STATUS_REFUND',6);

define('NOTIFICATION_TYPE_BOOKED',1);
define('NOTIFICATION_TYPE_IN_PROGRESS',2);
define('NOTIFICATION_TYPE_FINISH',3);
define('NOTIFICATION_TYPE_REQUEST_REJECTED',4);
define('NOTIFICATION_TYPE_ON_THE_WAY',5);
define('NOTIFICATION_TYPE_REFUND',6);

define('NOTIFICATION_TYPE_SERVICE_REJECTED',7);
define('NOTIFICATION_TYPE_REQUEST_UPDATED',8);
define('NOTIFICATION_TYPE_ADMIN',9);
define('NOTIFICATION_TYPE_REQUEST_TRANSFER',10);

/*
define('SERVICE_STATUS_PENDING',1);
define('SERVICE_STATUS_IN_PROGRESS',2);
define('SERVICE_STATUS_FINISH',3);
define('SERVICE_STATUS_REJECTED',4);
define('SERVICE_STATUS_REFUND',5);
*/

define('VAN_TYPE_OWN',1);
define('VAN_TYPE_THIRDPARTY',2);

define('VAN_STATUS_OFFLINE',1);
define('VAN_STATUS_ONLINE',2);
define('VAN_STATUS_BUSY',3);

define('VAN_STATUS_BLOCKED',5);

define('BILL_STATUS_PENDING',1);
define('BILL_STATUS_PAID',2);
define('BILL_STATUS_REFUND',3);

define('PAID_BY_CARD',1);
define('PAID_BY_CASH',2);

define('BLOCKED',1);
define('UNBLOCKED',0);

define('DAYS',1);
define('DATES',2);

define('THUMB_URL','http://localhost/carspa/uploads/files/thumb/');
define('IMAGE_URL','http://localhost/carspa/uploads/files/');

define('DOCUMENTONE_URL','http://localhost/carspa/uploads/documentone/');
define('DOCUMENTTWO_URL','http://localhost/carspa/uploads/documenttwo/');
define('DOCUMENTTHREE_URL','http://localhost/carspa/uploads/documentthree/');

define('LOGO_URL','http://localhost/carspa/assets/carspalogo.png');
define('EMAIL_HEADER_LOGO','http://localhost/carspa/assets/images/login-header-image.png');
define('IS_DEVELOPMENT',false);

define('KM','K');
define('MILES','N');
define('RADIUS_KM',25);

define("VAN_DEFAULT_LATITUDE",0);
define("VAN_DEFAULT_LONGITUDE",0);

define("REQUEST_DAYS",15);
define("ADD_MINT",10);
define("MAX_DISTANCE",25);

define("GATEGORY_TOW",1);


define("LANGUAGE_ENGLISH","en");
define("LANGUAGE_ARABIC","ar");

define('DEBUG_APN_WITH_SAMPLE',false);

define('EXT',"php");